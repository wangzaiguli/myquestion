package com.lw.leetcode.linked.c;

/**
 * Created with IntelliJ IDEA.
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/7 9:38
 */
public class TextEditor {


    public static void main(String[] args) {
        TextEditor test = new TextEditor();


        test.addText("leetcode");
        test.deleteText(4);
        test.addText("practice");
        System.out.println(test.cursorRight(3));
        System.out.println(test.cursorLeft(8));
        System.out.println(test.deleteText(10));
        System.out.println(test.cursorLeft(2));
        System.out.println(test.cursorRight(6));

        // ["TextEditor","cursorLeft","cursorRight","deleteText"]
        //[[],[1],[4],[3]]

    }


    private Node st;
    private Node end;
    private Node item;

    public TextEditor() {
        st = new Node(' ');
        end = new Node(' ');
        st.next = end;
        end.pre = st;
        item = st;
    }

    public void addText(String text) {
        Node b = item.next;
        int length = text.length();
        for (int i = 0; i < length; i++) {
            Node node = new Node(text.charAt(i));
            node.pre = item;
            item.next = node;
            item = node;
        }
        item.next = b;
        b.pre = item;
    }

    public int deleteText(int k) {
        int count = 0;
        Node node = item;
        while (k > 0) {
            if (node == st) {
                break;
            }
            k--;
            node = node.pre;
            count++;
        }
        node.next = item.next;
        item.next.pre = node;
        item = node;
        return count;
    }

    public String cursorLeft(int k) {
        item = getLeft(item, k);
        StringBuilder sb = new StringBuilder();
        Node node = getLeft(item, 9);
        while (node != item) {
            if (node.val != ' ') {
                sb.append(node.val);
            }
            node = node.next;
        }
        if (item.val != ' ') {
            sb.append(item.val);
        }
        return sb.toString();
    }

    public String cursorRight(int k) {
        item = getRight(item, k);
        StringBuilder sb = new StringBuilder();
        Node node = getLeft(item, 9);
        while (node != item) {
            if (node.val != ' ') {
                sb.append(node.val);
            }
            node = node.next;
        }
        if (item.val != ' ') {
            sb.append(item.val);
        }
        return sb.toString();
    }

    private Node getRight(Node node, int k) {
        while (k > 0) {
            if (node == end.pre) {
                break;
            }
            k--;
            node = node.next;
        }

        return node;
    }

    private Node getLeft(Node node, int k) {
        while (k > 0) {
            if (node == st) {
                break;
            }
            k--;
            node = node.pre;
        }

        return node;
    }

    private static class Node {
        private char val;
        private Node next;
        private Node pre;

        private Node(char val) {
            this.val = val;
        }
    }

}
