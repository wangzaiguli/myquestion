package com.lw.leetcode.linked.c;

/**
 * Created with IntelliJ IDEA.
 * 1250. 检查「好数组」
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/8 16:42
 */
public class IsGoodArray {
    public boolean isGoodArray(int[] nums) {
        int n = nums[0];
        if (n == 1) {
            return true;
        }
        for (int i = 1; i < nums.length; i++) {
            n = gcd(n, nums[i]);
            if (n == 1) {
                return true;
            }
        }
        return false;
    }

    private int gcd(int a, int b) {
        return a % b == 0 ? b : gcd(b, a % b);
    }

}
