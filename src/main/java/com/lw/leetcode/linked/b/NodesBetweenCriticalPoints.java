package com.lw.leetcode.linked.b;

import com.lw.leetcode.linked.ListNode;

/**
 * Created with IntelliJ IDEA.
 * 2058. 找出临界点之间的最小和最大距离
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/6 17:47
 */
public class NodesBetweenCriticalPoints {

    public int[] nodesBetweenCriticalPoints(ListNode head) {
        int[] values = new int[2];
        values[1] = -1;
        if (head == null || head.next == null || head.next.next == null) {
            values[0] = -1;
            return values;
        }
        values[0] = Integer.MAX_VALUE;
        ListNode n1 = head;
        ListNode n2 = n1.next;
        ListNode n3 = n2.next;
        int f = 0;
        int index = 2;
        int last = 0;

        while (n3 != null) {
            if ((n1.val < n2.val && n3.val < n2.val) || (n1.val > n2.val && n3.val > n2.val)) {
                if (f == 0) {
                    f = index;
                } else {
                    values[0] = Math.min(values[0], index - last);
                }
                last = index;
            }
            n1 = n2;
            n2 = n3;
            n3 = n3.next;
            index++;
        }
        if (values[0] == Integer.MAX_VALUE) {
            values[0] = -1;
            return values;
        }
        values[1] = last - f;
        return values;
    }

}
