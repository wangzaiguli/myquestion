package com.lw.leetcode.linked.b;

import com.lw.leetcode.linked.ListNode;

/**
 * 1721. 交换链表中的节点
 *
 * @Author liw
 * @Date 2021/5/14 10:14
 * @Version 1.0
 */
public class SwapNodes {


    public static void main(String[] args) {
//        SwapNodes test = new SwapNodes();
////        ListNode listNode = test.swapNodes(ListNode.getInstance(), 1);
////        System.out.println(listNode);

        for (int i = 0; i < 100; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("hello" + i);
        }
    }

    public ListNode swapNodes(ListNode head, int k) {
        ListNode f = new ListNode(0);
        f.next = head;
        ListNode a = f;
        int n = 1;
        while (n < k) {
            a = a.next;
            n++;
        }
        ListNode b = f;
        ListNode c = a.next.next;
        while (c != null) {
            b = b.next;
            c = c.next;
        }
        c = a.next;
        a.next = b.next;
        b.next = c;
        a = a.next;
        b = b.next;
        c = a.next;
        a.next = b.next;
        b.next = c;
        return f.next;
    }

}
