package com.lw.leetcode.linked.b;

import com.lw.leetcode.linked.ListNode;
import com.lw.leetcode.tree.TreeNode;

/**
 * 1367. 二叉树中的列表
 *
 * @Author liw
 * @Date 2021/5/14 15:02
 * @Version 1.0
 */
public class isSubPath {

    public boolean isSubPath(ListNode head, TreeNode root) {
        if (root == null) {
            return false;
        }
        if (head.val == root.val && check(head, root)) {
            return true;
        }
        return isSubPath(head, root.left) || isSubPath(head, root.right);
    }

    public boolean check (ListNode head, TreeNode root) {
        if (head == null) {
            return true;
        }
        if (root == null) {
            return false;
        }
        if (head.val != root.val) {
            return false;
        }
        return check(head.next, root.left) || check(head.next, root.right);
    }

}
