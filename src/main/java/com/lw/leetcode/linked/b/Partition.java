package com.lw.leetcode.linked.b;

import com.lw.leetcode.linked.ListNode;

/**
 * 面试题 02.04. 分割链表
 *
 * @Author liw
 * @Date 2021/5/14 13:02
 * @Version 1.0
 */
public class Partition {

    public ListNode partition(ListNode head, int x) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode f = head;
        while (head.next != null ) {
            if (head.next.val >= x) {
                head = head.next;
            } else {
                ListNode item = head.next;
                head.next = item.next;
                item.next = f;
                f = item;
            }
        }
        return f;
    }

}
