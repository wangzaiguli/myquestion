package com.lw.leetcode.linked.b;

import com.lw.leetcode.linked.ListNode;

/**
 * 1669. 合并两个链表
 *
 * @Author liw
 * @Date 2021/5/14 14:15
 * @Version 1.0
 */
public class MergeInBetween {
    public ListNode mergeInBetween(ListNode list1, int a, int b, ListNode list2) {
        ListNode f = new ListNode(0);
        f.next = list1;
        ListNode item = f;
        for (int i = 0; i < a; i++) {
            item = item.next;
        }
        ListNode st = item;
        b = b - a + 2;
        for (int i = 0; i < b; i++) {
            item = item.next;
        }
        ListNode end = item;
        st.next = list2;
        while (list2.next != null) {
            list2 = list2.next;
        }
        list2.next = end;
        return f.next;
    }
}
