package com.lw.leetcode.linked.b;

import com.lw.leetcode.linked.ListNode;

/**
 * Created with IntelliJ IDEA.
 * 86. 分隔链表
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/29 9:28
 */
public class Partition86 {

    public ListNode partition(ListNode head, int x) {
        ListNode a1 = new ListNode(0);
        ListNode a2 = a1;
        ListNode b1 = new ListNode(0);
        ListNode b2 = b1;
        while (head != null) {
            if (head.val >= x) {
                b1.next = head;
                b1 = b1.next;
            } else {
                a1.next = head;
                a1 = a1.next;
            }
            head = head.next;
        }
        b1.next = null;
        a1.next = b2.next;
        return a2.next;
    }
}
