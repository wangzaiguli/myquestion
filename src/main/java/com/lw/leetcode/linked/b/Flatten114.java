package com.lw.leetcode.linked.b;

import com.lw.leetcode.tree.TreeNode;

/**
 * Created with IntelliJ IDEA.
 * 114. 二叉树展开为链表
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/14 11:42
 */
public class Flatten114 {

    public void flatten(TreeNode root) {
        if (root == null) {
            return;
        }
        TreeNode a = new TreeNode(0);
        find(root, a);
    }

    private static TreeNode find(TreeNode node, TreeNode f) {
        f.right = node;
        TreeNode returnNode = node;
        TreeNode l = node.left;
        TreeNode r = node.right;
        node.left = null;
        node.right = null;
        if (l != null) {
            returnNode = find(l, node);
        }
        if (r != null) {
            returnNode = find(r, returnNode);
        }
        return returnNode;
    }

}
