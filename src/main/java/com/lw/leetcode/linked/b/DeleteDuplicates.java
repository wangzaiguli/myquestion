package com.lw.leetcode.linked.b;

import com.lw.leetcode.linked.ListNode;

/**
 * Created with IntelliJ IDEA.
 * 82. 删除排序链表中的重复元素 II
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/29 9:21
 */
public class DeleteDuplicates {

    public ListNode deleteDuplicates(ListNode head) {
        if (head == null) {
            return null;
        }
        if (head.next == null) {
            return head;
        }
        int b;
        if (head.val == 0) {
            b = 1;
        } else {
            b = 0;
        }
        ListNode frist = new ListNode(b);
        frist.next = head;
        ListNode last = frist;
        ListNode item = last.next;
        ListNode next = item.next;
        ListNode aa = frist;
        while (next != null) {
            if (last.val != item.val && item.val != next.val) {
                aa.next = item;
                aa = item;
            }
            last = last.next;
            item = item.next;
            next = next.next;
        }

        if (last.val != item.val) {
            aa.next = item;
            aa = item;
        }
        aa.next = null;
        return frist.next;
    }

}
