package com.lw.leetcode.linked.b;

import com.lw.leetcode.linked.ListNode;

/**
 * Created with IntelliJ IDEA.
 * 24. 两两交换链表中的节点
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/10 15:47
 */
public class SwapPairs {
    public ListNode swapPairs(ListNode head) {
        int k = 2;
        int length = 0;
        ListNode l = head;
        while (l != null) {
            length++;
            l = l.next;
        }
        if (length == 0) {
            return head;
        }
        int limit = length / k;
        ListNode a = new ListNode(0);
        a.next = head;
        ListNode f = a;
        int n;
        int count = 0;
        ListNode pr;
        ListNode item;
        ListNode zh;
        ListNode d;
        while (count < limit) {
            count++;
            n = 2;
            pr = f.next;
            item = pr.next;
            d = f.next;
            while (n <= k) {
                n++;
                zh = item.next;
                item.next = pr;
                pr = item;
                item = zh;
            }
            f.next = pr;
            d.next = item;
            f = d;
        }
        return a.next;
    }
}
