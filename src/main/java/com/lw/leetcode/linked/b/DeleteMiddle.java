package com.lw.leetcode.linked.b;

import com.lw.leetcode.linked.ListNode;

/**
 * Created with IntelliJ IDEA.
 * linked
 * 5943. 删除链表的中间节点
 * 2095. 删除链表的中间节点
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/6 17:45
 */
public class DeleteMiddle {
    public ListNode deleteMiddle(ListNode head) {
        if (head.next == null) {
            return null;
        }
        if (head.next.next == null) {
            head.next = null;
            return head;
        }

        ListNode a = head;
        ListNode b = head.next;

        while (b.next != null && b.next.next != null) {
            a = a.next;
            b = b.next.next;
        }
        a.next = a.next.next;
        return head;

    }
}
