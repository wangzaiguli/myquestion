package com.lw.leetcode.linked.b;

import com.lw.leetcode.linked.ListNode;

/**
 * Created with IntelliJ IDEA.
 * 92. 反转链表 II
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/31 19:17
 */
public class ReverseBetween {

    public ListNode reverseBetween(ListNode head, int left, int right) {
        ListNode root = new ListNode(0);
        root.next = head;
        head = root;
        right++;
        int index = 1;
        while (index < left) {
            head = head.next;
            index++;
        }
        ListNode n = head;
        ListNode m = head.next;
        ListNode a = head;
        ListNode b = head.next;
        ListNode c;
        while (index < right) {
            c = b.next;
            b.next = a;
            a = b;
            b = c;
            index++;
        }
        n.next = a;
        m.next = b;
        return root.next;
    }

}
