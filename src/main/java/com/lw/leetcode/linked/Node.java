package com.lw.leetcode.linked;

import java.util.List;

/**
 * @Author liw
 * @Date 2021/8/30 16:12
 * @Version 1.0
 */
public class Node {
    public int val;
    public Node prev;
    public Node next;
    public Node child;
    public List<Node> children;
    public List<Node> neighbors;
    public int flag;

    public Node() {

    }

    public Node(int val) {
        this.val = val;
    }


    public Node(int val, List<Node> children) {
        this.val = val;
        this.children = children;
    }

    @Override
    public String toString() {
        Node node = this;

        StringBuilder sb = new StringBuilder();
        while (node != null) {
            sb.append(node.val);
            sb.append(" -> ");
            node = node.next;
        }
        return sb.toString();
    }
    // [1,2,3,4,5,6,null,null,null,7,8,9,10,null,null,11,12]
    public static Node getInstance() {

        Node a = new Node(1);
        Node b = new Node(2);
        Node c = new Node(3);
        Node d = new Node(4);
        Node e = new Node(5);
        Node f = new Node(6);
        Node g = new Node(7);
        Node h = new Node(8);
        Node i = new Node(9);
        Node j = new Node(10);
        Node k = new Node(11);
        Node l = new Node(12);
        a.next = b;
        b.next = c;
        c.next = d;
        d.next = e;
        e.next = f;
        c.child = g;
        g.next = h;
        h.next = i;
        i.next = j;
        h.child = k;
        k.next = l;
        return a;
    }
    // [1,2,3,4,5,6,null,null,null,7,8,9,10,null,null,11,12]
    public static Node getInstance2() {

        Node a = new Node(1);
        Node b = new Node(2);
        Node c = new Node(3);
        a.child = b;
        b.child = c;
        return a;
    }
}
