package com.lw.leetcode.linked;

import lombok.ToString;

/**
 * @Author liw
 * @Date 2021/4/15 16:10
 * @Version 1.0
 */
@ToString
public class ListNode {
    public int val;
    public ListNode next;

    public ListNode(int x) {
        val = x;
        next = null;
    }

    public static ListNode getInstance() {
        ListNode a = new ListNode(1);
        ListNode b = new ListNode(2);
        ListNode c = new ListNode(3);
        ListNode d = new ListNode(4);
        ListNode e = new ListNode(5);
        ListNode f = new ListNode(6);
        ListNode g = new ListNode(7);
        ListNode h = new ListNode(8);
        ListNode i = new ListNode(9);
        ListNode j = new ListNode(10);
        ListNode k = new ListNode(11);

        a.next = b;
        b.next = c;
        c.next = d;
        d.next = e;
        e.next = f;
        f.next = g;
        g.next = h;
        h.next = i;
        i.next = j;
//        j.next = k;
        return a;
    }

    public static ListNode getInstance2() {
        ListNode a = new ListNode(5);
        ListNode b = new ListNode(1);
        ListNode c = new ListNode(Integer.MIN_VALUE);
        ListNode d = new ListNode(2);
        ListNode e = new ListNode(4);

        a.next = b;
        b.next = c;
        c.next = d;
        d.next = e;
        return a;
    }


    public static ListNode getInstance3() {
        ListNode a = new ListNode(1);
        ListNode b = new ListNode(4);
        ListNode c = new ListNode(5);

        a.next = b;
        b.next = c;
        return a;
    }

    public static ListNode getInstance4() {
        ListNode a = new ListNode(1);
        ListNode b = new ListNode(3);
        ListNode c = new ListNode(4);

        a.next = b;
        b.next = c;
        return a;
    }

    public static ListNode getInstance5() {
        ListNode a = new ListNode(2);
        ListNode b = new ListNode(6);
        a.next = b;
        return a;
    }

    public static ListNode getInstance6() {

        ListNode a = new ListNode(1);
        ListNode b = new ListNode(7);
        ListNode c = new ListNode(5);
        ListNode d = new ListNode(1);
        ListNode e = new ListNode(9);
        ListNode f = new ListNode(2);
        ListNode g = new ListNode(5);
        ListNode h = new ListNode(1);

        a.next = b;
        b.next = c;
        c.next = d;
        d.next = e;
        e.next = f;
        f.next = g;
        g.next = h;
        return a;
    }

    public String str () {
        StringBuilder sb = new StringBuilder();
        ListNode node = this;
        while (node != null) {
            sb.append(node.val);
            sb.append(" -> ");
            node = node.next;
        }
        return sb.toString();
    }

}
