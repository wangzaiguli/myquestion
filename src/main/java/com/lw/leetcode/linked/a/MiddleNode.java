package com.lw.leetcode.linked.a;

import com.lw.leetcode.linked.ListNode;

/**
 * 876. 链表的中间结点
 *
 * @Author liw
 * @Date 2021/5/14 16:15
 * @Version 1.0
 */
public class MiddleNode {
    public ListNode middleNode(ListNode head) {
        ListNode item = head;
        while (item != null && item.next != null) {
            head = head.next;
            item = item.next.next;
        }
        return head;
    }
}
