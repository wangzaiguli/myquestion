package com.lw.leetcode.linked.a;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import  com.lw.leetcode.linked.Node;

/**
 * Created with IntelliJ IDEA.
 * 590. N 叉树的后序遍历
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/12 18:12
 */
public class Postorder {

    public List<Integer> postorder(Node root) {
        List<Integer> list = new ArrayList<>();
        if (root == null) {
            return list;
        }
        Stack<Node> stack = new Stack<>();
        Stack<Boolean> flag = new Stack<>();
        stack.push(root);
        flag.push(false);
        while (!stack.isEmpty()) {
            Boolean f = flag.pop();
            if (f) {
                Node node = stack.pop();
                list.add(node.val);
            } else {
                flag.push(true);
                Node node = stack.peek();
                List<Node> children = node.children;
                if (children != null && !children.isEmpty()) {
                    for (int i = children.size() - 1; i >= 0; i--) {
                        stack.push(children.get(i));
                        flag.push(false);
                    }
                }
            }

        }
        return list;
    }

}
