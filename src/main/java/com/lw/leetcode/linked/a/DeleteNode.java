package com.lw.leetcode.linked.a;

import com.lw.leetcode.linked.ListNode;

/**
 * 剑指 Offer 18. 删除链表的节点
 *
 * @Author liw
 * @Date 2021/5/14 13:20
 * @Version 1.0
 */
public class DeleteNode {
    public ListNode deleteNode(ListNode head, int val) {
        ListNode f = new ListNode(0);
        f.next = head;
        ListNode item = f;
        while (item.next != null) {
            if (item.next.val == val) {
                item.next = item.next.next;
                break;
            }
            item = item.next;
        }
        return f.next;
    }

    /**
     * 面试题 02.03. 删除中间节点
     * 237. 删除链表中的节点
     *
     * @param node
     */
        public void deleteNode(ListNode node) {
            node.val = node.next.val;
            node.next = node.next.next;
        }
}
