package com.lw.leetcode.linked.a;

import com.lw.leetcode.linked.ListNode;

/**
 * 面试题 02.01. 移除重复节点
 *
 * @Author liw
 * @Date 2021/5/14 13:55
 * @Version 1.0
 */
public class RemoveDuplicateNodes {

    public ListNode removeDuplicateNodes(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode node = head;
        while (node != null) {
            int value = node.val;
            ListNode item = node;
            while (item.next != null) {
                if (item.next.val == value) {
                    item.next = item.next.next;
                } else {
                    item = item.next;
                }
            }
            node = node.next;
        }
        return head;
    }

}
