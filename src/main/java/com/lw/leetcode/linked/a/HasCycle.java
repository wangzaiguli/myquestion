package com.lw.leetcode.linked.a;

import com.lw.leetcode.linked.ListNode;

/**
 * Created with IntelliJ IDEA.
 * 141. 环形链表
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/30 21:08
 */
public class HasCycle {

    public boolean hasCycle(ListNode head) {
        ListNode a = head;
        ListNode b = head;
        while (a != null && b != null) {
            a = a.next;
            b = b.next;
            if (b == null) {
                return false;
            }
            b = b.next;
            if (a == b) {
                return true;
            }
        }
        return false;
    }

}
