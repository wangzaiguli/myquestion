package com.lw.leetcode.linked.a;

import com.lw.leetcode.linked.ListNode;

/**
 * Created with IntelliJ IDEA.
 * 移除链表元素
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/16 10:23
 */
public class RemoveElements {

    public ListNode removeElements(ListNode head, int val) {
        ListNode last = null;
        ListNode b = head;
        ListNode t = head;
        while (t != null) {
            int a = t.val;
            if (a == val) {
                if (last == null) {
                    b = b.next;
                } else {
                    last.next = last.next.next;
                }
            } else {
                last = t;
            }
            t = t.next;
        }
        return b;
    }

}
