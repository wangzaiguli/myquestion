package com.lw.leetcode.linked.a;

import com.lw.leetcode.linked.ListNode;

/**
 * 1290. 二进制链表转整数
 *
 * @Author liw
 * @Date 2021/5/14 15:23
 * @Version 1.0
 */
public class GetDecimalValue {

    public int getDecimalValue(ListNode head) {
        int value = 0;
        while (head != null) {
            value = (value << 1) + head.val;
            head = head.next;
        }
        return value;
    }

}
