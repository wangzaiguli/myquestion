package com.lw.leetcode.linked.a;

import com.lw.leetcode.linked.ListNode;

import java.util.ArrayList;
import java.util.List;

/**
 * 剑指 Offer 06. 从尾到头打印链表
 *
 * @Author liw
 * @Date 2021/5/13 16:13
 * @Version 1.0
 */
public class ReversePrint {

    public int[] reversePrint(ListNode head) {
        List<Integer> list = new ArrayList<>();
        find(head, list);
        int size = list.size();
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = list.get(i);
        }
        return arr;
    }

    private void find(ListNode node, List<Integer> list) {
        if (node == null) {
            return;
        }
        find(node.next, list);
        list.add(node.val);
    }

}
