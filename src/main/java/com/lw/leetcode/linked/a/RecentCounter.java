package com.lw.leetcode.linked.a;

import java.util.LinkedList;
import java.util.Queue;

/**
 * liked
 * a
 * 933. 最近的请求次数
 *
 * @Author liw
 * @Date 2021/11/6 23:13
 * @Version 1.0
 */
public class RecentCounter {

    private Queue<Integer> q;

    public RecentCounter() {
        q = new LinkedList<>();
    }

    public int ping(int t) {
        q.add(t);
        while (q.peek() < t - 3000) {
            q.poll();
        }
        return q.size();
    }

}
