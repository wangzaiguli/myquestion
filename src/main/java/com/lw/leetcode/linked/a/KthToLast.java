package com.lw.leetcode.linked.a;

import com.lw.leetcode.linked.ListNode;

/**
 * 面试题 02.02. 返回倒数第 k 个节点
 * 剑指 Offer 22. 链表中倒数第k个节点
 *
 * @Author liw
 * @Date 2021/5/14 13:14
 * @Version 1.0
 */
public class KthToLast {
    public int kthToLast(ListNode head, int k) {
        ListNode a = head;
        ListNode b = head;
        for (int i = 0; i < k; i++) {
            b = b.next;
        }
        while (b != null) {
            a = a.next;
            b = b.next;

        }
        return a.val;
    }
}
