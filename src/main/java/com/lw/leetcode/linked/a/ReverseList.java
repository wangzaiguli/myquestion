package com.lw.leetcode.linked.a;

import com.lw.leetcode.linked.ListNode;

/**
 * 206. 反转链表
 * 剑指 Offer 24. 反转链表
 *
 * @Author liw
 * @Date 2021/5/14 13:28
 * @Version 1.0
 */
public class ReverseList {
    public ListNode reverseList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode item = head.next;
        ListNode last = head;
        head.next = null;
        ListNode next;
        while (item != null) {
            next = item.next;
            item.next = last;
            last = item;
            item = next;
        }
        return last;
    }
}
