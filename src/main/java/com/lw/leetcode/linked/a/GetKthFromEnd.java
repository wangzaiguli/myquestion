package com.lw.leetcode.linked.a;

import com.lw.leetcode.linked.ListNode;

/**
 * Created with IntelliJ IDEA.
 * 剑指 Offer 22. 链表中倒数第k个节点
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/2 10:49
 */
public class GetKthFromEnd {

    public ListNode getKthFromEnd(ListNode head, int k) {
        ListNode a = head;
        ListNode b = head;
        for (int i = 0; i < k; i++) {
            b = b.next;
        }
        while (b != null) {
            a = a.next;
            b = b.next;
        }
        return a;
    }

}
