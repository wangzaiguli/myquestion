package com.lw.leetcode.topology.b;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * 210. 课程表 II
 * 剑指 Offer II 113. 课程顺序
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/15 14:03
 */
public class FindOrder {

    public static void main(String[] args) {
        FindOrder test = new FindOrder();
        int[][] arr = {{1, 0}, {2, 0}};
        int k = 4;
        int[] order = test.findOrder(k, arr);
        System.out.println(Arrays.toString(order));
    }

    public int[] findOrder(int numCourses, int[][] prerequisites) {
        int[] r = new int[numCourses];
        if (prerequisites == null || prerequisites.length == 0 || prerequisites[0].length == 0) {
            for (int i = 0; i < numCourses; i++) {
                r[i] = i;
            }
            return r;
        }
        int length = prerequisites.length;
        Stack<Integer> stack = new Stack<>();
        Map<Integer, Integer> map = new HashMap<>();
        Map<Integer, List<Integer>> all = new HashMap<>();
        for (int i = 0; i < length; i++) {
            int[] arr = prerequisites[i];
            List<Integer> item = all.computeIfAbsent(arr[1], k -> new ArrayList<>());
            item.add(arr[0]);
            map.putIfAbsent(arr[1], 0);
            map.merge(arr[0], 1, (a, b) -> a + b);
        }
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() == 0) {
                stack.add(entry.getKey());
            }
        }
        int count = 0;
        int size = map.size();
        while (!stack.isEmpty() && count < size) {
            Integer pop = stack.pop();
            List<Integer> list = all.get(pop);
            r[count++] = pop;
            if (list == null) {
                continue;
            }
            for (Integer item : list) {
                int v = map.get(item) - 1;
                if (v == 0) {
                    stack.add(item);
                }
                map.put(item, v);
            }
        }
        if (stack.isEmpty() && count == size) {
            for (int i = 0; i < numCourses; i++) {
                if (map.get(i) == null) {
                    r[count++] = i;
                }
            }
            return r;

        } else {
            return new int[0];
        }
    }
}
