package com.lw.leetcode.binary.b;

/**
 * binary
 * 875. 爱吃香蕉的珂珂
 * 剑指 Offer II 073. 狒狒吃香蕉
 *
 * @Author liw
 * @Date 2021/8/2 21:49
 * @Version 1.0
 */
public class MinEatingSpeed {

    public static void main(String[] args) {
        MinEatingSpeed test = new MinEatingSpeed();
        int[] piles = {3, 6, 7, 11};
        int h = 8;
        int i = test.minEatingSpeed(piles, h);
        System.out.println(i);
    }

    public int minEatingSpeed(int[] piles, int h) {
        int st = 1;
        int end = 1000000000;
        while (st < end) {
            int m = st + ((end - st) >> 1);
            int i = find(piles, m);
            if (i <= h) {
                end = m;
            } else {
                st = m + 1;
            }
        }
        return st;
    }

    private int find(int[] piles, int m) {
        int c = 0;
        for (int pile : piles) {
            c += ((pile - 1) / m + 1);
        }
        return c;
    }
}
