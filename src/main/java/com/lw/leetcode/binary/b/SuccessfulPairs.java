package com.lw.leetcode.binary.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 6096. 咒语和药水的成功对数
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/12 12:36
 */
public class SuccessfulPairs {

    public int[] successfulPairs(int[] spells, int[] potions, long success) {
        Arrays.sort(potions);
        int length = spells.length;
        int[] arr = new int[length];
        for (int i = 0; i < length; i++) {
            int v = spells[i];
            arr[i] = getCount((success + v - 1) / v - 1, potions);
        }
        return arr;
    }

    private int getCount(long t, int[] arr) {
        int end = arr.length - 1;
        if (arr[end] <= t) {
            return 0;
        }
        int st = 0;
        while (st < end) {
            int m = st + ((end - st) >> 1);
            if (arr[m] <= t) {
                st = m + 1;
            } else {
                end = m;
            }
        }
        return arr.length - st;
    }

}
