package com.lw.leetcode.binary.b;

/**
 * Created with IntelliJ IDEA.
 * 2226. 每个小孩最多能分到多少糖果
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/5 13:20
 */
public class MaximumCandies {

    public int maximumCandies(int[] candies, long k) {
        int end = 0;
        for (int candy : candies) {
            end = Math.max(end, candy);
        }
        int st = 0;
        while (st < end) {
            int m = st + ((end - st + 1) >> 1);
            long c = 0;
            for (int candy : candies) {
                c += candy / m;
            }
            if (c < k) {
                end = m - 1;
            } else {
                st = m;
            }
        }
        return st;
    }

}
