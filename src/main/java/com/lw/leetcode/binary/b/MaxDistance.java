package com.lw.leetcode.binary.b;

/**
 * 1855. 下标对中的最大距离
 *
 * @Author liw
 * @Date 2021/6/21 11:36
 * @Version 1.0
 */
public class MaxDistance {

    public static void main(String[] args) {
        MaxDistance test = new MaxDistance();
//        int[] a = {55,30,5,4,2};
//        int[] b = {100,20,10,10,5};
        int[] a = {2,2,2};
        int[] b = {10,10,1};
        int i = test.maxDistance(a, b);
        System.out.println(i);
    }

    public int maxDistance(int[] nums1, int[] nums2) {
        int length = nums1.length - 1;
        int max = 0;
        for (int i = nums2.length - 1; i > max; i--) {
            int end = Math.min(length, i);
            if (nums2[i] >= nums1[end]) {
                max = Math.max(max, i - find(nums1, nums2[i], end));

            }
        }
        return max;
    }

    private int find(int[] arr, int n, int end) {
        int st = 0;
        while (st < end) {
            int m = st + ((end - st) >> 1);
            if (arr[m] > n) {
                st = m + 1;
            } else {
                end = m ;
            }
        }
        return st;
    }

}
