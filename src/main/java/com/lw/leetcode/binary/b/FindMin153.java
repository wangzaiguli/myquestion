package com.lw.leetcode.binary.b;

/**
 * Created with IntelliJ IDEA.
 * 153. 寻找旋转排序数组中的最小值
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/7 11:19
 */
public class FindMin153 {

    public int findMin(int[] nums) {
        int length = nums.length;
        if (length == 1) {
            return nums[0];
        }
        int st = 0;
        int end = length - 1;
        int b = nums[length - 1];
        while (st <= end) {
            int m = st + ((end - st) >> 1);
            int value = nums[m];
            if (m != 0 && nums[m - 1] > value) {
                return nums[m];
            }
            if (value <= b) {
                end = m - 1;
            } else {
                st = m + 1;
            }
        }
        return nums[0];
    }

}
