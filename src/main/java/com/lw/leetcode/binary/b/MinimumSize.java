package com.lw.leetcode.binary.b;

/**
 * Created with IntelliJ IDEA.
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/4 10:17
 */
public class MinimumSize {

    public static void main(String[] args) {
        MinimumSize test = new MinimumSize();


        // 2
//        int[] arr = {2,4,8,2};
//        int k = 4;

        // 7
//        int[] arr = {7,17};
//        int k = 2;

        // 3
        int[] arr = {1};
        int k = 2;

        // 1
//        int[] arr = {1};
//        int k = 1;

        int i = test.minimumSize(arr, k);

        System.out.println(i);


    }


    public int minimumSize(int[] nums, int maxOperations) {
        int st = 0;
        int end = Integer.MAX_VALUE;
        while (st < end) {
            int m = st + ((end - st) >> 1);
            int sum = 0;
            if (m == 0) {
                return 1;
            }
            for (int num : nums) {
                sum += (num - 1) / m;
            }
            if (sum > maxOperations) {
                st = m + 1;
            } else {
                end = m;
            }
        }
        return st;
    }

}
