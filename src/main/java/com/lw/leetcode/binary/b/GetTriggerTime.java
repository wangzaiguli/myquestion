package com.lw.leetcode.binary.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * binay
 * LCP 08. 剧情触发时间
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/13 17:02
 */
public class GetTriggerTime {

    public static void main(String[] args) {
        GetTriggerTime test = new GetTriggerTime();

        // {2,-1,3,-1}
//        int[][] increase = {{2, 8, 4}, {2, 5, 0}, {10, 9, 8}};
//        int[][] requirements = {{2, 11, 3}, {15, 10, 7}, {9, 17, 12}, {8, 1, 14}};

        // {-1,4,3,3,3}
        int[][] increase = {{0, 4, 5}, {4, 8, 8}, {8, 6, 1}, {10, 10, 0}};
        int[][] requirements = {{12, 11, 16}, {20, 2, 6}, {9, 2, 6}, {10, 18, 3}, {8, 14, 9}};

        // {0}
//        int[][] increase = {{1, 1, 1}};
//        int[][] requirements = {{0, 0, 0}};

        int[] triggerTime = test.getTriggerTime(increase, requirements);
        System.out.println(Arrays.toString(triggerTime));
    }

    private int[][] increase;

    public int[] getTriggerTime(int[][] increase, int[][] requirements) {
        int length = increase.length;
        this.increase = increase;
        for (int i = 1; i < length; i++) {
            int[] as = increase[i];
            int[] bs = increase[i - 1];
            as[0] += bs[0];
            as[1] += bs[1];
            as[2] += bs[2];
        }
        int ma = increase[length - 1][0];
        int mb = increase[length - 1][1];
        int mc = increase[length - 1][2];
        int l = requirements.length;
        int[] arr = new int[l];
        for (int i = 0; i < l; i++) {
            int[] requirement = requirements[i];
            int a = requirement[0];
            int b = requirement[1];
            int c = requirement[2];
            if (ma < a || mb < b || mc < c) {
                arr[i] = -1;
                continue;
            } else if (a == 0 && b == 0 && c == 0) {
                arr[i] = 0;
                continue;
            }
            arr[i] = find(a - 1, b - 1, c - 1);
        }
        return arr;
    }

    private int find(int a, int b, int c) {
        int st = 0;
        int end = increase.length - 1;
        while (st < end) {
            int m = st + ((end - st) >> 1);
            if (increase[m][0] > a && increase[m][1] > b && increase[m][2] > c) {
                end = m;
            } else {
                st = m + 1;
            }
        }
        return st + 1;
    }

}
