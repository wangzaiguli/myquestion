package com.lw.leetcode.binary.b;

/**
 * 189. 旋转数组
 *
 * @Author liw
 * @Date 2021/5/10 10:24
 * @Version 1.0
 */
public class Rotate {

    public void rotate(int[] nums, int k) {
        int length = nums.length;
        k = k % length;
        change(nums, 0, length - 1);
        change(nums, 0, k - 1);
        change(nums, k , length - 1);
    }


    private void change(int[] nums, int st, int end) {
        while (st < end) {
            int item = nums[st];
            nums[st++] = nums[end];
            nums[end--] = item;
        }
    }
}
