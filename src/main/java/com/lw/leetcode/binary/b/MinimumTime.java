package com.lw.leetcode.binary.b;

/**
 * Created with IntelliJ IDEA.
 * binay
 * 2187. 完成旅途的最少时间
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/28 9:49
 */
public class MinimumTime {

    public static void main(String[] args) {
        MinimumTime test = new MinimumTime();
        // [1,2,3], totalTrips = 5

        // 3
//        int[] arr = {1,2,3};
//        int k = 5;

        // 2
        int[] arr = {2};
        int k = 1;

        long l = test.minimumTime(arr, k);
        System.out.println(l);

    }

    public long minimumTime(int[] time, int totalTrips) {
        long st = 1L;
        long end = Long.MAX_VALUE;
        long v = Long.MAX_VALUE;
        a:
        while (st < end) {
            long m = st + ((end - st) >> 1);
            long c = 0L;
            for (long i : time) {
                c = c + m / i;
                if (c < 0) {
                    end = m;
                    continue a;
                }
            }

            if (c < totalTrips) {
                st = m + 1L;
            } else {
                v = Math.min(v, m);
                end = m;
            }
        }
        return v;
    }

}
