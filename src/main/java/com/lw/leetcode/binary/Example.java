package com.lw.leetcode.binary;

/**
 * @Author liw
 * @Date 2021/5/7 15:42
 * @Version 1.0
 */
public class Example {
    /**
     * 大于目标数的最小值
     *
     * @param weights
     * @param value
     * @return
     */
    public int getGeMin(int[] weights, int value) {
        int st = 0;
        int end = weights.length - 1;
        int m;
        while (st < end) {
            m = st + ((end - st) >> 1);
            if (weights[m] >= value) {
                end = m;
            } else {
                st = m + 1;
            }
        }
        return st;
    }

    /**
     * 小于目标数的最大值
     *
     * @param weights
     * @param value
     * @return
     */
    public int getLeMax(int[] weights, int value) {
        int st = 0;
        int end = weights.length - 1;
        int m;
        while (st < end) {
            m = st + ((end - st + 1) >> 1);
            if (weights[m] <= value) {
                st = m;
            } else {
                end = m - 1;
            }
        }
        return end;
    }
}
