package com.lw.leetcode.binary.a;

/**
 * 面试题 08.03. 魔术索引
 *
 * @Author liw
 * @Date 2021/5/10 13:38
 * @Version 1.0
 */
public class FindMagicIndex {

    public static void main(String[] args) {
        FindMagicIndex test = new FindMagicIndex();
        int[] arr = {1,1,1};
        int magicIndex = test.findMagicIndex(arr);
        System.out.println(magicIndex);
    }

    public int findMagicIndex(int[] nums) {
        int length = nums.length;
        for (int i = 0; i < length; ) {
            if (i == nums[i]){
                return i;
            }
            i = Math.max(i + 1, nums[i]);
        }

        return -1;
    }
}
