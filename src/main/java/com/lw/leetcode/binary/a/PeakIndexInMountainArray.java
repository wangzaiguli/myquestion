package com.lw.leetcode.binary.a;

/**
 * binary
 * 852. 山脉数组的峰顶索引
 * 剑指 Offer II 069. 山峰数组的顶部
 *
 * @Author liw
 * @Date 2021/6/15 20:02
 * @Version 1.0
 */
public class PeakIndexInMountainArray {
    public int peakIndexInMountainArray(int[] A) {
        int length = A.length;
        int mid = length / 2;
        int st = 0;
        int end = length - 1;
        while (mid > st && mid < end) {
            if (A[mid] > A[mid - 1] && A[mid] > A[mid + 1]) {
                break;
            } else if (A[mid] > A[mid - 1] && A[mid] < A[mid + 1]) {
                st = mid;
            } else {
                end = mid;
            }
            mid = (st + end) / 2;
        }
        return mid;
    }
}
