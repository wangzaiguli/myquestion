package com.lw.leetcode.binary.a;

/**
 * binary
 * 35. 搜索插入位置
 * 剑指 Offer II 068. 查找插入位置
 *
 * @Author liw
 * @Date 2021/8/4 21:27
 * @Version 1.0
 */
public class SearchInsert {

    public int searchInsert(int[] nums, int target) {
        if (nums[0] >= target) {
            return 0;
        }
        int st = 0;
        int end = nums.length - 1;
        while (st < end) {
            int m = st + ((end - st + 1) >> 1);
            if (nums[m] < target) {
                st = m;
            } else {
                end = m - 1;
            }
        }
        return st + 1;
    }

    public int searchInsert2(int[] nums, int target) {
        for(int i = 0; i < nums.length;i++){
            if(nums[i] >= target){
                return i;
            }
        }
        return nums.length;
    }

}
