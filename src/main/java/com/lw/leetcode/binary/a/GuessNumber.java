package com.lw.leetcode.binary.a;

/**
 * 374. 猜数字大小
 *
 * @Author liw
 * @Date 2021/6/14 19:20
 * @Version 1.0
 */
public class GuessNumber {

    public int guessNumber(int n) {
        int st = 0;
        int end = n;
        while (st <= end) {
            int mid = st + ((end - st) >> 1);
            if (guess(mid) == -1) {
                end = mid - 1;
            } else if (guess(mid) == 1) {
                st = mid + 1;
            } else {
                return mid;
            }
        }
        return st;
    }

    private int guess(int st) {
        return -1;
    }

}
