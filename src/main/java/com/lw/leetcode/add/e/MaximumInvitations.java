package com.lw.leetcode.add.e;

/**
 * Created with IntelliJ IDEA.
 * 2127. 参加会议的最多员工数
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/2 11:41
 */
public class MaximumInvitations {


    public static void main(String[] args) {
        MaximumInvitations test = new MaximumInvitations();

        // 3
        int[] arr = {2, 2, 1, 2};

        // 3
//        int[] arr = {1, 2, 0};

        // 4
//        int[] arr = {3, 0, 1, 4, 1};

        int i = test.maximumInvitations(arr);
        System.out.println(i);
    }

    public int maximumInvitations(int[] favorite) {

        int max = -1;
        int length = favorite.length;
        int[] arr = new int[length];

        int count = 1;

        for (int i = 0; i < length; i++) {
            if (arr[i] > 0) {
                continue;
            }

            int item = i;

            while (arr[item] == 0) {
                arr[item] = count++;
                item = favorite[item];
            }

            max = Math.max(max, count - arr[item]);
        }


        return max;
    }
}
