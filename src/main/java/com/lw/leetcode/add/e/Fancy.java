package com.lw.leetcode.add.e;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * 1622. 奇妙序列
 *
 * @author liw
 * @version 1.0
 * @date 2022/11/8 17:00
 */
public class Fancy {

    public static void main(String[] args) {
        Fancy test = new Fancy();

        //[null, null, null, null, null, 10, null, null, null, 26, 34, 20]
//        test.append(2);
//        test.addAll(3);
//        test.append(7);
//        test.multAll(2);
//        System.out.println(test.getIndex(0));
//        test.addAll(3);
//        test.append(10);
//        test.multAll(2);
//        System.out.println(test.getIndex(0));
//        System.out.println(test.getIndex(1));
//        System.out.println(test.getIndex(2));
//
//        // ["Fancy","addAll","getIndex"]
//        //[[],[1],[0]]
//        test.addAll(1);
//        System.out.println(test.getIndex(0));


        // ["Fancy","append","append","getIndex","append","getIndex","addAll","append","getIndex","getIndex","append","append","getIndex","append","getIndex","append","getIndex","append","getIndex","multAll","addAll","getIndex","append","addAll","getIndex","multAll","getIndex","multAll","addAll","addAll","append","multAll","append","append","append","multAll","getIndex","multAll","multAll","multAll","getIndex","addAll","append","multAll","addAll","addAll","multAll","addAll","addAll","append","append","getIndex"]
        //[[],[12],[8],[1],[12],[0],[12],[8],[2],[2],[4],[13],[4],[12],[6],[11],[1],[10],[2],[3],[1],[6],[14],[5],[6],[12],[3],[12],[15],[6],[7],[8],[13],[15],[15],[10],[9],[12],[12],[9],[9],[9],[9],[4],[8],[11],[15],[9],[1],[4],[10],[9]]


        String[] strs = {"Fancy", "append", "append", "getIndex", "append", "getIndex", "addAll", "append", "getIndex", "getIndex", "append", "append", "getIndex", "append", "getIndex", "append", "getIndex", "append", "getIndex", "multAll", "addAll", "getIndex", "append", "addAll", "getIndex", "multAll",
                "getIndex", "multAll", "addAll", "addAll", "append", "multAll", "append", "append", "append", "multAll", "getIndex", "multAll", "multAll", "multAll", "getIndex", "addAll", "append", "multAll", "addAll", "addAll", "multAll", "addAll", "addAll", "append", "append", "getIndex", "multAll", "multAll", "multAll"};
        int[][] items = {{}, {12}, {8}, {1}, {12}, {0}, {12}, {8}, {2}, {2}, {4}, {13}, {4}, {12}, {6}, {11}, {1}, {10}, {2}, {3}, {1}, {6}, {14}, {5}, {6}, {12}, {3}, {12}, {15}, {6}, {7}, {8}, {13}, {15}, {15}, {10}, {9}, {12}, {12}, {9}, {9}, {9}, {9}, {4}, {8}, {11}, {15}, {9}, {1}, {4}, {10}, {9}, {97}, {99}, {90}};
        int length = strs.length;

        long tt = 1;
        long ttt = 1;
        System.out.println();
        System.out.println();
        for (int i = 0; i < length; i++) {
            if (strs[i].equals("multAll")) {
                tt *= items[i][0];
                ttt = (ttt * items[i][0]) % test.t;
                System.out.println(items[i][0] + "  " + tt + "  " + (tt % test.t) + "  " + ttt);
            }
        }
        System.out.println(tt / 432);
        System.out.println((tt / 432) % test.t);

        System.out.println(test.get(432, ttt));

        System.out.println(432 * 450778368L == 1000000007L * 194 + 736253618L);

    }

    private long a = 1;
    private long b;
    private TreeMap<Integer, Node> map = new TreeMap<>();
    private List<Integer> list = new ArrayList<>();
    private int t = 1000000007;

    public Fancy() {

        map.put(0, new Node(1, 0));
    }

    public void append(int val) {
        list.add(val);
    }

    public void addAll(int inc) {
        b += inc;
        b %= t;
        map.put(list.size(), new Node(a, b));
    }

    public void multAll(int m) {
        a *= m;
        b *= m;
        b %= t;
        a %= t;
        map.put(list.size(), new Node(a, b));
    }

    public int getIndex(int idx) {
        if (idx >= list.size()) {
            return -1;
        }
        long v = list.get(idx);
        Node node = map.floorEntry(idx).getValue();

        long x = get(node.a, a);
        System.out.println(node.a + "  " + a + "  " + x);
        long y = b - (node.b * x) % t;
        y = y < 0 ? y + t : y;
        return (int) ((v * x + y) % t);
    }

    private long get(long n, long m) {
        if ((m % n) == 0) {
            return m / n;
        }
        long c = 0;

        if (m > n) {
            c = m / n;
            m = m % n;
        }



        long i =  (m % n) / (n - (t % n));
        i = i < 0 ? i + t : i;
        return (i * t + m) / n;
    }

    private class Node {
        private long a;
        private long b;

        public Node(long a, long b) {
            this.a = a;
            this.b = b;
        }
    }

}
