package com.lw.leetcode.add.e;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 1039. 多边形三角剖分的最低得分
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/14 21:28
 */
public class MinScoreTriangulation {


    public static void main(String[] args) {
        MinScoreTriangulation test = new MinScoreTriangulation();

        int n = 50;

        int[] arr = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = (int) (Math.random() * 100);
        }
        System.out.println(Arrays.toString(arr));

        // 6
//        int[] arr = {1,2,3};

        // 144
//        int[] arr = {3,7,4,5};

        // 13
//        int[] arr = {1,3,1,4,1,5};

        int i = test.minScoreTriangulation(arr);
        System.out.println(i);
    }

    private Map<String, Integer> map = new HashMap<>();

    private int min;

    public int minScoreTriangulation(int[] arr) {


        int length = arr.length;
        if (length < 3) {
            return 0;
        }
        if (length == 3) {
            return arr[0] * arr[1] * arr[2];
        }
        if (length == 4) {
            return Math.min(arr[0] * arr[1] * arr[2] + arr[2] * arr[3] * arr[0],
                    arr[0] * arr[1] * arr[3] + arr[3] * arr[1] * arr[2]);
        }

        int[] arr1;
        int[] arr2;

        arr1 = new int[length - 1];
        int index = 0;
        for (int j = 2; j < length; j++) {
            arr1[index++] = arr[j];
        }
        arr1[index] = arr[0];


        arr2 = new int[length - 1];
        index = 0;
        for (int j = 1; j < length; j++) {
            arr2[index++] = arr[j];
        }

        min = Math.min(arr[0] * arr[1] * arr[2] + find(arr1), arr[0] * arr[1] * arr[length - 1] + find(arr2));


        return find(arr);
    }

    private int find(int[] arr) {
        String key = Arrays.toString(arr);
        System.out.println(key);
        if (map.containsKey(key)) {
            return map.get(key);
        }

        int length = arr.length;
        if (length < 3) {
            return 0;
        }
        if (length == 3) {
            return arr[0] * arr[1] * arr[2];
        }
        if (length == 4) {
            return Math.min(arr[0] * arr[1] * arr[2] + arr[2] * arr[3] * arr[0],
                    arr[0] * arr[1] * arr[3] + arr[3] * arr[1] * arr[2]);
        }
        int[] arr1;
        int[] arr2;

        arr1 = new int[length - 1];
        int index = 0;
        for (int j = 2; j < length; j++) {
            arr1[index++] = arr[j];
        }
        arr1[index] = arr[0];


        arr2 = new int[length - 1];
        index = 0;
        for (int j = 1; j < length; j++) {
            arr2[index++] = arr[j];
        }

        int min = Math.min(arr[0] * arr[1] * arr[2] + find(arr1), arr[0] * arr[1] * arr[length - 1] + find(arr2));
        for (int i = 3; i <= length - 2; i++) {
            arr1 = new int[i];
            index = 0;
            for (int j = 1; j <= i; j++) {
                arr1[index++] = arr[j];
            }
            arr2 = new int[length - i + 1];
            index = 0;
            for (int j = i; j < length; j++) {
                arr2[index++] = arr[j];
            }
            arr2[index] = arr[0];
            min = Math.min(min, arr[0] * arr[1] * arr[i] + find(arr1) + find(arr2));

        }
        map.put(key, min);
        return min;
    }

}
