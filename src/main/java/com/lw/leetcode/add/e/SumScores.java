package com.lw.leetcode.add.e;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2223. 构造字符串的总得分和
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/22 18:08
 */
public class SumScores {

    public static void main(String[] args) {
        SumScores test = new SumScores();

        // 14
//        String str = "azbazbzaz";

        // 9
        String str = "babab";

        long l = test.sumScores(str);
        System.out.println(l);
    }

    public long sumScores(String s) {

        char[] chars = s.toCharArray();
        int[] ints = find(chars);
        System.out.println(Arrays.toString(ints));
        return 0;
    }

    private int[] find(char[] chars) {

        int length = chars.length;
        int[] arr = new int[length];

        int i = 0;
        int j = -1;

        while (i < length && j < length) {
            if (j == -1 || chars[i] == chars[j]) {
                arr[i] = j;
                i++;
                j++;
            } else {
                j = arr[j];
            }
        }

        return arr;
    }
}
