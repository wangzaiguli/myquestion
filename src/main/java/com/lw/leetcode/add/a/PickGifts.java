package com.lw.leetcode.add.a;

import java.util.PriorityQueue;

/**
 * Created with IntelliJ IDEA.
 * a
 * sort
 * 6348. 从数量最多的堆取走礼物
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/6 9:53
 */
public class PickGifts {

    public long pickGifts(int[] gifts, int k) {
        PriorityQueue<Integer> queue = new PriorityQueue<>((a, b) -> Integer.compare(b, a));
        for (int gift : gifts) {
            queue.add(gift);
        }
        while (k > 0) {
            k--;
            int v = queue.poll();
            if (v == 1) {
                queue.add(v);
                break;
            }
            int t = (int) Math.pow(v, 0.5);
            queue.add(t);
        }
        long sum = 0;
        while (!queue.isEmpty()) {
            sum += queue.poll();
        }
        return sum;
    }

}
