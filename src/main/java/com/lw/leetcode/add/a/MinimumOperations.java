package com.lw.leetcode.add.a;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2357. 使数组中所有元素都等于零
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/1 15:02
 */
public class MinimumOperations {

    public int minimumOperations(int[] nums) {
      return (int) Arrays.stream(nums).filter(item -> item> 0).distinct().count();
    }

}
