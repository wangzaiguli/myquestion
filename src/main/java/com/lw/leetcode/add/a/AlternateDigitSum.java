package com.lw.leetcode.add.a;

/**
 * Created with IntelliJ IDEA.
 * a
 * other
 * 2544. 交替数字和
 *
 *@author liw
 * @version 1.0
 * @date 2023/1/28 9:28
 */
public class AlternateDigitSum {

    public int alternateDigitSum(int n) {
        char[] arr = String.valueOf(n).toCharArray();
        int sum = 0;
        int item = 1;
        for (char c : arr) {
            sum = sum + item * (c - '0');
            item *= -1;
        }
        return sum;
    }

}
