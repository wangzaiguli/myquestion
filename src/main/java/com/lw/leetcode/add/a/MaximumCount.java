package com.lw.leetcode.add.a;

/**
 * Created with IntelliJ IDEA.
 * 6283. 正整数和负整数的最大计数
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/9 10:17
 */
public class MaximumCount {

    public int maximumCount(int[] nums) {
        int num1 = 0;
        int num2 = 0;
        int ans = 0;
        for (int num : nums) {
            if (num < 0) {
                num1++;
            }
            if (num > 0) {
                num2++;
            }
            ans = Math.max(num2, Math.max(num1, ans));
        }
        return ans;
    }

}
