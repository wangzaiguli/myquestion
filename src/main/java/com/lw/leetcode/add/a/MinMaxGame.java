package com.lw.leetcode.add.a;

/**
 * Created with IntelliJ IDEA.
 * 2293. 极大极小游戏
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/7 9:08
 */
public class MinMaxGame {

    public int minMaxGame(int[] nums) {
        int n = nums.length;
        while (n != 1) {
            int cnt = 0;
            for (int i = 0; i < n; ) {
                if (cnt % 2 == 0) {
                    nums[cnt] = Math.min(nums[i], nums[i + 1]);
                } else {
                    nums[cnt] = Math.max(nums[i], nums[i + 1]);
                }
                i += 2;
                cnt++;
            }
            n = cnt;
        }
        return nums[0];
    }

}
