package com.lw.leetcode.add.a;

/**
 * Created with IntelliJ IDEA.
 * other
 * a
 * 2549. 统计桌面上的不同数字
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/29 13:40
 */
public class DistinctIntegers {

    public int distinctIntegers(int n) {
        return n == 1 ? 1 : n - 1;
    }

}
