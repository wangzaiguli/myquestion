package com.lw.leetcode.add.a;

/**
 * Created with IntelliJ IDEA.
 * a
 * arr
 * 2540. 最小公共值
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/28 10:16
 */
public class GetCommon {

    public int getCommon(int[] nums1, int[] nums2) {
        int a = nums1.length;
        int b = nums2.length;
        int i = 0;
        int j = 0;
        while (i < a && j < b) {
            if (nums1[i] == nums2[j]) {
                return nums1[i];
            }
            if (nums1[i] < nums2[j]) {
                i++;
            } else {
                j++;
            }
        }
        return -1;
    }

}
