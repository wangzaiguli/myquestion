package com.lw.leetcode.add.a;

/**
 * Created with IntelliJ IDEA.
 * 2220. 转换数字的最少位翻转次数
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/7 9:27
 */
public class MinBitFlips {

    public int minBitFlips(int start, int goal) {
        return Integer.bitCount(start ^ goal);
    }

}
