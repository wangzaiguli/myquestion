package com.lw.leetcode.add.a;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * a
 * other
 * 6303. 分割数组中数字的数位
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/6 9:16
 */
public class SeparateDigits {

    public int[] separateDigits(int[] nums) {
        List<Integer> list = new ArrayList<>();
        LinkedList<Integer> queue = new LinkedList<>();
        for (int num : nums) {
            while (num != 0) {
                queue.add(num % 10);
                num /= 10;
            }
            while (!queue.isEmpty()) {
                list.add(queue.pollLast());
            }
        }

        int size = list.size();
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = list.get(i);
        }
        return arr;
    }

}
