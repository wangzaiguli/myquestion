package com.lw.leetcode.add.a;

/**
 * Created with IntelliJ IDEA.
 * a
 * other
 * 2566. 替换一个数字后的最大差值
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/19 11:54
 */
public class MinMaxDifference {

    public int minMaxDifference(int num) {
        char[] arr = String.valueOf(num).toCharArray();
        int length = arr.length;
        int index = 0;
        while (index < length && arr[index] == '9') {
            index++;
        }
        int max = num;
        if (length != index) {
            char t = arr[index];
            char[] as = String.valueOf(num).toCharArray();
            for (int i = 0; i < length; i++) {
                if (as[i] == t) {
                    as[i] = '9';
                }
            }
            max = Integer.parseInt(String.valueOf(as));
        }
        index = 0;
        while (index < length && arr[index] == '0') {
            index++;
        }
        int min = num;
        if (length != index) {
            char t = arr[index];
            for (int i = 0; i < length; i++) {
                if (arr[i] == t) {
                    arr[i] = '0';
                }
            }
            min = Integer.parseInt(String.valueOf(arr));
        }
        return max - min;
    }

}
