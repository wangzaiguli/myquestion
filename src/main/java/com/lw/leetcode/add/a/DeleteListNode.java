package com.lw.leetcode.add.a;

import com.lw.leetcode.linked.ListNode;

/**
 * Created with IntelliJ IDEA.
 * a
 * link
 * https://leetcode.cn/contest/ccbft-2021fall/problems/woGGnF/
 * 建信01. 间隔删除链表结点
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/16 13:39
 */
public class DeleteListNode {

    public ListNode deleteListNode(ListNode head) {
        ListNode node = head;
        while (node != null && node.next != null) {
            node.next = node.next.next;
            node = node.next;
        }
        return head;
    }

}
