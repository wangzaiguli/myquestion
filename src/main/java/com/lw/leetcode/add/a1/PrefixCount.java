package com.lw.leetcode.add.a1;

/**
 * Created with IntelliJ IDEA.
 * str
 * 2185. 统计包含给定前缀的字符串
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/28 10:17
 */
public class PrefixCount {

    public int prefixCount(String[] words, String pref) {
        int cnt = 0;
        for (String s : words) {
            if (s.startsWith(pref)) {
                cnt++;
            }
        }
        return cnt;
    }

}
