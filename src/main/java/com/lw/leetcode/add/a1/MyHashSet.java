package com.lw.leetcode.add.a1;

/**
 * create by idea
 * arr
 * 705. 设计哈希集合
 *
 * @author lmx
 * @version 1.0
 * @date 2022/1/22 14:25
 */
public class MyHashSet {
    boolean[] map = new boolean[1000005];

    public MyHashSet() {

    }

    public void add(int key) {
        map[key] = true;
    }

    public void remove(int key) {
        map[key] = false;
    }

    public boolean contains(int key) {
        return map[key] == true;
    }
}
