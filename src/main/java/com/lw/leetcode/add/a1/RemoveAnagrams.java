package com.lw.leetcode.add.a1;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 5234. 移除字母异位词后的结果数组
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/15 20:58
 */
public class RemoveAnagrams {

    public List<String> removeAnagrams(String[] words) {
        int n = words.length;
        int[] pre = new int[26];
        List<String> ans = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int[] cnt = new int[26];
            for (char c : words[i].toCharArray()) {
                ++cnt[c - 'a'];
            }
            if (!isSame(pre, cnt)) {
                ans.add(words[i]);
                System.arraycopy(cnt, 0, pre, 0, 26);
            }
        }
        return ans;
    }

    private boolean isSame(int[] pre, int[] cnt) {
        for (int i = 0; i < 26; i++) {
            if (pre[i] != cnt[i]) {
                return false;
            }
        }
        return true;
    }

}
