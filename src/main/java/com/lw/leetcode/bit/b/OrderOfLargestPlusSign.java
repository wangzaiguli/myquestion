package com.lw.leetcode.bit.b;

/**
 * Created with IntelliJ IDEA.
 * 764. 最大加号标志
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/20 13:16
 */
public class OrderOfLargestPlusSign {

    public static void main(String[] args) {
        OrderOfLargestPlusSign test = new OrderOfLargestPlusSign();

        // 2
        int n = 5;
        int[][] arr = {{4, 2}};

        int i = test.orderOfLargestPlusSign(n, arr);
        System.out.println(i);
    }

    public int orderOfLargestPlusSign(int n, int[][] mines) {
        long[][] arr = new long[n][n];
        long item = (1L << 36) + (1L << 24) + (1L << 12) + 1;
        long item2 = 0XFFF;
        long item3 = 0XFFF000;
        long item4 = 0XFFF000000L;
        long item5 = 0XFFF000000000L;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = item;
            }
        }
        for (int[] mine : mines) {
            arr[mine[0]][mine[1]] = 0;
        }
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < n; j++) {
                if (arr[i][j] != 0) {
                    arr[i][j] += (arr[i][j - 1] & item5);
                }
                if (arr[j][i] != 0) {
                    arr[j][i] += (arr[j - 1][i] & item4);
                }
            }
        }
        for (int i = n - 1; i >= 0; i--) {
            for (int j = n - 2; j >= 0; j--) {
                if (arr[i][j] != 0) {
                    arr[i][j] += (arr[i][j + 1] & item3);
                }
                if (arr[j][i] != 0) {
                    arr[j][i] += (arr[j + 1][i] & item2);
                }
            }
        }
        long max = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (arr[i][j] == 0) {
                    continue;
                }
                long v = arr[i][j];
                long x1 = v >> 36;
                long y1 = ((v >> 24) & item2);
                long x2 = ((v >> 12) & item2);
                long y2 = (v & item2);
//                System.out.println(i + " " + j + " " + x1 + " " + y1 + " " + x2 + " " + y2);
                max = Math.max(max, Math.min(Math.min(x1, y1), Math.min(x2, y2)));
            }
        }
        return (int) max;
    }

}
