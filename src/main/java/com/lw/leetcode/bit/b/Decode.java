package com.lw.leetcode.bit.b;

/**
 * 1734. 解码异或后的排列
 *
 * @Author liw
 * @Date 2021/5/11 10:12
 * @Version 1.0
 */
public class Decode {
    public int[] decode(int[] encoded) {
        int length = encoded.length;
        int l = length + 1;
        int a = 0;
        for (int i = 1; i <= l; i++) {
            a ^= i;
        }
        int b = 0;
        for (int i = 1; i < length; i = i + 2) {
            b ^= encoded[i];
        }
        int f = a ^ b;

        int[] arr = new int[l];
        arr[0] = f;
        for (int i = 0; i < length; i++) {
            arr[i + 1] = arr[i] ^ encoded[i];
        }
        return arr;
    }
}
