package com.lw.leetcode.bit.b;

/**
 * 1310. 子数组异或查询
 *
 * @Author liw
 * @Date 2021/5/12 9:14
 * @Version 1.0
 */
public class XorQueries {
    public int[] xorQueries(int[] arr, int[][] queries) {
        int length = arr.length;
        int[] xorArr = new int[length + 1];
        int item = 0;
        for (int i = 0; i < length; i++) {
            item ^= arr[i];
            xorArr[i + 1] = item;
        }
        length = queries.length;
        int[] values = new int[length];
        for (int i = 0; i < length; i++) {
            int[] query = queries[i];
            values[i] = xorArr[query[0]] ^ xorArr[query[1] + 1];
        }
        return values;
    }
}
