package com.lw.leetcode.bit.b;

/**
 * 137. 只出现一次的数字 II
 * 剑指 Offer II 004. 只出现一次的数字
 *
 * @Author liw
 * @Date 2021/4/29 14:28
 * @Version 1.0
 */
public class SingleNumber137 {


    // [-2,-2,1,1,4,1,4,4,-4,-2]
    public static void main(String[] args) {
        int[] nums = {-2, -2, 1, 1, 4, 1, 4, 4, -4, -2};
        int i = new SingleNumber137().singleNumber(nums);
        System.out.println(i);
    }

    public int singleNumber(int[] nums) {
        int sum;
        int v = 0;
        for (int i = 31; i >= 0; i--) {
            sum = 0;
            for (int num : nums) {
                int value = num >>> i;
                if ((value & 1) != 0) {
                    sum++;
                }
            }
            v = v << 1;
            if (sum % 3 != 0) {
                v = v + 1;
            }
        }
        return v;
    }

}
