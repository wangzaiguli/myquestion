package com.lw.leetcode.bit.b;


/**
 * bit
 * 1442. 形成两个异或相等数组的三元组数目
 *
 * @Author liw
 * @Date 2021/5/18 9:01
 * @Version 1.0
 */
public class CountTriplets {

    public static void main(String[] args) {
        CountTriplets test = new CountTriplets();

        // 4
//        int[] arr = {2,3,1,6,7};

        // 10
        int[] arr = {1, 1, 1, 1, 1};

        int i = test.countTriplets(arr);
        System.out.println(i);
    }

    public int countTriplets(int[] arr) {
        int length = arr.length;
        int[] items = new int[length + 1];
        int count = 0;
        for (int i = 0; i < length; i++) {
            int v = items[i] ^ arr[i];
            items[i + 1] = v;
            for (int j = 0; j < i; j++) {
                if (items[j] == v) {
                    for (int k = j + 2; k <= i + 1; k++) {
                        if ((items[k - 1] ^ items[j]) == (items[i + 1] ^ items[k - 1])) {
                            count++;
                        }
                    }
                }
            }
        }
        return count;
    }

}
