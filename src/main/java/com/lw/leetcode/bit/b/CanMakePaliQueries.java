package com.lw.leetcode.bit.b;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 1177. 构建回文串检测
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/21 9:37
 */
public class CanMakePaliQueries {

    public static void main(String[] args) {
        CanMakePaliQueries test = new CanMakePaliQueries();


        // [true,false,false,true,true]
        String s = "abcda";
        int[][] queries = {{3, 3, 0}, {1, 2, 0}, {0, 3, 1}, {0, 3, 2}, {0, 4, 1}};

        List<Boolean> list = test.canMakePaliQueries(s, queries);
        System.out.println(list);

    }

    public List<Boolean> canMakePaliQueries(String s, int[][] queries) {
        int length = s.length();
        int[] arr = new int[length + 1];
        for (int i = 0; i < length; i++) {
            arr[i + 1] = arr[i] ^ (1 << (s.charAt(i) - 'a'));
        }
        List<Boolean> list = new ArrayList<>(queries.length);
        for (int[] query : queries) {
            int v = arr[query[1] + 1] ^ arr[query[0]];
            int c = 0;
            while (v != 0) {
                c++;
                v = v & (v - 1);
            }
            list.add(c <= (query[2] << 1) + 1);
        }
        return list;
    }

}
