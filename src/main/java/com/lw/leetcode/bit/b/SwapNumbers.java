package com.lw.leetcode.bit.b;

import java.util.Arrays;

/**
 * create by idea
 * 面试题 16.01. 交换数字
 *
 * @author lmx
 * @version 1.0
 * @date 2022/1/3 16:35
 */
public class SwapNumbers {

    public static void main(String[] args) {
        SwapNumbers test = new SwapNumbers();

        int[] arr = {1, 9};

        int[] ints = test.swapNumbers(arr);

        System.out.println(Arrays.toString(ints));
    }

    public int[] swapNumbers(int[] numbers) {
        numbers[0] = numbers[0] ^ numbers[1];
        numbers[1] = numbers[0] ^ numbers[1];
        numbers[0] = numbers[0] ^ numbers[1];
        return numbers;
    }

}
