package com.lw.leetcode.bit.b;


/**
 * Created with IntelliJ IDEA.
 * b
 * bit
 * 2568. 最小无法得到的或值
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/20 9:16
 */
public class MinImpossibleOR {

    public int minImpossibleOR(int[] nums) {
        int[] arr = new int[33];
        for (int num : nums) {
            if (Integer.bitCount(num) == 1) {
                arr[Integer.bitCount(num - 1)] = 1;
            }
        }
        int i = 0;
        while (arr[i] != 0) {
            i++;
        }
        return 1 << i;
    }

}
