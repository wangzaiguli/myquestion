package com.lw.leetcode.bit.b;

/**
 * Created with IntelliJ IDEA.
 * 6065. 按位与结果大于零的最长组合
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/15 19:28
 */
public class LargestCombination {

    public int largestCombination(int[] candidates) {
        int[] arr = new int[32];
        for (int candidate : candidates) {
            int i = 0;
            while (candidate != 0) {
                arr[i++] += (candidate & 1);
                candidate >>= 1;
            }
        }
        int max = 0;
        for (int i : arr) {
            max = Math.max(max, i);
        }
        return max;
    }

}
