package com.lw.leetcode.bit.b;

import com.lw.test.util.Utils;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 2401. 最长优雅子数组
 *
 * @author liw
 * @version 1.0
 * @date 2022/9/4 14:47
 */
public class LongestNiceSubarray {

    public static void main(String[] args) {
        LongestNiceSubarray test = new LongestNiceSubarray();

        int[] arr = Utils.getArr(100000, 1, 100000000);

        int i = test.longestNiceSubarray(arr);
        System.out.println(i);
    }

    public int longestNiceSubarray(int[] nums) {
        int length = nums.length;
        int count = 1;
        int last = 1;
        for (int i = 1; i < length; i++) {
            int c = 1;
            int n = nums[i];
            int end = i - last;
            for (int j = i - 1; j >= end; j--) {
                if ((n & nums[j]) == 0) {
                    c++;
                } else {
                    break;
                }
            }
            count = Math.max(count, c);
            last = c;
        }
        return count;
    }


}
