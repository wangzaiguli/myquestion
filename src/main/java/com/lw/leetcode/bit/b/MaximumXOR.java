package com.lw.leetcode.bit.b;

import com.lw.test.util.Utils;

/**
 * Created with IntelliJ IDEA.
 * 6105. 操作后的最大异或和
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/26 19:17
 */
public class MaximumXOR {

    public static void main(String[] args) {
        MaximumXOR test = new MaximumXOR();

        // 7
//        int[] arr = {3, 2, 4, 6};

        // 11
//        int[] arr = {1, 2, 3, 9, 2};

        int[] arr = Utils.getArr(1000, 0, 100000000);


        int i = test.maximumXOR(arr);
        System.out.println(i);
    }

    public int maximumXOR(int[] nums) {
        int item = 0;
        for (int num : nums) {
            item |= num;
        }
        return item;
    }

}
