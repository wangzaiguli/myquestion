package com.lw.leetcode.bit.b;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * bit
 * 2135. 统计追加字母可以获得的单词数
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/6 17:49
 */
public class WordCount {

    public static void main(String[] args) {
        WordCount test = new WordCount();

        // 2
        String[] arr = {"g", "vf", "ylpuk", "nyf", "gdj", "j", "fyqzg", "sizec"};
        String[] strs = {"r", "am", "jg", "umhjo", "fov", "lujy", "b", "uz", "y"};

        int i = test.wordCount(arr, strs);
        System.out.println(i);
    }

    public int wordCount(String[] startWords, String[] targetWords) {
        Set<Integer> set = new HashSet<>();
        for (String startWord : startWords) {
            set.add(find(startWord));
        }
        int count = 0;
        for (String targetWord : targetWords) {
            int v = find(targetWord);
            for (int i = 0; i < 26; i++) {
                int t = (1 << i);
                if ((v & t) != 0 && set.contains((v ^ t))) {
                    count++;
                    break;
                }
            }
        }
        return count;
    }

    private int find(String str) {
        int v = 0;
        for (int i = str.length() - 1; i >= 0; i--) {
            v += (1 << (str.charAt(i) - 'a'));
        }
        return v;
    }

}
