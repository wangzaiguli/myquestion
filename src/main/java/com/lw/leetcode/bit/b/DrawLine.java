package com.lw.leetcode.bit.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 面试题 05.08. 绘制直线
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/13 13:58
 */
public class DrawLine {

    public static void main(String[] args) {
        DrawLine test = new DrawLine();

        int length = 3;
        int w = 0;
        int x1 = 0;
        int x2 = 1;
        int y = 0;
        int[] ints = test.drawLine(length, w, x1, x2, y);
        System.out.println(Arrays.toString(ints));
    }

    public int[] drawLine(int length, int w, int x1, int x2, int y) {
        int[] arr = new int[length];
        int st = w * y + x1;
        int end = w * y + x2;
        int c = st >> 5;
        int d = end >> 5;
        if (c == d) {
           arr[c] = (int) (((1L << (x2 - x1 + 1)) - 1) << (31 - end % 32));
           return arr;
        }
        for (int i = c + 1; i < d; i++) {
            arr[i] = -1;
        }
        int c1 = 32 - st % 32;
        int end1 = end % 32 + 1;
        arr[c] = (int) ((1L << c1) - 1);
        arr[d] = (int) ((1L << 32) - (1L << (32 - end1)));
        return arr;
    }

}
