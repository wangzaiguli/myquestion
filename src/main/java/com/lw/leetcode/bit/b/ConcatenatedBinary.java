package com.lw.leetcode.bit.b;

/**
 * Created with IntelliJ IDEA.
 * 1680. 连接连续二进制数字
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/28 17:24
 */
public class ConcatenatedBinary {

    public static void main(String[] args) {
        ConcatenatedBinary test = new ConcatenatedBinary();


        // 27
//        int n = 3;

        // 27
//        int n = 4;

        // 14126
//        int n = 6;

        // 832506197
//        int n = 10000000;

        // 505379714
        int n = 12;

        // 1
//        int n = 1;

        int i = test.concatenatedBinary(n);
        System.out.println(i);
    }

    public int concatenatedBinary(int n) {
        long item = 1L;
        int b = 2;
        int c = 0;
        int end = 2;
        for (int i = 2; i <= n; i++) {
            if (c >= end) {
                c = 0;
                b++;
                end <<= 1;
            }
            c++;
            item = ((item << b) + i) % 1000000007;
        }
        return (int) item;
    }
}
