package com.lw.leetcode.bit.b;

/**
 * Created with IntelliJ IDEA.
 * 2425. 所有数对的异或和
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/8 9:59
 */
public class XorAllNums {
    public int xorAllNums(int[] nums1, int[] nums2) {
        int a = nums1.length;
        int b = nums2.length;
        boolean a1 = (a & 1) == 0;
        boolean b1 = (b & 1) == 0;
        if (a1 && b1) {
            return 0;
        }
        int item = 0;
        if (a1) {
            for (int i : nums1) {
                item ^= i;
            }
            return item;
        }
        if (b1) {
            for (int i : nums2) {
                item ^= i;
            }
            return item;
        }

        for (int i : nums1) {
            item ^= i;
        }

        for (int i : nums2) {
            item ^= i;
        }
        return item;
    }


}
