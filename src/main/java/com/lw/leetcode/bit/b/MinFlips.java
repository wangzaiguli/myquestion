package com.lw.leetcode.bit.b;

/**
 * Created with IntelliJ IDEA.
 * bit
 * 1318. 或运算的最小翻转次数
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/15 21:03
 */
public class MinFlips {

    public static void main(String[] args) {
        MinFlips test = new MinFlips();

        // 3
        int a = 2;
        int b = 6;
        int c = 5;

        int i = test.minFlips(a, b, c);
        System.out.println(i);
    }

    public int minFlips(int a, int b, int c) {
        int count = 0;
        while (a != 0 || b != 0 || c != 0) {
            if ((c & 1) == 0) {
                count += ((b & 1) + (a & 1));
            } else {
                count += ((b & 1) == 0 && (a & 1) == 0 ? 1 : 0);
            }
            a >>= 1;
            b >>= 1;
            c >>= 1;
        }
        return count;
    }

}
