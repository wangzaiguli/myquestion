package com.lw.leetcode.bit.b;

/**
 * Created with IntelliJ IDEA.
 * 1780. 判断一个数字是否可以表示成三的幂的和
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/27 21:32
 */
public class CheckPowersOfThree {

    public boolean checkPowersOfThree(int n) {
        while (n != 1) {
            if(n % 3 == 2) {
                return false;
            }
            n /= 3;
        }
        return true;
    }


}
