package com.lw.leetcode.bit.b;

/**
 * 1738. 找出第 K 大的异或坐标值
 *
 * @Author liw
 * @Date 2021/5/19 10:43
 * @Version 1.0
 */
public class KthLargestValue {

    public int kthLargestValue(int[][] matrix, int k) {
        int[] arr = new int[k];
        int a = matrix.length;
        int b = matrix[0].length;
        // 为了降低空间复杂度，构建一维数组，选取a,b中小的构建。
        if (a > b) {
            int[] aa = new int[b + 1];
            for (int i = 0; i < a; i++) {
                int item = 0;
                for (int j = 0; j < b; j++) {
                    int v = aa[j] ^ aa[j + 1] ^ matrix[i][j] ^ item;
                    item = aa[j + 1];
                    aa[j + 1] = v;
                    check(arr, v);
                }
            }
        } else {
            int[] aa = new int[a + 1];
            for (int i = 0; i < b; i++) {
                int item = 0;
                for (int j = 0; j < a; j++) {
                    int v = aa[j] ^ aa[j + 1] ^ matrix[j][i] ^ item;
                    item = aa[j + 1];
                    aa[j + 1] = v;
                    check(arr, v);
                }
            }
        }
        return arr[0];
    }

    /**
     * 判断是否插入最小堆中
     * @param arr
     * @param value
     */
    private void check(int[] arr, int value) {
        if (arr[0] < value) {
            arr[0] = value;
            sort(arr, 0, arr.length - 1);
        }
    }

    /**
     * 构建最小堆
     * @param arr
     * @param st
     * @param end
     */
    private void sort(int[] arr, int st, int end) {
        int l = (st << 1) + 1;
        if (l > end) {
            return;
        }
        l = l + 1 <= end ? (arr[l] < arr[l + 1] ? l : l + 1) : l;
        if (arr[l] < arr[st]) {
            int item = arr[l];
            arr[l] = arr[st];
            arr[st] = item;
            sort(arr, l, end);
        }
    }
}
