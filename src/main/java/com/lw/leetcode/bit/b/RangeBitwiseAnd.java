package com.lw.leetcode.bit.b;

/**
 * Created with IntelliJ IDEA.
 * 201. 数字范围按位与
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/16 10:08
 */
public class RangeBitwiseAnd {

    public int rangeBitwiseAnd(int m, int n) {
        int offset = 0;
        for (; m != n; ++offset) {
            m >>= 1;
            n >>= 1;
        }
        return n << offset;
    }

}
