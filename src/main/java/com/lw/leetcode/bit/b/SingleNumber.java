package com.lw.leetcode.bit.b;

/**
 * Created with IntelliJ IDEA.
 * 剑指 Offer 56 - II. 数组中数字出现的次数 II
 * 剑指 Offer II 004. 只出现一次的数字
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/24 16:57
 */
public class SingleNumber {

    public static void main(String[] args) {
        SingleNumber test = new SingleNumber();
        int[] arr = {1, 1, 1, 2, 2, 2, 5};
        int i = test.singleNumber(arr);
        System.out.println(i);
    }

    public int singleNumber2(int[] nums) {
        int ones = 0;
        int twos = 0;
        for (int num : nums) {
            ones = ones ^ num & ~twos;
            twos = twos ^ num & ~ones;
        }
        return ones;
    }

    public int singleNumber(int[] nums) {
        int[] arr = new int[32];
        for (int num : nums) {
            int i = 0;
            while (num != 0) {
                arr[i++] += (num & 1);
                num >>= 1;
            }
        }
        int value = 0;
        for (int i = 31; i >= 0; i--) {
            value <<= 1;
            value += arr[i] % 3;
        }
        return value;
    }

}
