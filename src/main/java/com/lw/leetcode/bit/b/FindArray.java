package com.lw.leetcode.bit.b;

import com.lw.test.util.Utils;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2433. 找出前缀异或的原始数组
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/10 15:42
 */
public class FindArray {

    public static void main(String[] args) {
        FindArray test = new FindArray();

        //
        int[] arr = Utils.getArr(10000, 0, 1000000);

        int[] array = test.findArray(arr);
        System.out.println(Arrays.toString(array));
    }

    public int[] findArray(int[] pref) {
        int length = pref.length;
        int item = pref[0];
        int t;
        for (int i = 1; i < length; i++) {
            t = pref[i];
            pref[i] ^= item;
            item = t;
        }
        return pref;
    }

}
