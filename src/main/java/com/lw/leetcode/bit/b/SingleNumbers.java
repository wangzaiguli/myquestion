package com.lw.leetcode.bit.b;

/**
 * 剑指 Offer 56 - I. 数组中数字出现的次数
 *
 * @Author liw
 * @Date 2021/6/21 15:05
 * @Version 1.0
 */
public class SingleNumbers {
    public int[] singleNumbers(int[] nums) {

        int item = 0;
        for (int num : nums) {
            item ^= num;
        }
        int n = 1;
        while ((item & n) == 0) {
            n <<= 1;
        }
        int a = 0;
        int b = 0;
        for (int num : nums) {
            if ((num & n) != 0) {
                a ^= num;
            } else {
                b ^= num;
            }
        }
        return new int[]{a, b};
    }
}
