package com.lw.leetcode.bit.b;

/**
 * 2429. 最小 XOR
 */
public class MinimizeXor {

    public static void main(String[] args) {

        MinimizeXor test = new MinimizeXor();

        //
//        int a = 15;
//        int b = 4;

        // 3
        int a = 1;
        int b = 12;

        int v = test.minimizeXor(a, b);
        System.out.println(v);
    }

    public int minimizeXor(int num1, int num2) {
        int a = Integer.bitCount(num1);
        int b = Integer.bitCount(num2);
        if (a == b) {
            return num1;
        }
        if (b < a) {
            int v = 0;
            int t = 1 << 30;
            while (b > 0) {
                if ((t & num1) == t) {
                    v |= t;
                    b--;
                }
                t >>= 1;
            }
            return v;
        }
        int n = b - a;
        int v = num1;
        int c = 0;
        while (n > 0) {
            int t = 1 << c;
            if ((num1 & t) == 0) {
                v += t ;
                n--;
            }
            c++;
        }
        return v;
    }


}
