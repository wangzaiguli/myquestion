package com.lw.leetcode.bit.b;

/**
 * Created with IntelliJ IDEA.
 * 2396. 严格回文的数字
 *
 * @author liw
 * @version 1.0
 * @date 2022/9/5 13:47
 */
public class IsStrictlyPalindromic {

    public static void main(String[] args) {
        IsStrictlyPalindromic test = new IsStrictlyPalindromic();

        int n = 9;


        for (int i = 4; i <= 100000; i++) {
            if (test.isStrictlyPalindromic(i)) {

                System.out.println(i);
            }
        }

        boolean strictlyPalindromic = test.isStrictlyPalindromic(n);
        System.out.println(strictlyPalindromic);
    }

    public boolean isStrictlyPalindromic(int n) {
        int[] arr = new int[32];
        for (int i = 2; i <= n - 2; i++) {
            if (!find(n, i, arr)) {
                return false;
            }
        }
        return true;
    }

    private boolean find (int n, int c, int[] arr) {
        int i = 0;
        while (n != 0) {
            arr[i++] = n % c;
            n = n / c;
        }
        int t = (i - 1) / 2;
        while (t >= 0) {
            int e = i - 1 - t;
            if (arr[e] != arr[t]) {
                return false;
            }
            t--;
        }
        return true;
    }


}
