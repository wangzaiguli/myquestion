package com.lw.leetcode.bit.b;

/**
 * Created with IntelliJ IDEA.
 * 2527. 查询数组 Xor 美丽值
 * b
 * bit
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/8 16:44
 */
public class XorBeauty {

    public int xorBeauty(int[] nums) {
        int item = 0;
        for (int num : nums) {
            item ^= num;
        }
        return item;
    }

}
