package com.lw.leetcode.bit.b;

/**
 * 318. 最大单词长度乘积
 * 剑指 Offer II 005. 单词长度的最大乘积
 *
 * @Author liw
 * @Date 2021/6/25 16:52
 * @Version 1.0
 */
public class MaxProduct {
    public int maxProduct(String[] words) {
        int max = 0;
        int length = words.length;
        int[] arr = new int[length];
        for (int i = 0; i < length; i++) {
            int value = 0;
            byte[] bytes = words[i].getBytes();
            for (byte b : bytes) {
                value |= (1 << (b - 97));
            }
            arr[i] = value;
        }

        for (int i = 0; i < length; i++) {
            int la = words[i].length();
            for (int j = i + 1; j < length; j++) {
                int a = arr[i];
                int b = arr[j];
                if ((a & b) == 0) {
                    max = Math.max(max, la * words[j].length());
                }
            }
        }
        return max;
    }
}
