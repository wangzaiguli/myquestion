package com.lw.leetcode.bit;

/**
 * @Author liw
 * @Date 2021/5/8 17:13
 * @Version 1.0
 */
public class ReverseBits {

    public static void main(String[] args) {
        ReverseBits test = new ReverseBits();
        int i = test.reverseBits(-26);
        System.out.println(i);
    }

    public int reverseBits(int num) {
        int[] res = new int[32];
        int index = 0;
        int max = 0;
        while (num != 0) {
            if (num % 2 == 0) {
                index++;
            } else {
                res[index]++;
            }
            num = num >>> 1;
        }
        for (int i = 0; i <= index; i++) {
            max = Math.max(max, res[i] + res[i + 1] + 1);
        }
        return max > 32 ? 32 : max;
    }
}
