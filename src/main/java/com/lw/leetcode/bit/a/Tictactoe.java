package com.lw.leetcode.bit.a;

/**
 * 1275. 找出井字棋的获胜者
 *
 * @Author liw
 * @Date 2021/6/16 21:47
 * @Version 1.0
 */
public class Tictactoe {

    public static void main(String[] args){
        Tictactoe test = new Tictactoe();

        // [[1,2],[2,1],[1,0],[0,0],[0,1],[2,0],[1,1]]
    }

    public String tictactoe(int[][] moves) {
        int length = moves.length;
        int a = 0;
        int b = 0;
        for (int i = 0; i < length; i = i + 2) {
            int[] arr = moves[i];
            a = a | (1 << (arr[0] * 3 + arr[1]));
        }
        // 292, 73 , 146 , 448 , 56 , 7 , 273, 84
        if (292 == (292 & a) || 73 == (73 & a) || 146 == (146 & a) || 448 == (448 & a)
                || 56 == (56 & a) || 7 == (7 & a) || 273 == (273 & a) || 84 == (84 & a)) {
            return "A";
        }
        for (int i = 1; i < length; i = i + 2) {
            int[] arr = moves[i];
            b = b | (1 << (arr[0] * 3 + arr[1]));
        }
        if (292 == (292 & b) || 73 == (73 & b) || 146 == (146 & b) || 448 == (448 & b)
                || 56 == (56 & b) || 7 == (7 & b) || 273 == (273 & b) || 84 == (84 & b)) {
            return "B";
        }
        return length == 9 ? "Draw" : "Pending";
    }
}
