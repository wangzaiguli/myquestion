package com.lw.leetcode.bit.a;

/**
 * bit
 * a
 * 面试题 16.07. 最大数值
 *
 * @Author liw
 * @Date 2021/11/6 22:23
 * @Version 1.0
 */
public class Maximum {
    public int maximum(int a, int b) {
        int k = (int) (((long) a - (long) b) >>> 63 & 1);
        return a * (1 - k) + b * k;
    }
}
