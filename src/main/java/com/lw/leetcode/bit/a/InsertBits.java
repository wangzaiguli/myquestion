package com.lw.leetcode.bit.a;

/**
 * Created with IntelliJ IDEA.
 * 面试题 05.01. 插入
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/8 9:28
 */
public class InsertBits {

    public static void main(String[] args) {
        InsertBits test = new InsertBits();

        // 2032243561
        //10
        //24
        //29

//        System.out.println(Integer.toBinaryString(2032243561));
//        System.out.println(Integer.toBinaryString(10));
//        System.out.println(Integer.toBinaryString(1780585321));
//        System.out.println(Integer.toBinaryString(1243714409));
//        System.out.println(Integer.toBinaryString( (2032243561 >> 29 << 29)));


        // 1143207437
        //1017033
        //11
        //31

        System.out.println(Integer.toBinaryString(1143207437));
        System.out.println(Integer.toBinaryString(1017033));
        System.out.println(Integer.toBinaryString(2082995725));
        System.out.println(Integer.toBinaryString(2082885133));
        System.out.println(Integer.toBinaryString((1143207437 >> 32 << 32)));

    }

    public int insertBits(int n, int m, int i, int j) {
        return (j == 31 ? 0 : (n >> (j + 1) << (j + 1))) | (m << i) | (((1 << i) - 1) & n);
    }

}
