package com.lw.leetcode.bit.a;

/**
 * 342. 4的幂
 *
 * @Author liw
 * @Date 2021/5/31 10:13
 * @Version 1.0
 */
public class IsPowerOfFour {
    public boolean isPowerOfFour(int n) {
        if (n < 1) {
            return false;
        }
        if (n == 1) {
            return true;
        }
        if ((n & (n - 1)) != 0) {
            return false;
        }
        for (int i = 0; i < 16; i++) {
            n = n >> 2;
            if (n == 1) {
                return true;
            }
        }
        return false;
    }

    public static String disemvowel(String str) {
        if (str == null) {
            return null;
        }
        int length = str.length();
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            char c = str.charAt(i);
            if (c == 'a' || c == 'A' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'E' || c == 'I' || c == 'O' || c == 'U') {
                continue;
            }
            sb.append(c);
        }
        return sb.toString();
    }

}
