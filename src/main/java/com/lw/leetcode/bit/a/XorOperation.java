package com.lw.leetcode.bit.a;

/**
 * 1486. 数组异或操作
 *
 * @Author liw
 * @Date 2021/5/7 9:31
 * @Version 1.0
 */
public class XorOperation {
    public int xorOperation(int n, int start) {
        int value = start;
        for (int i = 1; i < n; i++) {
            value = value ^ ((i << 1) + start);
        }
        return value;
    }
}
