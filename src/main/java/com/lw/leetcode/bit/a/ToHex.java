package com.lw.leetcode.bit.a;

/**
 * 405. 数字转换为十六进制数
 *
 * @Author liw
 * @Date 2021/5/8 14:28
 * @Version 1.0
 */
public class ToHex {
    public String toHex(int num) {
        if (num == 0) {
            return "0";
        }
        String str = "0123456789abcdef";
        StringBuilder value = new StringBuilder();
        while (num != 0) {
            value.insert(0, str.charAt(num & 15));
            num = num >>> 4;
        }
        return value.toString();
    }
}
