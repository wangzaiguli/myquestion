package com.lw.leetcode.bit.a;

/**
 * 剑指 Offer 15. 二进制中1的个数
 * 191. 位1的个数
 *
 * @Author liw
 * @Date 2021/6/23 9:12
 * @Version 1.0
 */
public class HammingWeight {
    public int hammingWeight(int n) {
        int count = 0;
        while (n != 0) {
            n = n & (n - 1);
            count++;
        }
        return count;
    }
}
