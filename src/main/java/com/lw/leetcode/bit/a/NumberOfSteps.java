package com.lw.leetcode.bit.a;

/**
 * Created with IntelliJ IDEA.
 * a
 * bit
 * 1342. 将数字变成 0 的操作次数
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/18 15:03
 */
public class NumberOfSteps {

    public int numberOfSteps(int num) {
        if (num == 0) {
            return 0;
        }
        int count = 0;
        while (num != 1) {
            count += (num & 1);
            count++;
            num >>= 1;
        }
        return count + 1;
    }

}
