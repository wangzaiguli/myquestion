package com.lw.leetcode.bit.a;

/**
 * Created with IntelliJ IDEA.
 * a
 * bit
 * 1832. 判断句子是否为全字母句
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/18 15:16
 */
public class CheckIfPangram {
    public boolean checkIfPangram(String sentence) {
        int res = 0;
        for (char c : sentence.toCharArray()) {
            res |= 1 << (c - 'a');
            if ((res ^ 0x3ffffff) == 0) {
                return true;
            }
        }
        return false;
    }

}
