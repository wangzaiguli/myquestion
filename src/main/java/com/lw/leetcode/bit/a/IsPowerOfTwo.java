package com.lw.leetcode.bit.a;

/**
 * 231. 2 的幂
 *
 * @Author liw
 * @Date 2021/5/31 10:15
 * @Version 1.0
 */
public class IsPowerOfTwo {
    public boolean isPowerOfTwo(int n) {
        if (n < 1) {
            return false;
        }
        return ((n - 1) & n) == 0;
    }
}
