package com.lw.leetcode.bit.a;

/**
 * Created with IntelliJ IDEA.
 * 292. Nim 游戏
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/18 9:32
 */
public class CanWinNim {
    public boolean canWinNim(int n) {
        return n >> 2 << 2 != n;
    }
}
