package com.lw.leetcode.bit.a;

/**
 * 67. 二进制求和
 * 剑指 Offer II 002. 二进制加法
 *
 * @Author liw
 * @Date 2021/11/7 22:52
 * @Version 1.0
 */
public class AddBinary {
    public String addBinary(String a, String b) {
        int al = a.length();
        int bl = b.length();
        int max = al > bl ? al : bl;
        int[] jw = new int[max];
        byte[] ab = a.getBytes();
        byte[] bb = b.getBytes();
        int av;
        int bv;
        byte j = 0;
        int sum;
        for (int i = 0; i < max; i++) {
            av = 48;
            bv = 48;
            if (i < al) {
                av = ab[al - i - 1];
            }
            if (i < bl) {
                bv = bb[bl - i - 1];
            }
            sum = av + bv + j - 96;
            jw[max - i - 1] = sum & 1;
            j = (byte) (sum / 2);
        }
        StringBuilder sb = new StringBuilder(max + 1);
        if (j == 1) {
            sb.append(1);
        }
        for (int i : jw) {
            sb.append(i);
        }
        return sb.toString();
    }
}
