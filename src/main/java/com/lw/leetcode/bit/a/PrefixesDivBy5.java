package com.lw.leetcode.bit.a;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * bit
 * 1018. 可被 5 整除的二进制前缀
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/27 17:37
 */
public class PrefixesDivBy5 {

    public List<Boolean> prefixesDivBy5(int[] nums) {
        int item = 0;
        List<Boolean> list = new ArrayList<>(nums.length);
        for (int num : nums) {
            item = ((item << 1) + num) % 5;
            list.add(item == 0);
        }
        return list;
    }

}
