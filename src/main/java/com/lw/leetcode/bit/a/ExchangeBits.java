package com.lw.leetcode.bit.a;

/**
 * Created with IntelliJ IDEA.
 * 面试题 05.07. 配对交换
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/8 9:17
 */
public class ExchangeBits {

    public int exchangeBits(int num) {
        return ((num & 0X55555555) << 1) | ((num >> 1) & 0X55555555);
    }

}
