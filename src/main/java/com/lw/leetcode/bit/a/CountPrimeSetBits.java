package com.lw.leetcode.bit.a;

/**
 * create by idea
 * 762. 二进制表示中质数个计算置位
 *
 * @author lmx
 * @version 1.0
 * @date 2022/1/22 13:43
 */
public class CountPrimeSetBits {
    public int countPrimeSetBits(int L, int R) {
        int ans = 0;
        for (int x = L; x <= R; ++x) {
            if (find(Integer.bitCount(x))) {
                ans++;
            }
        }
        return ans;
    }

    private boolean find(int x) {
        return (x == 2 || x == 3 || x == 5 || x == 7 ||
                x == 11 || x == 13 || x == 17 || x == 19);
    }

}
