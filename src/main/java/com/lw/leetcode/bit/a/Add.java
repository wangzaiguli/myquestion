package com.lw.leetcode.bit.a;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * bit
 * 剑指 Offer 65. 不用加减乘除做加法
 * 面试题 17.01. 不用加号的加法
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/29 16:15
 */
public class Add {
    public int add(int a, int b) {
        while (b != 0) {
            int c = (a & b) << 1;
            a ^= b;
            b = c;
        }
        return a;
    }
}
