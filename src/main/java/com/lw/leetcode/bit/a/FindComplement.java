package com.lw.leetcode.bit.a;

/**
 * 476. 数字的补数
 * 1009. 十进制整数的反码
 *
 * @Author liw
 * @Date 2021/5/31 15:02
 * @Version 1.0
 */
public class FindComplement {
    public int findComplement(int num) {
        int temp = num, c = 0;
        while (temp > 0) {
            temp >>= 1;
            c = (c << 1) + 1;
        }
        return num ^ c;
    }
}
