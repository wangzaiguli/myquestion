package com.lw.leetcode.bit.c;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 1178. 猜字谜
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/5 17:29
 */
public class FindNumOfValidWords {

    public static void main(String[] args) {
        FindNumOfValidWords test = new FindNumOfValidWords();

        // [1,1,3,2,4,0]
        String[] arr1 = {"aaaa", "asas", "able", "ability", "actt", "actor", "access"};
        String[] arr2 = {"aboveyz", "abrodyz", "abslute", "absoryz", "actresz", "gaswxyz"};

        List<Integer> numOfValidWords = test.findNumOfValidWords(arr1, arr2);
        System.out.println(numOfValidWords);
    }

    public List<Integer> findNumOfValidWords(String[] words, String[] puzzles) {
        int length = words.length;
        int[] arr = new int[length];
        for (int i = 0; i < length; i++) {
            arr[i] = toInt(words[i]);
        }
        List<Integer> list = new ArrayList<>(puzzles.length);
        for (String puzzle : puzzles) {
            int t = toInt(puzzle);
            int f = 1 << (puzzle.charAt(0) - 'a');
            int c = 0;
            for (int i : arr) {
                if (((i | f) == i) && ((i | t) == t)) {
                    c++;
                }
            }
            list.add(c);
        }
        return list;
    }

    private int toInt(String str) {
        int length = str.length();
        int item = 0;
        for (int i = 0; i < length; i++) {
            item |= (1 << (str.charAt(i) - 'a'));
        }
        return item;
    }

}
