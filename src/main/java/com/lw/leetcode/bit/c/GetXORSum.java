package com.lw.leetcode.bit.c;

/**
 * Created with IntelliJ IDEA.
 * 1835. 所有数对按位与结果的异或和
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/24 11:46
 */
public class GetXORSum {


    public static void main(String[] args) {
        GetXORSum test = new GetXORSum();

        // 0
//        int[] arr1 = {1,2,3};int[] arr2 = {6,5};

        // 4
        int[] arr1 = {12};
        int[] arr2 = {4};

        int xorSum = test.getXORSum(arr1, arr2);
        System.out.println(xorSum);
    }

    public int getXORSum(int[] arr1, int[] arr2) {
        int item = 0;
        for (int i : arr2) {
            item ^= i;
        }
        int v = 0;
        for (int i : arr1) {
            v ^= (i & item);
        }
        return v;
    }

}
