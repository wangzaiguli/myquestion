package com.lw.leetcode.bit.c;

/**
 * 810. 黑板异或游戏
 *
 * @Author liw
 * @Date 2021/5/22 11:36
 * @Version 1.0
 */
public class XorGame {
    public boolean xorGame(int[] nums) {
        int item = 0;
        for (int num : nums) {
            item ^= num;
        }
        return item == 0 || (nums.length & 1) == 0;
    }
}
