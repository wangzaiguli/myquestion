package com.lw.leetcode.other.c;

/**
 * Created with IntelliJ IDEA.
 * 65. 有效数字
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/21 15:57
 */
public class IsNumber {


    public static void main(String[] args) {
        IsNumber test = new IsNumber();

        // true ：".1" "-.1" "1." " 005047e+6""46.e3" ".2e81"
        //false："." "+e" "+3. e04116"

        // true
//        String[] as = {"53.5e93", " 6e-1", "2e10"," -90e3   ",".1", "-.1", "1." ," 005047e+6","46.e3", ".2e81","0", "2", "0089", "-0.1", "+3.14", "4.", "-.9", "2e10", "-90E3", "3e+7", "+6e-1", "53.5e93", "-123.456e789"};

        // false
        String[] as = {".-4", "95a54e53","-+3",  " --6 ", " 99e2.5 ", "e3", " 1e", "1 a",".", "+e", "+3. e04116","e", ".", "abc", "1a", "1e", "e3", "99e2.5", "--6", "-+3", "95a54e53"};

        for (String a : as) {
            boolean number = test.isNumber(a);
            System.out.println(number);
        }
    }

    public boolean isNumber(String s) {
        s = s.trim();
        int e = getE(s);
        if (e == -2) {
            return false;
        }
        if (e == -1) {
            return checkNum(s) || checklow(s);
        }
        String a = s.substring(0, e);
        String b = s.substring(e + 1);
        return checkNum(b) && (checkNum(a) || checklow(a));
    }

    private boolean checklow(String str) {
        if (str.length() == 0) {
            return false;
        }
        char c = str.charAt(0);
        if (c == '+' || c == '-') {
            str = str.substring(1);
        }
        int index = str.indexOf(".");
        if (index == -1) {
            return false;
        }
        int length = str.length();
        if (index == length - 1) {
            String a = str.substring(0, length - 1);
            return checkNum(a);
        }
        if (index == 0) {
            String a = str.substring(1);
            if (a.length() > 0 && (a.charAt(0) == '+' || a.charAt(0) == '-')) {
                return false;
            }
            return checkNum(a);
        }
        String a = str.substring(0, index);
        String b = str.substring(index + 1);
        if (b.length() > 0 && (b.charAt(0) == '+' || b.charAt(0) == '-')) {
            return false;
        }
        return checkNum(a) && checkNum(b);
    }

    private boolean checkNum(String str) {
        if (str.length() == 0) {
            return false;
        }
        char c = str.charAt(0);
        if (c == '+' || c == '-') {
            str = str.substring(1);
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            c = str.charAt(i);
            if (c < '0' || c > '9') {
                return false;
            }
        }
        return true;
    }

    private int getE(String str) {
        int length = str.length();
        int count = 0;
        int index = -1;
        for (int i = 0; i < length; i++) {
            char c = str.charAt(i);
            if (c == 'E' || c == 'e') {
                count++;
                index = i;
            }
        }
        if (count == 0) {
            return -1;
        }
        if (count > 1 || index == 0 || index == length - 1) {
            return -2;
        }
        return index;
    }

}
