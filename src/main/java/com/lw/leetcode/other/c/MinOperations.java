package com.lw.leetcode.other.c;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/18 13:47
 */
public class MinOperations {

    private int gcd(int a, int b) {
        return b != 0 ? gcd(b, a % b) : a;
    }

    public int minOperations(int[] nums, int[] numsDivide) {
        int d = 0;
        for (int num : numsDivide) {
            d = gcd(d, num);
        }

        Arrays.sort(nums);
        int x = 0;
        for (int num : nums) {
            if (d % num == 0) {
                break;
            }
            x++;
        }

        return x == nums.length ? -1 : x;
    }

}
