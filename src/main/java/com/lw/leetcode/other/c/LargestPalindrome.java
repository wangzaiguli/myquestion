package com.lw.leetcode.other.c;

/**
 * Created with IntelliJ IDEA.
 * 479. 最大回文数乘积
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/16 11:16
 */
public class LargestPalindrome {
    public int largestPalindrome(int n) {
        if (n == 1) {
            return 9;
        }
        int end = (int) Math.pow(10, n) - 1;
        int ans = 0;
        for (int i = end; ans == 0; --i) {
            long p = i;
            for (int j = i; j > 0; j /= 10) {
                p = p * 10 + j % 10;
            }
            for (long x = end; x * x >= p; --x) {
                if (p % x == 0) {
                    ans = (int) (p % 1337);
                    break;
                }
            }
        }
        return ans;
    }

}
