package com.lw.leetcode.other.c;

/**
 * Created with IntelliJ IDEA.
 * 780. 到达终点
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/9 20:17
 */
public class ReachingPoints {

    public boolean reachingPoints(int sx, int sy, int tx, int ty) {
        while (tx > sx && ty > sy && tx != ty) {
            if (tx > ty) {
                tx %= ty;
            } else {
                ty %= tx;
            }
        }
        if (tx == sx && ty == sy) {
            return true;
        }
        if (sx == tx) {
            return  ty > sy && (ty - sy) % tx == 0;
        }
        if (ty == sy) {
            return tx > sx && (tx - sx) % ty == 0;
        }
        return false;
    }

}
