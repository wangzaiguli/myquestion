package com.lw.leetcode.other.c;

/**
 * Created with IntelliJ IDEA.
 * 273. 整数转换英文表示
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/11 9:13
 */
public class NumberToWords {

    public static void main(String[] args) {
        NumberToWords test = new NumberToWords();

        //输出："One Hundred Twenty Three"
//        int n = 123;

        //输出："Twelve Thousand Three Hundred Forty Five"
//        int n = 12345;

        // "One Million Two Hundred Thirty Four Thousand Five Hundred Sixty Seven"
//        int n = 1234567;

        // "One Billion Two Hundred Thirty Four Million Five Hundred Sixty Seven Thousand Eight Hundred Ninety One"
//        int n = 1234567891;

//        int n = 15;

        // "One Million Ten"
        int n = 1000010;

        String s = test.numberToWords(n);
        System.out.println(s);
    }

    private static String[] a = {"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten",
            "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"};
    private static String[] b = {"", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};
    private static String[] c = {"Billion", "Million", "Thousand", "",};


    public String numberToWords(int num) {
        if (num == 0) {
            return "Zero";
        }
        int count = 3;
        int n = num;
        long it = 1000L;
        while (n >= it) {
            count--;
            it *= 1000;
        }
        int item = (int) (it / 1000);
        StringBuilder sb = new StringBuilder();
        while (n != 0) {
            int value = n / item;

            int h;

            if (value >= 100) {
                h = value / 100;
                sb.append(a[h]);
                sb.append(" ");
                sb.append("Hundred ");
                value %= 100;
            }
            if (value >= 20) {
                h = value / 10;
                sb.append(b[h]);
                sb.append(" ");
                value %= 10;
            }
            if (value > 0) {
                sb.append(a[value]);
                sb.append(" ");
            }
            if (n / item > 0) {
                sb.append(c[count]);
                sb.append(" ");
            }
            n %= item;
            item /= 1000;
            count++;
        }
        return sb.toString().trim();
    }

}
