package com.lw.leetcode.other.c;

/**
 * Created with IntelliJ IDEA.
 * 1553. 吃掉 N 个橘子的最少天数
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/18 10:22
 */
public class MinDays {

    public static void main(String[] args) {
        MinDays test = new MinDays();

        // 30
        int n = 1900000000;

        // 32
//        int n = 2000000000;

        // 6
//        int n = 56;

        int i = test.minDays(n);
        System.out.println(i);
    }

    private int[] arr = {0, 1, 2, 2, 3, 4, 3, 4, 4, 3};

    public int minDays(int n) {
        if (n < 10) {
            return arr[n];
        }
        return Math.min(minDays(n / 3) + n % 3, minDays(n / 2) + n % 2) + 1;
    }


}
