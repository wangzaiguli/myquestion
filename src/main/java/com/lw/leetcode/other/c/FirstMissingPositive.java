package com.lw.leetcode.other.c;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * 41. 缺失的第一个正数
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/18 12:14
 */
public class FirstMissingPositive {
    public int firstMissingPositive(int[] nums) {
        if (nums == null && nums.length == 0) {
            return 1;
        }
        int i;
        for (i = 0; i < nums.length; i++) {
            while (nums[i] != i + 1 && nums[i] >= 1 && nums[i] <= nums.length && nums[i] != nums[nums[i] - 1]) {
                int temp = nums[i];
                nums[i] = nums[nums[i] - 1];
                nums[temp - 1] = temp;
            }
        }
        for (i = 0; i < nums.length; i++) {
            if (nums[i] != i + 1) {
                return i + 1;
            }
        }
        return i + 1;
    }
}
