package com.lw.leetcode.other.c;

/**
 * Created with IntelliJ IDEA.
 * other
 * 564. 寻找最近的回文数
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/23 11:14
 */
public class NearestPalindromic {

    public static void main(String[] args) {
        NearestPalindromic test = new NearestPalindromic();

        String n = "1000000000000000000";
        String s = test.nearestPalindromic(n);
        System.out.println(s);

    }

    public String nearestPalindromic(String n) {
        long l = Long.parseLong(n);
        long low = getLow(n);
        long up = getUp(n);
        return l - low <= up - l ? String.valueOf(low) : String.valueOf(up);
    }

    private long getLow(String n) {
        long l = Long.parseLong(n);
        if (l < 10) {
            return l - 1;
        }
        if (l < 12) {
            return 9;
        }
        int length = n.length();
        int i = length / 2;
        if ((length & 1) == 0) {
            String str = n.substring(0, i);
            long l1 = Long.parseLong(str);
            long item = l1;
            long c = l1;
            while (item != 0) {
                l1 = l1 * 10 + item % 10;
                item /= 10;
            }
            if (l1 < l) {
                return l1;
            }
            l1 = c;
            l1--;
            if (String.valueOf(l1).length() < i) {
                while (i > 0) {
                    l1 = l1 * 10 + 9;
                    i--;
                }
                return l1;
            }
            item = l1;
            while (item != 0) {
                l1 = l1 * 10 + item % 10;
                item /= 10;
            }
            return l1;
        }
        String str = n.substring(0, i + 1);
        long l1 = Long.parseLong(str);
        long item = l1 / 10;
        long c = l1;
        while (item != 0) {
            l1 = l1 * 10 + item % 10;
            item /= 10;
        }
        if (l1 < l) {
            return l1;
        }
        l1 = c;
        l1--;
        if (String.valueOf(l1).length() < i + 1) {
            while (i > 0) {
                l1 = l1 * 10 + 9;
                i--;
            }
            return l1;
        }
        item = l1 / 10;
        while (item != 0) {
            l1 = l1 * 10 + item % 10;
            item /= 10;
        }
        return l1;
    }

    private long getUp(String n) {
        long l = Long.parseLong(n);
        if (l < 9) {
            return l + 1;
        }
        if (l < 11) {
            return 11;
        }
        int length = n.length();
        int i = length / 2;
        if ((length & 1) == 0) {
            String str = n.substring(0, i);
            long l1 = Long.parseLong(str);
            long item = l1;
            long c = l1;
            while (item != 0) {
                l1 = l1 * 10 + item % 10;
                item /= 10;
            }
            if (l1 > l) {
                return l1;
            }
            l1 = c;
            l1++;
            if (String.valueOf(l1).length() > i) {
                while (i > 0) {
                    l1 = l1 * 10;
                    i--;
                }
                return l1 + 1;
            }
            item = l1;
            while (item != 0) {
                l1 = l1 * 10 + item % 10;
                item /= 10;
            }
            return l1;
        }
        String str = n.substring(0, i + 1);
        long l1 = Long.parseLong(str);
        long item = l1 / 10;
        long c = l1;
        while (item != 0) {
            l1 = l1 * 10 + item % 10;
            item /= 10;
        }
        if (l1 > l) {
            return l1;
        }
        l1 = c;
        l1++;
        if (String.valueOf(l1).length() > i + 1) {
            while (i > 0) {
                l1 = l1 * 10;
                i--;
            }
            return l1 + 1;
        }
        item = l1 / 10;
        while (item != 0) {
            l1 = l1 * 10 + item % 10;
            item /= 10;
        }
        return l1;
    }

}
