package com.lw.leetcode.other.c;

/**
 * Created with IntelliJ IDEA.
 * 1359. 有效的快递序列数目
 * c
 * other
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/4 9:54
 */
public class CountOrders {

    public static void main(String[] args) {
        CountOrders test = new CountOrders();

        // 90
//        int n = 3;

        // 6
        int n = 2;

        int i = test.countOrders(n);
        System.out.println(i);
    }

    public int countOrders(int n) {
        if (n == 1) {
            return 1;
        }
        long count = 2;
        long sum = 1;
        while (n > 1) {
            sum = (((count + 2) * (count + 1) >> 1) * sum) % 1000000007;
            count += 2;
            n--;
        }
        return (int) sum;
    }

}
