package com.lw.leetcode.other.c;

/**
 * 6280. 范围内最接近的两个质数
 */
public class ClosestPrimes {

    public int[] closestPrimes(int left, int right) {
        int item;
        int st;
        if (left == 1) {
            item = 2;
            st = 3;
        } else {
             item =  find(left) ? left : -1;
             st = (left & 1) == 1 ? left + 2 : left + 1;
        }
        int[] arr = {-1, -1};
        int min = Integer.MAX_VALUE;
        for (int i = st; i <= right; i += 2) {
            if (find(i)) {
                if (item != -1) {
                    if (i - item < min) {
                        min = i - item;
                        arr[0] = item;
                        arr[1] = i;
                    }
                }
                item = i;
            }
        }
        return arr;
    }

    private boolean find(int t) {
        int l = (int) Math.pow(t, 0.5);
        for (int i = 2; i <= l; i++) {
            if (t % i == 0) {
                return false;
            }
        }
        return true;
    }

}
