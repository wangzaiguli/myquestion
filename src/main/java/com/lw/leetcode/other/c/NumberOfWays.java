package com.lw.leetcode.other.c;

/**
 * Created with IntelliJ IDEA.
 * 很简单，应该降级
 * 2147. 分隔长廊的方案数
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/7 8:45
 */
public class NumberOfWays {

    public static void main(String[] args) {
        NumberOfWays test = new NumberOfWays();

        // 3
        String str = "SSPPSPS";

        // 1
//        String str = "PPSPSP";

        int i = test.numberOfWays(str);
        System.out.println(i);
    }

    public int numberOfWays(String corridor) {
        char[] chars = corridor.toCharArray();
        int count = 0;
        for (char c : chars) {
            if (c == 'S') {
                count++;
            }
        }
        if ((count & 1) == 1 || count == 0) {
            return 0;
        }
        if (count == 2) {
            return 1;
        }
        int a = find(chars, 0);
        int item = find(chars, a + 1);
        int next = find(chars, item + 1);
        long sum = 1;
        while (next != -1) {
            sum = sum * (next - item) % 1000000007;
            item = find(chars, next + 1);
            next = find(chars, item + 1);
        }
        return (int) sum;
    }

    private int find(char[] chars, int st) {
        int length = chars.length;
        for (int i = st; i < length; i++) {
            if (chars[i] == 'S') {
                return i;
            }
        }
        return -1;
    }

}
