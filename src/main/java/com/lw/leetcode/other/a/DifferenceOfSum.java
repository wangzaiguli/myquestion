package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 6291. 数组元素和与数字和的绝对差
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/15 15:52
 */
public class DifferenceOfSum {

    public int differenceOfSum(int[] nums) {
        int sum1 = 0;
        int sum2 = 0;
        for (int num : nums) {
            sum1 += num;
            sum2 += find(num);
        }
        return Math.abs(sum1 - sum2);
    }

    private int find (int v) {
        int s = 0;
        while (v != 0) {
            s += v % 10;
            v /= 10;
        }
        return s;
    }

}
