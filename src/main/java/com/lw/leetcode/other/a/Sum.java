package com.lw.leetcode.other.a;

/**
 * create by idea
 * 2235. 两整数相加
 *
 * @author lmx
 * @version 1.0
 * @date 2022/7/17 10:48
 */
public class Sum {
    public int sum(int num1, int num2) {
        return num1 + num2;
    }
}
