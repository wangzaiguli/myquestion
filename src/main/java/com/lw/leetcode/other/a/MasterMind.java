package com.lw.leetcode.other.a;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 面试题 16.15. 珠玑妙算
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/7 9:16
 */
public class MasterMind {


    public static void main(String[] args) {
        MasterMind test = new MasterMind();

        // [0, 1]
        String s1 = "RGRB";
        String s2 = "BBBY";

        int[] ints = test.masterMind(s1, s2);
        System.out.println(Arrays.toString(ints));
    }

    public int[] masterMind(String solution, String guess) {
        int a = 0;
        int b = 0;
        Map<Character, Integer> as = new HashMap<>();
        Map<Character, Integer> bs = new HashMap<>();
        for (int i = 0; i < 4; i++) {
            if (solution.charAt(i) == guess.charAt(i)) {
                a++;
            }
            as.merge(solution.charAt(i), 1, (c1, c2) -> c1 + c2);
            bs.merge(guess.charAt(i), 1, (c1, c2) -> c1 + c2);
        }
        for (Map.Entry<Character, Integer> entry : as.entrySet()) {
            int bc = bs.getOrDefault(entry.getKey(), 0);
            int ac = entry.getValue();
            b += Math.min(bc, ac);
        }
        return new int[]{a, b - a};
    }

}
