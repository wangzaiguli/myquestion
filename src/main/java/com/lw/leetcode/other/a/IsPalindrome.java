package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 9. 回文数
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/24 19:38
 */
public class IsPalindrome {
    public boolean isPalindrome(int x) {
        int a = x;
        if (x < 0) {
            return false;
        }
        if (x < 10) {
            return true;
        }
        if (x % 10 == 0) {
            return false;
        }
        int t = 0;
        while (x > 0) {
            t = t * 10 + x % 10;
            x /= 10;
        }
        return t == a;
    }
}
