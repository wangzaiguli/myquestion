package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 1523. 在区间范围内统计奇数数目
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/28 11:07
 */
public class CountOdds {

    public int countOdds(int low, int high) {
        return pre(high) - pre(low - 1);
    }

    public int pre(int x) {
        return (x + 1) >> 1;
    }

}
