package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * a
 * other
 * 1365. 有多少小于当前数字的数字
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/18 15:23
 */
public class SmallerNumbersThanCurrent {
    public int[] smallerNumbersThanCurrent(int[] nums) {
        int n = nums.length;
        int[] ret = new int[n];
        for (int i = 0; i < n; i++) {
            int cnt = 0;
            for (int num : nums) {
                if (num < nums[i]) {
                    cnt++;
                }
            }
            ret[i] = cnt;
        }
        return ret;
    }
}
