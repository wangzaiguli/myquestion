package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 2469. 温度转换
 *
 * @author liw
 * @version 1.0
 * @date 2022/11/14 17:17
 */
public class ConvertTemperature {
    public double[] convertTemperature(double celsius) {
        double[] rs = new double[2];
        rs[0] = celsius + 273.15;
        rs[1] = celsius * 1.80 + 32.00;

        return rs;
    }
}
