package com.lw.leetcode.other.a;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 6112. 装满杯子需要的最短总时长
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/11 10:17
 */
public class FillCups {

    public int fillCups(int[] amount) {
        Arrays.sort(amount);
        int a = amount[0];
        int b = amount[1];
        int c = amount[2];
        if (c >= a + b) {
            return c;
        }
        int t = c - b + a;
        int a1 = Math.min(a, t / 2);
        int b1 = Math.min(b, c - a1);
        return c + Math.max(a - a1, b - b1);
    }

}
