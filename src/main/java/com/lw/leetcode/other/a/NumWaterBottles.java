package com.lw.leetcode.other.a;

/**
 * 1518. 换酒问题
 *
 * @Author liw
 * @Date 2021/6/15 20:39
 * @Version 1.0
 */
public class NumWaterBottles {
    public static void main(String[] args) {
        NumWaterBottles test = new NumWaterBottles();
        int i = test.numWaterBottles(2, 3);
        System.out.println(i);
    }

    public int numWaterBottles(int numBottles, int numExchange) {
        int a = numBottles / (numExchange - 1);
        if (numBottles % (numExchange - 1) == 0) {
            a--;
        }
        return numBottles + a;
    }
}
