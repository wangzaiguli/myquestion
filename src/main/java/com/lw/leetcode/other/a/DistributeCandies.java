package com.lw.leetcode.other.a;

import java.util.HashMap;
import java.util.Map;

/**
 * 575. 分糖果
 *
 * @Author liw。
 * @Date 2021/4/19 13:29。
 * @Version 1.0。
 */
public class DistributeCandies {

    public int distributeCandies(int[] candyType) {
        int length = candyType.length;
        Map<Integer, Integer> map = new HashMap<>(length << 1);
        for (int i : candyType) {
            map.putIfAbsent(i, 1);
        }
        int size = map.size();
        return Math.min(size, (length >> 1));
    }

}
