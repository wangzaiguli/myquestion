package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * a
 * other
 * 2496. 数组中字符串的最大值
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/11 13:31
 */
public class MaximumValue {

    public int maximumValue(String[] strs) {
        int max = 0;
        for (String str : strs) {
            int item = 0;
            for (char c : str.toCharArray()) {
                c -= '0';
                if (c <= 9) {
                    item = item * 10 + c;
                } else {
                    item = str.length();
                    break;
                }
            }
            max = Math.max(max, item);
        }
        return max;
    }

}
