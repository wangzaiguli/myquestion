package com.lw.leetcode.other.a;

import java.util.HashSet;
import java.util.Set;

/**
 * 290. 单词规律
 *
 * @Author liw
 * @Date 2021/4/16 15:54
 * @Version 1.0
 */
public class WordPattern {

    public static void main(String[] args) {
        String a = "abba";
        String b = "dog dd dd dog";
        boolean f = new WordPattern().wordPattern(a, b);
        System.out.println(f);
    }

    public boolean wordPattern(String pattern, String s) {
        if (pattern == null || s == null) {
            return false;
        }
        String[] arr = s.split(" ");
        int length = pattern.length();
        if (arr.length != length) {
            return false;
        }
        String[] strs = new String[26];
        Set<String> set = new HashSet<>();
        for (int i = 0; i < length; i++) {
            char c = pattern.charAt(i);
            String str = strs[c - 97];
            String s1 = arr[i];
            if (str == null) {
                strs[c - 97] = s1;
                int size = set.size();
                set.add(s1);
                if (set.size() == size) {
                    return false;
                }
            } else {
                if (!s1.equals(str)) {
                    return false;
                }
            }
        }
        return true;
    }

}
