package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * other
 * 1925. 统计平方和三元组的数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/3 11:21
 */
public class CountTriples {

    public static void main(String[] args) {
        CountTriples test = new CountTriples();

        int n = 10;

        int i = test.countTriples(n);
        System.out.println(i);
    }

    public int countTriples(int n) {
        int count = 0;
        for (int i = 1; i <= n; i++) {
            for (int j = i; j <= n; j++) {
                int t = i * i + j * j;
                int v = (int) Math.pow(t, 0.5);
                if (v <= n && v * v == t) {
                    count += (i == j ? 1 : 2);
                }
            }
        }
        return count;
    }

}
