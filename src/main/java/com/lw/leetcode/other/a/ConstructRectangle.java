package com.lw.leetcode.other.a;

/**
 * other
 * 492. 构造矩形
 *
 * @Author liw
 * @Date 2021/6/28 11:49
 * @Version 1.0
 */
public class ConstructRectangle {
    public int[] constructRectangle(int area) {
        int width = (int) Math.sqrt(area);
        while (area % width != 0) {
            width--;
        }
        return new int[]{area / width, width};
    }
}
