package com.lw.leetcode.other.a;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * other
 * 1317. 将整数转换为两个无零整数的和
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/19 16:09
 */
public class GetNoZeroIntegers {

    public static void main(String[] args) {
        GetNoZeroIntegers test = new GetNoZeroIntegers();
        for (int i = 2; i < 10000; i++) {
            int[] noZeroIntegers = test.getNoZeroIntegers(111);

            boolean a = String.valueOf(noZeroIntegers[0]).contains("0");
            boolean b = String.valueOf(noZeroIntegers[1]).contains("0");
            if (a || b) {
                System.out.println(i);
                System.out.println(Arrays.toString(noZeroIntegers));
            }
        }
    }

    public int[] getNoZeroIntegers(int n) {
        if (n < 10) {
            return new int[]{1, n - 1};
        }
        char[] chars = String.valueOf(n).toCharArray();
        int item = 0;
        if (chars[0] != '1') {
            item = 1;
        }
        int length = chars.length;
        for (int i = 1; i < length; i++) {
            char c = chars[i];
            if (c < '3' && c > '0') {
                item = item * 10 + 9;
            } else {
                item = item * 10 + 1;
            }
        }
        return new int[]{item, n - item};
    }
}
