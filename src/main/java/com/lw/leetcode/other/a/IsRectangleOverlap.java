package com.lw.leetcode.other.a;

/**
 * other
 * 836. 矩形重叠
 *
 * @Author liw
 * @Date 2021/7/9 11:50
 * @Version 1.0
 */
public class IsRectangleOverlap {
    public boolean isRectangleOverlap(int[] rec1, int[] rec2) {
        return !(rec1[2] <= rec2[0] || rec2[2] <= rec1[0] || rec1[3] <= rec2[1] || rec2[3] <= rec1[1]);
    }
}
