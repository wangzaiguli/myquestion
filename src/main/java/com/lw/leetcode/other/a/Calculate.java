package com.lw.leetcode.other.a;

/**
 * LCP 17. 速算机器人
 *
 * @Author liw
 * @Date 2021/6/18 16:58
 * @Version 1.0
 */
public class Calculate {
    public int calculate(String s) {
        return 1 << s.length();
    }
}
