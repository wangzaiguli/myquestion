package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 6004. 得到 0 的操作数
 * 2169. 得到 0 的操作数
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/13 22:17
 */
public class CountOperations {


    public static void main(String[] args) {
        CountOperations test = new CountOperations();

        int a = 2;
        int b = 3;

        int i = test.countOperations(a, b);
        System.out.println(i);
    }

    public int countOperations(int num1, int num2) {
        int t = num1;
        if (num1 < num2) {
            num1 = num2;
            num2 = t;
        }
        int count = 0;
        while (num2 != 0) {
            count += num1 / num2;
            t = num1;
            num1 = num2;
            num2 = t % num2;
        }
        return count;
    }


}
