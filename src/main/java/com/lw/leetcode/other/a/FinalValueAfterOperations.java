package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * other
 * 2011. 执行操作后的变量值
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/24 19:41
 */
public class FinalValueAfterOperations {

    public int finalValueAfterOperations(String[] operations) {
        int count = 0;
        for (String s : operations) {
            if (s.charAt(1) == '+') {
                count++;
            } else {
                count--;
            }
        }
        return count;
    }

}
