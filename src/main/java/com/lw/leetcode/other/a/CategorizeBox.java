package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 *2525. 根据规则将箱子分类
 * @author liw
 * @version 1.0
 * @date 2023/1/8 11:50
 */
public class CategorizeBox {
    public String categorizeBox(int length, int width, int height, int mass) {
        int item = 0;
        if (mass >= 100) {
            item = 2;
        }
        long s = (long)length * width * height;
        if (s >= 1000000000 || length >= 10000 || width >= 10000 || height >= 10000 || mass >= 10000) {
            item |= 1;
        }
        if (item == 0) {
            return "Neither";
        } else if (item == 1) {
            return "Bulky";
        } else if (item == 2) {
            return "Heavy";
        }
        return "Both";
    }

}
