package com.lw.leetcode.other.a;

import java.util.ArrayList;
import java.util.List;

/**
 * 401. 二进制手表
 *
 * @Author liw
 * @Date 2021/5/8 13:57
 * @Version 1.0
 */
public class ReadBinaryWatch {

    public static void main(String[] args) {
        ReadBinaryWatch test =  new ReadBinaryWatch();
        List<String> strings = test.readBinaryWatch(1);
        System.out.println(strings);
    }

    public List<String> readBinaryWatch(int turnedOn) {
        List<String> value = new ArrayList<>();
        if (turnedOn > 8) {
            return value;
        }
        ArrayList<Integer>[] list = (ArrayList<Integer>[])new ArrayList<?>[6];
        list[0] = new ArrayList<Integer>(){{
            add(0);
        }};
        for (int i = 1; i < 60; i++) {
            int n = 1;
            int item = i;
            while ((item & (item - 1)) != 0) {
                n++;
                item = (item & (item - 1));
            }
            List<Integer> strings = list[n];
            if (strings == null) {
                strings = new ArrayList<>();
                list[n] = (ArrayList<Integer>) strings;
            }
            strings.add(i);
        }
        for (int i = 0; i < 4; i++) {
            int b = turnedOn - i;
            if (b > 5 || b < 0) {
                continue;
            }
            ArrayList<Integer> aa = list[i];
            ArrayList<Integer> ba = list[b];

            for (Integer h : aa) {
                if (h > 11) {
                    break;
                }
                for (Integer m : ba) {
                    value.add(h + ":" + (m > 9 ? m : "0" + m));
                }
            }
        }
        return value;
    }
}
