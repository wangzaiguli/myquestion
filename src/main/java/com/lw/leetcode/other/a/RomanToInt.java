package com.lw.leetcode.other.a;

import java.util.HashMap;
import java.util.Map;

/**
 * 13. 罗马数字转整数
 */
public class RomanToInt {
    public int romanToInt(String s) {
        Map<String, Integer> bb = new HashMap<>();
        bb.put("CM", 900);
        bb.put("CD", 400);
        bb.put("XC", 90);
        bb.put("XL", 40);
        bb.put("IX", 9);
        bb.put("IV", 4);
        Map<String, Integer> all = new HashMap<>();
        all.put("M", 1000);
        all.put("D", 500);
        all.put("C", 100);
        all.put("L", 50);
        all.put("X", 10);
        all.put("V", 5);
        all.put("I", 1);
        int length = s.length() - 1;
        if (length == 0) {
            return all.get(s);
        }
        int sum = 0;
        for (int i = 0; i < length; i++) {
            String key = s.substring(i, i + 2);
            Integer value = bb.get(key);
            if (value == null) {
                sum = sum + all.get(s.substring(i, i + 1));
            } else {
                sum += value;
                i++;
            }
        }
        String key = s.substring(length - 1);
        Integer value = bb.get(key);
        if (value == null) {
            sum = sum + all.get(s.substring(length));
        }
        return sum;
    }
}
