package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 397. 整数替换
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/19 9:23
 */
public class IntegerReplacement {
    public int integerReplacement(int a) {
        int count = 0;
        long n = a;
        while (n != 1) {
            if (n == 3) {
                return 2 + count;
            }
            if ((n & 1) != 0) {
                count = count + 3;
                if ((n & 2) != 0) {
                    n++;
                } else {
                    n--;
                }
                n = n >> 2;
            } else {
                count++;
                n = n >> 1;
            }
        }
        return count;
    }
}
