package com.lw.leetcode.other.a;

import java.util.HashMap;
import java.util.Map;

/**
 * 594. 最长和谐子序列
 *
 * @Author liw
 * @Date 2021/4/19 11:05
 * @Version 1.0
 */
public class FindLHS {
    public static void main(String[] args) {
        int[] arr = {1, 2, 1, 2, 1, 2, 1};
        int lhs = new FindLHS().findLHS(arr);
        System.out.println(lhs);

    }

    public int findLHS(int[] nums) {
        if (nums == null || nums.length < 2) {
            return 0;
        }
        int length = nums.length;
        Map<Integer, Integer> map = new HashMap<>((length << 1) < 0 ? Integer.MAX_VALUE : (length << 1));

        for (int n : nums) {
            Integer integer = map.get(n);
            if (integer == null) {
                map.put(n, 1);
            } else {
                map.put(n, integer + 1);
            }
        }
        int max = 0;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            Integer value = map.get(entry.getKey() - 1);
            Integer n = entry.getValue();
            if (value != null) {
                n += value;
                max = n > max ? n : max;
            }

        }
        return max;
    }

}
