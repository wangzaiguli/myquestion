package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 441. 排列硬币
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/10 15:43
 */
public class ArrangeCoins {
    public int arrangeCoins(int n) {
        return (int) (Math.sqrt(2.0 * n + 0.25) - 0.5);
    }
}
