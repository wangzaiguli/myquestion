package com.lw.leetcode.other.a;

/**
 * 190. 颠倒二进制位
 *
 * @Author liw
 * @Date 2021/4/16 13:03
 * @Version 1.0
 */
public class ReverseBits {

    public static void main(String[] args) {
        int n = new ReverseBits().reverseBits(-3);
//        int i = new ReverseBits().reverseBits(1);
        System.out.println(n);


//        int k = -2147483648;
//        for (int i = 0; i < 31; i++) {
//            k = k >>> 1;
//            System.out.println( i  + "   " + k);
//        }
    }

    public int reverseBits(int n) {
        if (n == 0) {
            return 0;
        }
        int value = 0;
        for (int i = 0; i < 32; i++) {
            if ((n & 1) != 0) {
                value = (value << 1) + 1;
            } else {
                value = value << 1;
            }
            n = n >>> 1;
        }
        return value;
    }
}
