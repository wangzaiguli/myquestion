package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 *
 * other
 * 剑指 Offer 61. 扑克牌中的顺子
 * @author liw
 * @version 1.0
 * @date 2021/7/29 16:25
 */
public class IsStraight {
    public boolean isStraight(int[] nums) {
        int[] arr = new int[14];
        int max = -1;
        int min = 14;
        for (int num : nums) {
            if (num != 0) {
                max = Math.max(max, num);
                min = Math.min(min, num);
                if (arr[num] != 0) {
                    return false;
                }
                arr[num] = 1;
            }
        }
        return max - min < 5 ;
    }
}
