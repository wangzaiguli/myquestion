package com.lw.leetcode.other.a;

/**
 * other
 * a
 * 2427. 公因子的数目
 */
public class CommonFactors {

    public int commonFactors(int a, int b) {
        int l = Math.max(a, b);
        int count = 0;
        for (int i = l; i > 0; i--) {
            if (a % i == 0 && b % i == 0) {
                count++;
            }
        }
        return count;
    }

}
