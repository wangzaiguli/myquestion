package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 2432. 处理用时最长的那个任务的员工
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/10 15:59
 */
public class HardestWorker {

    public static void main(String[] args) {
        HardestWorker test = new HardestWorker();

        int n = 10;

        // 1
//        int[][] arr = {{0, 3}, {2, 5}, {0, 9}, {1, 15}};

        // 3
//        int[][] arr = {{1, 1}, {3, 7}, {2, 12}, {7, 17}};

        // 0
        int[][] arr = {{0, 10}, {1, 20}};

        int i = test.hardestWorker(n, arr);
        System.out.println(i);
    }

    public int hardestWorker(int n, int[][] logs) {
        int max = logs[0][1];
        int index = logs[0][0];
        int item = 0;
        for (int[] log : logs) {
            int b = log[1] - item;
            if (b > max) {
                max = b;
                index = log[0];
            } else if (b == max && index > log[0]) {
                index = log[0];
            }
            item = log[1];
        }
        return index;
    }

}
