package com.lw.leetcode.other.a;

/**
 * 2520. 统计能整除数字的位数
 * a
 * other
 */
public class CountDigits {

    public int countDigits(int num) {
        int item = num;
        int c = 0;
        while (item != 0) {
            if (num % (item % 10) == 0) {
                c++;
            }
            item /= 10;
        }
        return c;
    }

}
