package com.lw.leetcode.other.a;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 2231. 按奇偶性交换后的最大数字
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/15 9:44
 */
public class LargestInteger {


    public static void main(String[] args) {
        LargestInteger test = new LargestInteger();

        int n = 12345;

        int i = test.largestInteger(n);
        System.out.println(i);

    }

    public int largestInteger(int num) {
        char[] arr = String.valueOf(num).toCharArray();
        int length = arr.length;
        List<Character> a = new ArrayList<>();
        List<Character> b = new ArrayList<>();
        for (char c : arr) {
            if ((c & 1) == 0) {
                b.add(c);
            } else {
                a.add(c);
            }
        }
        a.sort(Comparator.reverseOrder());
        b.sort(Comparator.reverseOrder());
        int i = 0;
        int j = 0;
        for (int k = 0; k < length; k++) {
            if ((arr[k] & 1) == 0) {
                arr[k] = b.get(i++);
            } else {
                arr[k] = a.get(j++);
            }
        }
        return Integer.parseInt(String.valueOf(arr));
    }
}
