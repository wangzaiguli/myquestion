package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * a
 * other
 * 1791. 找出星型图的中心节点
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/18 14:17
 */
public class FindCenter {
    public int findCenter(int[][] edges) {
        return edges[0][0] == edges[1][0] || edges[0][0] == edges[1][1] ? edges[0][0] : edges[0][1];
    }
}
