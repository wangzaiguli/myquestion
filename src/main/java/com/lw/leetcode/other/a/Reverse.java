package com.lw.leetcode.other.a;

/**
 * 7. 整数反转
 *
 * @Author liw
 * @Date 2021/9/21 18:01
 * @Version 1.0
 */
public class Reverse {
    public int reverse(int x) {
        long b = 0;
        while (x != 0) {
            b = b * 10 + x % 10;
            x = x / 10;
        }
        if (b > Integer.MAX_VALUE || b < Integer.MIN_VALUE) {
            b = 0;
        }
        return (int) b;
    }
}
