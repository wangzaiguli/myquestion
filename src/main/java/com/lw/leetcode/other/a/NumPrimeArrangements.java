package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * other
 * 1175. 质数排列
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/21 16:42
 */
public class NumPrimeArrangements {

    public static void main(String[] args) {

    }

    public int numPrimeArrangements(int n) {

        if (n < 3) {
            return 1;
        }
        long c1 = numberOfPrime(n);
        long c2 = n - c1;
        long item = 1;
        while (c1 > 1) {
            item *= c1--;
            item %= 1000000007;
        }
        while (c2 > 1) {
            item *= c2--;
            item %= 1000000007;
        }
        return (int) item;
    }

    private int numberOfPrime(int n) {
        int res = 1;
        for (int i = 3; i <= n; i = i + 2) {
            if (isPrime(i)) {
                res++;
            }
        }
        return res;
    }

    private boolean isPrime(int n) {
        int l = (int) Math.pow(n, 0.5);
        for (int i = 3; i <= l; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }


}
