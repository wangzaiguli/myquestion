package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 2358. 分组的最大数量
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/1 15:08
 */
public class MaximumGroups {

    public int maximumGroups(int[] grades) {
        return (int) (Math.pow(((grades.length << 3) + 1), 0.5) - 1) >> 1;
    }
}
