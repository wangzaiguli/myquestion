package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 2224. 转化时间需要的最少操作数
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/15 18:01
 */
public class ConvertTime {

    public int convertTime(String current, String correct) {
        String[] split = current.split(":");
        int a = Integer.parseInt(split[0]);
        int b = Integer.parseInt(split[1]);
        split = correct.split(":");
        int c = Integer.parseInt(split[0]);
        int d = Integer.parseInt(split[1]);
        c = (c - a) * 60 + d - b;
        int sum = 0;
        int count = c / 60;
        sum += count;
        c %= 60;
        count = c / 15;
        sum += count;
        c %= 15;
        count = c / 5;
        sum += count;
        c %= 5;
        return sum + c;
    }

}
