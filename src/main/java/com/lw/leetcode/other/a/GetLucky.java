package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 1945. 字符串转化后的各位数字之和
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/15 9:13
 */
public class GetLucky {

    public static void main(String[] args) {
        GetLucky test = new GetLucky();

        // 8
        String s = "zbax";
        int k = 2;

        // 36
//        String s = "leetcode";
//        int k = 2;

        // 6
//        String s = "iiii";
//        int k = 1;

        int lucky = test.getLucky(s, k);
        System.out.println(lucky);

    }

    public int getLucky(String s, int k) {
        long sum = 0;
        for (char c : s.toCharArray()) {
            int t = c - 96;
            if (t < 10) {
                sum += t;
            } else if (t < 20) {
                sum += (t - 9);
            } else {
                sum += (t - 18);
            }
        }
        long sum2 = 0;
        for (int i = 1; i < k; i++) {
            while (sum != 0) {
                sum2 += (sum % 10);
                sum /= 10;
            }
            sum = sum2;
            sum2 = 0;
        }
        return (int) sum;
    }

}
