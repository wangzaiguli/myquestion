package com.lw.leetcode.other.a;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 6128. 最好的扑克手牌
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/24 12:22
 */
public class BestHand {

    public String bestHand(int[] ranks, char[] suits) {
        char c = suits[0];
        for (int i = 1; i < 5; i++) {
            if (c != suits[i]) {
                c = suits[i];
                break;
            }
        }
        if (c == suits[0]) {
            return "Flush";
        }
        Arrays.sort(ranks);
        for (int i = 2; i < 5; i++) {
            if (ranks[i] == ranks[i - 2]) {
                return "Three of a Kind";
            }
        }
        for (int i = 1; i < 5; i++) {
            if (ranks[i] == ranks[i - 1]) {
                return "Pair";
            }
        }
        return "High Card";
    }

}
