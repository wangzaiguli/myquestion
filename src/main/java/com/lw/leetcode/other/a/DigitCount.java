package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 *2283. 判断一个数的数字计数是否等于数位的值
 * @author liw
 * @version 1.0
 * @date 2023/1/11 10:34
 */
public class DigitCount {

    public boolean digitCount(String num) {
        int length = num.length();
        int[] arr = new int[10];
        for (int i = 0; i < length; i++) {
            arr[num.charAt(i) - 48]++;
        }
        for (int i = 0; i < length; i++) {
            if (arr[i] != num.charAt(i) - 48) {
                return false;
            }
        }
        return true;
    }

}
