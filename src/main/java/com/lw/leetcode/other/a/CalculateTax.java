package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 2303. 计算应缴税款总额
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/28 11:04
 */
public class CalculateTax {

    public double calculateTax(int[][] brackets, int income) {
        double totalTax = 0;
        int lower = 0;
        for (int[] bracket : brackets) {
            int upper = bracket[0];
            int percent = bracket[1];
            int tax = (Math.min(income, upper) - lower) * percent;
            totalTax += tax;
            if (income <= upper) {
                break;
            }
            lower = upper;
        }
        return totalTax / 100.0;
    }

}
