package com.lw.leetcode.other.a;

/**
 * mode
 * 1688. 比赛中的配对次数
 *
 * @Author liw
 * @Date 2021/6/18 16:47
 * @Version 1.0
 */
public class NumberOfMatches {
    public int numberOfMatches(int n) {
        return n - 1;
    }
}
