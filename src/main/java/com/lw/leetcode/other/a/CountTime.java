package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 2437. 有效时间的数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/17 16:27
 */
public class CountTime {

    public static void main(String[] args) {
        CountTime test = new CountTime();

        // 3
        String time = "?4:00";

        // 2
//        String time = "?5:00";

        // 100
//        String time = "0?:0?";

        // 1440
//        String time = "??:??";

        int i = test.countTime(time);
        System.out.println(i);
    }

    public int countTime(String time) {
        char a = time.charAt(0);
        char b = time.charAt(1);
        char c = time.charAt(3);
        char d = time.charAt(4);
        char t = '?';
        int item = 1;
        if (a == t && b == t) {
            item = 24;
        } else if (a == t) {
            if (b < '4') {
                item = 3;
            } else {
                item = 2;
            }
        } else if (b == t) {
            if (a <= '1') {
                item = 10;
            } else {
                item = 4;
            }
        }
        if (c == t) {
            item *= 6;
        }
        if (d == t) {
            item *= 10;
        }
        return item;
    }

}
