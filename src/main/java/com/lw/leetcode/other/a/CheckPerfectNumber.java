package com.lw.leetcode.other.a;

/**
 * other
 * 507. 完美数
 *
 * @Author liw
 * @Date 2021/6/28 13:20
 * @Version 1.0
 */
public class CheckPerfectNumber {
    public boolean checkPerfectNumber(int num) {
        if (num < 4) {
            return false;
        }
        int sum = 1;
        int sqrt = (int) Math.sqrt(num);
        for (int i = 2; i <= sqrt; i++) {
            if (num % i == 0) {
                sum += i;
                sum += num / i;
            }
        }
        return sum == num;
    }
}
