package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 1779. 找到最近的有相同 X 或 Y 坐标的点
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/1 9:04
 */
public class NearestValidPoint {

    public int nearestValidPoint(int x, int y, int[][] points) {
        int n = points.length;
        int min = Integer.MAX_VALUE;
        int index = -1;
        for (int i = 0; i < n; ++i) {
            int a = points[i][0];
            int b = points[i][1];
            if (x == a || y == b) {
                int v = Math.abs(y - b) + Math.abs(x - a);
                if (v < min) {
                    min = v;
                    index = i;
                }
            }
        }
        return index;
    }

}
