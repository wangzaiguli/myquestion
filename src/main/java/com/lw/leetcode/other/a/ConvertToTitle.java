package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 168. Excel表列名称
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/8 9:38
 */
public class ConvertToTitle {

    public String convertToTitle(int n) {
        char[] arr = new char[26];
        for (int i = 0; i < 26; i++) {
            arr[i] = (char) (i + 'A');
        }
        StringBuilder sb = new StringBuilder();
        while (n > 0) {
            n--;
            sb.append(arr[n % 26]);
            n = n / 26;
        }
        return sb.reverse().toString();
    }

}
