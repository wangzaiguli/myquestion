package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 2180. 统计各位数字之和为偶数的整数个数
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/6 9:32
 */
public class CountEven {

    public int countEven(int num) {
        int c = 0;
        for (int i = 2; i <= num; i++) {
            if (find(i)) {
                c++;
            }
        }
        return c;
    }

    private boolean find(int t) {
        int c = 0;
        while (t != 0) {
            c += t % 10;
            t /= 10;
        }
        return (c & 1) == 0;
    }

}
