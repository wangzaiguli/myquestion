package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 6249. 分割圆的最少切割次数
 *
 * @author liw
 * @version 1.0
 * @date 2022/11/27 19:34
 */
public class NumberOfCuts {

    public static void main(String[] args) {
        NumberOfCuts test = new NumberOfCuts();
        for (int i = 1; i < 101; i++) {
            int a = test.numberOfCuts(i);
            System.out.println(i + "  " + a);
        }
    }

    public int numberOfCuts(int n) {
        if (n == 1) {
            return 0;
        }
        if ((n & 1) == 1) {
            return n;
        }
        int t = n >> 1;
        if ((t & 1) == 0) {
            return numberOfCuts(n >> 1) << 1;
        }
        return t;
    }

}
