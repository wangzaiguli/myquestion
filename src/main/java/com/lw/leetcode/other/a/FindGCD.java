package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * a
 * other
 * 1979. 找出数组的最大公约数
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/18 15:00
 */
public class FindGCD {

    public int findGCD(int[] nums) {
        int max = 0;
        int min = 1001;
        for (int num : nums) {
            if (max < num) {
                max = num;
            }
            if (min > num) {
                min = num;
            }
        }
        while (min != 0) {
            int temp = max % min;
            max = min;
            min = temp;
        }
        return max;
    }

}
