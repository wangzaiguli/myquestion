package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * other
 * 1281. 整数的各位积和之差
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/14 17:20
 */
public class SubtractProductAndSum {
    public int subtractProductAndSum(int n) {
        int product = 1;
        int sum = 0;
        while (n / 10 != 0) {
            product *= n % 10;
            sum += n % 10;
            n = n / 10;
        }
        product *= n;
        sum += n;
        return product - sum;
    }
}
