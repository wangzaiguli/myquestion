package com.lw.leetcode.other.a;

import java.util.HashMap;
import java.util.Map;

/**
 * arr
 * 888. 公平的糖果棒交换
 *
 * @Author liw
 * @Date 2021/8/3 22:19
 * @Version 1.0
 */
public class FairCandySwap {

    public int[] fairCandySwap(int[] aliceSizes, int[] bobSizes) {
        int a = 0;
        int b = 0;
        Map<Integer, Integer> map = new HashMap<>(bobSizes.length << 1);
        for (int aliceSize : aliceSizes) {
            a += aliceSize;
        }
        for (int bobSize : bobSizes) {
            b += bobSize;
            map.put(bobSize, 1);
        }
        int c = ((a + b) >> 1);
        int d = a - c;
        for (int aliceSize : aliceSizes) {
            if (map.get(aliceSize - d) != null) {
                return new int[]{aliceSize, aliceSize - d};
            }
        }
        return null;
    }
}
