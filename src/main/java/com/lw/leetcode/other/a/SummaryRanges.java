package com.lw.leetcode.other.a;

import java.util.ArrayList;
import java.util.List;

/**
 * 228. 汇总区间
 *
 * @Author liw
 * @Date 2021/4/16 14:35
 * @Version 1.0
 */
public class SummaryRanges {

    public List<String> summaryRanges(int[] nums) {
        List<String> list = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return list;
        }
        int length = nums.length;
        if (length == 1) {
            list.add(String.valueOf(nums[0]));
            return list;
        }
        int st = nums[0];
        int item = st;
        for (int i = 1; i < length; i++) {
            int num = nums[i];
            if (item + 1 != num) {
                if (st != item) {
                    list.add(st + "->" + item);
                } else {
                    list.add(String.valueOf(st));
                }
                st = num;
            }
            item = num;
        }
        if (st != item) {
            list.add(st + "->" + item);
        } else {
            list.add(String.valueOf(st));
        }
        return list;
    }

}
