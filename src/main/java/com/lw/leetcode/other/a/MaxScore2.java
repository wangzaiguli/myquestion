package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 1422. 分割字符串的最大得分
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/24 13:52
 */
public class MaxScore2 {
    public int maxScore(String s) {
        int max = 0;
        int n = s.length();
        max += s.charAt(0) == '0' ? 1 : 0;
        for (int i = 1; i < n; i++) {
            if (s.charAt(i) == '1') {
                max++;
            }
        }
        int pre = max;
        for (int i = 1; i < n - 1; i++) {
            if (s.charAt(i) == '0') {
                pre++;
            } else {
                pre--;
            }
            max = Math.max(max, pre);
        }
        return max;
    }

}
