package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 1154. 一年中的第几天
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/21 16:56
 */
public class DayOfYear {
    public int dayOfYear(String date) {
        String y = date.substring(0, 4);
        String m = date.substring(5, 7);
        String d = date.substring(8, 10);

        int yi = Integer.parseInt(y);
        int mi = Integer.parseInt(m);
        int di = Integer.parseInt(d);

        int e = ((yi % 4 == 0 && yi % 100 != 0) || (yi % 400 == 0)) ? 29 : 28;

        int[] a = new int[]{31, e, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int mm = mi - 1;
        int sum = 0;
        if (mm > 0) {
            for (int i = 0; i < mm; i++) {
                sum += a[i];
            }
        }
        return sum + di;
    }
}
