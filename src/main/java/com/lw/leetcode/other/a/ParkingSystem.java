package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 1603. 设计停车系统
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/15 11:56
 */
public class ParkingSystem {

    private int big;
    private int medium;
    private int small;

    public ParkingSystem(int big, int medium, int small) {
        this.big = big;
        this.medium = medium;
        this.small = small;
    }

    public boolean addCar(int carType) {
        if (carType == 1) {
            if (big > 0) {
                big--;
                return true;
            }
        } else if (carType == 2) {
            if (medium > 0) {
                medium--;
                return true;
            }
        } else {
            if (small > 0) {
                small--;
                return true;
            }
        }
        return false;
    }


}
