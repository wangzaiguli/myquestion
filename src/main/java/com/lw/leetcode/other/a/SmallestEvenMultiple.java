package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 2413. 最小偶倍数
 *
 * @author liw
 * @version 1.0
 * @date 2022/9/18 20:52
 */

public class SmallestEvenMultiple {

    public int smallestEvenMultiple(int n) {
        return ((n & 1) == 0) ? n : (n << 1);
    }

}
