package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 1796. 字符串中第二大的数字
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/6 11:16
 */
public class SecondHighest {

    public int secondHighest(String s) {
        int length = s.length();
        int[] arr = new int[10];
        for (int i = 0; i < length; i++) {
            int c = s.charAt(i) - '0';
            if (c < 10) {
                arr[c] = 1;
            }
        }
        boolean f = true;
        for (int i = 9; i >= 0; i--) {
            if (arr[i] > 0) {
                if (f) {
                    f = false;
                } else {
                    return i;
                }
            }
        }
        return -1;
    }

}
