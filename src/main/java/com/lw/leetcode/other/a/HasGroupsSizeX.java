package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 914. 卡牌分组
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/30 17:48
 */
public class HasGroupsSizeX {
    public boolean hasGroupsSizeX(int[] deck) {
        int[] counter = new int[10000];
        for (int num : deck) {
            counter[num]++;
        }
        int x = 0;
        for (int cnt : counter) {
            if (cnt > 0) {
                x = gcd(x, cnt);
                if (x == 1) {
                    return false;
                }
            }
        }
        return x >= 2;
    }

    private int gcd(int a, int b) {
        return b == 0 ? a : gcd(b, a % b);
    }
}
