package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 *1037. 有效的回旋镖
 * @author liw
 * @version 1.0
 * @date 2022/1/23 21:46
 */
public class IsBoomerang {
    public boolean isBoomerang(int[][] points) {
        return ((points[0][0] - points[1][0]) * (points[0][1] - points[2][1])) != ((points[0][0] - points[2][0]) * (points[0][1] - points[1][1]));
    }
}
