package com.lw.leetcode.other.a;

/**
 * Created with IntelliJ IDEA.
 * 1952. 三除数
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/20 9:14
 */
public class IsThree {

    public boolean isThree(int n) {
        int cnt = 0;
        for (int x = 1; x <= n; x++) {
            if (n % x == 0) {
                cnt++;
                if (cnt > 3) {
                    break;
                }
            }
        }
        return cnt == 3;
    }

}
