package com.lw.leetcode.other.a;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2293. 极大极小游戏
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/15 15:37
 */
public class MinMaxGame {

    public static void main(String[] args) {
        MinMaxGame test = new MinMaxGame();

        // 2
//        int[] arr = {3, 4, 1, 2};

        // 2
        int[] arr = {810, 831, 908, 631, 554, 917, 392, 544};

        int i = test.minMaxGame(arr);
        System.out.println(i);
    }

    public int minMaxGame(int[] nums) {
        int length = nums.length;
        while (length != 1) {
            int size = length >> 1;
            for (int j = 0; j < size; j++) {
                int t = j << 1;
                nums[j] = ((j & 1) == 0) ? Math.min(nums[t], nums[t + 1]) : Math.max(nums[t], nums[t + 1]);
            }
            System.out.println(Arrays.toString(nums));
            length = size;
        }
        return nums[0];
    }

}
