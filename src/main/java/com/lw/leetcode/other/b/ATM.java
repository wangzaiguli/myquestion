package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * other
 * 2241. 设计一个 ATM 机器
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/20 9:16
 */
public class ATM {
    private long[] current;
    private int[] money;

    public ATM() {
        current = new long[5];
        money = new int[]{20, 50, 100, 200, 500};
    }

    public void deposit(int[] banknotesCount) {
        for (int i = 0; i < 5; i++) {
            current[i] += banknotesCount[i];
        }
    }

    public int[] withdraw(int amount) {
        int[] ans = new int[5];
        for (int i = 4; i >= 0; i--) {
            int times = (int) Math.min(amount / money[i], current[i]);
            ans[i] = times;
            amount -= money[i] * times;
        }
        if (amount != 0) {
            return new int[]{-1};
        }
        for (int i = 0; i < 5; i++) {
            current[i] -= ans[i];
        }
        return ans;
    }
}
