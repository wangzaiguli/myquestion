package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * other
 * 825. 适龄的朋友
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/21 17:36
 */
public class NumFriendRequests {
    public int numFriendRequests(int[] ages) {
        int[] count = new int[121];
        for (int age : ages) {
            count[age]++;
        }
        int ans = 0;
        for (int ageA = 0; ageA <= 120; ageA++) {
            int countA = count[ageA];
            int item = (ageA >> 1) + 7;
            for (int ageB = 0; ageB <= 120; ageB++) {
                if (item >= ageB || ageA < ageB) {
                    continue;
                }
                ans += countA * count[ageB];
                if (ageA == ageB) {
                    ans -= countA;
                }
            }
        }
        return ans;
    }

}
