package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * b
 * other
 * 2546. 执行逐位运算使字符串相等
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/28 9:47
 */
public class MakeStringsEqual {

    public static void main(String[] args) {
        MakeStringsEqual test = new MakeStringsEqual();

        // true
        String a = "1010";
        String b = "0110";

        // false
//        String a = "11";
//        String b = "00";

        boolean boo = test.makeStringsEqual(a, b);
        System.out.println(boo);
    }

    public boolean makeStringsEqual(String s, String target) {
        int length = s.length();
        int a = 0;
        int b = 0;
        for (int i = 0; i < length; i++) {
            if (s.charAt(i) == '1') {
                a++;
            }
            if (target.charAt(i) == '1') {
                b++;
            }
        }
        return !((a == 0 && b != 0) || (a != 0 && b == 0));
    }

}
