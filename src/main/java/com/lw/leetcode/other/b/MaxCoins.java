package com.lw.leetcode.other.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 1561. 你可以获得的最大硬币数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/18 15:27
 */
public class MaxCoins {

    public int maxCoins(int[] piles) {
        Arrays.sort(piles);
        int sum = 0;
        int length = piles.length;
        int st = length / 3;
        for (int i = length - 2; i >= st; i -= 2) {
            sum += piles[i];
        }
        return sum;
    }

}
