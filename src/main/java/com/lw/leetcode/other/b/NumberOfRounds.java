package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * other
 * 1904. 你完成的完整对局数
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/25 9:26
 */
public class NumberOfRounds {

    public static void main(String[] args) {
        NumberOfRounds test = new NumberOfRounds();

        // 1
        String startTime = "12:01";
        String finishTime = "12:44";

        // 40
//        String startTime = "20:00";
//        String finishTime = "06:00";

        // 95
//        String startTime = "00:00";
//        String finishTime = "23:59";

        int i = test.numberOfRounds(startTime, finishTime);
        System.out.println(i);

    }

    public int numberOfRounds(String loginTime, String logoutTime) {
        int a = find(loginTime);
        int b = find(logoutTime);
        if (b < a) {
            b += 24 * 60;
        }
        a = findEnd(b) - findSt(a);
        return (a <= 0 ? 0 : a) / 15;
    }

    private int find(String str) {
        int a = str.charAt(0) - '0';
        int b = str.charAt(1) - '0';
        int c = str.charAt(3) - '0';
        int d = str.charAt(4) - '0';
        return a * 600 + b * 60 + c * 10 + d;
    }

    private int findSt(int v) {
        int i = v % 60;
        v -= i;
        if (i == 0) {
            i = 0;
        } else if (i <= 15) {
            i = 15;
        } else if (i <= 30) {
            i = 30;
        } else if (i <= 45) {
            i = 45;
        } else {
            i = 60;
        }
        return v + i;
    }

    private int findEnd(int v) {
        int i = v % 60;
        v -= i;
        if (i < 15) {
            i = 0;
        } else if (i < 30) {
            i = 15;
        } else if (i < 45) {
            i = 30;
        } else {
            i = 45;
        }
        return v + i;
    }
}
