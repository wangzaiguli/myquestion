package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 1432. 改变一个整数能得到的最大差值
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/16 14:49
 */
public class MaxDiff {

    public static void main(String[] args) {
        MaxDiff test = new MaxDiff();

        // 820000
//        int n = 123456;
        // 80000
//        int n = 10000;
        // 888
        int n = 111;

        int i = test.maxDiff(n);
        System.out.println(i);
    }

    public int maxDiff(int num) {
        char[] chars = String.valueOf(num).toCharArray();
        char[] chars2 = String.valueOf(num).toCharArray();
        int length = chars.length;
        char a = ' ';
        char b = ' ';
        int i = 0;
        for (; i < length; i++) {
            if (chars[i] != '9') {
                a = chars[i];
                b = '9';
                break;
            }
        }
        for (; i < length; i++) {
            if (chars2[i] == a) {
                chars2[i] = b;
            }
        }
        if (chars[0] == '1') {
            i = 1;
            for (; i < length; i++) {
                if (chars[i] != '0' && chars[i] != '1') {
                    a = chars[i];
                    b = '0';
                    break;
                }
            }
            for (; i < length; i++) {
                if (chars[i] == a) {
                    chars[i] = b;
                }
            }
        } else {
            a = chars[0];
            b = '1';
            for (i = 0; i < length; i++) {
                if (chars[i] == a) {
                    chars[i] = b;
                }
            }
        }
        return Integer.parseInt(String.valueOf(chars2)) - Integer.parseInt(String.valueOf(chars));
    }

}
