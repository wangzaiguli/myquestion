package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 2029. 石子游戏 IX
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/20 17:44
 */
public class StoneGameIX {

    public static void main(String[] args) {
        StoneGameIX test = new StoneGameIX();

        // true
//        int[] arr = {19,2,17,20,7,17};

        // true
//        int[] arr = {2,1};

        // false
//        int[] arr = {2};

        // false
//        int[] arr = {5,1,2,4,3};

        // false
        int[] arr = {15, 20, 10, 13, 14, 15, 5, 2, 3};


        boolean b = test.stoneGameIX(arr);
        System.out.println(b);

    }

    public boolean stoneGameIX(int[] stones) {
        int a = 0;
        int b = 0;
        int c = 0;
        for (int i : stones) {
            i %= 3;
            if (i == 0) {
                a++;
            } else if (i == 1) {
                b++;
            } else {
                c++;
            }
        }
        if (b == 0 && c == 0) {
            return false;
        }
        boolean flag = (a & 1) == 0;
        int max = Math.max(b, c);
        int min = Math.min(b, c);
        if (min == 0) {
            if (max <= 2) {
                return false;
            }
            return !flag;
        }
        if (max == min) {
            return flag;
        }
        if (flag) {
            return true;
        } else {
            return max - min >= 3;
        }
    }

}
