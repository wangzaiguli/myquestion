package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 6222. 美丽整数的最小增量
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/31 9:28
 */
public class MakeIntegerBeautiful {

    public static void main(String[] args) {
        MakeIntegerBeautiful test = new MakeIntegerBeautiful();

        // 4
//        long n = 16;
//        int t = 6;

        // 33
//        long n = 467;
//        int t = 6;

        // 0
//        long n = 1;
//        int t = 1;

        // 13578515
//        long n = 357486421485L;
//        int t = 25;

        // 29384
//        long n = 70616;
//        int t = 5;

        // 5402
//        long n = 94598;
//        int t = 6;

        // 3931939239
        long n = 6068060761L;
        int t = 3;

        long l1 = test.makeIntegerBeautiful(n, t);
        System.out.println(l1);
    }

    public long makeIntegerBeautiful(long n, int target) {
        int sum = find(n);
        long c = 0;
        if (sum <= target) {
            return c;
        }
        long i = 10;
        while (sum > target) {
            long t = n % i;
            n -= t;
            long v = (t * 10) / i;
            if (v != 0) {
                n += i;
                v = 10 - v;
            }
            sum = find(n);
            c += (i / 10) * v;
            i *= 10;
        }
        return c;
    }

    private int find (long item) {
        int sum = 0;
        while (item != 0) {
            sum += item % 10;
            item /= 10;
        }
        return sum;
    }

}
