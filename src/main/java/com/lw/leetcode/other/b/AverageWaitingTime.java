package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 1701. 平均等待时间
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/28 17:08
 */
public class AverageWaitingTime {

    public double averageWaitingTime(int[][] customers) {
        int item = 0;
        double sum = 0D;
        for (int[] arr : customers) {
            int st = arr[0];
            int t = arr[1];
            if (item <= st) {
                item = st + t;
                sum += t;
            } else {
                item += t;
                sum += (item - st);
            }
        }
        return sum / customers.length;
    }

}
