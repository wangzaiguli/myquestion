package com.lw.leetcode.other.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2149. 按符号重排数组
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/7 22:02
 */
public class RearrangeArray {

    public static void main(String[] args) {
        RearrangeArray test = new RearrangeArray();
        int[] arr = {3, 1, -2, -5, 2, -4};
        int[] ints = test.rearrangeArray(arr);
        System.out.println(Arrays.toString(ints));
    }

    public int[] rearrangeArray(int[] nums) {
        int length = nums.length;
        int i = 0;
        int j = 1;
        int[] arr = new int[length];
        for (int num : nums) {
            if (num > 0) {
                arr[i] = num;
                i += 2;
            } else {
                arr[j] = num;
                j += 2;
            }
        }
        return arr;
    }

}
