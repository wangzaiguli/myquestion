package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * other
 * 949. 给定数字能组成的最大时间
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/24 17:07
 */
public class LargestTimeFromDigits {

    public static void main(String[] args) {
        LargestTimeFromDigits test = new LargestTimeFromDigits();

        // 23:41
        int[] arr = {1, 2, 3, 4};
        String s = test.largestTimeFromDigits(arr);
        System.out.println(s);
    }


    public String largestTimeFromDigits(int[] arr) {
        int max = -1;
        for (int i = 0; i < 3; i++) {
            int a = arr[i];
            for (int j = i + 1; j < 4; j++) {
                int b = arr[j];
                if (a > 2 && b > 2) {
                    continue;
                }
                int c = 0;
                if (i == 0) {
                    c = j == 1 ? 2 : 1;
                }
                int d = arr[6 - i - j - c];
                c = arr[c];
                int h2 = a * 10 + b;
                int m2 = c * 10 + d;
                int h1 = b * 10 + a;
                int m1 = d * 10 + c;
                max = Math.max(max, find(h2, m2));
                max = Math.max(max, find(h1, m2));
                max = Math.max(max, find(h2, m1));
                max = Math.max(max, find(h1, m1));
            }
        }
        int s1 = max >> 12;
        int s2 = max & 0Xfff;
        if (s1 == -1) {
            return "";
        }
        return (s1 > 9 ? s1 : "0" + s1) + ":" + (s2 > 9 ? s2 : "0" + s2);
    }

    private int find(int h, int m) {
        if (h < 24 && m < 60) {
            return (h << 12) + m;
        }
        return -1;
    }

}
