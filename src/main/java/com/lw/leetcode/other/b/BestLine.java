package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 面试题 16.14. 最佳直线
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/14 11:19
 */
public class BestLine {

    public int[] bestLine(int[][] points) {
        int numOfPoints = points.length;
        int[] res = new int[]{0, 1};
        int max = 2;
        for (int i = 0; i < numOfPoints - 1; i++) {
            int x1 = points[i][0];
            int y1 = points[i][1];
            for (int j = i + 1; j < numOfPoints; j++) {
                int x2 = points[j][0];
                int y2 = points[j][1];
                int count = 2;
                for (int k = j + 1; k < numOfPoints; k++) {
                    int x3 = points[k][0];
                    int y3 = points[k][1];
                    if ((y1 - y2) * (x1 - x3) == (y1 - y3) * (x1 - x2)) {
                        count++;
                    }
                    if (count > max) {
                        res[0] = i;
                        res[1] = j;
                        max = count;
                    }
                }
            }
        }
        return res;
    }

}
