package com.lw.leetcode.other.b;

import java.util.ArrayList;
import java.util.List;

/**
 * 剑指 Offer II 079. 所有子集
 * 78. 子集
 *
 * @Author liw
 * @Date 2021/4/23 13:57
 * @Version 1.0
 */
public class Subsets {

    public List<List<Integer>> subsets(int[] nums) {
        int length = nums.length;
        List<List<Integer>> all = new ArrayList<>(2 << length);
        all.add(new ArrayList<>(0));
        for (int num : nums) {
            int size = all.size();
            for (int i = 0; i < size; i++) {
                List<Integer> item = all.get(i);
                List<Integer> list = new ArrayList<>(item.size());
                list.addAll(item);
                list.add(num);
                all.add(list);
            }
        }
        return all;
    }

}
