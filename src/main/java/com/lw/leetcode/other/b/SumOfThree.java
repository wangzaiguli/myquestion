package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 2177. 找到和为给定整数的三个连续整数
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/12 14:49
 */
public class SumOfThree {
    public long[] sumOfThree(long num) {
        if (num % 3 != 0) {
            return new long[0];
        } else {
            long mid = num / 3;
            return new long[]{mid - 1, mid, mid + 1};
        }
    }
}
