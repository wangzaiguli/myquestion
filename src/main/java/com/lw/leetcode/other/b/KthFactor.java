package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 1492. n 的第 k 个因子
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/10 9:21
 */
public class KthFactor {

    public int kthFactor(int n, int k) {
        int count = 0;
        for (int i = 1; i <= n; ++i) {
            if (n % i == 0) {
                ++count;
                if (count == k) {
                    return i;
                }
            }
        }
        return -1;
    }


}
