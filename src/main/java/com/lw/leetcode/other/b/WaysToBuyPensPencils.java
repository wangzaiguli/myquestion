package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 2240. 买钢笔和铅笔的方案数
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/18 14:09
 */
public class WaysToBuyPensPencils {

    public long waysToBuyPensPencils(int total, int cost1, int cost2) {
        long sum = 0;
        int n = total / cost1;
        for (int i = 0; i <= n; i++) {
            if (total < 0) {
                break;
            }
            sum += total / cost2 + 1;
            total -= cost1;
        }
        return sum;
    }

}
