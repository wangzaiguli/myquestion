package com.lw.leetcode.other.b;

/**
 * 162. 寻找峰值
 *
 * @Author liw
 * @Date 2021/4/30 11:03
 * @Version 1.0
 */
public class FindPeakElement {

    public int findPeakElement(int[] nums) {
        int length = nums.length - 1;
        if (length == 0) {
            return 0;
        }
        if (nums[0] > nums[1]) {
            return 0;
        }
        if (nums[length] > nums[length - 1]) {
            return length;
        }
        for (int i = 1; i < length; i++) {
            int value = nums[i];
            if (value > nums[i - 1] && value > nums[i + 1]) {
                return i;
            }
        }
        return 0;
    }
}
