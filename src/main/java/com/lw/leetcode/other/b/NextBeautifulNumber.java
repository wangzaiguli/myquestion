package com.lw.leetcode.other.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2048. 下一个更大的数值平衡数
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/6 17:13
 */
public class NextBeautifulNumber {

    private int[] xf = new int[10];

    public int nextBeautifulNumber(int n) {
        int num = n + 1;
        while (!find(num)) {
            num++;
        }
        return num;
    }

    private boolean find(int num) {
        Arrays.fill(xf, 0);
        while (num != 0) {
            xf[num % 10]++;
            num /= 10;
        }
        for (int x = 0; x < 10; x++) {
            if (xf[x] != 0 && x != xf[x]) {
                return false;
            }
        }
        return true;
    }

}
