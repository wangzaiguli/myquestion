package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 1599. 经营摩天轮的最大利润
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/4 10:27
 */
public class MinOperationsMaxProfit {

    public static void main(String[] args) {
        MinOperationsMaxProfit test = new MinOperationsMaxProfit();

        // 3
//        int[] arr = {8, 3};
//        int a = 5;
//        int b = 6;

        // 7
//        int[] arr = {10, 9, 6};
//        int a = 6;
//        int b = 4;

        // -1
        int[] arr = {3, 4, 0, 5, 1};
        int a = 1;
        int b = 92;

        // -1
//        int[] arr = {2};
//        int a = 2;
//        int b = 4;

        int i = test.minOperationsMaxProfit(arr, a, b);

        System.out.println(i);
    }

    public int minOperationsMaxProfit(int[] customers, int boardingCost, int runningCost) {
        int item = 0;
        int max = 0;
        int sum = 0;
        int count = -1;
        int t = 0;
        int s = (boardingCost << 2) - runningCost;
        for (int customer : customers) {
            item += customer;
            t++;
            if (item < 4) {
                sum += item * boardingCost - runningCost;
                item = 0;
            } else {
                sum += s;
                item -= 4;
            }
            if (sum > max) {
                max = sum;
                count = t;
            }
        }
        sum += (item >> 2) * s;
        if (sum > max) {
            max = sum;
            t += (item >> 2);
            count = t;
        }
        sum += (item % 4) * boardingCost - runningCost;
        if (sum > max) {
            count = t + 1;
        }
        return count;
    }

}
