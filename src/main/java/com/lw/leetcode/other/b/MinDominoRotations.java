package com.lw.leetcode.other.b;

/**
 * 1007. 行相等的最少多米诺旋转
 *
 * @Author liw
 * @Date 2021/10/31 13:04
 * @Version 1.0
 */
public class MinDominoRotations {


    public static void main(String[] args) {
        MinDominoRotations test = new MinDominoRotations();

        // [6,1,6,4,6,6]
        //[5,6,2,6,3,6]
        int[] arr = {6, 1, 6, 4, 6, 6};
        int[] arr2 = {5, 6, 2, 6, 3, 6};

        int i = test.minDominoRotations(arr, arr2);
        System.out.println(i);
    }


    public int minDominoRotations(int[] tops, int[] bottoms) {
        int length = tops.length;
        int[] arr = new int[7];
        int[] count1 = new int[7];
        int[] count2 = new int[7];

        for (int i = 0; i < length; i++) {
            int a = tops[i];
            int b = bottoms[i];
            if (a != b) {
                arr[b]++;
            }
            arr[a]++;
            count1[a]++;
            count2[b]++;
        }
        int count = Integer.MAX_VALUE;
        for (int i = 0; i < 7; i++) {
            if (arr[i] != length) {
                continue;
            }
            int item1 = count1[i];
            int item2 = count2[i];
            if (item1 == length || count2[i] == length) {
                return 0;
            }
            item1 = Math.min(length - item1, length - item2);
            if (item1 < count) {
                count = item1;
            }
        }
        return count == Integer.MAX_VALUE ? -1 : count;
    }

}
