package com.lw.leetcode.other.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2280. 表示一个折线图的最少线段数
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/24 9:13
 */
public class MinimumLines {

    public static void main(String[] args) {
        MinimumLines test = new MinimumLines();

        int[][] arr = {{1, 7}, {2, 6}, {3, 5}, {4, 4}, {5, 4}, {6, 3}, {7, 2}, {8, 1}};

        int i = test.minimumLines(arr);
        System.out.println(i);
    }

    public int minimumLines(int[][] stockPrices) {
        int length = stockPrices.length;
        if (length < 3) {
            return length - 1;
        }
        int count = 1;
        Arrays.sort(stockPrices, (a, b) -> Integer.compare(a[0], b[0]));
        for (int i = 2; i < length; i++) {
            int[] a = stockPrices[i - 2];
            int[] b = stockPrices[i - 1];
            int[] c = stockPrices[i];
            if (((long) c[0] - b[0]) * ((long) b[1] - a[1]) != ((long) c[1] - b[1]) * ((long) b[0] - a[0])) {
                count++;
            }
        }
        return count;
    }

}
