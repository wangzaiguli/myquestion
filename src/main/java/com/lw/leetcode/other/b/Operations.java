package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 面试题 16.09. 运算
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/13 17:30
 */
public class Operations {

    public static void main(String[] args) {
        Operations test = new Operations();
        int a = -12;
        int b = -5;

//        int multiply = test.multiply(a, b);
//        System.out.println(multiply);
//        int minus = test.divide(a, b);
//        System.out.println(minus);



        System.out.println(test.minus(0, -2147483647));
        System.out.println(test.minus(-1, 2147483647));
        System.out.println(test.multiply(-1, 2147483647));
        System.out.println(test.multiply(-100, 21474836));
//        System.out.println(test.divide(2147483647, -1));
        System.out.println(test.divide(-2147483648, 1));
    }


    public Operations() {

    }

    public int minus(int a, int b) {
        return a + ~b + 1;
    }

    public int multiply(int a, int b) {
        if (a == 0 || b == 0) {
            return 0;
        }
        boolean flag = false;
        long a1 = a;
        long b1 = b;
        if (a1 < 0) {
            a1 = ~a1 + 1;
            flag = true;
        }
        if (b1 < 0) {
            b1 = ~b1 + 1;
            flag = !flag;
        }
        long item = 1;
        long sum = 0;
        long c = a1;
        while (item <= b1) {
            if ((item & b1) == item) {
                sum += c;
            }
            item += item;
            c += c;
        }
        return (int) (flag ? ~sum + 1 : sum);
    }

    public int divide(int a, int b) {
        if (a == 0 || b == 0) {
            return 0;
        }
        boolean flag = false;
        long a1 = a;
        long b1 = b;
        if (a1 < 0) {
            a1 = ~a1 + 1;
            flag = true;
        }
        if (b1 < 0) {
            b1 = ~b1 + 1;
            flag = !flag;
        }
        long sum = 0;
        while (a1 >= b1) {
            long item = 1;
            long c = 0;
            while (item < a1 && multiply2(b1, item) < a1) {
                c = (int) item;
                item += item;
            }
            sum += c;
            if (b1 == a1) {
                sum++;
                break;
            }
            a1 = minus2(a1, multiply2(b1, c));
        }
        return (int) (flag ? ~sum + 1 : sum);
    }


    public long minus2(long a, long b) {
        return a + ~b + 1;
    }

    public long multiply2(long a1, long b1) {
        if (a1 == 0 || b1 == 0) {
            return 0;
        }
        boolean flag = false;
        if (a1 < 0) {
            a1 = ~a1 + 1;
            flag = true;
        }
        if (b1 < 0) {
            b1 = ~b1 + 1;
            flag = !flag;
        }
        long item = 1;
        long sum = 0;
        long c = a1;
        while (item <= b1) {
            if ((item & b1) == item) {
                sum += c;
            }
            item += item;
            c += c;
        }
        return flag ? ~sum + 1 : sum;
    }

}
