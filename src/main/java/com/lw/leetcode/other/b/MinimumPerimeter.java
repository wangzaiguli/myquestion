package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 1954. 收集足够苹果的最小花园周长
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/17 10:26
 */
public class MinimumPerimeter {

    public static void main(String[] args) {
        MinimumPerimeter test = new MinimumPerimeter();

        // 5040
//        long n = 1000000000;

        // 232
        long n = 100000;

        // 16
//        long n = 13;

        // 8
//        long n = 1;

        // 8
//        long n = 1;

        long l = test.minimumPerimeter(n);
        System.out.println(l);
    }

    public long minimumPerimeter(long neededApples) {
        long sum = 12;
        long n = 2;
        while (sum < neededApples) {
            n += 2;
            sum += (n * n * 3);
        }
        return n << 2;
    }

}
