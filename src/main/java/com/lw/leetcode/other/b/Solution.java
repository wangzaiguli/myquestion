package com.lw.leetcode.other.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 478. 在圆内随机生成点
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/8 14:47
 */
public class Solution {

    public static void main(String[] args) {


        double a = 0.01;
        double b = -73839.1;
        double c = -3289891.3;
        Solution test = new Solution(a,b,c);



        for (int i = 0; i < 100; i++) {

            double[] doubles = test.randPoint();

            System.out.println(Arrays.toString(doubles));
        }

    }

    private double radius;
    private double x_center;
    private double y_center;

    public Solution(double radius, double x_center, double y_center) {
        this.radius = radius;
        this.x_center = x_center;
        this.y_center = y_center;
    }

    public double[] randPoint() {
        double v = Math.random()  * Math.PI * 2 ;
        double d = radius * Math.sqrt(Math.random());
        return new double[]{Math.cos(v)* d + x_center, Math.sin(v)* d + y_center};
    }
}
