package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 2249. 统计圆内格点数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/26 11:05
 */
public class CountLatticePoints {

    public int countLatticePoints(int[][] circles) {
        int[][] arrs = new int[201][201];
        for (int[] circle : circles) {
            int l = circle[0] - circle[2];
            int r = circle[0] + circle[2];
            for (int x = l; x <= r; x++) {
                int d = circle[1] - circle[2];
                int u = circle[1] + circle[2];
                for (int y = d; y <= u; y++) {
                    if (inCircle(x, y, circle)) {
                        arrs[x][y] = 1;
                    }
                }
            }
        }
        int res = 0;
        for (int[] arr : arrs) {
            for (int v : arr) {
                res += v;
            }
        }
        return res;
    }

    private boolean inCircle(int x, int y, int[] circle) {
        return (circle[0] - x) * (circle[0] - x) + (circle[1] - y) * (circle[1] - y) <= circle[2] * circle[2];
    }

}
