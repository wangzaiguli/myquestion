package com.lw.leetcode.other.b;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * other
 * 1352. 最后 K 个数的乘积
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/11 13:22
 */
public class ProductOfNumbers {

    public static void main(String[] args) {
        ProductOfNumbers test = new ProductOfNumbers();
        System.out.println(Integer.MAX_VALUE);
    }

    private List<Long> list;
    private int index = -1;
    private int count = 1;
    private long item = 1L;

    public ProductOfNumbers() {
        list = new ArrayList<>();
        list.add(1L);
    }

    public void add(int num) {
        if (num == 0) {
            index = count;
            list.add(list.get(count - 1));
        } else {
            long l = list.get(count - 1) * num;
            if (l > Integer.MAX_VALUE) {
                item = l;
                l = 1;
            }
            list.add(l);
        }
        count++;
    }

    public int getProduct(int k) {
        if (count - k <= index) {
            return 0;
        }
        long a = list.get(count - 1);
        long b = list.get(count - k - 1);

        if (b > a) {
            return (int) (a * item / b);
        }
        return (int) (a / b);
    }
}
