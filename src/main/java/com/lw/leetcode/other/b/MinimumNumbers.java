package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * other
 * 5218. 个位数字为 K 的整数之和
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/20 13:23
 */
public class MinimumNumbers {

    public int minimumNumbers(int num, int k) {
        if (num == 0 ) {
            return 0;
        }
        if (num < k) {
            return -1;
        }
        int a = num % 10;
        if (a == k) {
            return 1;
        }
        for (int i = 1; i <= num && k * i <= num + 1; i++) {
            if (a == (k * i) % 10) {
                int v = num - k * (i - 1);
                if (v >= 0 && v % 10 == k ) {
                    return i;
                }
            }
        }
        return -1;
    }

}
