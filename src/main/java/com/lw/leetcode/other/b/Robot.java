package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 2069. 模拟行走机器人 II
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/7 10:29
 */
public class Robot {

    private long sum = 0;
    private int width;
    private int height;
    private int all;

    public Robot(int width, int height) {
        this.width = width;
        this.height = height;
        all = (width + height - 2) << 1;
    }

    public void step(int num) {
        sum += num;
    }

    public int[] getPos() {
        int[] arr = new int[2];
        if (sum == 0) {
            return arr;
        }
        int t = (int) (sum % all);
        if (t >= 1 && t <= width - 1) {
            arr[0] = t;
            return arr;
        }
        if (t >= width && t <= width + height - 2) {
            arr[0] = width - 1;
            arr[1] = t - width + 1;
            return arr;
        }
        if (t >= width + height - 1 && t <= (width << 1) + height - 3) {
            arr[0] = (width << 1) + height - 3 - t;
            arr[1] = height - 1;
            return arr;
        }
        arr[1] = t == 0 ? 0 : all - t;
        return arr;
    }

    public String getDir() {
        if (sum == 0) {
            return "East";
        }
        int t = (int) (sum % all);
        if (t >= 1 && t <= width - 1) {
            return "East";
        }
        if (t >= width && t <= width + height - 2) {
            return "North";
        }
        if (t >= width + height - 1 && t <= (width << 1) + height - 3) {
            return "West";
        }
        return "South";
    }
}
