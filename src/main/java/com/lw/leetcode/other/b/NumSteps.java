package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 1404. 将二进制表示减到 1 的步骤数
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/16 11:37
 */
public class NumSteps {

    public static void main(String[] args) {
        NumSteps test = new NumSteps();

        // 5
        String str = "101";

        // 3
//        String str = "11";

        // 2
//        String str = "100";

        int i = test.numSteps(str);
        System.out.println(i);
    }

    public int numSteps(String s) {
        int item = 0;
        int sum = 0;
        int length = s.length();
        for (int i = length - 1; i > 0; i--) {
            char c = s.charAt(i);
            if ((c == '0' && item == 0 ) ||(c == '1' && item == 1 )  ) {
                sum++;
            } else {
                sum += 2;
                item = 1;
            }
        }
        if (item == 1) {
            sum++;
        }
        return sum;
    }
}
