package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 2120. 执行所有后缀指令
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/24 14:23
 */
public class ExecuteInstructions {

    public int[] executeInstructions(int n, int[] startPos, String s) {
        int x = startPos[0];
        int y = startPos[1];
        int len = s.length();
        int[] ans = new int[len];
        char[] chars = s.toCharArray();
        for (int i = 0; i < len; i++) {
            ans[i] = getStepCount(n, x, y, chars, i);
        }
        return ans;
    }

    public int getStepCount(int n, int x, int y, char[] chars, int st) {
        int step = 0;
        int length = chars.length;
        for (int i = st; i < length; i++) {
            switch (chars[i]) {
                case 'U':
                    x--;
                    break;
                case 'D':
                    x++;
                    break;
                case 'R':
                    y++;
                    break;
                default:
                    y--;
                    break;
            }
            if (x < 0 || x >= n || y < 0 || y >= n) {
                return step;
            }
            step++;
        }
        return step;
    }
}
