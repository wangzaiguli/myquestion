package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 1860. 增长的内存泄露
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/16 10:25
 */
public class MemLeak {
    public int[] memLeak(int memory1, int memory2) {
        for (int i = 1; ; i++) {
            if (i > memory1 && i > memory2) {
                return new int[]{i, memory1, memory2};
            }
            if (memory2 > memory1) {
                memory2 -= i;
            } else {
                memory1 -= i;
            }
        }
    }
}
