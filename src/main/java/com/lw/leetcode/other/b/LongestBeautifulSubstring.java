package com.lw.leetcode.other.b;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 1839. 所有元音按顺序排布的最长子字符串
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/25 13:32
 */
public class LongestBeautifulSubstring {


    public static void main(String[] args) {
        LongestBeautifulSubstring test = new LongestBeautifulSubstring();

        // 13
        String str = "aeiaaioaaaaeiiiiouuuooaauuaeiu";
        // 13
//        String str = "aeiaaioaaaaeiiiiouuu";

        // 13
//        String str = "aaaaeiiiiouuuooaauuaeiu";

        // 5
//        String str = "aeeeiiiioooauuuaeiou";


        int i = test.longestBeautifulSubstring(str);

        System.out.println(i);
    }

    private char[] arr;

    public int longestBeautifulSubstring(String word) {
        Map<Character, Integer> map = new HashMap<>();
        map.put('a', 1);
        map.put('e', 2);
        map.put('i', 3);
        map.put('o', 4);
        map.put('u', 5);
        int length = word.length();
        char[] arr = word.toCharArray();
        this.arr = arr;
        int i = 0;
        int j = 0;
        int max = 0;
        while (j < length) {
            i = find(i);
            if (i == -1) {
                break;
            }
            j = i + 1;

            while (j < length) {
                if (arr[j] == arr[j - 1] || map.get(arr[j - 1]) + 1 == map.get(arr[j])) {
                    j++;
                } else {
                    if (arr[j - 1] == 'u') {
                        max = Math.max(max, j - i);
                    }
                    i = j;
                    break;
                }
            }
        }
        if (i != -1 && arr[j - 1] == 'u') {
            max = Math.max(max, j - i);
        }
        return max;
    }

    private int find(int i) {
        int length = arr.length;
        while (i < length) {
            if (arr[i] == 'a') {
                return i;
            }
            i++;
        }
        return -1;
    }

}
