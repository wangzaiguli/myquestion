package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * other
 * 1344. 时钟指针的夹角
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/13 15:48
 */
public class AngleClock {

    public static void main(String[] args) {
        AngleClock test = new AngleClock();
        int a = 12;
        int b = 30;
        double v = test.angleClock(a, b);
        System.out.println(v);
    }

    public double angleClock(int hour, int minutes) {
        hour = hour == 12 ? 0 : hour;
        double a = 0.5 * minutes + hour * 30;

        double b = minutes * 6;
        double c = a > b ? a - b : b - a;
        return c >= 180 ? 360 - c : c;
    }

}
