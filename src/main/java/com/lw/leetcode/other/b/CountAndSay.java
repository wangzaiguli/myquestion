package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 38. 外观数列
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/15 8:59
 */
public class CountAndSay {
    public String countAndSay(int n) {
        String str = "1";
        if (n < 2) {
            return str;
        } else {
            return find(str, n, 1);
        }
    }

    public String find(String str, int n, int count) {
        if (n == count) {
            return str;
        } else {
            count++;
            char[] chars = str.toCharArray();
            int sl = 1;
            char zf = chars[0];
            StringBuilder sb = new StringBuilder();
            for (int i = 1; i < chars.length; i++) {
                if (chars[i] == zf) {
                    sl++;
                } else {
                    sb.append(sl);
                    sb.append(zf);
                    zf = chars[i];
                    sl = 1;
                }
            }
            sb.append(sl);
            sb.append(zf);
            return find(sb.toString(), n, count);
        }
    }
}
