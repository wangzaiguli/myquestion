package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 1401. 圆和矩形是否有重叠
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/16 11:24
 */
public class CheckOverlap {

    public static void main(String[] args) {
        CheckOverlap test = new CheckOverlap();

        // true
        boolean b = test.checkOverlap(2, 8, 6, 5, 1, 10, 4);

        System.out.println(b);
    }


    public boolean checkOverlap(int radius, int xCenter, int yCenter, int x1, int y1, int x2, int y2) {
        boolean xin = xCenter >= x1 && xCenter <= x2;
        boolean yin = yCenter <= y2 && yCenter >= y1;
        if (xin && yin) {
            return true;
        }
        long xmin = Math.min(Math.abs(x1 - xCenter), Math.abs(x2 - xCenter));
        long ymin = Math.min(Math.abs(y1 - yCenter), Math.abs(y2 - yCenter));
        if (xmin * xmin + ymin * ymin <= (long) radius * radius) {
            return true;
        }
        return (xmin <= radius && yin) || (ymin <= radius && xin);
    }

}
