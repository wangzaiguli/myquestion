package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 2079. 给植物浇水
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/27 20:30
 */
public class WateringPlants {

    public static void main(String[] args) {
        WateringPlants test = new WateringPlants();

        // 14
//        int[] arr = {2, 2, 3, 3};
//        int n = 5;

        // 30
//        int[] arr = {1, 1, 1, 4, 2, 3};
//        int n = 4;

        // 49
        int[] arr = {7, 7, 7, 7, 7, 7, 7};
        int n = 8;

        int i = test.wateringPlants(arr, n);
        System.out.println(i);
    }


    public int wateringPlants(int[] plants, int capacity) {
        int length = plants.length;
        int sum = 0;
        int item = capacity;
        for (int i = 0; i < length; i++) {
            int v = plants[i];
            item -= v;
            sum++;
            if (item < 0) {
                item = capacity - v;
                sum += (i << 1);
            }
        }
        return sum;
    }

}
