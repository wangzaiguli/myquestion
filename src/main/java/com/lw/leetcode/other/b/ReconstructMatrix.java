package com.lw.leetcode.other.b;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 1253. 重构 2 行二进制矩阵
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/19 17:02
 */
public class ReconstructMatrix {

    public static void main(String[] args) {
        ReconstructMatrix test = new ReconstructMatrix();

        // [[1,1,0],[0,0,1]]
//        int a = 2;
//        int b = 1;
//        int[] arr = {1, 1, 1};

        // []
//        int a = 2;
//        int b = 3;
//        int[] arr = {2, 2, 1, 1};

        // [[1,1,1,0,1,0,0,1,0,0],[1,0,1,0,0,0,1,1,0,1]]
        int a = 5;
        int b = 5;
        int[] arr = {2, 1, 2, 0, 1, 0, 1, 2, 0, 1};

        List<List<Integer>> lists = test.reconstructMatrix(a, b, arr);
        System.out.println(lists);
    }

    public List<List<Integer>> reconstructMatrix(int upper, int lower, int[] colsum) {
        int length = colsum.length;
        List<List<Integer>> list = new ArrayList<>(2);
        List<Integer> a = new ArrayList<>(length);
        List<Integer> b = new ArrayList<>(length);
        for (int i = 0; i < length; i++) {
            a.add(0);
            b.add(0);
        }
        list.add(a);
        list.add(b);
        for (int i = 0; i < length; i++) {
            if (colsum[i] == 2) {
                upper--;
                lower--;
                list.get(0).set(i, 1);
                list.get(1).set(i, 1);
            }
        }
        for (int i = 0; i < length; i++) {
            if (colsum[i] != 1) {
                continue;
            }
            if (upper <= 0 && lower <= 0) {
                return new ArrayList<>();
            }

            if (upper > 0) {
                list.get(0).set(i, 1);
                upper--;
            } else {
                list.get(1).set(i, 1);
                lower--;
            }
        }
        if (upper != 0 || lower != 0) {
            return new ArrayList<>();
        }
        return list;
    }

}
