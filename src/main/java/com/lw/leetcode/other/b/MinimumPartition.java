package com.lw.leetcode.other.b;

/**
 * 6196. 将字符串分割成值不超过 K 的子字符串
 *
 */
public class MinimumPartition {

    public int minimumPartition(String s, int k) {
        long item = 0;
        char[] arr = s.toCharArray();
        int count = 1;
        for (char c : arr) {
            int t = c - '0';
            if (t > k) {
                return -1;
            }
            item = item * 10 + t;
            if (item > k) {
                item = t;
                count++;
            }
        }
        return count;
    }

}
