package com.lw.leetcode.other.b;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * 672. 灯泡开关 Ⅱ
 *
 * @author liw
 * @version 1.0
 * @date 2021/8/13 16:20
 */
public class FlipLights {

    public static void main(String[] args) {
        FlipLights test = new FlipLights();

        for (int i = 0; i <= 1000; i++) {
            for (int j = 0; j <= 1000; j++) {
                int a = test.flipLights(i, j);
                int b = test.flipLights2(i, j);
                if (a != b) {

                    System.out.println("" + i + "," + j + ": " + "  " + a + "  " + b);
                }

            }
        }

        System.out.println("OK");

    }

    public int flipLights(int n, int presses) {
        if (n == 0 || presses == 0) {
            return 1;
        }
        int[] arr;
        if (presses == 1) {
            arr = new int[]{2, 3, 4, 4};
        } else if (presses == 2) {
            arr = new int[]{2, 4, 7, 7};
        } else {
            arr = new int[]{2, 4, 8, 8};
        }
        if (n < 4) {
            return arr[n - 1];
        }
        return arr[3];
    }


    public int flipLights2(int n, int m) {
        Set<Integer> seen = new HashSet();
        n = Math.min(n, 6);
        int shift = Math.max(0, 6-n);
        for (int cand = 0; cand < 16; ++cand) {
            int bcount = Integer.bitCount(cand);
            if (bcount % 2 == m % 2 && bcount <= m) {
                int lights = 0;
                if (((cand >> 0) & 1) > 0) lights ^= 0b111111 >> shift;
                if (((cand >> 1) & 1) > 0) lights ^= 0b010101 >> shift;
                if (((cand >> 2) & 1) > 0) lights ^= 0b101010 >> shift;
                if (((cand >> 3) & 1) > 0) lights ^= 0b100100 >> shift;
                seen.add(lights);
            }
        }
        return seen.size();
    }


}
