package com.lw.leetcode.other.b;

import com.lw.test.util.Utils;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * 6205. 反转之后不同整数的数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/17 10:22
 */
public class CountDistinctIntegers {

    public static void main(String[] args) {
        CountDistinctIntegers test = new CountDistinctIntegers();

        int[] arr = Utils.getArr(10000, 1, 1000000);

        int i = test.countDistinctIntegers(arr);
        System.out.println(i);

    }

    public int countDistinctIntegers(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int num : nums) {
            set.add(num);
            set.add(find(num));
        }
        return set.size();
    }

    private int find(int num) {
        int v = 0;
        while (num != 0) {
            v = v * 10 + num % 10;
            num /= 10;
        }
        return v;
    }

}
