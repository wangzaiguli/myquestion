package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 537. 复数乘法
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/8 21:49
 */
public class ComplexNumberMultiply {
    public String complexNumberMultiply(String a, String b) {
        String[] strs1 = a.split("\\+|i");
        String[] strs2 = b.split("\\+|i");
        int a1 = Integer.parseInt(strs1[0]);
        int a2 = Integer.parseInt(strs1[1]);
        int b1 = Integer.parseInt(strs2[0]);
        int b2 = Integer.parseInt(strs2[1]);
        return (a1 * b1 - a2 * b2) + "+" + (a1 * b2 + a2 * b1) + "i";
    }


}
