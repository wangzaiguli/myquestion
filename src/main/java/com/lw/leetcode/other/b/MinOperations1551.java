package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * other
 * 1551. 使数组中所有元素相等的最小操作数
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/26 9:15
 */
public class MinOperations1551 {
    public int minOperations(int n) {
        return (n * n) >> 2;
    }
}
