package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 1739. 放置盒子
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/20 21:31
 */
public class MinimumBoxes {


    public static void main(String[] args) {
        MinimumBoxes test = new MinimumBoxes();
        for (int i = 1; i < 10000; i++) {
            int k = test.minimumBoxes(i);
            System.out.println(i + "   " + k);
        }
    }

    public int minimumBoxes(int n) {
        if (n < 4) {
            return n;
        }
        int a = 1;
        int b = 1;
        int c = 1;
        while (c <= n) {
            a++;
            b += a;
            c += b;
        }
        n -= c - b;
        b -= a;
        int d = (int) (Math.pow(8 * n + 1D, 0.5) - 1) >> 1;
        return ((d + 1) * d) >> 1 == n ? b + d : b + d + 1;
    }

}
