package com.lw.leetcode.other.b;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * other
 * 1604. 警告一小时内使用相同员工卡大于等于三次的人
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/10 17:12
 */
public class AlertNames {

    public static void main(String[] args) {
        AlertNames test = new AlertNames();
        String[] a = {"daniel", "daniel", "daniel", "luis", "luis", "luis", "luis"};
        String[] b = {"10:00", "10:40", "11:00", "09:00", "11:00", "13:00", "15:00"};
        List<String> list = test.alertNames(a, b);
        System.out.println(list);
    }


    public List<String> alertNames(String[] keyName, String[] keyTime) {
        List<String> ans = new ArrayList<>();
        int n = keyName.length;
        int[] times = new int[n];
        for (int i = 0; i < n; i++) {
            times[i] = parse(keyTime[i]);
        }
        Map<String, List<Integer>> map = new HashMap<>();
        for (int i = 0; i < n; i++) {
            map.computeIfAbsent(keyName[i], v -> new ArrayList<>()).add(times[i]);
        }
        for (Map.Entry<String, List<Integer>> entry : map.entrySet()) {
            List<Integer> value = entry.getValue();
            if (value.size() < 3) {
                continue;
            }
            Collections.sort(value);
            int size = value.size();
            for (int i = 0; i < size - 2; i++) {
                if (value.get(i + 2) - value.get(i) <= 60) {
                    ans.add(entry.getKey());
                    break;
                }
            }
        }
        Collections.sort(ans);
        return ans;
    }

    private int parse(String time) {
        String[] split = time.split(":");
        int hour = Integer.valueOf(split[0]);
        int minute = Integer.valueOf(split[1]);
        return hour * 60 + minute;
    }

}
