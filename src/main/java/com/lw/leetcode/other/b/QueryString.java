package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 1016. 子串能表示从 1 到 N 数字的二进制串
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/13 22:53
 */
public class QueryString {

    public boolean queryString(String s, int n) {
        for (int i = 1; i <= n; i++) {
            if (!s.contains(Integer.toBinaryString(i))) {
                return false;
            }
        }
        return true;
    }

}
