package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 1375. 二进制字符串前缀一致的次数
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/15 17:27
 */
public class NumTimesAllBlue {

    public int numTimesAllBlue(int[] light) {
        int length = light.length;
        int max = 0;
        int count = 0;
        for (int i = 0; i < length; i++) {
            max = Math.max(max, light[i]);
            if (max == i + 1) {
                count++;
            }
        }
        return count;
    }

}
