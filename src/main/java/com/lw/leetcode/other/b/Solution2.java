package com.lw.leetcode.other.b;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/27 10:33
 */
public class Solution2 {

    public static void main(String[] args) {

        int a = 3;
        int b = 1;

        Solution2 test = new Solution2(a, b);
        int[] flip = test.flip();
        System.out.println(Arrays.toString(flip));
        flip = test.flip();
        System.out.println(Arrays.toString(flip));
        flip = test.flip();
        System.out.println(Arrays.toString(flip));
        test.reset();
        flip = test.flip();
        System.out.println(Arrays.toString(flip));

    }

    private Map<Integer, Integer> map;
    private int count;
    private int m;
    private int n;

    private Random random = new Random();

    public Solution2(int m, int n) {
        this.map = new HashMap<>();
        this.count = m * n;
        this.m = m;
        this.n = n;
    }


    public int[] flip() {
        int i = random.nextInt(count--);
        int value = map.getOrDefault(i, i);
        map.put(i, map.getOrDefault(count, count));
        return new int[]{value / n, value % n};
    }


    public void reset() {
        this.map.clear();
        this.count = n * m;
    }

}
