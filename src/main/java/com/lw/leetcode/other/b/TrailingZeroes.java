package com.lw.leetcode.other.b;

/**
 * 172. 阶乘后的零
 * 面试题 16.05. 阶乘尾数
 *
 * @Author liw
 * @Date 2021/5/8 16:43
 * @Version 1.0
 */
public class TrailingZeroes {
    public int trailingZeroes(int n) {
        int sum = 0;
        while (n != 0) {
            n /= 5;
            sum += n;
        }
        return sum;
    }
}
