package com.lw.leetcode.other.b;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * create by idea
 *2013. 检测正方形
 * @author lmx
 * @version 1.0
 * @date 2022/1/26 20:52
 */
public class DetectSquares {

    private Map<Integer, Map<Integer, Integer>> map;

    public DetectSquares() {
        map = new HashMap<Integer, Map<Integer, Integer>>();
    }

    public void add(int[] point) {
        int x = point[0], y = point[1];
        map.putIfAbsent(y, new HashMap<>());
        Map<Integer, Integer> yCnt = map.get(y);
        yCnt.put(x, yCnt.getOrDefault(x, 0) + 1);
    }

    public int count(int[] point) {
        int res = 0;
        int x = point[0], y = point[1];
        if (!map.containsKey(y)) {
            return 0;
        }
        Map<Integer, Integer> yCnt = map.get(y);
        Set<Map.Entry<Integer, Map<Integer, Integer>>> entries = map.entrySet();
        for (Map.Entry<Integer, Map<Integer, Integer>> entry : entries) {
            int col = entry.getKey();
            Map<Integer, Integer> colCnt = entry.getValue();
            if (col != y) {
                // 根据对称性，这里可以不用取绝对值
                int d = col - y;
                res += colCnt.getOrDefault(x, 0) * yCnt.getOrDefault(x + d, 0) * colCnt.getOrDefault(x + d, 0);
                res += colCnt.getOrDefault(x, 0) * yCnt.getOrDefault(x - d, 0) * colCnt.getOrDefault(x - d, 0);
            }
        }
        return res;
    }

}
