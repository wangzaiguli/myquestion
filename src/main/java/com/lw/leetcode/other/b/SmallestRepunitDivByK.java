package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 1015. 可被 K 整除的最小整数
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/13 22:40
 */
public class SmallestRepunitDivByK {

    public static void main(String[] args) {
        SmallestRepunitDivByK test = new SmallestRepunitDivByK();

        // 6173
        int a = 12347;

        int i = test.smallestRepunitDivByK(a);
        System.out.println(i);
    }

    public int smallestRepunitDivByK(int k) {
        if (k % 2 == 0 || k % 5 == 0) {
            return -1;
        }
        int ans = 1;
        int temp = 1 % k;
        while (temp != 0) {
            temp = temp * 10 + 1;
            temp = temp % k;
            ans++;
        }
        return ans;
    }
}
