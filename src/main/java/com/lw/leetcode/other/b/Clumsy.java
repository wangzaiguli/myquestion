package com.lw.leetcode.other.b;

/**
 * other
 * 1006. 笨阶乘
 *
 * @Author liw
 * @Date 2021/8/8 10:29
 * @Version 1.0
 */
public class Clumsy {

    public static void main(String[] args) {
        Clumsy test = new Clumsy();
        int clumsy = test.clumsy(10);
        System.out.println(clumsy);
    }

    public int clumsy(int n) {
        long num = 0;
        if (n > 4) {
            num = n * (n - 1) / (n - 2) + n - 3;
            int i;
            for (i = n - 4; i > 4; i = i - 4) {
                num -= i * (i - 1) / (i - 2) - i + 3;
            }
            if (i < 3) {
                return (int) (num - i);
            }
            if (i == 3) {
                return (int) (num - 6);
            }
            return (int) (num - 5);
        } else {
            if (n < 3) {
                return n;
            }
            if (n == 3) {
                return 6;
            }
            return 7;
        }
    }
}
