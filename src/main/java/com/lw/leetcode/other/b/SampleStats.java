package com.lw.leetcode.other.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 1093. 大样本统计
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/12 14:43
 */
public class SampleStats {

    public static void main(String[] args) {
        SampleStats test = new SampleStats();

        // [66.0,124.0,86.75,78.5,66.0]
//        int[] arr = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        int[] arr = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3510,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6718,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,27870,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,35,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,26256,0,0,0,0,9586565,0,0,0,0,0,0,0,2333,0,0,0,0};

        double[] doubles = test.sampleStats(arr);

        System.out.println(Arrays.toString(doubles));

    }

    public double[] sampleStats(int[] count) {
        int min = 0;
        int max = 255;
        for (int i = 0; i < 256; i++) {
            if (count[i] != 0) {
                min = i;
                break;
            }
        }
        for (int i = 255; i >= 0; i--) {
            if (count[i] != 0) {
                max = i;
                break;
            }
        }
        long sum = 0L;
        int c = 0;
        int m = 0;
        int k = 0;
        for (int i = min; i <= max; i++) {
            long item = count[i];
            if (item != 0) {
                c += item;
                if (item > m) {
                    m = (int) item;
                    k = i;
                }
                m = (int) Math.max(m, item);
                sum = sum + (item * i);
            }
        }
        double a =  sum / (double)c;
        if ((c & 1) == 0) {
            int m1 = c >> 1;
            int m2 = m1 + 1;
            int t1 = -1;
            int t2 = -1;
            int n = 0;
            for (int i = min; i <= max; i++) {
                if (t1 == -1 && m1 > n && m1 <= n + count[i]) {
                    t1 = i;
                }
                if (m2 > n && m2 <= n + count[i]) {
                    t2 = i;
                    break;
                }
                n += count[i];
            }
            return new double[]{min, max, a, (double) (t1 + t2)/2, k};
        } else {
            int m1 = (c + 1) >> 1;
            int n = 0;
            for (int i = min; i <= max; i++) {
                if (m1 > n && m1 <= n + count[i]) {
                    return new double[]{min, max, a, i, k};
                }
                n += count[i];
            }
        }
        return null;
    }
}