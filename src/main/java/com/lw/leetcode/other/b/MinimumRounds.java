package com.lw.leetcode.other.b;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *6071. 完成所有任务需要的最少轮数
 * @author liw
 * @version 1.0
 * @date 2022/4/18 9:12
 */
public class MinimumRounds {

    public int minimumRounds(int[] tasks) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int task : tasks) {
            map.merge(task, 1, (a, b) -> a + b);
        }
        int sum = 0;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue()  == 1) {
                return -1;
            }
            sum += (entry.getValue() + 2) / 3;
        }
        return sum;
    }

}
