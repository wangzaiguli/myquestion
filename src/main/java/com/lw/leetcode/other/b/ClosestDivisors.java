package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 1362. 最接近的因数
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/28 12:08
 */
public class ClosestDivisors {

    public int[] closestDivisors(int num) {
        int[] res1 = find(num + 1);
        int[] res2 = find(num + 2);
        return Math.abs(res1[0] - res1[1]) < Math.abs(res2[0] - res2[1]) ? res1 : res2;
    }

    private int[] find(int sum) {
        int num1 = (int) Math.sqrt(sum);
        while (true) {
            if (sum % num1 == 0) {
                int num2 = sum / num1;
                return new int[]{num1, num2};
            } else {
                num1--;
            }
        }
    }

}
