package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 1899. 合并若干三元组以形成目标三元组
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/25 9:55
 */
public class MergeTriplets {

    public boolean mergeTriplets(int[][] triplets, int[] target) {
        boolean a = false;
        boolean b = false;
        boolean c = false;
        int t1 = target[0];
        int t2 = target[1];
        int t3 = target[2];
        for (int[] arr : triplets) {
            if (arr[0] > t1 || arr[1] > t2 || arr[2] > t3) {
                continue;
            }
            a = a || arr[0] == t1;
            b = b || arr[1] == t2;
            c = c || arr[2] == t3;
        }
        return a && b && c;
    }

}
