package com.lw.leetcode.other.b;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * other
 * 1357. 每隔 n 个顾客打折
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/25 18:34
 */
public class Cashier {

    private int n;
    private int temp = 0;
    private int discount;
    private HashMap<Integer, Integer> map = new HashMap<>();

    public Cashier(int n, int discount, int[] products, int[] prices) {
        this.n = n;
        this.discount = discount;
        for (int i = 0; i < products.length; i++) {
            map.put(products[i], prices[i]);
        }
    }

    public double getBill(int[] product, int[] amount) {
        temp++;
        double sum = 0;
        for (int i = 0; i < product.length; i++) {
            sum += map.get(product[i]) * amount[i];
        }
        if (temp == n) {
            temp = 0;
            sum = sum * (100 - discount) / 100.0D;
        }
        return sum;
    }


}
