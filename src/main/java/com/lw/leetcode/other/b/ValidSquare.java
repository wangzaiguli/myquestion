package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 593. 有效的正方形
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/8 17:02
 */
public class ValidSquare {

    public static void main(String[] args) {
        ValidSquare test = new ValidSquare();
        // [0,0]
        //[5,0]
        //[5,4]
        //[0,4]
    }

    public boolean validSquare(int[] p1, int[] p2, int[] p3, int[] p4) {
        if (find(p1, p2, p3, p4)) {
            if (find(p1, p3, p2, p4)) {
                return find2(p1, p4, p2, p3) && find2(p1, p2, p1, p3);
            } else if (find(p1, p4, p2, p3)) {
                return find2(p1, p3, p2, p4) && find2(p1, p2, p1, p4);
            }

        } else if (find(p1, p3, p2, p4)) {
            if (find(p1, p4, p2, p3)) {
                return find2(p1, p2, p3, p4) && find2(p1, p3, p1, p4);
            }
        }
        return false;
    }

    private boolean find(int[] p1, int[] p2, int[] p3, int[] p4) {
        int a = p1[0] - p2[0];
        int b = p1[1] - p2[1];
        int c = p3[0] - p4[0];
        int d = p3[1] - p4[1];
        return a * d == b * c;
    }

    private boolean find2(int[] p1, int[] p2, int[] p3, int[] p4) {
        int a = p1[0] - p2[0];
        int b = p1[1] - p2[1];
        int c = p3[0] - p4[0];
        int d = p3[1] - p4[1];
        a = a * a + b * b;
        c = c * c + d * d;
        if (a == 0 || c == 0) {
            return false;
        }
        return a == c;
    }
}
