package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 400. 第 N 位数字
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/30 9:04
 */
public class FindNthDigit {

    public static void main(String[] args) {
        FindNthDigit test = new FindNthDigit();

        // 4
//        int n = 564;

        // 4
//        int n = 11;

        // 1
        int n = 1000000000;

        int nthDigit = test.findNthDigit(n);
        System.out.println(nthDigit);
    }

    public int findNthDigit(int n) {
        long nn = n;
        long item = 9L;
        int count = 1;
        while (nn > 0) {
            nn = nn - item * count;
            item *=10;
            count++;
        }
        if (nn == 0) {
            return 9;
        }
        count--;
        item /= 10;
        nn = nn + item * count - 1;
        return String.valueOf(nn / count + item / 9).charAt((int) (nn % count)) - '0';
    }
}
