package com.lw.leetcode.other.b;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * 1557. 可以到达所有点的最少点数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/18 14:22
 */
public class FindSmallestSetOfVertices {

    public List<Integer> findSmallestSetOfVertices(int n, List<List<Integer>> edges) {
        Set<Integer> set = new HashSet<>();
        for (List<Integer> edge : edges) {
            set.add(edge.get(1));
        }
        List<Integer> list = new ArrayList<>(n - set.size());
        for (int i = 0; i < n; i++) {
            if (!set.contains(i)) {
                list.add(i);
            }
        }
        return list;
    }

}
