package com.lw.leetcode.other.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * other
 * 869. 重新排序得到 2 的幂
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/10 16:51
 */
public class ReorderedPowerOf2 {

    public static void main(String[] args) {
        ReorderedPowerOf2 test = new ReorderedPowerOf2();
        int n = 46;

        boolean b = test.reorderedPowerOf2(n);
        System.out.println(b);
    }

    public boolean reorderedPowerOf2(int n) {
        int[] arr = find(n);
        for (int i = 0; i < 31; ++i) {
            if (Arrays.equals(arr, find(1 << i))) {
                return true;
            }
        }
        return false;
    }

    private int[] find(int n) {
        int[] ans = new int[10];
        while (n > 0) {
            ans[n % 10]++;
            n /= 10;
        }
        return ans;
    }

}
