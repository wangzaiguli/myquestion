package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * other
 * 858. 镜面反射
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/10 16:04
 */
public class MirrorReflection {

    public static void main(String[] args) {
        MirrorReflection test = new MirrorReflection();
        int a = 4;
        int b = 4;
        int i = test.mirrorReflection(a, b);
        System.out.println(i);
    }

    public int mirrorReflection(int p, int q) {
        if (q == 0) {
            return 0;
        }
        if (p == q) {
            return 1;
        }
        int c = p * q / find(p, q);
        int a = c / q;
        int b = c / p;
        if ((a & 1) == 0) {
            return 2;
        }
        if ((b & 1) == 0) {
            return 0;
        }
        return 1;
    }

    private int find(int a, int b) {
        while (b != 0) {
            int c = a % b;
            a = b;
            b = c;
        }
        return a;
    }
}
