package com.lw.leetcode.other.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2166. 设计位集
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/9 13:24
 */
public class Bitset {

    public static void main(String[] args) {

        // [null, null, null, null, false, null, null, true, null, 2, "01010"]
        Bitset test = new Bitset(5);
        test.fix(3);
        test.fix(1);
        test.flip();
        boolean all = test.all();
        System.out.println(all);
        test.unfix(0);
        test.flip();
        boolean one = test.one();
        System.out.println(one);
        test.unfix(0);
        int count = test.count;
        System.out.println(count);
        String s = test.toString();
        System.out.println(s);
    }

    private char[] arr;
    private int size;
    private int count;
    private boolean flag;

    public Bitset(int size) {
        this.arr = new char[size];
        Arrays.fill(arr, '0');
        this.flag = true;
        this.size = size;
    }

    public void fix(int idx) {
        if (flag) {
            if (arr[idx] == '0') {
                arr[idx] = '1';
                count++;
            }
        } else {
            if (arr[idx] == '1') {
                arr[idx] = '0';
                count++;
            }
        }
    }

    public void unfix(int idx) {
        if (flag) {
            if (arr[idx] == '1') {
                arr[idx] = '0';
                count--;
            }
        } else {
            if (arr[idx] == '0') {
                arr[idx] = '1';
                count--;
            }
        }
    }

    public void flip() {
        count = size - count;
        flag = !flag;
    }

    public boolean all() {
        return count == size;
    }

    public boolean one() {
        return count > 0;
    }

    public int count() {
        return count;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(size);
        if (flag) {
            for (char c : arr) {
                sb.append(c);
            }
        } else {
            for (char c : arr) {
                sb.append((char) (97 - c));
            }
        }
        return sb.toString();
    }
}
