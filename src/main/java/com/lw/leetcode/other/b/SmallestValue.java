package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * b
 * other
 * 2507. 使用质因数之和替换后可以取到的最小值
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/19 11:19
 */
public class SmallestValue {

    public static void main(String[] args) {
        SmallestValue test = new SmallestValue();

        // 5
//        int n = 8;

        // 5
//        int n = 15;

        // 7
//        int n = 7;

        // 7
//        int n = 21;

        // 7
        int n = 4;

        int i = test.smallestValue(n);
        System.out.println(i);
    }

    public int smallestValue(int n) {
        while (true) {
            int t = find(n);
            if (t == n) {
                return n;
            }
            n = t;
        }
    }

    private int find(int n) {
        int t = (int) Math.pow(n, 0.5);
        int s = 0;
        for (int i = 2; i <= t; i++) {
            while (n % i == 0) {
                n /= i;
                s += i;
            }
            if (n == 1) {
                return s;
            }
        }
        return s + n;
    }

}
