package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * b
 * 2419. 按位与最大的最长子数组
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/9 10:41
 */
public class LongestSubarray {

    public int longestSubarray(int[] nums) {
        int max = 0;
        for (int num : nums) {
            max = Math.max(max, num);
        }
        int count = 0;
        int item = 0;
        for (int num : nums) {
            if (num == max) {
                item++;
                count = Math.max(count, item);
            } else {
                item = 0;
            }
        }
        return Math.max(count, item);
    }

}
