package com.lw.leetcode.other.b;

/**
 * create by idea
 * 面试题 16.04. 井字游戏
 *
 * @author lmx
 * @version 1.0
 * @date 2022/1/3 16:48
 */
public class Tictactoe {

    public String tictactoe(String[] board) {
        int m = board.length;
        boolean hasBlank = false;
        int rowX, rowO, colX, colO;
        for (int i = 0; i < m; i++) {
            rowX = 0;
            rowO = 0;
            colX = 0;
            colO = 0;
            for (int j = 0; j < m; j++) {
                if (board[i].charAt(j) == ' ') {
                    hasBlank = true;
                }
                if (board[i].charAt(j) == 'X') {
                    rowX++;
                } else if (board[i].charAt(j) == 'O') {
                    rowO++;
                }
                if (board[j].charAt(i) == 'X') {
                    colX++;
                } else if (board[j].charAt(i) == 'O') {
                    colO++;
                }
            }
            if (rowX == m || colX == m) {
                return "X";
            }
            if (rowO == m || colO == m) {
                return "O";
            }
        }
        int leftTopX = 0, leftTopO = 0, leftBottomX = 0, leftBottomO = 0;
        for (int i = 0; i < m; i++) {
            if (board[i].charAt(i) == 'X') {
                leftTopX++;
            } else if (board[i].charAt(i) == 'O') {
                leftTopO++;
            }
            if (board[m - 1 - i].charAt(i) == 'X') {
                leftBottomX++;
            } else if (board[m - 1 - i].charAt(i) == 'O') {
                leftBottomO++;
            }
        }
        if (leftTopX == m || leftBottomX == m) {
            return "X";
        }
        if (leftTopO == m || leftBottomO == m) {
            return "O";
        }
        return hasBlank ? "Pending" : "Draw";
    }


}
