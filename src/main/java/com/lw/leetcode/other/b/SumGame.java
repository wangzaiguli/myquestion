package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 1927. 求和游戏
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/11 11:17
 */
public class SumGame {

    public static void main(String[] args) {
        SumGame test = new SumGame();

        // false
        String str = "?3295???";

        // true
//        String str = "25??";

        // true
//        String str = "5023";

        boolean b = test.sumGame(str);
        System.out.println(b);
    }


    public boolean sumGame(String num) {
        int length = num.length();
        int l = length >> 1;
        int count = 0;
        int sum = 0;
        for (int i = 0; i < l; i++) {
            char c = num.charAt(i);
            if (c == '?') {
                count++;
            } else {
                sum += (c - '0');
            }
            c = num.charAt(i + l);
            if (c == '?') {
                count--;
            } else {
                sum -= (c - '0');
            }
        }
        if (count % 2 != 0) {
            return true;
        }
        if (sum == 0) {
            return count != 0;
        }
        if (sum > 0) {
            if (count >= 0) {
                return true;
            }
            return (count >> 1) * 9 != -sum;
        } else {
            if (count <= 0) {
                return true;
            }
            return (count >> 1) * 9 != -sum;
        }
    }
}
