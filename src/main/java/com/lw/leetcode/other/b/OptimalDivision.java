package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 553. 最优除法
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/27 20:12
 */
public class OptimalDivision {

    public String optimalDivision(int[] nums) {
        int length = nums.length;
        StringBuilder sb = new StringBuilder();
        sb.append(nums[0]);
        if (length < 3) {
            for (int i = 1; i < length; i++) {
                sb.append("/");
                sb.append(nums[i]);
            }
            return sb.toString();
        }
        sb.append("/(");
        sb.append(nums[1]);
        for (int i = 2; i < length; i++) {
            sb.append("/");
            sb.append(nums[i]);
        }
        sb.append(")");
        return sb.toString();
    }

}
