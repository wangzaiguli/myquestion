package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 319. 灯泡开关
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/15 9:07
 */
public class BulbSwitch {
    public int bulbSwitch(int n) {
        return (int) Math.sqrt(n);
    }
}
