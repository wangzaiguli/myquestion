package com.lw.leetcode.other.b;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 1414. 和为 K 的最少斐波那契数字数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/27 10:01
 */
public class FindMinFibonacciNumbers {
    public int findMinFibonacciNumbers(int k) {
        int a = 1;
        int b = 1;
        List<Integer> fibo = new ArrayList<>(Arrays.asList(a, b));
        while (a + b <= k) {
            fibo.add(a + b);
            int c = a + b;
            a = b;
            b = c;
        }
        int ans = 0;
        for (int i = fibo.size() - 1; i >= 0; --i) {
            if (k >= fibo.get(i)) {
                ++ans;
                k -= fibo.get(i);
            }
        }
        return ans;
    }

}
