package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 1558. 得到目标数组的最少函数调用次数
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/18 15:15
 */
public class MinOperations {

    public static void main(String[] args) {
        MinOperations test = new MinOperations();

        // 7
//        int[] arr = {3,2,2,4};

        // 3
//        int[] arr = {2,2};

        // 5
//        int[] arr = {1, 5};

        // 6
//        int[] arr = {2,4,5};

        // 8
        int[] arr = {2, 4, 8, 16};

        int i = test.minOperations(arr);
        System.out.println(i);
    }

    public int minOperations(int[] nums) {
        int max = 0;
        int sum = 0;
        for (int num : nums) {
            int c = -1;
            while (num != 0) {
                sum += (num & 1);
                num >>= 1;
                c++;
            }
            max = Math.max(max, c);
        }
        return sum + max;
    }

}
