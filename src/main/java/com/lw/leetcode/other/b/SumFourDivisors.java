package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 1390. 四因数
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/9 13:19
 */
class SumFourDivisors {
    public int sumFourDivisors(int[] nums) {
        int ans = 0;
        a:
        for (int num : nums) {
            int cnt = 2;
            int sum = num + 1;
            for (int i = 2; i * i <= num; ++i) {
                if (num % i == 0) {
                    ++cnt;
                    sum += i;
                    if (i * i != num) {
                        ++cnt;
                        sum += num / i;
                    }
                }
                if (cnt > 4) {
                    continue a;
                }
            }
            if (cnt == 4) {
                ans += sum;
            }
        }
        return ans;
    }
}

