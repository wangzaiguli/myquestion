package com.lw.leetcode.other.b;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 1237. 找出给定方程的正整数解
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/22 17:44
 */
public class FindSolution {
    public List<List<Integer>> findSolution(CustomFunction fun, int z) {
        List<List<Integer>> ans = new ArrayList<>();
        int x = 1;
        int y = 1000;
        while (x <= 1000 && y >= 1) {
            int f = fun.f(x, y);
            if (f > z) {
                --y;
            } else if (f < z) {
                ++x;
            } else {
                ans.add(Arrays.asList(x++, y--));
            }
        }
        return ans;
    }

    interface CustomFunction {
        // Returns f(x, y) for any given positive integers x and y.
        // Note that f(x, y) is increasing with respect to both x and y.
        // i.e. f(x, y) < f(x + 1, y), f(x, y) < f(x, y + 1)
        int f(int x, int y);
    }
}
