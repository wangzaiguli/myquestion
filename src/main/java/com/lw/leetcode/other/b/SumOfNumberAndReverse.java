package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 6219. 反转之后的数字和
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/17 10:12
 */
public class SumOfNumberAndReverse {

    public static void main(String[] args) {
        SumOfNumberAndReverse test = new SumOfNumberAndReverse();

        // false
//        int num = 63;

        // true
//        int num = 181;

        // true
        int num = 443;

        boolean b = test.sumOfNumberAndReverse(num);
        System.out.println(b);
    }

    public boolean sumOfNumberAndReverse(int num) {
        int t = num >> 1;
        for (int i = 0; i <= t; i++) {
            if (find(num -i) == i) {
                return true;
            }
        }
        return false;
    }

    private int find(int num) {
        int v = 0;
        while (num != 0) {
            v = v * 10 + num % 10;
            num /= 10;
        }
        return v;
    }

}
