package com.lw.leetcode.other.b;

/**
 * 12. 整数转罗马数字
 */
public class IntToRoman {
    public String intToRoman(int num) {
        int[] a = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        String[] b = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
        StringBuilder sb = new StringBuilder();
        while (num > 0) {
            for (int i = 0; i < a.length; i++) {
                if (num >= a[i]) {
                    sb.append(b[i]);
                    num -= a[i];
                    break;
                }
            }
        }
        return sb.toString();
    }

}
