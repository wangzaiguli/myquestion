package com.lw.leetcode.other.b;

import java.util.ArrayList;
import java.util.List;

/**
 * 89. 格雷编码
 *
 * @Author liw
 * @Date 2021/4/20 15:05
 * @Version 1.0
 */
public class GrayCode {

    public static void main(String[] args) {
        GrayCode grayCode = new GrayCode();

        List<Integer> integers = grayCode.grayCode3(3);

        System.out.println(integers);
    }

    public List<Integer> grayCode(int n) {
        int a = 2 << n;
        List<Integer> list = new ArrayList<>(a);
        list.add(0);
        if (n == 0) {
            return list;
        }
        int b = 1;
        for (int j = 0; j < n; j++) {

            for (int i = list.size() - 1; i >= 0; i--) {
                list.add(b + list.get(i));
            }
            b = b << 1;
        }
        return list;
    }


    public List<Integer> grayCode3(int n) {
        List<Integer> list = new ArrayList<>((n - 1));
        list.add(0);
        if (n == 0) {
            return list;
        }
        int b = 2;
        for (int j = 1; j < n; j++) {
            for (int i = list.size() - 1; i >= 0; i--) {
                list.add(b + list.get(i));
            }
            b <<= 1;
        }
        for (int i = list.size() - 1; i >= 0; i--) {
            list.add(list.get(i) + 1);
        }
        return list;
    }


}
