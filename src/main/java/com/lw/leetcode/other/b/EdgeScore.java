package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 2374. 边积分最高的节点
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/15 10:05
 */
public class EdgeScore {

    public static void main(String[] args) {
        EdgeScore test = new EdgeScore();

        // 7
//        int[] arr = {1, 0, 0, 0, 0, 7, 7, 5};

        // 0
//        int[] arr = {2, 0, 0, 2};

        // 0
        int[] arr = {1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

        int i = test.edgeScore(arr);
        System.out.println(i);
    }

    public int edgeScore(int[] edges) {
        int length = edges.length;
        long[] sums = new long[length];
        for (int i = 0; i < length; i++) {
            int edge = edges[i];
            sums[edge] += i;
        }
        long max = 0;
        for (long i : sums) {
            max = Math.max(max, i);
        }
        for (int i = 0; i < length; i++) {
            if (sums[i] == max) {
                return i;
            }
        }
        return 0;
    }

}
