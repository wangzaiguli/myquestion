package com.lw.leetcode.other.b;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * other
 * 2125. 银行中的激光束数量
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/9 16:31
 */
public class NumberOfBeams {

    public int numberOfBeams(String[] bank) {
        List<Integer> list = new ArrayList<>();
        for (String s : bank) {
            int count = find(s);
            if (count == 0) {
                continue;
            }
            list.add(count);
        }
        int sum = 0;
        for (int i = 0; i < list.size() - 1; i++) {
            sum += list.get(i) * list.get(i + 1);
        }
        return sum;
    }

    private int find(String num) {
        int len = num.length();
        int count = 0;
        for (int i = 0; i < len; i++) {
            if (num.charAt(i) == '1') {
                count++;
            }
        }
        return count;
    }
}
