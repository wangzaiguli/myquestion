package com.lw.leetcode.other.b;

import com.lw.test.util.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * LCP 63. 弹珠游戏
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/9 17:32
 */
public class BallGame {

    public static void main(String[] args) {
        BallGame test = new BallGame();

        // {{2,1}}
//        int num = 4;
//        String[] plate = {"..E.", ".EOW", "..W."};

        // {{0,1},{1,0},{2,4},{3,2}}
//        int num = 5;
//        String[] plate = {".....", "..E..", ".WO..", "....."};

        // {}
//        int num = 3;
//        String[] plate = {".....", "....O", "....O", "....."};

        // {}
//        int num = 500;
//        String[] plate = {"OEO.WO.OO...EOOOWWO.", "E.OO.EOWW...WE.OWWOW", ".WOE..OWW.EOEEEEOEOE", "OW.E.W.OOWOWWOWEEOEE", "W...OE.E...OWOO.OEEE", "OWOOWWO.OEEEEWOWO.WE", "EW.WWWEEO.EWEE.OWE..", "OW.W.WOE.WE..OOWE..E", "OOOOW.WWOW...WEW.WOE", "WW..E...O...WEEEW.EW", "EOWOWOOEWWOOOEWEEOEW", "E..OWE.O.WOOOOOW..OO", ".OOEE.OEOOOOO.OEW.OE", "EOOEEOW.EOWEEWOEEO.E", "OEO.WOWWEWOOEEOWEEWO", "..OW.WEWEWEO.OWO.E.O", "OW.EWE.OEW.EO.WEOO.O", "WO..OWWOW..OEO.W.OOW", "OO.EWOE.WWWWWEO.OWWE", "EO..E.EOEEEOOOEE.E.E"};

        //
        int num = 1000000;
        System.out.println(num);
        String[] plate = Utils.getStrs(1000, 1000, new char[]{'.', 'O', 'E', 'W'});

        int[][] ints = test.ballGame(num, plate);
        Utils.toPrint(ints);
        System.out.println();

        String path1 = "C:\\lw\\a.txt";
        String path2 = "C:\\lw\\b.txt";
        int[][] as = Utils.getArr2(path1);
        int[][] bs = Utils.getArr2(path2);

        System.out.println();
        System.out.println(as.length);
        System.out.println(bs.length);
        Arrays.sort(as, (a, b) -> a[0] == b[0] ? Integer.compare(a[1], b[1]) : Integer.compare(a[0], b[0]));
        Arrays.sort(bs, (a, b) -> a[0] == b[0] ? Integer.compare(a[1], b[1]) : Integer.compare(a[0], b[0]));

        Utils.toPrint(as);
        Utils.toPrint(bs);
        System.out.println(Utils.compare(as, bs));
    }

    public int[][] ballGame(int num, String[] plate) {
        int m = plate.length;
        int n = plate[0].length();
        int u = 1 << 24;
        int d = 2 << 24;
        int l = 3 << 24;
        int r = 4 << 24;
        LinkedList<Integer> list = new LinkedList<>();
        for (int i = 0; i < m; i++) {
            String s = plate[i];
            for (int j = 0; j < n; j++) {
                if (s.charAt(j) == 'O') {
                    int v = (i << 12) + j;
                    list.addLast(u + v);
                    list.addLast(d + v);
                    list.addLast(l + v);
                    list.addLast(r + v);
                }
            }
        }
        List<Integer> values = new ArrayList<>();
        while (!list.isEmpty() && num > 0) {
            int limit = list.size();
            for (int i = 0; i < limit; i++) {
                int t = list.pollFirst();
                int f = t & 0XF000000;
                int x = (t >> 12) & 0XFFF;
                int y = t & 0XFFF;
                if (f == u) {
                    x--;
                } else if (f == d) {
                    x++;
                } else if (f == l) {
                    y--;
                } else {
                    y++;
                }
                if (x < 0 || y < 0 || x == m || y == n) {
                    continue;
                }
                char c = plate[x].charAt(y);
                if (c == 'O') {
                    continue;
                }
                int v = (x << 12) + y;
                if ((x == 0 || x == m - 1 || y == 0 || y == n - 1) && c == '.') {
                    if ((x == 0 && y == 0) || (x == m - 1 && y == 0) || (x == 0 && y == n - 1) || (x == m - 1 && y == n - 1)) {
                        continue;
                    }
                    if ((x == 0 && f == u) || (x == m - 1 && f == d) || (y == 0 && f == l) || (y == n - 1 && f == r)) {
                        values.add(v);
                        continue;
                    }
                }
                if (c == 'E') {
                    if (f == u) {
                        v += l;
                    } else if (f == d) {
                        v += r;
                    } else if (f == l) {
                        v += d;
                    } else {
                        v += u;
                    }
                } else if (c == 'W') {
                    if (f == u) {
                        v += r;
                    } else if (f == d) {
                        v += l;
                    } else if (f == l) {
                        v += u;
                    } else {
                        v += d;
                    }
                } else {
                    v += f;
                }
                list.addLast(v);
            }
            num--;
        }
        int size = values.size();
        int[][] arr = new int[size][2];
        int i = 0;
        for (int t : values) {
            arr[i][0] = t >> 12;
            arr[i][1] = t & 0XFFF;
            i++;
        }
        return arr;
    }

}
