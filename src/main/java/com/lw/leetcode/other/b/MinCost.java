package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 2087. 网格图中机器人回家的最小代价
 * other
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/27 21:29
 */
public class MinCost {

    public int minCost(int[] startPos, int[] homePos, int[] rowCosts, int[] colCosts) {
        int x1 = startPos[0];
        int y1 = startPos[1];
        int x2 = homePos[0];
        int y2 = homePos[1];
        int sx = x1 < x2 ? 1 : -1;
        int sy = y1 < y2 ? 1 : -1;
        int sum = 0;
        while (y1 != y2) {
            y1 += sy;
            sum += colCosts[y1];
        }
        while (x1 != x2) {
            x1 += sx;
            sum += rowCosts[x1];
        }
        return sum;
    }

}
