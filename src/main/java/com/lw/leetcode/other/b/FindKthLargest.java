package com.lw.leetcode.other.b;

import java.util.Arrays;
import java.util.Random;

/**
 * 215. 数组中的第K个最大元素
 * 剑指 Offer II 076. 数组中的第 k 大的数字
 *
 * @Author liw
 * @Date 2021/4/30 13:01
 * @Version 1.0
 */
public class FindKthLargest {

    public static void main(String[] args) {
        FindKthLargest test = new FindKthLargest();
        for (int i = 2; i < 3; i++) {
            int[] arr = {3, 2, 3, 1, 2, 4, 5, 5, 6};

            int kthLargest = test.findKthLargest3(arr, i);
            System.out.println(kthLargest);
        }

    }

    Random random = new Random();

    public int findKthLargest2(int[] nums, int k) {
        return quickSelect(nums, 0, nums.length - 1, nums.length - k);
    }

    public int quickSelect(int[] a, int l, int r, int index) {
        int q = randomPartition(a, l, r);
        if (q == index) {
            return a[q];
        } else {
            return q < index ? quickSelect(a, q + 1, r, index) : quickSelect(a, l, q - 1, index);
        }
    }

    public int randomPartition(int[] a, int l, int r) {
        int i = random.nextInt(r - l + 1) + l;
        swap(a, i, r);
        return partition(a, l, r);
    }

    public int partition(int[] a, int l, int r) {
        int x = a[r], i = l - 1;
        for (int j = l; j < r; ++j) {
            if (a[j] <= x) {
                swap(a, ++i, j);
            }
        }
        swap(a, i + 1, r);
        return i + 1;
    }

    public void swap(int[] a, int i, int j) {
        System.out.println(Arrays.toString(a));
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    public int findKthLargest1(int[] nums, int k) {
        return find(nums, k - 1, 0, nums.length - 1);
    }

    private static int find(int[] nums, int k, int st, int end) {
        if (st >= end) {
            return nums[k];
        }
        int item = st;

        int limit = end;
        int value = nums[st];
        while (item < limit) {
            int t = nums[item + 1];
            if (value < t) {
                nums[item] = t;
                nums[item + 1] = value;
                item++;
            } else if (value > t) {
                nums[item + 1] = nums[limit];
                nums[limit] = t;
                limit--;
            } else {
                item++;
            }
            System.out.println(Arrays.toString(nums));
        }
        if (item == k) {
            return nums[k];
        }
        if (item > k) {
            return find(nums, k, st, item - 1);
        } else {
            return find(nums, k, item + 1, end);
        }
    }

    public int findKthLargest3(int[] nums, int k) {
        int length = nums.length;
        for (int i = length / 2 - 1; i >= 0; i--) {
            find(nums, i, length);
        }
        for (int i = 0; i < k - 1; i++) {
            nums[0] = nums[length - 1 - i];
            find(nums, 0, length - 1 - i);
        }
        return nums[0];
    }

    private static void find(int[] nums, int st, int end) {
        int index = 2 * st + 1;
        if (index >= end) {
            return;
        }
        index = index + 1 >= end ? index : nums[index] > nums[index + 1] ? index : index + 1;
        if (nums[index] > nums[st]) {
            int c = nums[index];
            nums[index] = nums[st];
            nums[st] = c;
            find(nums, index, end);
        }
    }

}
