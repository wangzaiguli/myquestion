package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 1476. 子矩形查询
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/17 21:10
 */
public class SubrectangleQueries {

    private int[][] rectangle;

    public SubrectangleQueries(int[][] rectangle) {
        this.rectangle = rectangle;
    }

    public void updateSubrectangle(int row1, int col1, int row2, int col2, int newValue) {
        for (int i = row1; i <= row2; i++) {
            for (int j = col1; j <= col2; j++) {
                rectangle[i][j] = newValue;
            }
        }
    }

    public int getValue(int row, int col) {
        return rectangle[row][col];
    }

}
