package com.lw.leetcode.other.b;

/**
 * Created with IntelliJ IDEA.
 * 2217. 找到指定长度的回文数
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/19 9:06
 */
public class KthPalindrome {

    public long[] kthPalindrome(int[] queries, int intLength) {
        boolean flag = (intLength & 1) == 0;
        long st = 0;
        int a = (intLength + 1) >> 1;
        for (int i = 1; i < a; i++) {
            st = st * 10 + 9;
        }
        long end = st * 10 + 9;
        int length = queries.length;
        long[] arr = new long[length];
        for (int i = 0; i < length; i++) {
            long t = st + queries[i];
            if (t > end) {
                arr[i] = -1;
                continue;
            }
            long item = t;
            if (!flag) {
                item /= 10;
            }
            while (item != 0) {
                t = t * 10 + item % 10;
                item /= 10;
            }
            arr[i] = t;
        }
        return arr;
    }

}
