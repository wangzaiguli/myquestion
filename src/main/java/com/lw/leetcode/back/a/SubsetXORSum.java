package com.lw.leetcode.back.a;

/**
 * Created with IntelliJ IDEA.
 * back
 * 1863. 找出所有子集的异或总和再求和
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/13 15:26
 */
public class SubsetXORSum {

    public static void main(String[] args) {
        SubsetXORSum test = new SubsetXORSum();

        // 6
        int[] arr = {1, 3};

        // 28
//        int[] arr = {5,1,6};

        // 480
//        int[] arr = {3,4,5,6,7,8};

        int i = test.subsetXORSum(arr);
        System.out.println(i);
    }


    private int sum = 0;
    private int[] nums;
    private int length;

    public int subsetXORSum(int[] nums) {
        this.nums = nums;
        this.length = nums.length;
        for (int i = 0; i < length; i++) {
            find(0, i);
        }
        return sum;
    }

    private void find(int item, int index) {
        item ^= nums[index];

        sum += item;
        for (int i = index + 1; i < length; i++) {
            find(item, i);
        }
    }
}
