package com.lw.leetcode.back.a;

import java.util.ArrayList;
import java.util.List;

/**
 * 面试题 08.06. 汉诺塔问题
 *
 * @Author liw
 * @Date 2021/9/12 22:11
 * @Version 1.0
 */
public class Hanota {

    public static void main(String[] args) {
        Hanota test = new Hanota();
        List<Integer> a = new ArrayList<>();
        List<Integer> b = new ArrayList<>();
        List<Integer> c = new ArrayList<>();
        a.add(3);
        a.add(2);
        a.add(1);
        test.find(a, b, c, a.size(), "a", "b", "c");

        System.out.println(c);

    }

    public void hanota(List<Integer> a, List<Integer> b, List<Integer> c) {
        find(a, b, c, a.size());
    }

    private void find(List<Integer> a, List<Integer> b, List<Integer> c, int size) {
        if (size == 1) {
            c.add(a.get(a.size() - 1));
            a.remove(a.size() - 1);
            return;
        }
        find(a, c, b, size - 1);
        c.add(a.get(a.size() - 1));
        a.remove(a.size() - 1);
        find(b, a, c, size - 1);
    }

    private void find(List<Integer> a, List<Integer> b, List<Integer> c, int size, String aName, String bName, String cName) {
        if (size == 1) {
            System.out.println(aName + " -> " + cName + " : " + a.get(a.size() - 1));
            c.add(a.get(a.size() - 1));
            a.remove(a.size() - 1);
            return;
        }

        find(a, c, b, size - 1, aName, cName, bName);
        System.out.println(aName + " -> " + cName + " : " + a.get(a.size() - 1));
        c.add(a.get(a.size() - 1));
        a.remove(a.size() - 1);

        find(b, a, c, size - 1, bName, aName, cName);
    }
}
