package com.lw.leetcode.back.a;

/**
 * back
 * 733. 图像渲染
 * 面试题 08.10. 颜色填充
 *
 * @Author liw
 * @Date 2021/7/10 17:33
 * @Version 1.0
 */
public class FloodFill {
    public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
        return dfs(image, sr, sc, newColor, image[sr][sc]);
    }

    private int[][] dfs(int[][] image, int i, int j, int newColor, int num) {
        if (i < 0 || i >= image.length || j < 0 || j >= image[0].length || image[i][j] == newColor || image[i][j] != num) {
            return image;
        }
        int temp = image[i][j];
        image[i][j] = newColor;
        dfs(image, i + 1, j, newColor, temp);
        dfs(image, i - 1, j, newColor, temp);
        dfs(image, i, j + 1, newColor, temp);
        dfs(image, i, j - 1, newColor, temp);
        return image;
    }
}
