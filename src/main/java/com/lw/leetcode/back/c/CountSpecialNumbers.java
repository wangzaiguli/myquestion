package com.lw.leetcode.back.c;

/**
 * Created with IntelliJ IDEA.
 * 2376. 统计特殊整数
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/16 11:20
 */
public class CountSpecialNumbers {


    public static void main(String[] args) {
        CountSpecialNumbers test = new CountSpecialNumbers();

        // 19
//        int n = 20;

        // 5
//        int n = 5;

        // 110
//        int n = 135;

        // 5974650
        int n = 2000000000;

        // 47610
//        int n = 200000;

        int i = test.countSpecialNumbers(n);
        System.out.println(i);
    }

    private int count = 0;
    private long n;

    public int countSpecialNumbers(int n) {
        this.n = n;
        int[] arr = new int[10];
        find(0, arr);
        return count;
    }

    private void find(long v, int[] arr) {
        for (int i = 0; i < 10; i++) {
            if (arr[i] == 1 || (v == 0 && i == 0)) {
                continue;
            }
            arr[i] = 1;
            long t = v * 10 + i;
            if (t <= n) {
                count++;
                find(t, arr);
            }
            arr[i] = 0;
        }
    }

}
