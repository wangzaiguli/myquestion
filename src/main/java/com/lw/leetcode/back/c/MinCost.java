package com.lw.leetcode.back.c;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 1368. 使网格图至少有一条有效路径的最小代价
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/16 15:13
 */
public class MinCost {


    public static void main(String[] args) {
        MinCost test = new MinCost();

        // 3
        int[][] grid = {{1, 1, 1, 1}, {2, 2, 2, 2}, {1, 1, 1, 1}, {2, 2, 2, 2}};

        // 0
//        int[][] grid = {{1, 1, 3}, {3, 2, 2}, {1, 1, 4}};

        // 1
//        int[][] grid = {{1, 2}, {4, 3}};

        // 3
//        int[][] grid = {{2, 2, 2}, {2, 2, 2}};

        // 0
//        int[][] grid = {{4}};

        //
//        int[][] grid = Utils.getArr(10, 100, 1, 5);

        int i = test.minCost(grid);
        System.out.println(i);

//        System.out.println();
//        System.out.println();
//        for (int[] ints : test.arr) {
//            System.out.println(Arrays.toString(ints));
//        }
//        System.out.println();
//        System.out.println();
//        for (int[] ints : grid) {
//            System.out.println(Arrays.toString(ints));
//        }

    }

    private int[][] arr;
    private int m;
    private int n;
    private int[][] grid;
    private Map<Integer, Integer> c1 = new HashMap<>();
    private Map<Integer, Integer> c2 = new HashMap<>();
    private int[][] items = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
    private int[] nexts = {2, 1, 4, 3};

    public int minCost(int[][] grid) {
        this.m = grid.length;
        this.n = grid[0].length;
        this.arr = new int[m][n];
        this.grid = grid;
        arr[m - 1][n - 1] = 1;
        find(m - 1, n - 1, 1);
        while (!c2.isEmpty()) {
            c1.clear();
            c1.putAll(c2);
            c2.clear();
            for (Map.Entry<Integer, Integer> entry : c1.entrySet()) {
                Integer key = entry.getKey();
                int x = key >> 16;
                int y = key & 0XFFFF;
                if (arr[x][y] == 0) {
                    find(x, y, entry.getValue());
                }
            }
        }
        return arr[0][0] - 1;
    }

    private boolean find(int x, int y, int t) {
        arr[x][y] = t;
        if (x == 0 && y == 0) {
            return true;
        }
        for (int i = 0; i < 4; i++) {
            int[] item = items[i];
            int a = x + item[0];
            int b = y + item[1];
            int v = (a << 16) + b;
            if (a < 0 || a == m || b < 0 || b == n || arr[a][b] > 0) {
                continue;
            }
            if (grid[a][b] == nexts[i]) {
                c2.remove(v);
                if (find(a, b, t)) {
                    return true;
                }
            } else {
                c2.put(v,  t + 1);
            }
        }
        return false;
    }

}
