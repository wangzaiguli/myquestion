package com.lw.leetcode.back.b;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 784. 字母大小写全排列
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/9 9:22
 */
public class LetterCasePermutation {

    public static void main(String[] args) {
        LetterCasePermutation test = new LetterCasePermutation();
        List<String> list = test.letterCasePermutation("a1b2");
        System.out.println(list);
    }

    private StringBuilder sb = new StringBuilder();
    private List<String> list = new ArrayList<>();
    private char[] arr;
    private int length;

    public List<String> letterCasePermutation(String s) {
        arr = s.toCharArray();
        length = s.length();
        find(0);
        return list;
    }

    private void find(int index) {
        if (length == index) {
            list.add(sb.toString());
            return;
        }
        char c = arr[index];
        sb.append(c);
        find(index + 1);
        if (c >= 'A' ) {
            sb.deleteCharAt(sb.length() - 1);
            sb.append((char) (c^32));
            find(index + 1);
        }
        sb.deleteCharAt(sb.length() - 1);
    }

}
