package com.lw.leetcode.back.b;

/**
 * Created with IntelliJ IDEA.
 * 剑指 Offer II 104. 排列的数目
 * 377. 组合总和 Ⅳ
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/17 14:09
 */
public class combinationSum4 {
    public int combinationSum4(int[] nums, int target) {
        int limit = target + 1;
        int[] dp = new int[limit];
        dp[0] = 1;
        for (int i = 1; i < limit; i++) {
            int sum = 0;
            for (int num : nums) {
                sum += i - num < 0 ? 0 : dp[i - num];
            }
            dp[i] = sum;
        }
        return dp[target];
    }
}
