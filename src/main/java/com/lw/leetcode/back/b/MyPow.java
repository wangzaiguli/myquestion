package com.lw.leetcode.back.b;

/**
 * Created with IntelliJ IDEA.
 * 50. Pow(x, n)
 * 剑指 Offer 16. 数值的整数次方
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/13 11:40
 */
public class MyPow {

    public double myPow(double x, int a) {
        long n = a;
        if (n == 0) {
            return 1;
        }
        if (x == 0) {
            return 0;
        }
        if (n < 0) {
            n = ~n + 1;
            x = 1 / x;
        }
        double sum = 1;
        double item = x;
        while (n != 1) {
            if ((n & 1) != 0) {
                sum = sum * item;
            }
            item = item * item;
            n = n >> 1;
        }
        return sum * item;
    }

}
