package com.lw.leetcode.back.b;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 47. 全排列 II
 * 剑指 Offer II 084. 含有重复元素集合的全排列
 *
 * @Author liw
 * @Date 2021/4/22 8:59
 * @Version 1.0
 */
public class ZigzagLevelOrder {

    public static void main(String[] args) {

//        int[][] a = {{1,3,5,7},{10,11,16,20},{23,30,34,60}};
//        int[] b = {10,9,2,5,3,7,101,18};
        int[] b = {1};
        ZigzagLevelOrder test = new ZigzagLevelOrder();
        List<List<Integer>> lists = test.permuteUnique(b);
        System.out.println(lists);


    }

    public List<List<Integer>> permuteUnique(int[] nums) {

        List<List<Integer>> list = new ArrayList<>();
        int[] b = new int[nums.length];
        aa(nums, 0, b, list);
        return list.stream().distinct().collect(Collectors.toList());
    }

    private static void aa(int[] arr, int index, int[] printArr, List<List<Integer>> list) {

        int length = arr.length;
        if (length == 0) {
            List<Integer> ints = new ArrayList<>(printArr.length);
            for (int i : printArr) {
                ints.add(i);
            }
            list.add(ints);
            return;
        }
        for (int i = 0; i < length; i++) {
            printArr[index] = arr[i];
            int[] arr2 = copy(arr, i);
            aa(arr2, index + 1, printArr, list);
        }
    }

    // 回溯
    private static int[] copy(int[] arr, int n) {
        int length = arr.length;
        int[] copyArr = new int[length - 1];
        int c = 0;
        for (int i = 0; i < length; i++) {
            if (i != n) {
                copyArr[c++] = arr[i];
            }
        }
        return copyArr;
    }

}
