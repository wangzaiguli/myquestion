package com.lw.leetcode.back.b;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * back
 * 2059. 转化数字的最小运算数
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/7 9:49
 */
public class MinimumOperations2059 {

    public static void main(String[] args) {
        MinimumOperations2059 test = new MinimumOperations2059();

        // -1
//        int[] arr = {2,8,16};
//        int a = 0;
//        int b = 1;

        // 2
//        int[] arr = {3,5,7};
//        int a = 0;
//        int b = -4;

        // 2
        int[] arr = {2, 4, 12};
        int a = 2;
        int b = 12;

        int i = test.minimumOperations(arr, a, b);
        System.out.println(i);


    }

    public int minimumOperations(int[] nums, int start, int goal) {
        int count = 0;
        if (start == goal) {
            return count;
        }
        List<Integer> list1 = new ArrayList<>();
        List<Integer> list2 = new ArrayList<>();
        int[] arr = new int[1001];
        list1.add(start);
        while (!list1.isEmpty()) {
            count++;
            for (int v : list1) {
                for (int num : nums) {
                    int t = v + num;
                    if (t == goal) {
                        return count;
                    }
                    if (t >= 0 && t <= 1000 && arr[t] == 0) {
                        arr[t] = 1;
                        list2.add(t);
                    }
                    t = v - num;
                    if (t == goal) {
                        return count;
                    }
                    if (t >= 0 && t <= 1000 && arr[t] == 0) {
                        arr[t] = 1;
                        list2.add(t);
                    }
                    t = v ^ num;
                    if (t == goal) {
                        return count;
                    }
                    if (t >= 0 && t <= 1000 && arr[t] == 0) {
                        arr[t] = 1;
                        list2.add(t);
                    }
                }
            }
            list1.clear();
            list1.addAll(list2);
            list2.clear();
        }
        return -1;
    }

}
