package com.lw.leetcode.back.b;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * back
 * 841. 钥匙和房间
 *
 * @Author liw
 * @Date 2021/8/8 18:15
 * @Version 1.0
 */
public class CanVisitAllRooms {

    public static void main(String[] args) {
        CanVisitAllRooms test = new CanVisitAllRooms();

        // [[1,3],[3,0,1],[2],[0]]
        List<List<Integer>> rooms = new ArrayList<>();
        List<Integer> a = Arrays.asList(1, 3);
        rooms.add(a);
        List<Integer> b = Arrays.asList(3, 0, 1);
        rooms.add(b);
        List<Integer> c = Arrays.asList(2);
        rooms.add(c);
        List<Integer> d = Arrays.asList(0);
        rooms.add(d);
        boolean b1 = test.canVisitAllRooms(rooms);
        System.out.println(b1);
    }

    private int[] arr;
    private List<List<Integer>> rooms;
    private int count = 1;

    public boolean canVisitAllRooms(List<List<Integer>> rooms) {
        int size = rooms.size();
        arr = new int[size];
        arr[0] = 1;
        this.rooms = rooms;
        find(0);
        return size == count;
    }

    private void find(int n) {
        List<Integer> list = rooms.get(n);
        for (Integer value : list) {
            if (arr[value] == 0) {
                arr[value] = 1;
                count++;
                find(value);
            }
        }
    }


}
