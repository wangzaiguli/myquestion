package com.lw.leetcode.back.b;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 690. 员工的重要性
 */
public class GetImportance {

    public static void main(String[] args) {
        GetImportance test = new GetImportance();
        test.getImportance(new ArrayList<>(), 2);
    }

    public int getImportance(List<Employee> employees, int id) {
        Map<Integer, Employee> map = new HashMap<>(employees.size() << 1);
        for (Employee employee : employees) {
            map.put(employee.id, employee);
        }
        Employee employee = map.get(id);
        return find(employee, map);
    }

    private static int find (Employee employee, Map<Integer, Employee> map) {
        List<Integer> list = employee.subordinates;
        if (list == null) {
            return 0;
        }
        int sum = employee.importance;
        for (Integer integer : list) {
            Employee e = map.get(integer);
            sum = sum + find(e, map);
        }
        return sum;
    }

    @Data
    public class Employee {
        public int id;
        public int importance;
        public List<Integer> subordinates;
    }

}
