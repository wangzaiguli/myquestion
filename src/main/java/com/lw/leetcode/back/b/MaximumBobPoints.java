package com.lw.leetcode.back.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2212. 射箭比赛中的最大得分
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/21 17:37
 */
public class MaximumBobPoints {

    public static void main(String[] args) {
        MaximumBobPoints test = new MaximumBobPoints();

        // [0,0,0,0,1,1,0,0,1,2,3,1]
        int[] arr = {1, 1, 0, 1, 0, 0, 2, 1, 0, 1, 2, 0};
        int k = 9;

        int[] ints = test.maximumBobPoints(k, arr);

        System.out.println(Arrays.toString(ints));
    }

    private int[] as;
    private int[] bs;
    private int[] cs;
    private int b = 0;
    private int max;

    public int[] maximumBobPoints(int numArrows, int[] aliceArrows) {
        as = aliceArrows;
        bs = new int[12];
        cs = new int[12];
        find(11, numArrows);
        return cs;
    }

    private void find(int index, int sum) {
        if (index == -1) {
            if (b > max) {
                max = b;
                System.arraycopy(bs, 0, cs, 0, 12);
                cs[0] += sum;
            }
            return;
        }
        int v = as[index];
        if (v < sum) {
            b += index;
            bs[index] = v + 1;
            find(index - 1, sum - v - 1);
            b -= index;
            bs[index] = 0;
        }
        find(index - 1, sum);
    }

}
