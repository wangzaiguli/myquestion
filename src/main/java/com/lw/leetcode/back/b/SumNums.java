package com.lw.leetcode.back.b;

/**
 * Created with IntelliJ IDEA.
 * 剑指 Offer 64. 求1+2+…+n
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/13 11:27
 */
public class SumNums {

    public static void main(String[] args) {
        SumNums test = new SumNums();
        int i = test.sumNums(100);
        System.out.println(i);
    }

    public int sumNums(int n) {
        int sum = 0;
        boolean b = n == 0 || (sum = n + sumNums(n - 1)) == -1;
        return sum;
    }

}
