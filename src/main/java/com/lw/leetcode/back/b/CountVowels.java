package com.lw.leetcode.back.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 2063. 所有子字符串中的元音
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/27 20:52
 */
public class CountVowels {

    public long countVowels(String word) {
        int length = word.length();
        long sum = 0L;
        for (int i = 0; i < length; i++) {
            char c = word.charAt(i);
            if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' ) {
                sum += length + (long)i * (length - i - 1);;
            }
        }
        return sum;
    }

}
