package com.lw.leetcode.back.b;

/**
 * Created with IntelliJ IDEA.
 * back
 * 1849. 将字符串拆分为递减的连续值
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/9 10:07
 */
public class SplitString {

    public static void main(String[] args) {
        SplitString test = new SplitString();

        // true
//        String str = "050043";

        // true
        String str = "200100";

        // true
//        String str = "1000";

        // true
//        String str = "10009998";

        // false
//        String str = "1234";

        // false
//        String str = "9080701";

        // false
//        String str = "2109";

        boolean b = test.splitString(str);
        System.out.println(b);
    }

    private char[] chars;
    private int length;

    public boolean splitString(String s) {
        length = s.length() - 1;
        chars = s.toCharArray();
        long item = 0L;
        for (int i = 0; i < length; i++) {
            item = item * 10L + chars[i] - 48L;
            boolean b = find(item, chars[i + 1] - 48L, i + 2);
            if (b) {
                return true;
            }
        }
        return false;
    }

    private boolean find(long last, long item, int index) {
        if (last <= item) {
            return false;
        }
        if (last - item == 1) {
            if (index > length) {
                return true;
            }
            long it = item;
            for (int i = index; i <= length; i++) {
                item = item * 10L + chars[i] - 48L;
                if (find(last, item, i + 1)) {
                    return true;
                }
            }
            item = it;
            long next = 0L;
            for (int i = index; i <= length; i++) {
                next = next * 10L + chars[i] - 48L;
                boolean b = find(item, next, i + 1);
                if (b) {
                    return true;
                }
            }
            return false;
        }
        if (index > length) {
            return false;
        }
        return find(last, item * 10L + chars[index] - 48L, index + 1);
    }

}
