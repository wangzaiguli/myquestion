package com.lw.leetcode.back.b;

/**
 * Created with IntelliJ IDEA.
 * b
 * back
 * https://leetcode.cn/contest/ubiquant2022/problems/3PHTGp/
 * 九坤-02. 池塘计数
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/10 17:00
 */
public class LakeCount {

    private int[][] items = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}, {1, 1}, {1, -1}, {-1, 1}, {-1, -1}};
    private char[][] arr;
    private int m;
    private int n;

    public int lakeCount(String[] field) {
        this.m = field.length;
        this.n = field[0].length();
        this.arr = new char[m][];
        for (int i = 0; i < m; i++) {
            arr[i] = field[i].toCharArray();
        }
        int count = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (arr[i][j] == 'W') {
                    arr[i][j] = '.';
                    count++;
                    find(i, j);
                }
            }
        }
        return count;
    }

    private void find(int x, int y) {
        for (int[] item : items) {
            int a = item[0] + x;
            int b = item[1] + y;
            if (a < 0 || b < 0 || a == m || b == n) {
                continue;
            }
            if (arr[a][b] == '.') {
                continue;
            }
            arr[a][b] = '.';
            find(a, b);
        }
    }

}
