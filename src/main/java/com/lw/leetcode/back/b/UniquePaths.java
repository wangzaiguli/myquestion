package com.lw.leetcode.back.b;

/**
 * Created with IntelliJ IDEA.
 * 62. 不同路径
 * 剑指 Offer II 098. 路径的数目
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/30 10:19
 */
public class UniquePaths {
    public int uniquePaths(int m, int n) {
        if (m == 0 || n == 0) {
            return 0;
        }
        int[][] a = new int[m][n];

        for (int i = 0; i < m; i++) {
            a[i][0] = 1;
        }
        for (int i = 0; i < n; i++) {
            a[0][i] = 1;
        }
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                a[i][j] = a[i - 1][j] + a[i][j - 1];
            }
        }
        return a[m - 1][n - 1];
    }

}
