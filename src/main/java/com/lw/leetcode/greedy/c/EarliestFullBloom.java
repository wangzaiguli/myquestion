package com.lw.leetcode.greedy.c;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 很简单，应该降级
 * 2136. 全部开花的最早一天
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/7 8:57
 */
public class EarliestFullBloom {

    public int earliestFullBloom(int[] plantTime, int[] growTime) {
        int length = plantTime.length;
        int[][] arr = new int[length][2];
        for (int i = 0; i < length; i++) {
            arr[i][0] = plantTime[i];
            arr[i][1] = growTime[i];
        }
        Arrays.sort(arr, (a, b) -> Integer.compare(b[1], a[1]));
        int max = 0;
        int sum = 0;
        for (int i = 0; i < length; i++) {
            sum += arr[i][0];
            max = Math.max(max, sum + arr[i][1]);
        }
        return max;
    }

}
