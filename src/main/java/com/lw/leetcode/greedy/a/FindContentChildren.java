package com.lw.leetcode.greedy.a;

import java.util.Arrays;

/**
 * 455. 分发饼干
 *
 * @Author liw
 * @Date 2021/5/13 9:54
 * @Version 1.0
 */
public class FindContentChildren {

    public int findContentChildren(int[] g, int[] s) {
        Arrays.sort(g);
        Arrays.sort(s);
        int a = g.length;
        int b = s.length;
        int count = 0;
        int i = 0;
        int j = 0;
        while (i < a && j < b) {
            if (s[j] >= g[i]) {
                i++;
                count++;
            }
            j++;

        }
        return count;
    }
}
