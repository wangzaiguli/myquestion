package com.lw.leetcode.greedy.a;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 1710. 卡车上的最大单元数
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/28 11:59
 */
public class MaximumUnits {
    public int maximumUnits(int[][] boxTypes, int truckSize) {
        Arrays.sort(boxTypes, (a, b) -> Integer.compare(b[1], a[1]));
        int sum = 0;
        for (int[] boxType : boxTypes) {
            if (boxType[0] > truckSize) {
                sum += truckSize * boxType[1];
                break;
            }
            sum += boxType[0] * boxType[1];
            truckSize -= boxType[0];
        }
        return sum;
    }
}
