package com.lw.leetcode.greedy.b;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 2178. 拆分成最多数目的正偶数之和
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/12 14:35
 */
public class MaximumEvenSplit {

    public static void main(String[] args) {
        MaximumEvenSplit test = new MaximumEvenSplit();

        // [2,4,6,8,10,12,14,16,18,20,22,24,44]
//        long n = 200;

        // [2,4,6]
        long n = 12;

        List<Long> longs = test.maximumEvenSplit(n);
        System.out.println(longs);
    }

    public List<Long> maximumEvenSplit(long finalSum) {
        List<Long> list = new ArrayList<>();
        if ((finalSum & 1) != 0) {
            return list;
        }
        long item = 2L;
        long sum = item;
        while (sum <= finalSum) {
            list.add(item);
            item += 2L;
            sum += item;
        }
        list.set(list.size() - 1, (item << 1) - sum - 2 + finalSum);
        return list;
    }

}
