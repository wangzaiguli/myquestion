package com.lw.leetcode.greedy.b;

/**
 * Created with IntelliJ IDEA.
 * b
 * greedy
 * 2483. 商店的最少代价
 *
 * @author liw
 * @version 1.0
 * @date 2022/11/27 19:50
 */
public class BestClosingTime {

    public int bestClosingTime(String customers) {
        int length = customers.length();
        int c = 0;
        for (int i = 0; i < length; i++) {
            if (customers.charAt(i) == 'Y') {
                c++;
            }
        }
        int min = c;
        int index = 0;
        for (int i = 0; i < length; i++) {
            if (customers.charAt(i) == 'Y') {
                c--;
            } else {
                c++;
            }
            if (c < min) {
                c = min;
                index = i + 1;
            }
        }
        return index;
    }

}
