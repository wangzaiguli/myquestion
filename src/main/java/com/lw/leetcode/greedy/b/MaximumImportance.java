package com.lw.leetcode.greedy.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 6085. 道路的最大总重要性
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/30 11:15
 */
public class MaximumImportance {

    public long maximumImportance(int n, int[][] roads) {
        int[] arr = new int[n];
        for (int[] road : roads) {
            arr[road[0]]++;
            arr[road[1]]++;
        }
        long sum = 0;
        Arrays.sort(arr);
        for (int i = arr.length - 1; i >= 0; i--) {
            sum += (i + 1) * arr[i];
        }
        return sum;
    }

}
