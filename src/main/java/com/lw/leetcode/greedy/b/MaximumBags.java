package com.lw.leetcode.greedy.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2279. 装满石头的背包的最大数量
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/24 9:11
 */
public class MaximumBags {

    public int maximumBags(int[] capacity, int[] rocks, int additionalRocks) {
        int length = capacity.length;
        for (int i = 0; i < length; i++) {
            capacity[i] -= rocks[i];
        }
        Arrays.sort(capacity);
        int ans = 0;
        for (int i = 0; i < length && additionalRocks >= capacity[i]; i++) {
            ans++;
            additionalRocks -= capacity[i];
        }
        return ans;
    }

}
