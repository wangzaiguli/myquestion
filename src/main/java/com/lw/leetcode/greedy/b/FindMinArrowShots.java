package com.lw.leetcode.greedy.b;

import java.util.Arrays;

/**
 * 452. 用最少数量的箭引爆气球
 *
 * @Author liw
 * @Date 2021/6/23 9:36
 * @Version 1.0
 */
public class FindMinArrowShots {


    public static void main(String[] args) {
        FindMinArrowShots test = new FindMinArrowShots();
        int[][] arr = {{10,16},{2,8},{1,6},{7,12}};
        // [[-2147483646,-2147483645],[2147483646,2147483647]]  2
//        int[][] arr = {{-2147483646,-2147483645},{2147483646,2147483647}};
//        int[][] arr = {{1,2},{4,5},{3,4},{6,7}};
        int minArrowShots = test.findMinArrowShots(arr);
        System.out.println(minArrowShots);
    }


    public int findMinArrowShots(int[][] points) {
        int length = points.length;
        if (length < 2) {
            return length;
        }
        Arrays.sort(points, (a, b) -> a[0] == b[0] ? Integer.compare(a[1], b[1]) : Integer.compare(a[0], b[0]));
        int count = 1;
        int min = points[0][1];
        for (int i = 1; i < length; i++) {
            int[] arr = points[i];
            if (arr[0] > min) {
                count++;
                min = arr[1];
            } else if (arr[1] < min) {
                min = arr[1];
            }
        }
        return count;
    }


}
