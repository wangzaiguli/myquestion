package com.lw.leetcode.greedy.b;

import com.lw.test.util.Utils;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 *6185. 运动员和训练师的最大匹配数
 * @author liw
 * @version 1.0
 * @date 2022/9/18 21:37
 */
public class MatchPlayersAndTrainers {

    public static void main(String[] args) {
        MatchPlayersAndTrainers test = new MatchPlayersAndTrainers();
        int[] arr = Utils.getArr(1000, 2, 10000);
        int[] arr2 = Utils.getArr(1000, 2, 10000);

        int i = test.matchPlayersAndTrainers(arr, arr2);
        System.out.println(i);
    }

    public int matchPlayersAndTrainers(int[] players, int[] trainers) {
        Arrays.sort(players);
        Arrays.sort(trainers);
        int a = players.length;
        int b = trainers.length;
        int i = 0;
        int j = 0;
        int count = 0;
        while (i < a && j < b) {
            if (players[i] <= trainers[j]) {
                count++;
                i++;
                j++;
            } else {
                j++;
            }
        }
        return count;
    }

}
