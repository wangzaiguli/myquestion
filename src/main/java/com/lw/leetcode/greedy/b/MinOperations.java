package com.lw.leetcode.greedy.b;

/**
 * Created with IntelliJ IDEA.
 * 1775. 通过最少操作次数使数组的和相等
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/27 20:42
 */
public class MinOperations {

    public static void main(String[] args) {
        MinOperations test = new MinOperations();

        // 3
//        int[] arr2 = {1,2,3,4,5,6};
//        int[] arr1 = {1,1,2,2,2,2};


        // 3
//        int[] arr2 = {6,6};
//        int[] arr1 = {1};

        // -1
//        int[] arr2 = {1,1,1,1,1,1,1};
//        int[] arr1 = {6};

        // 1
//        int[] arr1 = {5,2,1,5,2,2,2,2,4,3,3,5};
//        int[] arr2 = {1,4,5,5,6,3,1,3,3};

        // 184[1,2,5,4,3,3,5,1,1,6,2,5,4,4,5,6,6,4,2,5,6,2,3,4,5,2,4,4,3,6,6,5,4,1,2,1,2,3,3,2,6,1,1,1,1,3,5,6,2,1,1,1,4,6,5]
        int[] arr1 = {1, 5, 5, 2, 1, 1, 1, 1, 4, 4, 4, 1, 5, 2, 2, 4, 6, 5, 1, 5, 3, 5, 6, 2, 3, 1, 5, 4, 4, 1, 2, 4, 1, 1, 6, 3, 6, 4, 4, 4, 3, 5, 5, 5, 2, 6, 4, 2, 5, 4, 2, 6, 3, 4, 6, 1, 5, 3, 2, 3, 5, 2, 1, 3, 2, 4, 4, 4, 5, 3, 5, 5, 4, 1, 1, 6, 5, 6, 3, 5, 3, 6, 5, 6, 5, 4, 4, 4, 5, 6, 6, 6, 4, 2, 4, 6, 1, 2, 1, 5, 3, 4, 5, 5, 6, 6, 1, 4, 3, 1, 5, 3, 4, 1, 2, 1, 4, 4, 5, 6, 5, 3, 1, 5, 1, 3, 3, 6, 5, 3, 5, 6, 2, 6, 3, 1, 2, 3, 3, 1, 1, 4, 3, 2, 6, 6, 2, 1, 2, 4, 3, 5, 5, 4, 3, 1, 1, 5, 2, 5, 1, 4, 5, 6, 4, 5, 2, 1, 2, 5, 3, 2, 6, 3, 4, 3, 4, 5, 4, 6, 3, 4, 4, 3, 3, 4, 2, 2, 6, 2, 6, 3, 1, 1, 5, 3, 1, 1, 4, 2, 5, 5, 5, 4, 3, 6, 5, 5, 5, 1, 1, 3, 6, 2, 3, 6, 3, 4, 2, 5, 4, 4, 3, 5, 6, 4, 3, 5, 1, 1, 3, 3, 1, 1, 6, 4, 6, 2, 1, 4, 3, 5, 5};
        int[] arr2 = {1, 2, 5, 4, 3, 3, 5, 1, 1, 6, 2, 5, 4, 4, 5, 6, 6, 4, 2, 5, 6, 2, 3, 4, 5, 2, 4, 4, 3, 6, 6, 5, 4, 1, 2, 1, 2, 3, 3, 2, 6, 1, 1, 1, 1, 3, 5, 6, 2, 1, 1, 1, 4, 6, 5};

        int i = test.minOperations(arr1, arr2);

        System.out.println(i);
    }

    public int minOperations(int[] nums1, int[] nums2) {
        int[] arr1 = new int[7];
        int[] arr2 = new int[7];
        int s1 = 0;
        int s2 = 0;
        for (int i : nums1) {
            arr1[i]++;
            s1 += i;
        }
        for (int i : nums2) {
            arr2[i]++;
            s2 += i;
        }
        if (s1 == s2) {
            return 0;
        }
        int sum = 0;
        int s = Math.abs(s1 - s2);
        for (int i = 1; i < 7; i++) {
            int step = 6 - i;
            int c = arr1[step + 1] + arr2[i];
            if (s1 < s2) {
                c = arr2[step + 1] + arr1[i];
            }
            if (step * c >= s) {
                return sum + (s + step - 1) / step;
            } else {
                sum += c;
                s -= step * c;
            }
        }
        return -1;
    }

}
