package com.lw.leetcode.dp.a;

/**
 * dp
 * LCS 01. 下载插件
 *
 * @Author liw
 * @Date 2021/8/28 12:46
 * @Version 1.0
 */
public class LeastMinutes {

    public int leastMinutes(int n) {
        int count = 1;
        int item = 1;
        while (item < n) {
            count++;
            item <<= 1;
        }
        return count;
    }

}
