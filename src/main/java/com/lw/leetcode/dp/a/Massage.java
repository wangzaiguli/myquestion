package com.lw.leetcode.dp.a;

/**
 * 面试题 17.16. 按摩师
 *
 * @Author liw
 * @Date 2021/5/28 15:21
 * @Version 1.0
 */
public class Massage {

    public static void main(String[] args) {
        Massage test = new Massage();
        int[] arr = {2,1,4,5,3,1,1,3};
        int massage = test.massage(arr);
        System.out.println(massage);
    }
    public int massage(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int length = nums.length;
        if (length == 1) {
            return nums[0];
        }

        if (length == 2) {
            return Math.max(nums[0], nums[1]);
        }
        if (length == 3) {
            return Math.max(nums[0] + nums[2], nums[1]);
        }
        nums[2] = Math.max(nums[0] + nums[2], nums[1]);
        for (int i = 3; i < length; i++) {
            nums[i] = Math.max(nums[i - 2], nums[i - 3]) + nums[i];
        }
        return Math.max(nums[length - 2], nums[length - 1]);
    }
}
