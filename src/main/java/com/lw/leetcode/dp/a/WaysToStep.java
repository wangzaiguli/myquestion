package com.lw.leetcode.dp.a;

/**
 * 面试题 08.01. 三步问题
 *
 * @Author liw
 * @Date 2021/5/28 15:48
 * @Version 1.0
 */
public class WaysToStep {
    public int waysToStep(int n) {
        if (n < 3) {
            return n;
        }
        long[] arr = new long[n + 1];
        arr[1] = 1;
        arr[2] = 2;
        arr[3] = 4;
        for (int i = 4; i <= n; i++) {
            arr[i] = ((arr[i - 1] + arr[i - 2] + arr[i - 3])% 1000000007);
        }
        return (int) arr[n];
    }

}
