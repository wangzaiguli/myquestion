package com.lw.leetcode.dp.a;

/**
 * Created with IntelliJ IDEA.
 * 剑指 Offer 63. 股票的最大利润
 * 121. 买卖股票的最佳时机
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/1 12:35
 */
public class MaxProfit121 {
    public int maxProfit(int[] prices) {
        int sum = 0;
        if (prices == null || prices.length < 1) {
            return sum;
        }
        int length = prices.length;
        int max = prices[length - 1];
        int b;
        for (int i = length - 2; i >= 0; i--) {
            b = max - prices[i];
            sum = sum > b ? sum : b;
            if (b < 0) {
                max = prices[i];
            }
        }

        return sum;
    }
}
