package com.lw.leetcode.dp.a;

/**
 * 303. 区域和检索 - 数组不可变
 *
 * @Author liw
 * @Date 2021/5/28 16:07
 * @Version 1.0
 */
public class NumArray {

    int[] arr;
    public NumArray(int[] nums) {
        int length = nums.length;
        arr = new int[length + 1];
        for (int i = 0; i < length; i++) {
            arr[i + 1] = arr[i] + nums[i];
        }
    }

    public int sumRange(int left, int right) {
        return arr[right + 1] - arr[left];
    }

}
