package com.lw.leetcode.dp.a;

/**
 * 746. 使用最小花费爬楼梯
 * 剑指 Offer II 088. 爬楼梯的最少成本
 *
 * @Author liw
 * @Date 2021/5/28 11:54
 * @Version 1.0
 */
public class MinCostClimbingStairs {

    public static void main(String[] args) {
        MinCostClimbingStairs test = new MinCostClimbingStairs();
        int[] arr = {1, 100, 1, 1, 1, 100, 1, 1, 100};
        int i = test.minCostClimbingStairs(arr);
        System.out.println(i);
    }

    public int minCostClimbingStairs(int[] cost) {
        int length = cost.length;
        if (length == 2) {
            return Math.min(cost[0], cost[1]);
        }
        for (int i = 2; i < length; i++) {
            cost[i] += Math.min(cost[i - 1], cost[i - 2]);
        }
        return Math.min(cost[length - 1], cost[length - 2]);
    }
}
