package com.lw.leetcode.dp.a;

import java.util.Arrays;

/**
 * dp
 * 1299. 将每个元素替换为右侧最大元素
 *
 * @Author liw
 * @Date 2021/6/7 15:42
 * @Version 1.0
 */
public class ReplaceElements {

    public static void main(String[] args) {
        ReplaceElements test = new ReplaceElements();
        int[] arr = {17};
        int[] ints = test.replaceElements(arr);
        System.out.println(Arrays.toString(ints));
    }

    public int[] replaceElements(int[] arr) {
        int length = arr.length;
        int max = -1;
        for (int i = length - 1; i >= 0; i--) {
            int value = Math.max(arr[i], max);
            arr[i] = max;
            max = value;
        }
        return arr;
    }

}
