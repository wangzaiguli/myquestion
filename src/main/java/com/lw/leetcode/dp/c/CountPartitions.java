package com.lw.leetcode.dp.c;

/**
 * Created with IntelliJ IDEA.
 * c
 * dp
 * 2518. 好分区的数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/26 14:47
 */
public class CountPartitions {


    public static void main(String[] args) {
        CountPartitions test = new CountPartitions();

        // 6
//        int[] arr = {1, 2, 3, 4};
//        int k = 4;

        // 0
//        int[] arr = {3, 3, 3};
//        int k = 4;

        // 2
        int[] arr = {6, 6};
        int k = 2;

        int i = test.countPartitions(arr, k);
        System.out.println(i);
    }

    public int countPartitions(int[] nums, int k) {
        long sum = 0;
        for (int num : nums) {
            sum += num;
        }
        if ((sum >> 1) < k) {
            return 0;
        }
        long[] arr = new long[k];
        arr[0] = 1;
        for (int num : nums) {
            for (int i = k - 1 - num; i >= 0; i--) {
                arr[i + num] = (arr[i + num] + arr[i]) % 1000000007;
            }
        }
        long s = 0;
        for (int i = 0; i < k; i++) {
            s += arr[i];
        }
        int length = nums.length;
        long item = 1;
        for (int i = 0; i < length; i++) {
            item = (item << 1) % 1000000007;
        }
        item -= (s << 1);

        if (item < 0) {
            return (int) (((item * -1) + 1000000006) / 1000000007 * 1000000007 + item);
        }
        return (int) (item % 1000000007);
    }

}
