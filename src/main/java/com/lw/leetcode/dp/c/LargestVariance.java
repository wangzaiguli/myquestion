package com.lw.leetcode.dp.c;

/**
 * Created with IntelliJ IDEA.
 * 2272. 最大波动的子字符串
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/17 14:19
 */
public class LargestVariance {

    public static void main(String[] args) {
        LargestVariance test = new LargestVariance();

        // 3
//        String str = "aabbaaa";
        // 3
//        String str = "aababbb";

        // 1
        String str = "bbc";

        int i = test.largestVariance(str);
        System.out.println(i);
    }

    public int largestVariance(String s) {
        char[] chars = s.toCharArray();
        int max = 0;
        for (char i = 'a'; i < 'z'; i++) {
            for (char j = (char) (i + 1); j <= 'z'; j++) {
                max = Math.max(max, find(chars, i, j));
                max = Math.max(max, find(chars, j, i));
            }
        }
        return max;
    }

    private int find(char[] nums, char a, char b) {
        int max = 0;
        int item = 0;
        int count = 0;
        boolean bflag = false;
        for (char num : nums) {
            if (num == a) {
                item++;
                count++;
                if (bflag && item > max) {
                    max = count == item ? item - 1 : item;
                }
            } else if (num == b) {
                bflag = true;
                item--;
                if (item < 0) {
                    item = 0;
                    count = 0;
                }
                max = Math.max(max, item);
            }
        }
        return max;
    }

}
