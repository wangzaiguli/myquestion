package com.lw.leetcode.dp.c;

/**
 * Created with IntelliJ IDEA.
 * 132. 分割回文串 II
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/21 9:16
 */
public class MinCut {


    public static void main(String[] args) {
        MinCut test = new MinCut();

        // 1
//        String str = "aab";

        // 0
//        String str = "a";

        // 1
        String str = "ab";

        int i = test.minCut(str);
        System.out.println(i);

    }

    public int minCut(String s) {
        int length = s.length();
        char[] arr = s.toCharArray();
        boolean[][] items = new boolean[length][length];
        for (int i = 0; i < length; i++) {
            items[i][i] = true;
        }
        for (int i = length - 1; i >= 0; i--) {
            for (int j = i + 1; j < length; j++) {
                int a = i + 1;
                int b = j - 1;
                if (((i + 1 == j)||(a <= b && items[a][b])) && arr[i] == arr[j]) {
                    items[i][j] = true;
                }
            }
        }
        int[] counts = new int[length + 1];
        counts[0] = -1;
        for (int i = 1; i < length; i++) {
            int v = i + 1;
            for (int j = 0; j <= i; j++) {
                if (items[j][i]) {
                    v = Math.min(v, counts[j] + 1);
                }
            }
            counts[i + 1] = v;
        }
        return counts[length];
    }

}
