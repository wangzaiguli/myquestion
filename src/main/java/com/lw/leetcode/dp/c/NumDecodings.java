package com.lw.leetcode.dp.c;

/**
 * Created with IntelliJ IDEA.
 *639. 解码方法 II
 * @author liw
 * @version 1.0
 * @date 2021/9/27 12:26
 */
public class NumDecodings {

    public int numDecodings(String s) {
        char c = s.charAt(0);
        if (c == '0'){
            return 0;
        }
        int length = s.length();
        long[] arr = new long[length + 1];
        arr[0] = 1;
        if (c == '*') {
            arr[1] = 9;
        } else {
            arr[1] = 1;
        }
        for (int i = 1; i < length; i++) {
            c = s.charAt(i);
            char p = s.charAt(i - 1);
            if (c == '*') {
                if (p == '1') {
                    arr[i + 1] = arr[i] * 9 + arr[i - 1] * 9 ;
                } else if (p == '2') {
                    arr[i + 1] = arr[i] * 9 + arr[i - 1] * 6;
                } else if (p == '*') {
                    arr[i + 1] = arr[i] * 9 + arr[i - 1] * 15 ;
                } else {
                    arr[i + 1] = arr[i] * 9;
                }

            } else if (c == '0') {
                if (p == '1' || p == '2') {
                    arr[i + 1] = arr[i - 1];
                } else if (p == '*') {
                    arr[i + 1] = arr[i - 1] * 2;
                }
            } else if (c >= '1' && c <= '6') {
                if (p == '1' || p == '2') {
                    arr[i + 1] = arr[i - 1] + arr[i];
                } else if (p == '*') {
                    arr[i + 1] = arr[i - 1] * 2 + arr[i];
                } else {
                    arr[i + 1] = arr[i];
                }
            } else{
                if (p == '1') {
                    arr[i + 1] = arr[i - 1] + arr[i];
                } else if (p == '*') {
                    arr[i + 1] = arr[i - 1] + arr[i];
                } else {
                    arr[i + 1] = arr[i];
                }
            }
            arr[i + 1] %= 1000000007;
        }
        return (int) arr[length];
    }

}
