package com.lw.leetcode.dp.c;

import com.lw.test.util.Utils;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * c
 * dp
 * 2547. 拆分数组的最小代价
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/28 13:48
 */
public class MinCost {

    public static void main(String[] args) {
        MinCost test = new MinCost();

        //
        int[] arr = Utils.getArr(1000, 0, 1);
        int k = 1000000000;
        System.out.println(k);

        int i = test.minCost(arr, k);
        System.out.println(i);
    }

    public int minCost(int[] nums, int k) {
        int length = nums.length;
        int[] arr = new int[length + 1];
        int[] items = new int[1001];
        for (int i = 1; i <= length; i++) {
            arr[i] = arr[i - 1] + k;
            int count = 0;
            Arrays.fill(items, 0);
            for (int j = i - 1; j >= 0; j--) {
                int t = nums[j];
                if (items[t] == 1) {
                    count += 2;
                } else if (items[t] > 1) {
                    count += 1;
                }
                items[t]++;
                arr[i] = Math.min(arr[i], arr[j] + k + count );
            }
        }
        return arr[length];
    }

}
