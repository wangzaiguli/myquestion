package com.lw.leetcode.dp.c;

import com.lw.test.util.Utils;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * c
 * dp
 * 2430. 对字母串可执行的最大删除数
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/8 13:32
 */
public class DeleteString {

    public static void main(String[] args) {
        DeleteString test = new DeleteString();

        // 4
//        String str = "aaabaab";

        // 5
//        String str = "aaaaa";

        // 1
//        String str = "ac";

        //
        String str = Utils.getStr(4000, 'a', 'a');

        int i = test.deleteString(str);
        System.out.println(i);

    }

    public int deleteString(String s) {
        int length = s.length();
        int[][] arr = new int[length + 1][length + 1];
        char[] chars = s.toCharArray();
        for (int i = length - 1; i >= 0; i--) {
            for (int j = i - 1; j >= 0; j--) {
                if (chars[i] == chars[j]) {
                    arr[i][j] = arr[i + 1][j + 1] + 1;
                }
            }
        }
        int[][] arr2 = new int[length + 1][length + 1];
        for (int i = 1; i < length; i++) {
            for (int j = 0; j < i; j++) {
                if (arr[i][j] >= i - j) {
                    arr2[i][j] = 1;
                }
            }
        }
        int[] arr3 = new int[length + 1];
        Arrays.fill(arr3, -1);
        arr3[0] = 0;
        int max = 0;
        for (int i = 1; i <= length; i++) {
            int[] items = arr2[i];
            for (int j = 0; j < length; j++) {
                if (items[j] > 0 && arr3[j] >= 0) {
                    arr3[i] = Math.max(arr3[i], arr3[j] + 1);
                }
            }
            max = Math.max(arr3[i], max);
        }
        return max + 1;
    }

}
