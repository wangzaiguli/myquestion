package com.lw.leetcode.dp.b;

/**1105. 填充书架
 * @Author liw
 * @Date 2021/10/7 14:12
 * @Version 1.0
 */
public class MinHeightShelves {

    public static void main(String[] args){
        MinHeightShelves test = new MinHeightShelves();
        int[][] arr = {{1,1},{2,3},{2,3},{1,1},{1,1},{1,1},{1,2}};
        int k = 4;
        int i = test.minHeightShelves(arr, k);
        System.out.println(i);
    }

    public int minHeightShelves(int[][] books, int shelfWidth) {
        int length = books.length;
        int[] arr = new int[length + 1];
        arr[1] = books[0][1];
        for (int i = 1; i < length; i++) {
            int sum = books[i][0];
            int m = books[i][1];
            arr[i + 1] += arr[i] + m;
            for (int j = i - 1; j >= 0; j--) {
                sum += books[j][0];
                if (sum > shelfWidth) {
                    break;
                }
                m = Math.max(m, books[j][1]);
                arr[i + 1] = Math.min(arr[j] + m , arr[i + 1]);
            }
        }
        return arr[length];
    }

}
