package com.lw.leetcode.dp.b;

/**
 * 650. 只有两个键的键盘
 *
 * @Author liw
 * @Date 2021/5/28 16:12
 * @Version 1.0
 */
public class MinSteps {


    public static void main(String[] args) {
        MinSteps test = new MinSteps();
        int i = test.minSteps(562);
        System.out.println(i);
    }

    public int minSteps(int n) {
        if (n == 1) {
            return 0;
        }
        if (n < 6) {
            return n;
        }
        int[] arr = new int[n + 1];
        arr[2] = 2;
        arr[3] = 3;
        arr[4] = 4;
        arr[5] = 5;
        for (int i = 6; i <= n; i++) {
            int pow = (int) Math.pow(i, 0.5);
            arr[i] = i;
            for (int j = 2; j <= pow; j++) {
                if (i % j == 0) {
                    arr[i] = j + arr[i / j];
                    break;
                }
            }
        }
        return arr[n];
    }

}
