package com.lw.leetcode.dp.b;

/**
 * Created with IntelliJ IDEA.
 * 剑指 Offer II 091. 粉刷房子
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/30 10:29
 */
public class MinCost {

    public int minCost(int[][] costs) {
        int length = costs.length;
        for (int i = 1; i < length; i++) {
            int[] cost = costs[i];
            int[] last = costs[i - 1];
            cost[0] = Math.min(last[1] + cost[0], last[2] + cost[0]);
            cost[1] = Math.min(last[0] + cost[1], last[2] + cost[1]);
            cost[2] = Math.min(last[1] + cost[2], last[0] + cost[2]);
        }
        return Math.min(Math.min(costs[length - 1][0], costs[length - 1][1]), costs[length - 1][2]);
    }

}
