package com.lw.leetcode.dp.b;

/**
 * Created with IntelliJ IDEA.
 * 2
 * dp
 * 2498. 青蛙过河 II
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/11 13:29
 */
public class MaxJump {

    public int maxJump(int[] stones) {
        int length = stones.length;
        int max = stones[1] - stones[0];
        for (int i = 2; i < length; i++) {
            max = Math.max(max, stones[i] - stones[i - 2]);
        }
        return max;
    }

}
