package com.lw.leetcode.dp.b;

/**
 * Created with IntelliJ IDEA.
 * 6109. 知道秘密的人数
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/4 9:20
 */
public class PeopleAwareOfSecret {

    public int peopleAwareOfSecret(int n, int delay, int forget) {
        long[] arr = new long[n];
        arr[0] = 1;
        boolean flag = true;
        for (int i = 1; i < n; i++) {
            arr[i] = arr[i - 1];
            if (i - delay >= 0) {
                arr[i] += arr[i - delay];
            }
            if (i - forget >= 0) {
                arr[i] -= arr[i - forget];
                if (flag) {
                    arr[i]--;
                    flag = false;
                }
            }
            if (arr[i] < 0) {
                arr[i] += 1000000007;
            }
            arr[i] %= 1000000007;
        }
        return (int) arr[n - 1];
    }

}
