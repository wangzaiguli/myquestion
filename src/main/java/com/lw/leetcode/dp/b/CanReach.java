package com.lw.leetcode.dp.b;

/**
 * Created with IntelliJ IDEA.
 * 1871. 跳跃游戏 VII
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/16 21:08
 */
public class CanReach {

    public static void main(String[] args) {
        CanReach test = new CanReach();

        // true
//        String str = "011010";
//        int a = 2;
//        int b = 3;

        // false
//        String str = "01101110";
//        int a = 2;
//        int b = 3;

        // false
        String str = "00111011110";
        int a = 1;
        int b = 2;

        boolean b1 = test.canReach(str, a, b);
        System.out.println(b1);
    }

    public boolean canReach(String s, int minJump, int maxJump) {
        int length = s.length();
        int count = 0;
        char[] chars = s.toCharArray();
        for (int i = 1; i < length; i++) {
            if (i - minJump >= 0 && chars[i - minJump] == '0') {
                count++;
            }
            if (i - maxJump - 1 >= 0 && chars[i - maxJump - 1] == '0') {
                count--;
            }
            if (s.charAt(i) == '0' && count == 0) {
                chars[i] = '1';
            }
        }
        return chars[length - 1] == '0';
    }

}
