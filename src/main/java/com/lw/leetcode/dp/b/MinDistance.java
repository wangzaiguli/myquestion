package com.lw.leetcode.dp.b;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * dp
 * 583. 两个字符串的删除操作
 *
 * @author liw
 * @version 1.0
 * @date 2021/8/13 14:14
 */
public class MinDistance {

    public static void main(String[] args) {
        MinDistance test = new MinDistance();
        // 2
//        String str1 = "eat";
//        String str2 = "sea";


        // 89
        String str1 = "asdfbvbjhjkuyfghtrbnhgfjftyytjhgnjhkhfgnjhghbvhytiuyoiwetwe";
        String str2 = "sdjfgskdtiorjgkfkkfkfvbknpslkspwertlrktlrpowertksfkkjtwekowkqjfkdsjfkj";

        int i = test.minDistance(str1, str2);
        System.out.println(i);
    }

    public int minDistance(String word1, String word2) {
        if (word1 == null || word1.length() == 0) {
            return word2 == null ? 0 : word2.length();
        }
        if (word2 == null || word2.length() == 0) {
            return word1.length();
        }
        int m = word1.length();
        int n = word2.length();
        int[] arr = new int[n + 1];
        for (int i = 0; i <= n; i++) {
            arr[i] = i;
        }
        for (int i = 1; i <= m; i++) {
            int item = arr[0];
            arr[0] = i;
            for (int j = 1; j <= n; j++) {
                int value = arr[j];
                if (word1.charAt(i - 1) == word2.charAt(j - 1)) {
                    arr[j] = item;
                } else {
                    arr[j] = Math.min(Math.min(value + 1, arr[j - 1] + 1), item + 2);
                }
                item = value;
            }
        }
        return arr[n];
    }

}
