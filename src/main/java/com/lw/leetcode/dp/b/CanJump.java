package com.lw.leetcode.dp.b;

/**
 * 55. 跳跃游戏
 *
 * @Author liw
 * @Date 2021/8/30 16:41
 * @Version 1.0
 */
public class CanJump {
    public boolean canJump(int[] nums) {

        int m = nums[0];
        int length = nums.length - 1;
        if (length == 0) {
            return true;
        }
        if (m == 0) {
            return false;
        }
        for (int i = 1; i <= length; i++) {
            m = Math.max(m, i + nums[i]);
            if (m == i) {
                return false;
            }
            if (m >= length) {
                return true;
            }
        }
        return false;
    }
}
