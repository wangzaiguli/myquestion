package com.lw.leetcode.dp.b;

/**
 * Created with IntelliJ IDEA.
 * dp
 * 1749. 任意子数组和的绝对值的最大值
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/25 18:43
 */
public class MaxAbsoluteSum {

    public int maxAbsoluteSum(int[] nums) {
        int a = 0;
        int b = 0;
        int c = 0;
        for (int num : nums) {
            if (num > 0) {
                a = a + num;
                b = Math.min(b + num, 0);
            } else {
                b = b + num;
                a = Math.max(0, a + num);
            }
            c = Math.max(c, Math.max(a, -b));
        }
        return c;
    }

}
