package com.lw.leetcode.dp.b;

/**
 * Created with IntelliJ IDEA.
 * 122. 买卖股票的最佳时机 II
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/17 16:17
 */
public class MaxProfit122 {

    public int maxProfit(int[] prices) {
        int sum = 0;
        if (prices == null || prices.length < 2) {
            return sum;
        }
        int length = prices.length;
        int i1;
        for (int i = 0; i < length - 1; i++) {
            i1 = prices[i + 1] - prices[i];
            if (i1 > 0) {
                sum += i1;
            }
        }
        return sum;
    }

}
