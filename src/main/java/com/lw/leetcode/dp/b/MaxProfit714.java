package com.lw.leetcode.dp.b;

/**
 * Created with IntelliJ IDEA.
 * 714. 买卖股票的最佳时机含手续费
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/22 22:06
 */
public class MaxProfit714 {

    public static void main(String[] args) {
        MaxProfit714 test = new MaxProfit714();
        int[] arr = {1, 3, 2, 8, 4, 9};
        int fee = 2;

        int i = test.maxProfit(arr, fee);
        System.out.println(i);
    }

    public int maxProfit(int[] prices, int fee) {
        int a = 0;
        int b = -prices[0];
        int a1;
        int b1;
        int length = prices.length;
        for (int i = 1; i < length; i++) {
            int price = prices[i];
            b1 = Math.max(b, a - price);
            a1 = Math.max(b + price - fee, a);
            a = a1;
            b = b1;
        }
        return Math.max(a, b);
    }

}
