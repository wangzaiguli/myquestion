package com.lw.leetcode.dp.b;

/**
 * Created with IntelliJ IDEA.
 * 799. 香槟塔
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/4 18:24
 */
public class ChampagneTower {

    public static void main(String[] args) {
        ChampagneTower test = new ChampagneTower();

        // 0.71875
//        int a = 20;
//        int b = 6;
//        int c = 2;

        // 0.5
        int a = 2;
        int b = 1;
        int c = 1;

        double v = test.champagneTower(a, b, c);
        System.out.println(v);
    }

    public double champagneTower(int poured, int row, int glass) {

        if (row == 0) {
            return poured > 1 ? 1 : poured;
        }

        double[] arr1 = new double[row + 1];
        double[] arr2 = new double[row + 1];
        arr1[0] = poured;

        for (int i = 1; i < row; i++) {

            arr2[0] = find(arr1[0]);
            arr2[i] = find(arr1[i - 1]);
            for (int j = 1; j < i; j++) {
                arr2[j] = find(arr1[j - 1]) + find(arr1[j]);
            }

            for (int j = 0; j <= row; j++) {
                arr1[j] = arr2[j];
            }
        }

        double v;
        if (glass == 0) {
            v = find(arr1[0]);
            return v > 1 ? 1 : v;
        }
        if (glass == row) {
            v = find(arr1[row - 1]);
            return v > 1 ? 1 : v;
        }
        v = find(arr1[glass - 1]) + find(arr1[glass]);
        return v > 1 ? 1 : v;
    }

    private double find(double v) {
        if (v > 1D) {
            return (v - 1) / 2;
        }
        return 0D;
    }

}
