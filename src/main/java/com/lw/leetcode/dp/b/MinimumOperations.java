package com.lw.leetcode.dp.b;

/**
 * Created with IntelliJ IDEA.
 * LCP 19. 秋叶收藏集
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/23 16:12
 */
public class MinimumOperations {


    public static void main(String[] args) {
        MinimumOperations test = new MinimumOperations();

        // 2
//        String str = "rrryyyrryyyrr";

        // 24
//        String str = "rrryyyrryyrrrrrrryrrrrrryyyyyyyyyyyyrrrrrrrrrrrrrrrrrryyyrrrrrrrrrrrryyyyyyyyyyrrrrryyrryyyrr";

        // 41
        String str = "ryyryyyrryyyyyryyyrrryyyryryyyyryyrrryryyyryrryrrrryyyrrrrryryyrrrrryyyryyryrryryyryyyyryyrryrryryy";

        // 5
//        String str = "rrrrrrrrrrrryyyyyyyyyyrrrrryyrryyyrr";

        // 3

//        String str = "yry";

        // 3
//        String str = "ryr";

        int i = test.minimumOperations(str);
        System.out.println(i);
    }


    public int minimumOperations(String leaves) {
        int length = leaves.length();
        int a = 0;
        int b = 0;
        int c = 0;
        char ch = leaves.charAt(0);
        if (ch == 'y') {
            a++;
            b++;
            c++;
        }
        ch = leaves.charAt(1);
        if (ch == 'y') {
            a++;
        } else {
            c++;
        }
        ch = leaves.charAt(2);
        if (ch == 'y') {
            a++;
            c++;
        } else {
            b++;
        }
        for (int i = 3; i < length; i++) {
            ch = leaves.charAt(i);
            if (ch == 'y') {
                c = Math.min(c, b) + 1;
                b = Math.min(a, b);
                a++;
            } else {
                c = Math.min(c, b);
                b = Math.min(a, b) + 1;
            }
        }
        return c;
    }

}
