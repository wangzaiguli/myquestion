package com.lw.leetcode.dp.b;

/**
 * Created with IntelliJ IDEA.
 * dp
 * 1824. 最少侧跳次数
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/9 9:30
 */
public class MinSideJumps {

    public static void main(String[] args) {
        MinSideJumps test = new MinSideJumps();

        // 2
//        int[] arr = {0, 1, 2, 3, 0};
        // 0
//        int[] arr = {0,1,1,3,3,0};

        // 2
        int[] arr = {0, 2, 1, 0, 3, 0};

        int i = test.minSideJumps(arr);
        System.out.println(i);
    }

    public int minSideJumps(int[] obstacles) {

        int a = 1;
        int b = 0;
        int c = 1;
        int a1 = 0;
        int b1 = 0;
        int c1 = 0;
        int length = obstacles.length;
        for (int i = 1; i < length; i++) {
            int v = obstacles[i];
            int t = obstacles[i - 1];
            if (v != 1) {
                a1 = t == 1 ? Integer.MAX_VALUE : a;
                if (v != 2 && t != 2) {
                    a1 = Math.min(a1, b + 1);
                }
                if (v != 3 && t != 3) {
                    a1 = Math.min(a1, c + 1);
                }
            }
            if (v != 2) {
                b1 = t == 2 ? Integer.MAX_VALUE : b;
                if (v != 1 && t != 1) {
                    b1 = Math.min(b1, a + 1);
                }
                if (v != 3 && t != 3) {
                    b1 = Math.min(b1, c + 1);
                }
            }
            if (v != 3) {
                c1 = t == 3 ? Integer.MAX_VALUE : c;
                if (v != 1 && t != 1) {
                    c1 = Math.min(c1, a + 1);
                }
                if (v != 2 && t != 2) {
                    c1 = Math.min(c1, b + 1);
                }
            }
            a = a1;
            b = b1;
            c = c1;
        }
        return Math.min(a, Math.min(b, c));
    }

}
