package com.lw.leetcode.dp.b;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 2266. 统计打字方案数
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/12 22:06
 */
public class CountTexts {

    public static void main(String[] args) {
        CountTexts test = new CountTexts();

        // 8
//        String str = "22233";

        // 82876089
//        String str = "222222222222222222222222222222222222";

        // 312882411
//        String str = "777777777777777777777777777777777777";

        // 617572060
        String str = "2227777777799999999999933333333354366666666666654333333333365467777777222222";

        int i = test.countTexts(str);

        System.out.println(i);
    }

    public int countTexts(String pressedKeys) {
        long sum = 1;
        int length = pressedKeys.length();
        int c = 1;
        char item = ' ';
        List<Integer> list = new ArrayList<>();
        int ma = 0;
        int mb = 0;
        for (int i = 0; i < length; i++) {
            char charAt = pressedKeys.charAt(i);
            if (charAt != item) {
                if (item == '7' || item == '9') {
                    mb = Math.max(mb, c);
                    c = (~c) + 1;
                } else {
                    ma = Math.max(ma, c);
                }
                list.add(c);
                c = 1;
                item = charAt;
            } else {
                c++;
            }
        }
        if (item == '7' || item == '9') {
            mb = Math.max(mb, c);
            c = (~c) + 1;
        } else {
            ma = Math.max(ma, c);
        }
        list.add(c);
        long[] as = new long[ma + 1];
        long[] bs = new long[mb + 1];
        as[0] = 1;
        if (ma > 0) {
            as[1] = 1;
        }
        bs[0] = 1;
        if (mb > 0) {
            bs[1] = 1;
        }

        for (int i = 2; i <= ma; i++) {
            for (int j = Math.max(i - 3, 0); j < i; j++) {
                as[i] += as[j];
            }
            as[i] %= 1000000007;
        }

        for (int i = 2; i <= mb; i++) {
            for (int j = Math.max(i - 4, 0); j < i; j++) {
                bs[i] += bs[j];
            }
            bs[i] %= 1000000007;
        }
        int size = list.size();
        for (int i = 1; i < size; i++) {
            int v = list.get(i);
            if (v > 0) {
                sum = (sum * as[v]) % 1000000007;
            } else {
                sum = (sum * bs[~v + 1]) % 1000000007;
            }
        }

        return (int) sum;

    }
}
