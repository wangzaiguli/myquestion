package com.lw.leetcode.dp.b;

/**
 * Created with IntelliJ IDEA.
 * dp
 * 978. 最长湍流子数组
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/7 16:49
 */
public class MaxTurbulenceSize {


    public static void main(String[] args) {
        MaxTurbulenceSize test = new MaxTurbulenceSize();
        int[] arr = {9, 4, 2, 10, 7, 8, 8, 1, 9};
        int i = test.maxTurbulenceSize(arr);
        System.out.println(i);
    }


    public int maxTurbulenceSize(int[] arr) {
        int length = arr.length;
        int max = 1;
        int m1 = 1;
        int m2 = 1;
        for (int i = 1; i < length; i++) {
            if (arr[i] == arr[i - 1]) {
                m1 = 1;
                m2 = 1;
                continue;
            }
            if (arr[i] > arr[i - 1]) {
                if ((i & 1) == 0) {
                    m1 = 1;
                    m2++;
                } else {
                    m2 = 1;
                    m1++;
                }
            } else {
                if ((i & 1) == 0) {
                    m2 = 1;
                    m1++;
                } else {
                    m1 = 1;
                    m2++;
                }
            }
            max = Math.max(max, Math.max(m1, m2));
        }
        return max;
    }


}
