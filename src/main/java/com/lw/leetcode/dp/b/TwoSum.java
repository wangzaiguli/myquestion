package com.lw.leetcode.dp.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * dp
 * 剑指 Offer 60. n个骰子的点数
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/15 11:21
 */
public class TwoSum {

    public static void main(String[] args) {
        TwoSum test = new TwoSum();
        double[] doubles = test.dicesProbability(2);
        System.out.println(Arrays.toString(doubles));
    }

    public double[] dicesProbability(int n) {
        double all = Math.pow(6, n);
        int length = 6 * n;
        int[] arr = new int[length];
        int l = 5 * n + 1;
        double[] ans = new double[n * 5 + 1];
        for (int i = 0; i < 6; i++) {
            arr[i] = 1;
        }
        for (int i = 1; i < n; i++) {
            for (int j = 6 * i + 5; j >= i; j--) {
                int sum = 0;
                for (int k = Math.max(j - 6, 0); k < j; k++) {
                    sum += arr[k];
                }
                arr[j] = sum;
            }
            for (int j = 0; j < i; j++) {
                arr[j] = 0;
            }
        }
        while (l > 0) {
            ans[--l] = arr[--length] / all;
        }
        return ans;
    }

}
