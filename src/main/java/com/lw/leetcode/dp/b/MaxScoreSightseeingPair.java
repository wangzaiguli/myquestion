package com.lw.leetcode.dp.b;

/**
 * 1014. 最佳观光组合
 *
 * @Author liw
 * @Date 2021/6/22 17:14
 * @Version 1.0
 */
public class MaxScoreSightseeingPair {


    public static void main(String[] args) {
        MaxScoreSightseeingPair test = new MaxScoreSightseeingPair();
        //  [8,1,5,2,6]
//        int[] arr = {8,1,5,2,6};
        int[] arr = {1, 2};
        int i = test.maxScoreSightseeingPair(arr);
        System.out.println(i);
    }


    public int maxScoreSightseeingPair(int[] values) {
        int length = values.length;
        int max = values[0];
        int item = max;
        for (int i = 1; i < length; i++) {
            int v = values[i];
            item = Math.max(item, v + max - i);
            max = Math.max(max, v + i);
        }
        return item;
    }
}
