package com.lw.leetcode.dp.b;

/**
 * Created with IntelliJ IDEA.
 * 1911. 最大子序列交替和
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/25 11:37
 */
public class MaxAlternatingSum {

    public static void main(String[] args) {
        MaxAlternatingSum test = new MaxAlternatingSum();

        // 7
//        int[] arr = {4,2,5,3};

        // 8
//        int[] arr = {5,6,7,8};

        // 10
        int[] arr = {6,2,1,2,4,5};

        long sum = test.maxAlternatingSum(arr);
        System.out.println(sum);

    }

    public long maxAlternatingSum(int[] nums) {
        long a = nums[0];
        long b = 0L;
        int length = nums.length;
        for (int i = 1; i < length; i++) {
            long b1 = Math.max(a - nums[i], b);
            a = Math.max(b + nums[i], a);
            b = b1;
        }
        return Math.max(a, b);
    }

}
