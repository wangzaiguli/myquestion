package com.lw.leetcode.dp.b;

/**
 * 718. 最长重复子数组
 *
 * @Author liw
 * @Date 2021/6/2 14:32
 * @Version 1.0
 */
public class FindLength {

    public static void main(String[] args) {
        FindLength test = new FindLength();
        int[] n1 = {0,1,1,1,1};
        int[] n2 = {1,0,1,0,1};
//        int[] n1 = {1,2,3,2,1};
//        int[] n2 = {3,2,1,4,7};
//        int[] n1 = {1,0,0,0,1};
//        int[] n2 = {1,0,0,1,1};
        int length = test.findLength(n1, n2);
        System.out.println(length);
    }

    public int findLength(int[] nums1, int[] nums2) {
        int a = nums1.length;
        int b = nums2.length;
        int[] arr = new int[b + 1];
        int max = 0;
        for (int i = 1; i <= a; i++) {
            int item = 0;
            for (int j = 1; j <= b; j++) {
                int value = arr[j];
                if (nums1[i - 1] == nums2[j - 1]) {
                    arr[j] = ++item;
                    max = Math.max(max, item);
                } else {
                    arr[j] = 0;
                }
                item = value;
            }
        }
        return max;
    }
}
