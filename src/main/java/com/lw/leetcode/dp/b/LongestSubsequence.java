package com.lw.leetcode.dp.b;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 1218. 最长定差子序列
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/15 15:04
 */
public class LongestSubsequence {

    public int longestSubsequence(int[] arr, int difference) {
        Map<Integer, Integer> map = new HashMap<>();
        int max = 1;
        for (int i : arr) {
            Integer count = map.get(i - difference);
            if (count == null) {
                map.put(i, 1);
            } else {
                map.put(i, count + 1);
                max = Math.max(max, count + 1);
            }
        }
        return max;
    }

}
