package com.lw.leetcode.dp.b;

import java.util.Arrays;

/**
 * 1641. 统计字典序元音字符串的数目
 *
 * @Author liw
 * @Date 2021/5/24 14:01
 * @Version 1.0
 */
public class CountVowelStrings {

    public static void main(String[] args) {
        CountVowelStrings test = new CountVowelStrings();
        for (int i = 1; i < 10; i++) {
            int n = test.countVowelStrings(i);
            System.out.println(n);
        }

    }

    public int countVowelStrings(int n) {
        if (n == 1) {
            return 5;
        }
        int[] arr = new int[5];
        Arrays.fill(arr, 1);
        for (int i = 2; i < n; i++) {
            for (int j = 1; j < 5; j++) {
                arr[j] = arr[j - 1] + arr[j];
            }
        }
        int sum = 0;
        for (int i = 0; i < 5; i++) {
            sum += (arr[i] * (5 - i));
        }
        return sum;
    }
}
