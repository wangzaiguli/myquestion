package com.lw.leetcode.dp.b;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * dp
 * 1884. 鸡蛋掉落-两枚鸡蛋
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/16 21:42
 */
public class TwoEggDrop {

    public static void main(String[] args) {
        TwoEggDrop test = new TwoEggDrop();

        // 14
//        int n = 100;

        // 45
        int n = 1000;

        int i = test.twoEggDrop(n);
        System.out.println(i);
        System.out.println(test.map);
    }

    private Map<Integer, Integer> map = new HashMap<>();

    public int twoEggDrop(int n) {
        return find(n, 2);
    }

    private int find(int c, int k) {
        if (c <= 0) {
            return 0;
        }
        if (k == 1) {
            return c;
        }
        int key = c * 2 + k - 1;
        if (map.containsKey(key)) {
            return map.get(key);
        }
        int m = Integer.MAX_VALUE;
        for (int i = 1; i <= c; i++) {
            int v = Math.max(find(i - 1, k - 1), find(c - i, k)) + 1;
            m = Math.min(m, v);
        }
        map.put(key, m);
        return m;
    }

}
