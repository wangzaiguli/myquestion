package com.lw.leetcode.dp.b;

/**
 * Created with IntelliJ IDEA.
 * 63. 不同路径 II
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/3 13:31
 */
public class UniquePathsWithObstacles {
    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        int xl = obstacleGrid.length;
        int yl = obstacleGrid[0].length;
        int t;
        int[][] aa = new int[xl][yl];
        int f = 1;
        for (int i = 0; i < xl; i++) {
            if (f == 1) {
                if (obstacleGrid[i][0] == 1) {
                    f = 0;
                }
            }
            aa[i][0] = f;
        }
        f = 1;
        for (int i = 0; i < yl; i++) {
            if (f == 1) {
                if (obstacleGrid[0][i] == 1) {
                    f = 0;
                }
            }
            aa[0][i] = f;
        }
        for (int i = 1; i < xl; i++) {
            for (int j = 1; j < yl; j++) {
                t = obstacleGrid[i][j];
                if (t == 0) {
                    aa[i][j] = aa[i - 1][j] + aa[i][j - 1];
                }
            }
        }
        return aa[xl - 1][yl - 1];
    }
}
