package com.lw.leetcode.dp.b;

/**
 * dp
 * 647. 回文子串
 * 剑指 Offer II 020. 回文子字符串的个数
 *
 * @Author liw
 * @Date 2021/7/5 17:16
 * @Version 1.0
 */
public class CountSubstrings {

    public static void main(String[] args) {
        CountSubstrings test = new CountSubstrings();
        String str = "cocunttSubstrrtings";
        int i = test.countSubstrings(str);
        System.out.println(i);
    }

    public int countSubstrings(String s) {
        int l = s.length();
        char[] chars = s.toCharArray();
        int[][] arr = new int[l][l];
        int sum = l;
        for (int i = 1; i < l; i++) {
            char value = chars[i];
            for (int j = i - 1; j >= 0; j--) {
                if (chars[j] == value) {
                    if (j + 2 >= i) {
                        arr[i][j] = 1;
                    } else {
                        arr[i][j] = arr[i - 1][j + 1];
                    }
                    sum += arr[i][j];
                }
            }
        }
        return sum;
    }
}
