package com.lw.leetcode.dp.b;

/**
 * 91. 解码方法
 *
 * @Author liw
 * @Date 2021/5/7 15:36
 * @Version 1.0
 */
public class NumDecodings {
    public int numDecodings(String s) {
        char f = s.charAt(0);
        if (f == '0') {
            return 0;
        }
        int length = s.length();
        if (length == 1) {
            return 1;
        }
        int[] dp = new int[length + 1];
        dp[0] = 1;
        dp[1] = 1;
        char l;
        for (int i = 1; i < length; i++) {
            f = s.charAt(i - 1);
            l = s.charAt(i);
            if (l == '0') {
                if (f > '2' || f == '0') {
                    return 0;
                } else {
                    dp[i + 1] = dp[i - 1];
                }
            } else {
                if (f == '0' || ((f - 48) * 10 + (l - 48) > 26)) {
                    dp[i + 1] = dp[i];
                } else {
                    dp[i + 1] = dp[i] + dp[i - 1];
                }
            }
        }
        return dp[length];
    }

}
