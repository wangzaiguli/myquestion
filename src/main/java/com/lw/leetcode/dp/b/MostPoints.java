package com.lw.leetcode.dp.b;

/**
 * Created with IntelliJ IDEA.
 * 2140. 解决智力问题
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/13 17:46
 */
public class MostPoints {

    public long mostPoints(int[][] questions) {
        int length = questions.length;
        long[] arr = new long[length];
        arr[length - 1] = questions[length - 1][0];
        for (int i = length - 2; i >= 0; i--) {
            int[] question = questions[i];
            int index = question[1] + i + 1;
            arr[i] = Math.max(arr[i + 1], question[0] + (index < length ? arr[index] : 0));
        }
        return arr[0];
    }

}
