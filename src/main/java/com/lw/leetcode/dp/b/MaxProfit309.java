package com.lw.leetcode.dp.b;

/**
 * 309. 最佳买卖股票时机含冷冻期
 *
 * @Author liw
 * @Date 2021/7/6 10:38
 * @Version 1.0
 */
public class MaxProfit309 {
    public int maxProfit(int[] prices) {
        int length = prices.length;
        int a = 0;
        int b = -1 * prices[0];
        int c = 0;
        int a1;
        int b1;
        int c1;
        for (int i = 1; i < length; i++) {
            a1 = Math.max(a, c);
            b1 = Math.max(b, a - prices[i]);
            c1 = b + prices[i];
            a = a1;
            b = b1;
            c = c1;
        }
        return a;
    }
}
