package com.lw.leetcode.dp.b;

/**
 * Created with IntelliJ IDEA.
 *1186. 删除一次得到子数组最大和
 * @author liw
 * @version 1.0
 * @date 2021/10/15 15:12
 */
public class MaximumSum {

    public static void main(String[] args) {
        MaximumSum test = new MaximumSum();

        // 4
//        int[] arr = {1,-2,0,3};

        // 3
//        int[] arr = {1,-2,-2,3};

        // -1
        int[] arr = {-1,-1,-1,-1};

        int i = test.maximumSum(arr);
        System.out.println(i);
    }

    public int maximumSum(int[] arr) {
        int length = arr.length;
        if (length == 1) {
            return arr[0];
        }
        int a = Math.max(arr[0], arr[1]);
        int b = Math.max(arr[1], arr[0] + arr[1]);
        int c ;
        int d;
        int max = Math.max(a, b);
        for (int i = 2; i < length; i++) {
            c = Math.max(Math.max(a + arr[i], b), arr[i]);
            d = Math.max(b + arr[i], arr[i]);
            max = Math.max(Math.max(c, max), d);
            a = c;
            b = d;
        }
        return max;
    }
}
