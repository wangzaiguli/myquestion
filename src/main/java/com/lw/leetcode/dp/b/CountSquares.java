package com.lw.leetcode.dp.b;

/**
 * 1277. 统计全为 1 的正方形子矩阵
 *
 * @Author liw
 * @Date 2021/5/28 17:43
 * @Version 1.0
 */
public class CountSquares {


    public static void main(String[] args) {
        CountSquares test = new CountSquares();

    }


    public int countSquares(int[][] matrix) {
        int a = matrix.length;
        int b = matrix[0].length;
        int sum = 0;
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                if (matrix[i][j] == 1) {
                    if (i == 0 || j == 0) {
                        sum++;
                        continue;
                    }
                    int item = Math.min(Math.min(matrix[i - 1][j], matrix[i][j - 1]), matrix[i - 1][j - 1]) + 1;
                    sum += item;
                    matrix[i][j] = item;
                }
            }
        }
        return sum;
    }

}
