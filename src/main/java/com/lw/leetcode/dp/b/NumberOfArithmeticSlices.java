package com.lw.leetcode.dp.b;

/**
 * 413. 等差数列划分
 *
 * @Author liw
 * @Date 2021/6/4 14:52
 * @Version 1.0
 */
public class NumberOfArithmeticSlices {

    public static void main(String[] args) {
        NumberOfArithmeticSlices test = new NumberOfArithmeticSlices();
        int[] arr = {1,2,3,4,5, 11,12,13,14};
        int i = test.numberOfArithmeticSlices(arr);
        System.out.println(i);
    }

    public int numberOfArithmeticSlices(int[] nums) {
        if (nums == null || nums.length < 3) {
            return 0;
        }
        int length = nums.length;
        int count = 0;
        int item = nums[1] - nums[0];
        int sum = 0;
        for (int i = 2; i < length; i++) {
            if (nums[i] - nums[i - 1] == item) {
                count++;
                sum += count;
            } else {
                item = nums[i] - nums[i - 1];
                count = 0;
            }
        }
        return sum;
    }
}
