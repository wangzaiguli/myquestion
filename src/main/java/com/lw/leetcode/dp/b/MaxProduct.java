package com.lw.leetcode.dp.b;

/**
 * Created with IntelliJ IDEA.
 * 152. 乘积最大子数组
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/17 14:03
 */
public class MaxProduct {
    public int maxProduct(int[] nums) {
        int length = nums.length;
        if (length == 1) {
            return nums[0];
        }
        int min = nums[0];
        int max = nums[0];
        int im = 0;
        int am = 0;
        int re = nums[0];
        for (int i = 1; i < length; i++) {
            am = Math.max(Math.max(min * nums[i], max * nums[i]), nums[i]);
            im = Math.min(Math.min(min * nums[i], max * nums[i]), nums[i]);
            re = Math.max(re, am);
            min = im;
            max = am;
        }
        return re;
    }
}
