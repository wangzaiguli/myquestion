package com.lw.leetcode.dp.b;

/**
 * Created with IntelliJ IDEA.
 * 6035. 选择建筑的方案数
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/7 10:10
 */
public class NumberOfWays {

    public static void main(String[] args) {
        NumberOfWays test = new NumberOfWays();

        // 6
//        String str = "001101";

        // 0
//        String str = "11100";

        // 4828
        String str = "1000101010001111010001111100100010111010101110001";

        long l = test.numberOfWays(str);
        System.out.println(l);
    }

    public long numberOfWays(String s) {
        int length = s.length();
        long[] arr = new long[length];
        long a = 0L;
        long b = 0L;
        for (int i = 0; i < length; i++) {
            if (s.charAt(i) == '0') {
                a++;
                arr[i] = b;
            } else {
                b++;
                arr[i] = a;
            }
        }
        a = 0L;
        b = 0L;
        long sum = 0L;
        for (int i = 0; i < length; i++) {
            if (s.charAt(i) == '0') {
                a += arr[i];
                sum += b;
            } else {
                b += arr[i];
                sum += a;
            }
        }
        return sum;
    }
}
