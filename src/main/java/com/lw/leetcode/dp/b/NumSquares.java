package com.lw.leetcode.dp.b;

/**
 * 279. 完全平方数
 *
 * @Author liw
 * @Date 2021/5/6 15:18
 * @Version 1.0
 */
public class NumSquares {

    public static void main(String[] args) {
        NumSquares test = new NumSquares();
        int i = test.numSquares(13);
        System.out.println(i);
    }

    public int numSquares(int n) {
        if (n < 4) {
            return n;
        }
        int[] dp = new int[n + 1];
        dp[0] = 1;
        dp[1] = 1;
        dp[2] = 2;
        dp[3] = 3;
        for (int i = 4; i <= n; i++) {
            int pow = (int) Math.pow(i, 0.5);
            if (pow * pow == i) {
                dp[i] = 1;
                continue;
            }
            int a = i;
            for (int j = 1; j <= pow; j++) {
                a = Math.min(a, dp[i - j * j] + 1);
            }
            dp[i] = a;
        }
        return dp[n];
    }

}
