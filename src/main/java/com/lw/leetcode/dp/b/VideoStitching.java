package com.lw.leetcode.dp.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 1024. 视频拼接
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/22 13:13
 */
public class VideoStitching {

    public static void main(String[] args) {
        VideoStitching test = new VideoStitching();

        // 3
//        int[][] arr = {{0, 2}, {4, 6}, {8, 10}, {1, 9}, {1, 5}, {5, 9}};
//        int time = 10;

        // -1
//        int[][] arr ={{0,1},{1,2}};
//        int time = 5;

        // 3
//        int[][] arr ={{0,1},{6,8},{0,2},{5,6},{0,4},{0,3},{6,7},{1,3},{4,7},{1,4},{2,5},{2,6},{3,4},{4,5},{5,7},{6,9}};
//        int time = 9;

        // 2
//        int[][] arr ={{0,4},{2,8}};
//        int time = 5;

        // -1
        int[][] arr = {{0, 4}, {4, 8}};
        int time = 3;


        int i = test.videoStitching(arr, time);
        System.out.println(i);
    }


    public int videoStitching(int[][] clips, int time) {
        Arrays.sort(clips, (a, b) -> a[0] - b[0]);
        int k = 0;
        int m = 0;
        int c = 0;
        for (int[] clip : clips) {
            if (clip[0] <= k) {
                m = Math.max(m, clip[1]);
            }
            if (clip[0] > k) {
                if (clip[0] > m) {
                    return -1;
                }
                c++;
                k = m;
                m = clip[1];
            }
            if (clip[1] >= time) {
                return c + 1;
            }
        }
        return -1;
    }

}
