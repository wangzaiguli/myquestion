package com.lw.leetcode.dp.b;

/**
 * 64. 最小路径和
 * 剑指 Offer II 099. 最小路径之和
 *
 * @Author liw
 * @Date 2021/11/7 22:50
 * @Version 1.0
 */
public class MinPathSum {
    public int minPathSum(int[][] grid) {
        int xl = grid.length;
        int yl = grid[0].length;
        for (int i = 1; i < yl; i++) {
            grid[0][i] += grid[0][i - 1];
        }
        for (int i = 1; i < xl; i++) {
            grid[i][0] += grid[i - 1][0];
        }
        for (int i = 1; i < xl; i++) {
            for (int j = 1; j < yl; j++) {
                grid[i][j] += Math.min(grid[i - 1][j], grid[i][j - 1]);
            }
        }

        return grid[xl - 1][yl - 1];
    }
}
