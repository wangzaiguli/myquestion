package com.lw.leetcode.dp.b;

/**
 * 474. 一和零
 *
 * @Author liw
 * @Date 2021/6/24 12:52
 * @Version 1.0
 */
public class FindMaxForm {

    public static void main(String[] args) {
        FindMaxForm test = new FindMaxForm();

        // strs = ["10", "0001", "111001", "1", "0"], m = 5, n = 3   4
//        String[] strs = {"10", "0001", "111001", "1", "0"};
//        int m = 5;
//        int n = 3;

        // strs = ["10", "0", "1"], m = 1, n = 1
        String[] strs = {"10", "0", "1"};
        int m = 1;
        int n = 1;

        int maxForm = test.findMaxForm(strs, m, n);
        System.out.println(maxForm);
    }

    public int findMaxForm(String[] strs, int m, int n) {
        int[][] arr = new int[m + 1][n + 1];
        int max = 0;
        for (String str : strs) {
            int length = str.length();
            int a = str.replace("1", "").length();
            int b = length - a;
            for (int i = m; i >= a; i--) {
                for (int j = n; j >= b; j--) {
                    arr[i][j] = Math.max(arr[i - a][j - b] + 1, arr[i][j]);
                    max = Math.max(arr[i][j], max);
                }
            }
        }
        return max;
    }

}
