package com.lw.leetcode.tree;

import java.util.List;

/**
 * @Author liw
 * @Date 2021/4/19 16:03
 * @Version 1.0
 */
public class MaxDepth {

    public int maxDepth(Node root) {
        if (root == null) {
            return 0;
        }
        List<Node> children = root.children;
        int depth = 0;
        if (children != null && !children.isEmpty()) {
            for (int i = children.size() - 1; i >= 0; i--) {
                depth = Math.max(maxDepth(children.get(i)), depth);
            }
        }
        return depth + 1;
    }


}
