package com.lw.leetcode.tree;

import lombok.ToString;

import java.util.List;

/**
 * @Author liw
 * @Date 2021/4/19 12:47
 * @Version 1.0
 */
@ToString
public class Node {
    public int val;
    public List<Node> children;
    public List<Node> neighbors;

    public Node left;

    public Node right;

    public Node next;

    public Node random;

    public Node() {}

    public Node(int _val) {
        val = _val;
    }

    public Node(int _val, List<Node> _children) {
        val = _val;
        children = _children;
    }

    public static Node getInstance() {
        Node a = new Node(2);
        Node b = new Node(1);
        Node c = new Node(3);
        Node d = new Node(0);
        Node e = new Node(7);
        Node f = new Node(9);
        Node g = new Node(1);
        Node h = new Node(2);
        Node i = new Node(1);

        Node j = new Node(0);
        Node k = new Node(8);
        Node l = new Node(8);
        Node m = new Node(7);

        a.left = b;
        a.right = c;
        b.left = d;
        b.right = e;
        c.left = f;
        c.right = g;

        d.left = h;
        e.left = i;
        e.right = j;
        g.left = k;
        g.right = l;
        j.right = m;
        return a;
    }
}
