package com.lw.leetcode.tree.b;

/**
 * Created with IntelliJ IDEA.
 * 96. 不同的二叉搜索树
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/14 21:54
 */
public class NumTrees {
    public int numTrees(int n) {
        if (n < 2) {
            return 1;
        }
        n++;
        int[] dp = new int[n];
        dp[0] = 1;
        dp[1] = 1;
        for (int i = 2; i < n; i++) {
            int j = i - 1;
            int sum = 0;
            for (int k = 0; k <= j; k++) {
                sum += (dp[k] * dp[j - k]);
            }
            dp[i] = sum;
        }
        return dp[n - 1];
    }
}
