package com.lw.leetcode.tree.b;

import com.lw.leetcode.tree.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * 剑指 Offer II 046. 二叉树的右侧视图
 * 199. 二叉树的右视图
 *
 * @Author liw
 * @Date 2021/9/4 21:29
 * @Version 1.0
 */
public class RightSideView {
    public List<Integer> rightSideView(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        if (root == null) {
            return list;
        }
        Queue<TreeNode> a = new LinkedList<>();
        Queue<TreeNode> b = new LinkedList<>();
        a.offer(root);
        TreeNode pop = null;
        while (!a.isEmpty()) {
            while (!a.isEmpty()) {
                pop = a.poll();
                if (pop.left != null) {
                    b.offer(pop.left);
                }
                if (pop.right != null) {
                    b.offer(pop.right);
                }
            }
            a = b;
            b = new LinkedList<>();
            list.add(pop.val);
        }
        return list;
    }
}
