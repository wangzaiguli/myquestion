package com.lw.leetcode.tree.b;

import com.lw.leetcode.tree.TreeNode;

/**
 * Created with IntelliJ IDEA.
 * 701. 二叉搜索树中的插入操作
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/3 13:27
 */
public class InsertIntoBST {

    private int val;
    public TreeNode insertIntoBST(TreeNode root, int val) {
        if (root == null) {
            return new TreeNode(val);
        }
        this.val = val;
        find(root);
        return root;
    }

    private void find (TreeNode node) {
        if (node.val < val) {
            if (node.right == null) {
                node.right = new TreeNode(val);
            } else {
                find(node.right);
            }
        } else {
            if (node.left == null) {
                node.left = new TreeNode(val);
            } else {
                find(node.left);
            }
        }
    }

}
