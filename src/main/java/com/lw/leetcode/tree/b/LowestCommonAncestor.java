package com.lw.leetcode.tree.b;

import com.lw.leetcode.tree.TreeNode;

/**
 * Created with IntelliJ IDEA.
 * 235. 二叉搜索树的最近公共祖先
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/15 9:46
 */
public class LowestCommonAncestor {

    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        while (true) {
            if (root == null) {
                break;
            }
            int a = root.val;
            int b = p.val;
            int c = q.val;
            if (b > c) {
                b = c;
                c = p.val;
            }
            if (a >= b && a <= c) {
                break;
            }
            if (a < b) {
                root = root.right;
            } else {
                root = root.left;
            }
        }
        return root;
    }

}
