package com.lw.leetcode.tree.b;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 1376. 通知所有员工所需的时间
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/15 17:34
 */
public class NumOfMinutes {

    public static void main(String[] args) {
        NumOfMinutes test = new NumOfMinutes();

        //
        int n = 7;
        int id = 6;
        int[] arr1 = {1,2,3,4,5,6,-1};
        int[] arr2 = {0,6,5,4,3,2,1};
        int i = test.numOfMinutes(n, id, arr1, arr2);
        System.out.println(i);
    }

    private int max = 0;
    private int[] informTime;
    private Map<Integer, List<Integer>> map;
    public int numOfMinutes(int n, int headID, int[] manager, int[] informTime) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        int length = manager.length;
        for (int i = 0; i < length; i++) {
            map.computeIfAbsent(manager[i], v -> new ArrayList<>()).add(i);
        }
        this.informTime = informTime;
        this.map = map;
        find(-1, 0);
        return max;
    }

    private void find (int item, int sum) {
        List<Integer> list = map.get(item);
        if (list == null) {
            max = Math.max(max, sum);
            return;
        }
        for (int value : list) {
            find(value, sum + informTime[value]);
        }
    }

}
