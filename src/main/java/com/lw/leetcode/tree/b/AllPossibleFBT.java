package com.lw.leetcode.tree.b;

import com.lw.leetcode.tree.TreeNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 894. 所有可能的满二叉树
 *
 * @Author liw
 * @Date 2021/5/21 22:35
 * @Version 1.0
 */
public class AllPossibleFBT {


    public static void main(String[] args) {
        AllPossibleFBT test = new AllPossibleFBT();

        int n = 7;

        List<TreeNode> treeNodes = test.allPossibleFBT(n);
        System.out.println(treeNodes.size());
        System.out.println(treeNodes);
    }

    public List<TreeNode> allPossibleFBT(int n) {
        if ((n & 1) == 0) {
            return Collections.emptyList();
        }
        List[] arr = new ArrayList[n + 1];
        arr[1] = new ArrayList<>(1);
        arr[1].add(new TreeNode(0));
        for (int i = 3; i <= n; i = i + 2) {
            List<TreeNode> list = new ArrayList<>();
            arr[i] = list;
            for (int j = 1; j < i; j = j + 2) {
                for (TreeNode right : (List<TreeNode>) arr[i - j - 1]) {
                    for (TreeNode left : (List<TreeNode>) arr[j]) {
                        TreeNode node = new TreeNode(0);
                        node.right = right;
                        node.left = left;
                        list.add(node);
                    }
                }
            }
        }
        return arr[n];
    }


}
