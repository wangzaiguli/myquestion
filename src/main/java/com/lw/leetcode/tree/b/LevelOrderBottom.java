package com.lw.leetcode.tree.b;

import com.lw.leetcode.tree.TreeNode;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * 二叉树的层序遍历 II
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/10 10:59
 */
public class LevelOrderBottom {

    public List<List<Integer>> levelOrderBottom(TreeNode root) {
        List<List<Integer>> all = new ArrayList<>();
        if (root == null) {
            return all;
        }
        Queue<TreeNode> a = new LinkedList<>();
        Queue<TreeNode> b = new LinkedList<>();
        a.add(root);
        List<Integer> list = new ArrayList<>();
        do {
            while (!a.isEmpty()) {
                TreeNode node = a.poll();
                if (node.left != null) {
                    b.add(node.left);
                }
                if (node.right != null) {
                    b.add(node.right);
                }
                list.add(node.val);
            }
            all.add(list);
            list = new ArrayList<>();
            a = b;
            b = new LinkedList<>();
        } while (!a.isEmpty());
        Collections.reverse(all);
        return all;
    }

}
