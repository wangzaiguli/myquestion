package com.lw.leetcode.tree.b;

import com.lw.leetcode.tree.TreeNode;

/**
 * 538. 把二叉搜索树转换为累加树
 * 1038. 把二叉搜索树转换为累加树
 * 剑指 Offer II 054. 所有大于等于节点的值之和
 *
 * @Author liw
 * @Date 2021/5/11 17:08
 * @Version 1.0
 */
public class ConvertBST {

    int sum = 0;

    public TreeNode convertBST(TreeNode root) {
        find(root);
        return root;
    }

    private void find(TreeNode node) {
        if (node == null) {
            return;
        }
        find(node.right);
        node.val = sum + node.val;
        sum = node.val;
        find(node.left);
    }

}
