package com.lw.leetcode.tree.b;

import com.lw.leetcode.tree.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * 剑指 Offer 32 - I. 从上到下打印二叉树
 *
 * @Author liw
 * @Date 2021/5/13 14:02
 * @Version 1.0
 */
public class LevelOrder3 {
    public int[] levelOrder(TreeNode root) {
        if (root == null) {
            return new int[0];
        }
        Queue<TreeNode> a = new LinkedList<>();
        a.add(root);
        List<Integer> list = new ArrayList<>();
        while (!a.isEmpty()) {
            TreeNode node = a.poll();
            if (node.left != null) {
                a.add(node.left);
            }
            if (node.right != null) {
                a.add(node.right);
            }
            list.add(node.val);
        }
        int size = list.size();
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = list.get(i);
        }
        return arr;
    }
}
