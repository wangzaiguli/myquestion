package com.lw.leetcode.tree.b;

import com.lw.leetcode.tree.TreeNode;

/**
 * Created with IntelliJ IDEA.
 * LCP 67. 装饰树
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/8 17:55
 */
public class ExpandBinaryTree {
    public TreeNode expandBinaryTree(TreeNode root) {
        find(root);
        return root;
    }

    private void find (TreeNode node) {

        if (node.left != null) {
            TreeNode item = node.left;
            TreeNode no = new TreeNode(-1);
            node.left = no;
            no.left = item;
            find(no);
        }
        if (node.right != null) {
            TreeNode item = node.right;
            TreeNode no = new TreeNode(-1);
            node.right = no;
            no.right = item;
            find(no);
        }

    }
}
