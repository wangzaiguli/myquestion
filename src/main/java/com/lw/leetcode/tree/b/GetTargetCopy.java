package com.lw.leetcode.tree.b;

import com.lw.leetcode.tree.TreeNode;

/**
 * 1379. 找出克隆二叉树中的相同节点
 *
 * @Author liw
 * @Date 2021/5/21 22:01
 * @Version 1.0
 */
public class GetTargetCopy {

    private TreeNode item;

    public final TreeNode getTargetCopy(final TreeNode original, final TreeNode cloned, final TreeNode target) {
        find(cloned, target);
        return item;
    }

    private void find(final TreeNode cloned, final TreeNode target) {
        if (cloned == null) {
            return;
        }
        if (cloned.val == target.val) {
            item = cloned;
            return;
        }
        find(cloned.left, target);
        find(cloned.right, target);
    }

}
