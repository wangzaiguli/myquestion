package com.lw.leetcode.tree.b;

import com.lw.leetcode.tree.TreeNode;

/**
 * 剑指 Offer 26. 树的子结构
 *
 * @Author liw
 * @Date 2021/5/13 14:26
 * @Version 1.0
 */
public class IsSubStructure {

    public boolean isSubStructure(TreeNode A, TreeNode B) {
        if (B == null) {
            return false;
        }
        return find(A, B);
    }

    private boolean find(TreeNode A, TreeNode B) {
        if (A == null) {
            return false;
        }
        return check(A, B) || find(A.left, B) || find(A.right, B);
    }

    private boolean check(TreeNode A, TreeNode B) {

        if (B == null) {
            return true;
        }
        if (A == null) {
            return false;
        }
        return A.val == B.val && check(A.left, B.left) && check(A.right, B.right);
    }

}
