package com.lw.leetcode.tree.b;

import com.lw.leetcode.tree.TreeNode;

/**
 * 129. 求根节点到叶节点数字之和
 * 剑指 Offer II 049. 从根节点到叶节点的路径数字之和
 *
 * @Author liw
 * @Date 2021/5/7 15:39
 * @Version 1.0
 */
public class SumNumbers {

    public int sumNumbers(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return find(root, 0);
    }

    private static int find(TreeNode node, int sum) {
        sum = sum * 10 + node.val;
        if (node.left == null && node.right == null) {
            return sum;
        }
        int returnValue = 0;
        if (node.left != null) {
            returnValue += find(node.left, sum);
        }
        if (node.right != null) {
            returnValue += find(node.right, sum);
        }
        return returnValue;
    }

}
