package com.lw.leetcode.tree.b;

import com.lw.leetcode.tree.TreeNode;

/**
 * 222. 完全二叉树的节点个数
 *
 * @Author liw
 * @Date 2021/5/23 10:58
 * @Version 1.0
 */
public class CountNodes {

    private int count = 0;

    public int countNodes(TreeNode root) {
        find(root);
        return count;
    }

    private void find(TreeNode node) {
        if (node == null) {
            return;
        }
        count++;
        find(node.left);
        find(node.right);
    }

}
