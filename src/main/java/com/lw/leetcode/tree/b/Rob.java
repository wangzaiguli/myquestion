package com.lw.leetcode.tree.b;

import com.lw.leetcode.tree.TreeNode;

/**
 * @Author liw
 * @Date 2021/7/6 17:16
 * @Version 1.0
 */
public class Rob {



    public static void main(String[] args) {
        Rob test = new Rob();
        TreeNode node = TreeNode.getInstance7();

        // [4,1,null,2,null,3]  6
//        TreeNode node = TreeNode.getInstance8();
        int rob = test.rob(node);
        System.out.println(rob);
    }

    public int rob(TreeNode root) {
        int[] arr = find(root);
        return Math.max(arr[0], arr[1]);
    }

    private int[] find (TreeNode node) {
        if (node == null) {
            return new int[]{0, 0};
        }
        int[] arr1 = find(node.left);
        int[] arr2 = find(node.right);
        int a = arr1[0] + arr2[0];
        int b = arr1[1] + arr2[1];
        arr1[0] = Math.max(b + node.val, a);
        arr1[1] = Math.max(a, b);
        return arr1;
    }
}
