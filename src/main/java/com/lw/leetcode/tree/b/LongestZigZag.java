package com.lw.leetcode.tree.b;

import com.lw.leetcode.tree.TreeNode;

/**
 * Created with IntelliJ IDEA.
 * 1372. 二叉树中的最长交错路径
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/13 22:42
 */
public class LongestZigZag {

    private int max;

    public int longestZigZag(TreeNode root) {
        find(root.left, 1, 0);
        find(root.right, 2, 0);
        return max;
    }

    private void find(TreeNode node, int flag, int a) {
        if (node == null) {
            max = Math.max(a, max);
            return;
        }
        if (flag == 1) {
            find(node.left, 1, 0);
            find(node.right, 2, a + 1);
        } else {
            find(node.left, 1, a + 1);
            find(node.right, 2, 0);
        }
    }

}
