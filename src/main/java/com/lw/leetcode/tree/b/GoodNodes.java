package com.lw.leetcode.tree.b;

import com.lw.leetcode.tree.TreeNode;

import java.util.Stack;

/**
 * 1448. 统计二叉树中好节点的数目
 *
 * @Author liw
 * @Date 2021/6/14 22:29
 * @Version 1.0
 */
public class GoodNodes {

    private int count = 0;
    private Stack<Integer> stack = new Stack<>();

    public int goodNodes(TreeNode root) {
        stack.add(root.val);
        find(root);
        return count;
    }
    private void find(TreeNode node) {
        if (node == null) {
            return;
        }
        if (node.val >= stack.peek()) {
            count++;
            stack.add(node.val);
        } else {
            stack.add(stack.peek());
        }
        find(node.left);
        find(node.right);
        stack.pop();
    }
}
