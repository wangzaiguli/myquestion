package com.lw.leetcode.tree.b;

import com.lw.leetcode.tree.TreeNode;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * 1302. 层数最深叶子节点的和
 *
 * @Author liw
 * @Date 2021/5/18 14:49
 * @Version 1.0
 */
public class DeepestLeavesSum {
    public int deepestLeavesSum(TreeNode root) {
        Queue<TreeNode> a = new ArrayDeque<>();
        Queue<TreeNode> b = new ArrayDeque<>();
        a.add(root);
        while (!a.isEmpty() ) {
            int sum = 0;
            while (!a.isEmpty()) {
                TreeNode poll = a.poll();
                if (poll.left != null) {
                    b.add(poll.left);
                }
                if (poll.right != null) {
                    b.add(poll.right);
                }
                sum += poll.val;
            }
            if (b.isEmpty()) {

                return sum;
            }
            a = b;
            b = new ArrayDeque<>();
        }
        return 0;
    }

}
