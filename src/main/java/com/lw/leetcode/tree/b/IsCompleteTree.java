package com.lw.leetcode.tree.b;

import com.lw.leetcode.tree.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * tree
 * 958. 二叉树的完全性检验
 *
 * @Author liw
 * @Date 2021/7/30 23:09
 * @Version 1.0
 */
public class IsCompleteTree {
    public boolean isCompleteTree(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        boolean flag = true;
        while (!queue.isEmpty()) {
            TreeNode poll = queue.poll();
            if (poll.left == null) {
                if (flag) {
                    flag = false;
                }
            } else {
                if (!flag) {
                    return false;
                }
                queue.offer(poll.left);
            }
            if (poll.right == null) {
                if (flag) {
                    flag = false;
                }
            } else {
                if (!flag) {
                    return false;
                }
                queue.offer(poll.right);
            }
        }
        return true;
    }
}
