package com.lw.leetcode.tree.b;

import com.lw.leetcode.tree.Node;

/**
 * 剑指 Offer 36. 二叉搜索树与双向链表
 *
 * @Author liw
 * @Date 2021/5/14 17:43
 * @Version 1.0
 */
public class TreeToDoublyList {

    private Node item = new Node(0);
    private Node f = item;

    public Node treeToDoublyList(Node root) {
        if (root == null) {
            return null;
        }
        find(root);
        Node next = f.right;
        next.left = item;
        item.right = next;
        return next;
    }

    private void find(Node node) {
        if (node == null) {
            return;
        }
        find(node.left);
        item.right = node;
        node.left = item;
        item = item.right;
        find(node.right);
    }

}
