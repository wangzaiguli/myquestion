package com.lw.leetcode.tree.b;

import com.lw.leetcode.tree.TreeNode;

/**
 * 951. 翻转等价二叉树
 *
 * @Author liw
 * @Date 2021/5/11 11:42
 * @Version 1.0
 */
public class FlipEquiv {

    public boolean flipEquiv(TreeNode root1, TreeNode root2) {

        if (root1 == null && root2 ==  null) {
            return true;
        }
        if (root1 == null || root2 == null) {
            System.out.println(root1);
            return false;
        }
        if (root1.val!= root2.val) {
            System.out.println(root1.val + " " + root2.val);
            return false;
        }
        boolean b = flipEquiv(root1.left, root2.right) && flipEquiv(root1.right, root2.left);
        if (b) {
            return b;
        }
        return flipEquiv(root1.left, root2.left) && flipEquiv(root1.right, root2.right);
    }

}
