package com.lw.leetcode.tree.b;

import com.lw.leetcode.tree.TreeNode;

/**
 * 面试题 04.05. 合法二叉搜索树
 * 98. 验证二叉搜索树
 *
 * @Author liw
 * @Date 2021/5/21 11:23
 * @Version 1.0
 */
public class IsValidBST {

    private long item = Long.MIN_VALUE;

    public boolean isValidBST(TreeNode root) {
        if (root == null) {
            return true;
        }
        boolean b = isValidBST(root.left);
        if (!b) {
            return b;
        }
        if (root.val <= item) {
            return false;
        }
        item = root.val;
        return isValidBST(root.right);
    }

}
