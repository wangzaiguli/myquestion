package com.lw.leetcode.tree.b;

import com.lw.leetcode.linked.ListNode;

/**
 * Created with IntelliJ IDEA.
 * tree
 * 2181. 合并零之间的节点
 *
 * @author liw
 * @date 2022/3/2 9:13
 */
public class MergeNodes {

    public ListNode mergeNodes(ListNode head) {
        ListNode ans = new ListNode(0);
        ListNode p = ans;
        while (head.next != null) {
            if (head.val == 0) {
                int sum = 0;
                head = head.next;
                while (head.val != 0) {
                    sum += head.val;
                    head = head.next;
                }
                p.next = new ListNode(sum);
                p = p.next;
            }
        }
        return ans.next;
    }


}
