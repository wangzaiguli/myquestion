package com.lw.leetcode.tree.b;

import com.lw.leetcode.tree.TreeNode;

/**
 * Created with IntelliJ IDEA.
 * 2265. 统计值等于子树平均值的节点数
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/12 21:51
 */
public class AverageOfSubtree {

    private int c = 0;

    public int averageOfSubtree(TreeNode root) {
        find(root);
        return c;
    }

    private int find(TreeNode node) {
        if (node == null) {
            return 0;
        }
        int a = find(node.left);
        int b = find(node.right);
        int sum = (a >> 10) + (b >> 10) + node.val;
        int count = (a & 0X3FF) + (b & 0X3FF) + 1;
        if (sum / count == node.val) {
            c++;
        }
        return (sum << 10) + count;
    }

}
