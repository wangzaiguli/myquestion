package com.lw.leetcode.tree.a;

import com.lw.leetcode.tree.TreeNode;

/**
 * Created with IntelliJ IDEA.
 * 965. 单值二叉树
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/24 9:09
 */
public class IsUnivalTree {

    public boolean isUnivalTree(TreeNode root) {
        if (root == null) {
            return true;
        }
        int item = root.val;
        return find(root, item);
    }

    public boolean find(TreeNode root, int item) {
        if (root != null) {
            if (root.val == item) {
                boolean aa = find(root.left, item);
                if (!aa) {
                    return false;
                }
                return find(root.right, item);
            } else {
                return false;
            }
        }
        return true;
    }

}
