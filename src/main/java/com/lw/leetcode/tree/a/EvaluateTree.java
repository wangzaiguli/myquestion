package com.lw.leetcode.tree.a;

import com.lw.leetcode.tree.TreeNode;

/**
 * Created with IntelliJ IDEA.
 * tree
 * a
 * 6116. 计算布尔二叉树的值
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/11 10:31
 */
public class EvaluateTree {

    public boolean evaluateTree(TreeNode root) {
        if (root.val == 1) {
            return true;
        }
        if (root.val == 0) {
            return false;
        }
        if (root.val == 2) {
            return evaluateTree(root.left) || evaluateTree(root.right);
        }
        return evaluateTree(root.left) && evaluateTree(root.right);
    }

}
