package com.lw.leetcode.tree.a;

import com.lw.leetcode.tree.TreeNode;

import java.util.Stack;

/**
 * tree
 * a
 * <p>
 * 897. 递增顺序搜索树
 * 剑指 Offer II 052. 展平二叉搜索树
 *
 * @Author liw
 * @Date 2021/4/25 14:04
 * @Version 1.0
 */
public class IncreasingBST {

    public static void main(String[] args) {
        IncreasingBST test = new IncreasingBST();
        TreeNode treeNode = test.increasingBST(TreeNode.getInstance());
        System.out.println(treeNode);
    }

    public TreeNode increasingBST(TreeNode root) {
        Stack<TreeNode> stack = new Stack<>();
        TreeNode item = root;
        TreeNode a = new TreeNode(0);
        TreeNode r = a;
        while (!stack.isEmpty() || item != null) {
            if (item != null) {
                stack.add(item);
                item = item.left;
            } else {
                TreeNode node = stack.pop();
                a.right = node;
                item = node.right;
                node.right = null;
                node.left = null;
                a = a.right;
            }
        }
        return r.right;
    }

}
