package com.lw.leetcode.tree.a;

import com.lw.leetcode.tree.TreeNode;

/**
 * 671. 二叉树中第二小的节点
 */
public class FindSecondMinimumValue {
    long min = Long.MAX_VALUE;
    public int findSecondMinimumValue(TreeNode root) {
        find(root, root.val);
        return min == Long.MAX_VALUE ? -1 : (int) min;
    }

    private void find(TreeNode node, int f) {
        if (node == null) {
            return ;
        }
        if (node.val == f) {
            find(node.left, f);
            find(node.right, f);
        } else {
            min = Math.min(node.val, min);
        }
    }
}
