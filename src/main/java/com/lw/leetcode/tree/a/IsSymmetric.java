package com.lw.leetcode.tree.a;

import com.lw.leetcode.tree.TreeNode;

/**
 * 剑指 Offer 28. 对称的二叉树
 * 101. 对称二叉树
 *
 * @Author liw
 * @Date 2021/5/13 14:15
 * @Version 1.0
 */
public class IsSymmetric {
    public boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        }
        return isSymmertric(root.left, root.right);
    }

    private boolean isSymmertric(TreeNode t1, TreeNode t2) {
        if (t1 == null && t2 == null) {
            return true;
        }
        if (t1 == null || t2 == null) {
            return false;
        }
        return t1.val == t2.val && isSymmertric(t1.left, t2.right) && isSymmertric(t1.right, t2.left);
    }
}
