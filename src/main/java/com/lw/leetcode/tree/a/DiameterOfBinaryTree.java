package com.lw.leetcode.tree.a;

import com.lw.leetcode.tree.TreeNode;

/**
 * 543. 二叉树的直径
 *
 * @Author liw
 * @Date 2021/5/6 16:09
 * @Version 1.0
 */
public class DiameterOfBinaryTree {
    public static void main(String[] args) {
        DiameterOfBinaryTree test = new DiameterOfBinaryTree();
        int i = test.diameterOfBinaryTree(TreeNode.getInstance());
        System.out.println(i);
    }

    static int sum = 0;

    public int diameterOfBinaryTree(TreeNode root) {
        find(root);
        return sum;
    }

    private static int find(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int l = find(root.left);
        int r = find(root.right);
        sum = Math.max(sum, l + r);
        return Math.max(r, l) + 1;
    }

}
