package com.lw.leetcode.tree.a;

import com.lw.leetcode.tree.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 94. 二叉树的中序遍历
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/15 9:10
 */
public class InorderTraversal {
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<Integer>();
        if (root == null) {
            return list;
        }
        find(root, list);
        return list;
    }

    private static void find(TreeNode node, List<Integer> list) {
        if (node.left != null) {
            find(node.left, list);
        }
        list.add(node.val);
        if (node.right != null) {
            find(node.right, list);
        }
    }
}
