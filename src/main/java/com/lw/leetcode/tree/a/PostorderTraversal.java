package com.lw.leetcode.tree.a;

import com.lw.leetcode.tree.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * 145. 二叉树的后序遍历
 *
 * @Author liw
 * @Date 2021/4/29 16:16
 * @Version 1.0
 */
public class PostorderTraversal {

    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        find(root, list);
        return list;
    }

    private void find(TreeNode node, List<Integer> list) {
        if (node == null) {
            return;
        }
        find(node.left, list);
        find(node.right, list);
        list.add(node.val);
    }

}
