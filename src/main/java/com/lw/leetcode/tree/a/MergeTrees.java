package com.lw.leetcode.tree.a;

import com.lw.leetcode.tree.TreeNode;

/**
 * 617. 合并二叉树
 *
 * @Author liw
 * @Date 2021/5/11 17:51
 * @Version 1.0
 */
public class MergeTrees {

    public TreeNode mergeTrees(TreeNode root1, TreeNode root2) {
        if (root1 == null) {
            return root2;
        }
        if (root2 == null) {
            return root1;
        }
        find(root1, root2);
        return root1;
    }

    private void find(TreeNode root1, TreeNode root2) {
        root1.val = root1.val + root2.val;
        if (root1.left == null) {
            root1.left = root2.left;
        } else {
            if (root2.left != null) {
                find(root1.left, root2.left);
            }
        }
        if (root1.right == null) {
            root1.right = root2.right;
        } else {
            if (root2.right != null) {
                find(root1.right, root2.right);
            }
        }
    }

}
