package com.lw.leetcode.tree.a;

import com.lw.leetcode.tree.TreeNode;

/**
 * 剑指 Offer 55 - I. 二叉树的深度
 * 104. 二叉树的最大深度
 *
 * @Author liw
 * @Date 2021/4/19 16:03
 * @Version 1.0
 */
public class MaxDepth2 {

    public int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return maxDepth(root, 0);
    }

    public int maxDepth(TreeNode root, int n) {
        if (root.left != null && root.right != null) {
            return Math.max(maxDepth(root.left, n + 1), maxDepth(root.right, n + 1));
        } else if (root.left != null) {
            return maxDepth(root.left, n + 1);
        } else if (root.right != null) {
            return maxDepth(root.right, n + 1);
        } else {
            return n + 1;
        }
    }


}
