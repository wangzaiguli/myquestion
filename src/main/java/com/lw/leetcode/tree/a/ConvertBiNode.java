package com.lw.leetcode.tree.a;

import com.lw.leetcode.tree.TreeNode;

/**
 * 面试题 17.12. BiNode
 *
 * @Author liw
 * @Date 2021/5/10 16:15
 * @Version 1.0
 */
public class ConvertBiNode {


    public static void main(String[] args) {
        ConvertBiNode test = new ConvertBiNode();
        TreeNode instance = TreeNode.getInstance2();
        System.out.println(instance);
        TreeNode treeNode = test.convertBiNode(instance);
        System.out.println(treeNode);

    }

    private TreeNode head = new TreeNode(0);

    private TreeNode item = head;

    public TreeNode convertBiNode(TreeNode root) {
        if (root == null) {
            return null;
        }
        convertBiNode(root.left);
        TreeNode right = root.right;
        item.right = root;
        root.right = null;
        root.left = null;
        item = root;
        convertBiNode(right);
        return head.right;
    }


}
