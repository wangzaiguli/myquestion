package com.lw.leetcode.tree.a;

import com.lw.leetcode.tree.TreeNode;

/**
 * 700. 二叉搜索树中的搜索
 *
 * @Author liw
 * @Date 2021/5/14 16:23
 * @Version 1.0
 */
public class SearchBST {
    public TreeNode searchBST(TreeNode root, int val) {

        if (root == null || root.val == val) {
            return root;
        }
        if (root.val > val) {
            return searchBST(root.left, val);
        }
        return searchBST(root.right, val);
    }
}
