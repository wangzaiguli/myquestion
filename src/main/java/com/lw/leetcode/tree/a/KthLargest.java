package com.lw.leetcode.tree.a;

import com.lw.leetcode.tree.TreeNode;

/**
 * 剑指 Offer 54. 二叉搜索树的第k大节点
 *
 * @Author liw
 * @Date 2021/5/13 13:12
 * @Version 1.0
 */
public class KthLargest {

    private int value = 0;
    private int count = 1;

    public int kthLargest(TreeNode root, int k) {
        find(root, k);
        return value;
    }

    private boolean find(TreeNode node, int k) {
        if (node == null) {
            return true;
        }
        boolean b = find(node.right, k);
        if (!b) {
            return b;
        }
        if (k == count) {
            value = node.val;
            return false;
        }
        count++;
        return find(node.left, k);
    }

}
