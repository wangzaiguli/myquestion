package com.lw.leetcode.tree.a;

import com.lw.leetcode.tree.TreeNode;

/**
 * Created with IntelliJ IDEA.
 * 100. 相同的树
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/14 21:35
 */
public class IsSameTree {
    public boolean isSameTree(TreeNode p, TreeNode q) {

        if (p == null && q == null) {
            return true;
        }
        if (p == null || q == null) {
            return false;
        }
        if (p.val == q.val) {
            if (isSameTree(p.left, q.left)) {
                return isSameTree(p.right, q.right);
            }
        }
        return false;
    }
}
