package com.lw.leetcode.tree.a;

import com.lw.leetcode.tree.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 144. 二叉树的前序遍历
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/30 21:10
 */
public class PreorderTraversal {

    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        find(root, list);
        return list;
    }

    private static void find(TreeNode root, List<Integer> list) {
        if (root == null) {
            return;
        }
        list.add(root.val);
        find(root.left, list);
        find(root.right, list);
    }

}
