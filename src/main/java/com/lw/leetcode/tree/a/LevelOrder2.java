package com.lw.leetcode.tree.a;

import com.lw.leetcode.tree.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * 102. 二叉树的层序遍历
 * 剑指 Offer 32 - II. 从上到下打印二叉树 II
 *
 * @Author liw
 * @Date 2021/5/13 14:02
 * @Version 1.0
 */
public class LevelOrder2 {
    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> all = new ArrayList<>();
        if (root == null) {
            return all;
        }
        Queue<TreeNode> a = new LinkedList<>();
        Queue<TreeNode> b = new LinkedList<>();
        a.add(root);
        List<Integer> list = new ArrayList<>();
        do {
            while (!a.isEmpty()) {
                TreeNode node = a.poll();
                if (node.left != null) {
                    b.add(node.left);
                }
                if (node.right != null) {
                    b.add(node.right);
                }
                list.add(node.val);
            }
            all.add(list);
            list = new ArrayList<>();
            a = b;
            b = new LinkedList<>();
        } while (!a.isEmpty());
        return all;
    }
}
