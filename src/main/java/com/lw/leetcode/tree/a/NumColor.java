package com.lw.leetcode.tree.a;

import com.lw.leetcode.tree.TreeNode;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * LCP 44. 开幕式焰火
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/24 20:06
 */
public class NumColor {

    public int numColor(TreeNode root) {
        HashMap<Integer, Integer> map = new HashMap<>();
        dfs(root, map);
        return map.size();
    }

    private void dfs(TreeNode root, HashMap<Integer, Integer> map) {
        if (root == null) {
            return;
        }
        map.put(root.val, map.getOrDefault(root.val, 0) + 1);
        dfs(root.left, map);
        dfs(root.right, map);
    }

}
