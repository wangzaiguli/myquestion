package com.lw.leetcode.tree.a;

import com.lw.leetcode.tree.TreeNode;

/**
 * 530. 二叉搜索树的最小绝对差
 * 783. 二叉搜索树节点最小距离
 *
 * @Author liw
 * @Date 2021/5/11 13:50
 * @Version 1.0
 */
public class GetMinimumDifference {
    TreeNode item = null;
    int min = Integer.MAX_VALUE;
    public int getMinimumDifference(TreeNode root) {
        find(root);
        return min;
    }

    public void find(TreeNode node) {
        if (node== null) {
            return;
        }
        find(node.left);
        if (item != null) {
            min = Math.min(node.val - item.val, min);
        }
        item = node;
        find(node.left);
    }

}
