package com.lw.leetcode.tree.a;

import com.lw.leetcode.tree.TreeNode;

/**
 * 1022. 从根到叶的二进制数之和
 */
public class SumRootToLeaf {

    private int sum = 0;

    public int sumRootToLeaf(TreeNode root) {
        if (root == null) {
            return 0;
        }
        find(root, 0);
        return sum;
    }

    private void find(TreeNode root, int value) {
        value = (value << 1) + root.val;
        if (root.left == null && root.right == null) {
            sum += value;
            return;
        }
        if (root.left != null) {
            find(root.left, value);
        }
        if (root.right != null) {
            find(root.right, value);
        }
    }

}
