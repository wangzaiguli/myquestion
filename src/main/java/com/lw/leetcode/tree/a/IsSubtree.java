package com.lw.leetcode.tree.a;

import com.lw.leetcode.tree.TreeNode;

/**
 * 572. 另一个树的子树
 *
 * @Author liw
 * @Date 2021/5/11 17:15
 * @Version 1.0
 */
public class IsSubtree {

    public boolean isSubtree(TreeNode s, TreeNode t) {
        if (s == null && t == null) {
            return true;
        }
        if (s == null || t == null) {
            return false;
        }
        if (s.val == t.val && (find(s.left, t.left) && find(s.right, t.right))) {
            return true;
        }
        return isSubtree(s.left, t) || isSubtree(s.right, t);
    }

    private boolean find(TreeNode s, TreeNode t) {
        if (s == null && t == null) {
            return true;
        }
        if (s == null || t == null) {
            return false;
        }
        return s.val == t.val && (find(s.left, t.left) && find(s.right, t.right));
    }

}
