package com.lw.leetcode.stack.b;

import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * stack
 * 1003. 检查替换后的词是否有效
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/7 16:58
 */
public class IsValid {

    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        int length = s.length();
        for (int i = 0; i < length; i++) {
            char c = s.charAt(i);
            if (c == 'c') {
                if (stack.size() < 2) {
                    return false;
                }
                if (stack.pop() != 'b') {
                    return false;
                }
                if (stack.pop() != 'a') {
                    return false;
                }
            } else {
                stack.add(c);
            }
        }
        return stack.isEmpty();
    }

}
