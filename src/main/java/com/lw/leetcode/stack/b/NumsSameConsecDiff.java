package com.lw.leetcode.stack.b;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * back
 * 967. 连续差相同的数字
 *
 * @Author liw
 * @Date 2021/10/30 22:01
 * @Version 1.0
 */
public class NumsSameConsecDiff {

    public static void main(String[] args){
        NumsSameConsecDiff test = new NumsSameConsecDiff();
        int[] ints = test.numsSameConsecDiff(3, 7);
        System.out.println(Arrays.toString(ints));
    }

    List<Integer> list;
    private int k;
    private int n;

    public int[] numsSameConsecDiff(int n, int k) {
        list = new ArrayList<>();
        this.n = n - 1;
        this.k = k;


        for (int i = 1; i < 10; i++) {
            find(0, i);
        }
        int length = list.size();
        int[] arr = new int[length];

        for (int i = 0; i < length; i++) {
            arr[i] = list.get(i);
        }
        return arr;
    }

    private void find (int count, int item) {
        if (count == n) {
            list.add(item);
            return;
        }

        int v = item % 10;
        if (v + k < 10) {
            find(count + 1, item * 10 + v + k);
        }
        if (k == 0) {
            return;
        }

        if (v - k >= 0) {
            find(count + 1, item * 10 + v - k);
        }
    }

}
