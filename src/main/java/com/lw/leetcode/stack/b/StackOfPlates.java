package com.lw.leetcode.stack.b;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * stack
 * 面试题 03.03. 堆盘子
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/13 11:45
 */
public class StackOfPlates {

    public static void main(String[] args) {
        StackOfPlates test = new StackOfPlates(1);
        long a = 1;
        long b = (a << 32) - 1;
        System.out.println(b);
        System.out.println((int)b);
    }


    private List<Stack<Integer>> stacks = new ArrayList<>();
    private int cap;

    public StackOfPlates(int cap) {
        this.cap = cap;
    }

    public void push(int val) {
        if (cap < 1) {
            return;
        }
        if (stacks.isEmpty() || stacks.get(stacks.size() - 1).size() == cap) {
            stacks.add(new Stack<>());
        }
        stacks.get(stacks.size() - 1).push(val);
    }

    public int pop() {
        return popAt(stacks.size() - 1);
    }

    public int popAt(int index) {
        if (cap == 0 || index < 0 || index >= stacks.size()) {
            return -1;
        }
        int pop = stacks.get(index).pop();
        if (stacks.get(index).empty()) {
            stacks.remove(index);
        }
        return pop;
    }
}
