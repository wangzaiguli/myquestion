package com.lw.leetcode.stack.b;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * 636. 函数的独占时间
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/7 20:12
 */
public class ExclusiveTime {

    public static void main(String[] args) {
        ExclusiveTime test = new ExclusiveTime();

        // [3,4]
//        int n = 2;
//        List<String> list = Arrays.asList("0:start:0", "1:start:2", "1:end:5", "0:end:6");

        // [10]
//        int n = 1;
//        List<String> list = Arrays.asList("0:start:0","0:start:1","0:start:2","0:start:3","0:start:4","0:end:5","0:end:6","0:end:7","0:end:8","0:end:9");

        // [8]
        int n = 1;
        List<String> list = Arrays.asList("0:start:0", "0:start:2", "0:end:5", "0:start:6", "0:end:6", "0:end:7");

        // [7, 1]
//        int n = 2;
//        List<String> list = Arrays.asList("0:start:0", "0:start:2", "0:end:5", "1:start:6", "1:end:6", "0:end:7");

        int[] ints = test.exclusiveTime(n, list);
        System.out.println(Arrays.toString(ints));

    }

    public int[] exclusiveTime(int n, List<String> logs) {
        int[] arr = new int[n];
        long[] counts = new long[501];
        int index = -1;
        for (String log : logs) {
            int a = 0;
            int type = 0;
            long b = 0;
            int i = -1;
            while (log.charAt(++i) != ':') {
                a = a * 10 + (int) log.charAt(i) - 48;
            }
            if (log.charAt(++i) == 'e') {
                i += 3;
                type = 1;
            } else {
                i += 5;
            }
            while (++i < log.length()) {
                b = b * 10 + (int) log.charAt(i) - 48;
            }

            if (type == 0) {
                counts[++index] = b << 32;
            } else {
                long t = counts[index];
                int pop = (int) (t >> 32);
                int p = (int) t;
                arr[a] += (b - pop - p + 1);
                index--;
                if (index != -1) {
                    counts[index] += (b - pop + 1);
                }
            }
        }
        return arr;
    }

    public int[] exclusiveTime3(int n, List<String> logs) {
        int[] arr = new int[n];
        long[] counts = new long[501];
        int index = -1;
        for (String log : logs) {
            String[] split = log.split(":");
            int a = Integer.parseInt(split[0]);
            long b = Long.parseLong(split[2]);
            if ("start".equals(split[1])) {
                counts[++index] = b << 32;
            } else {
                long t = counts[index];
                int pop = (int) (t >> 32);
                int p = (int) t;
                arr[a] += (b - pop - p + 1);
                index--;
                if (index != -1) {
                    counts[index] += (b - pop + 1);
                }
            }
        }
        return arr;
    }

    public int[] exclusiveTime2(int n, List<String> logs) {
        int[] arr = new int[n];
        Stack<Integer> stack = new Stack<>();
        Stack<Integer> counts = new Stack<>();
        for (String log : logs) {
            String[] split = log.split(":");
            int a = Integer.parseInt(split[0]);
            int b = Integer.parseInt(split[2]);
            if ("start".equals(split[1])) {
                stack.add(b);
                counts.add(0);
            } else {
                Integer pop = stack.pop();
                Integer p = counts.pop();
                arr[a] += (b - pop - p + 1);
                if (!stack.isEmpty()) {
                    counts.add(counts.pop() + b - pop + 1);
                }
            }
        }
        return arr;
    }

}
