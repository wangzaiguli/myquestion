package com.lw.leetcode.stack.b;

import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * 1472. 设计浏览器历史记录
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/10 10:36
 */
public class BrowserHistory {

    private Stack<String> back;
    private Stack<String> head;

    public BrowserHistory(String homepage) {
        back = new Stack<>();
        head = new Stack<>();
        back.add(homepage);
    }

    public void visit(String url) {
        back.add(url);
        head.clear();
    }

    public String back(int steps) {
        for (int i = 0; i < steps; i++) {
            if (back.isEmpty()) {
                back.add(head.pop());
                break;
            }
            head.add(back.pop());
        }
        if (back.isEmpty()) {
            back.add(head.pop());
        }
        return back.peek();
    }

    public String forward(int steps) {
        for (int i = 0; i < steps; i++) {
            if (head.isEmpty()) {
                break;
            }
            back.add(head.pop());
        }
        return back.peek();
    }

}
