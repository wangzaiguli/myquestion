package com.lw.leetcode.stack.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * stack
 * 1381. 设计一个支持增量操作的栈
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/16 9:58
 */
public class CustomStack {

    public static void main(String[] args) {

        // [null,null,null,2,null,null,null,null,null,103,202,201,-1]
        int size = 14;
        CustomStack test = new CustomStack(size);
        test.push(48);
        test.increment(7, 2);
//        test.push(2);
        System.out.println(test.pop());
        System.out.println(test.pop());
        System.out.println(test.pop());
        test.increment(10, 13);
        test.increment(10, 13);
        test.push(3);
        test.push(96);
//        test.push(41);
//        test.increment(5, 100);
        System.out.println(Arrays.toString(test.values));
        System.out.println(Arrays.toString(test.items));
//        System.out.println(test.pop());
//        System.out.println(test.pop());
//        test.push(89);
//        test.push(4);
//        test.push(96);
//        System.out.println(test.pop());
//        System.out.println(test.pop());

        // ["CustomStack","push","increment","pop","increment","push","push","push","pop","pop","push","push","push"]
        //[[14],[48],[7,2],[],[10,13],[3],[96],[41],[],[],[89],[4],[96]]
    }


    private int[] values;
    private int[] items;
    private int maxSize;
    private int count;

    public CustomStack(int maxSize) {
        this.values = new int[maxSize];
        this.items = new int[maxSize];
        this.maxSize = maxSize;
    }

    public void push(int x) {
        if (count < maxSize) {
            values[count++] = x;
        }
    }

    public int pop() {
        if (count == 0) {
            return -1;
        }
        count--;
        int v = values[count] + items[count];
        if (count != 0) {
            items[count - 1] += items[count];
        }
        items[count] = 0;
        return v;
    }

    public void increment(int k, int val) {
        if (count == 0) {
            return;
        }
        k--;
        if (k >= count) {
            k = count - 1;
        }
        items[k] += val;
    }

}
