package com.lw.leetcode.stack.b;

import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * 901. 股票价格跨度
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/9 17:47
 */
class StockSpanner {

    private Stack<Integer> prices;
    private Stack<Integer> weights;

    public StockSpanner() {
        prices = new Stack<>();
        weights = new Stack<>();
    }

    public int next(int price) {
        int w = 1;
        while (!prices.isEmpty() && prices.peek() <= price) {
            prices.pop();
            w += weights.pop();
        }
        prices.push(price);
        weights.push(w);
        return w;
    }
}
