package com.lw.leetcode.stack.b;

import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * 388. 文件的最长绝对路径
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/20 9:04
 */
public class LengthLongestPath {

    public static void main(String[] args) {
        LengthLongestPath test = new LengthLongestPath();

        // 20
//        String str = "dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext";

        // 32
//        String str = "dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext";

        // 0
//        String str = "a";

        // 12
//        String str = "file1.txt\nfile2.txt\nlongfile.txt";

        // 16
        String str = "dir\n        file.txt";

        int i = test.lengthLongestPath(str);
        System.out.println(i);
    }

    public int lengthLongestPath(String input) {
        int length = input.length();
        int index = 0;
        int a = 0;
        int b = 0;
        char[] arr = input.toCharArray();
        Stack<Integer> stack = new Stack<>();
        int max = 0;
        while (index != length) {
            while (index != length && (arr[index] == '\t' || arr[index] == '\n')) {
                index++;
                a++;
            }
            if (a > 0) {
                a--;
            }
            boolean f = false;
            while (index != length && (arr[index] != '\t' && arr[index] != '\n')) {
                if (arr[index] == '.') {
                    f = true;
                }
                index++;
                b++;
            }
            while (stack.size() > a) {
                stack.pop();
            }
            if (f) {
                if (!stack.isEmpty()) {
                    b += stack.peek() + stack.size();
                }
                max = Math.max(max, b);
            } else {
                if (!stack.isEmpty()) {
                    b += stack.peek();
                }
                stack.add(b);
            }
            a = 0;
            b = 0;
        }
        return max;
    }

}
