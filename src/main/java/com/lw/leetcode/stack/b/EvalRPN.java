package com.lw.leetcode.stack.b;

import java.util.Stack;

/**
 * 150. 逆波兰表达式求值
 * 剑指 Offer II 036. 后缀表达式
 *
 * @Author liw
 * @Date 2021/4/29 17:54
 * @Version 1.0
 */
public class EvalRPN {

    public int evalRPN(String[] tokens) {
        Stack<Integer> stack = new Stack<>();
        for (String token : tokens) {
            if ("+".equals(token)) {
                Integer a = stack.pop();
                Integer b = stack.pop();
                stack.add(a + b);
            } else if ("-".equals(token)) {
                Integer a = stack.pop();
                Integer b = stack.pop();
                stack.add(b - a);
            } else if ("*".equals(token)) {
                Integer a = stack.pop();
                Integer b = stack.pop();
                stack.add(a * b);
            } else if ("/".equals(token)) {
                Integer a = stack.pop();
                Integer b = stack.pop();
                stack.add(b / a);
            } else {
                stack.add(Integer.parseInt(token));
            }
        }
        return stack.pop();
    }

}
