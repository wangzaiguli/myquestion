package com.lw.leetcode.stack.b;

import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * stack
 * 1111. 有效括号的嵌套深度
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/12 14:10
 */
public class MaxDepthAfterSplit {

    public int[] maxDepthAfterSplit(String seq) {
        int length = seq.length();
        char[] chars = seq.toCharArray();
        int[] arr = new int[length];
        chars[0] = 0;
        int index = 0;
        for (int i = 1; i < length; i++) {
            if (index == -1) {
                index = 0;
                continue;
            }
            char c = chars[i];
            char p = chars[index];
            if (c == '(') {
                chars[++index] = (char) (p ^ 1);
                arr[i] = p ^ 1;
            } else {
                index--;
                arr[i] = p;
            }
        }
        return arr;
    }


    public int[] maxDepthAfterSplit2(String seq) {
        int length = seq.length();
        char[] chars = seq.toCharArray();
        int[] arr = new int[length];
        Stack<Integer> stack = new Stack<>();
        stack.add(0);
        chars[0] = 0;
        for (int i = 1; i < length; i++) {
            if (stack.isEmpty()) {
                stack.add(0);
                continue;
            }
            char c = chars[i];
            int p = stack.peek();
            if (c == '(') {
                stack.add(p ^ 1);
                arr[i] = p ^ 1;
            } else {
                stack.pop();
                arr[i] = p;
            }
        }
        return arr;
    }

}
