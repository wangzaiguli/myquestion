package com.lw.leetcode.stack.b;

import java.util.Stack;

/**
 * 962. 最大宽度坡
 *
 * @Author liw
 * @Date 2021/5/17 13:22
 * @Version 1.0
 */
public class MaxWidthRamp {

    public int maxWidthRamp(int[] nums) {
        Stack<Integer> stack = new Stack<>();
        int length = nums.length;
        for (int i = 0; i < length; i++) {
            int num = nums[i];
            if (stack.isEmpty() || nums[stack.peek()] > num) {
                stack.add(i);
            }
        }
        int max = 0;
        for (int i = length - 1; i >= 0; i--) {
            int num = nums[i];
            while (!stack.isEmpty() && nums[stack.peek()] <= num) {
                max = Math.max(max, i - stack.pop());
            }

        }
        return max;
    }

}
