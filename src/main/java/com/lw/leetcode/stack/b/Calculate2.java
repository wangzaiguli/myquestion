package com.lw.leetcode.stack.b;

import java.util.Stack;

/**
 * stack
 * 227. 基本计算器 II
 *
 * @Author liw
 * @Date 2021/6/28 17:18
 * @Version 1.0
 */
public class Calculate2 {

    public static void main(String[] args) {
        Calculate2 test = new Calculate2();
        String str = "3+2*2-6";
        int calculate = test.calculate(str);
        System.out.println(calculate);
    }

    public int calculate(String s) {
        s = s + "+";
        char op = '+';
        Stack<Integer> stack = new Stack<>();
        // 保存当前数字，如：12是两个字符，需要进位累加
        int num = 0;
        int result = 0;
        int length = s.length();
        for (int i = 0; i < length; i++) {
            char c = s.charAt(i);
            if (c >= '0') {
                num = num * 10 - '0' + c;
            } else if  ((c != ' ') || i == length - 1) {
                switch (op) {
                    case '+':
                        stack.push(num);
                        break;
                    case '-':
                        stack.push(-num);
                        break;
                    case '*':
                        stack.push(stack.pop() * num);
                        break;
                    case '/':
                        stack.push(stack.pop() / num);
                        break;
                }
                op = c;
                num = 0;
            }
        }
        while (!stack.isEmpty()) {
            result += stack.pop();
        }
        return result;
    }
}
