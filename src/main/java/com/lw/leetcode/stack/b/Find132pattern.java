package com.lw.leetcode.stack.b;

import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * 456. 132 模式
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/19 11:43
 */
public class Find132pattern {

    public boolean find132pattern(int[] nums) {
        Stack<Integer> stack = new Stack<>();
        Stack<Integer> stack2 = new Stack<>();
        for (int num : nums) {
            int min = num;
            if (!stack2.isEmpty()) {
                min = Math.min(min, stack2.peek());
            }
            while (!stack.isEmpty() && stack.peek() <= num) {
                stack.pop();
                stack2.pop();
            }
            if (!stack2.isEmpty() && stack2.peek() < num) {
                return true;
            }
            stack.add(num);
            stack2.add(min);
        }
        return false;
    }

}
