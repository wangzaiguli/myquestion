package com.lw.leetcode.stack.a;

import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * 682. 棒球比赛
 *
 * @author liw
 * @version 1.0
 * @date 2021/8/26 14:12
 */
public class CalPoints {

    public int calPoints(String[] ops) {
        Stack<Integer> stack = new Stack<>();
        for (String op : ops) {
            if ("C".equals(op)) {
                stack.pop();
            } else if ("D".equals(op)) {
                stack.push(stack.peek() << 1);
            } else if ("+".equals(op)) {
                int m = stack.pop();
                int n = stack.peek() + m;
                stack.push(m);
                stack.push(n);
            } else {
                stack.push(Integer.parseInt(op));
            }
        }
        int sum = 0;
        while (!stack.isEmpty()) {
            sum += stack.pop();
        }
        return sum;
    }

}
