package com.lw.leetcode.stack.a;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created with IntelliJ IDEA.
 * 225. 用队列实现栈
 *
 * @author liw
 * @version 1.0
 * @date 2021/8/26 11:55
 */
public class MyStack {

    private Queue<Integer> a;
    private Queue<Integer> b;

    public MyStack() {
        a = new LinkedList<>();
        b = new LinkedList<>();
    }

    public void push(int x) {
        a.offer(x);
        while(!b.isEmpty()) {
            a.offer(b.poll());
        }
        Queue temp = a;
        a = b;
        b = temp;
    }

    public int pop() {
        return b.poll();
    }

    public int top() {
        return b.peek();
    }

    public boolean empty() {
        return b.isEmpty();
    }

}
