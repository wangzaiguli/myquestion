package com.lw.leetcode.stack.a;

/**
 * Created with IntelliJ IDEA.
 * 155. 最小栈
 * 面试题 03.02. 栈的最小值
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/9 11:20
 */
public class MinStack {

    private int[] arr;
    private int pox = -1;
    private int limit = 20;
    private int min = Integer.MAX_VALUE;

    public MinStack() {
        arr = new int[limit];
    }

    public void push(int x) {
        if (pox >= limit - 2) {
            limit = limit << 1;
            int[] arr2 = new int[limit];
            int length = arr.length;
            System.arraycopy(arr, 0, arr2, 0, length);
            arr = arr2;
        }
        if (x < min) {
            min = x;
        }
        arr[++pox] = x;
        arr[++pox] = min;
    }

    public void pop() {
        if (pox >= 0) {
            pox = pox - 2;
            arr[pox + 1] = 0;
            arr[pox + 2] = 0;
            if (pox < 0) {
                min = Integer.MAX_VALUE;
            } else {
                min = arr[pox];
            }
        }
    }

    public int top() {
        return arr[pox - 1];
    }

    public int getMin() {
        return arr[pox];
    }

}
