package com.lw.leetcode.stack.a;

import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * 1544. 整理字符串
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/16 10:48
 */
public class MakeGood {
    public String makeGood(String s) {
        Stack<Character> stack = new Stack<>();
        char[] arr = s.toCharArray();
        for (char c : arr) {
            if (stack.isEmpty()) {
                stack.add(c);
            } else {
                char value = stack.peek();
                if (value + 32 == c || value - 32 == c) {
                    stack.pop();
                } else {
                    stack.add(c);
                }
            }
        }
        int size = stack.size();
        char[] chars = new char[size];
        for (int i = size - 1; i >= 0; i--) {
            chars[i] = stack.pop();
        }
        return String.valueOf(chars);
    }
}
