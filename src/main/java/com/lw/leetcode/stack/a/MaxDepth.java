package com.lw.leetcode.stack.a;

import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * a
 * stack
 * 1614. 括号的最大嵌套深度
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/18 15:10
 */
public class MaxDepth {

    public int maxDepth(String s) {
        char[] chars = s.toCharArray();
        int d = 0;
        int max = 0;
        for (char c : chars) {
            if (c == '(') {
                d++;
                max = Math.max(max, d);
            } else if (c == ')') {
                d--;
            }
        }
        return max;
    }

}
