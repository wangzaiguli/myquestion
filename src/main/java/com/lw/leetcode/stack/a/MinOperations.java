package com.lw.leetcode.stack.a;

import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * 1598. 文件夹操作日志搜集器
 *
 * @author liw
 * @version 1.0
 * @date 2022/9/9 9:17
 */
public class MinOperations {

    public int minOperations(String[] logs) {
        int size = 0;
        for (String log : logs) {
            if ("./".equals(log)) {
                continue;
            }
            if ("../".equals(log)) {
                if (size > 0) {
                    size--;
                }
                continue;
            }
            size++;
        }
        return size;
    }

}
