package com.lw.leetcode.stack.c;

import java.util.PriorityQueue;

/**
 * Created with IntelliJ IDEA.
 * 2102. 序列顺序查询
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/8 16:50
 */
public class SORTracker {

    private PriorityQueue<Node> as = new PriorityQueue<>((a, b) -> a.score == b.score ? b.name.compareTo(a.name) : Integer.compare(a.score, b.score));
    private PriorityQueue<Node> bs = new PriorityQueue<>((a, b) -> a.score == b.score ? a.name.compareTo(b.name) : Integer.compare(b.score, a.score));

    public SORTracker() {

    }

    public void add(String name, int score) {
        Node node = new Node(score, name);
        if (as.isEmpty()) {
            bs.add(node);
        } else {
            Node peek = as.peek();
            if (peek.score < score || (peek.score == score && peek.name.compareTo(name) > 0)) {
                bs.add(as.poll());
                as.add(node);
            } else {
                bs.add(node);
            }
        }
    }

    public String get() {
        Node node = bs.poll();
        as.add(node);
        return node.name;
    }

    private static class Node {
        private int score;
        private String name;

        public Node(int score, String name) {
            this.score = score;
            this.name = name;
        }
    }

}
