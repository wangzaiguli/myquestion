package com.lw.leetcode.stack.c;

import com.lw.test.util.Utils;

import java.util.Arrays;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * 1944. 队列中可以看到的人数
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/13 14:50
 */
public class CanSeePersonsCount {

    public static void main(String[] args) {
        CanSeePersonsCount test = new CanSeePersonsCount();

        // [3,1,2,1,1,0]
//        int[] arr = {10, 6, 8, 5, 11, 9};

        // [4,1,1,1,0]
//        int[] arr = {5, 1, 2, 3, 10};

        int[] arr = Utils.getArrExcit(1000, 1, 100000);

        int[] ints = test.canSeePersonsCount(arr);

        System.out.println(Arrays.toString(ints));


    }

    public int[] canSeePersonsCount(int[] heights) {
        int length = heights.length;
        int[] arr = new int[length];
        Stack<Integer> stack = new Stack<>();
        for (int i = length - 1; i >= 0; i--) {
            int c = 0;
            int v = heights[i];
            while (!stack.isEmpty()) {
                c++;
                if (stack.peek() > v) {
                    break;
                } else {
                    stack.pop();
                }
            }
            stack.add(v);
            arr[i] = c;
        }
        return arr;
    }

}
