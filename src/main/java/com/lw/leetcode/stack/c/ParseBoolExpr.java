package com.lw.leetcode.stack.c;

import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * 1106. 解析布尔表达式
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/31 21:49
 */
public class ParseBoolExpr {

    public static void main(String[] args) {
        ParseBoolExpr test = new ParseBoolExpr();

        // fasle
//        String str = "|(&(t,f,t),!(t))";

        // fasle
//        String str = "&(t,f)";

        // true
//        String str = "|(f,t)";

        // true
//        String str = "!(f)";

        // true
//        String str = "!(&(!(t),&(f),|(f)))";

        // true
        String str = "!(&(&(f),&(!(t),&(f),|(f)),&(!(&(f)),&(t),|(f,f,t))))";

//        String str = "!(f)";

        boolean b = test.parseBoolExpr(str);
        System.out.println(b);
    }

    public boolean parseBoolExpr(String expression) {
        int length = expression.length();
        Stack<Character> op = new Stack<>();
        Stack<Boolean> values = new Stack<>();
        for (int i = 0; i < length; i++) {
            char c = expression.charAt(i);
            if (c == ',') {
                continue;
            }
            if (c == '&' || c == '|' || c == '!') {
                op.add(c);
            } else if (c == '(') {
                values.add(null);
            } else if (c == ')') {
                char f = op.pop();
                boolean item = f == '&';
                while (values.peek() != null) {
                    if (f == '&') {
                        item &= values.pop();
                    } else if (f == '|') {
                        item |= values.pop();
                    } else {
                        item = !values.pop();
                    }
                }
                values.pop();
                values.add(item);
            } else {
                values.add(c == 't');
            }
        }
        if (op.isEmpty()) {
            return values.pop();
        }
        char f = op.pop();
        boolean item = f == '&';
        while (values.peek() != null) {
            if (f == '&') {
                item &= values.pop();
            } else if (f == '|') {
                item |= values.pop();
            } else {
                item = !values.pop();
            }
        }
        return item;
    }
}
