package com.lw.leetcode.bingChaJi.b;

/**
 * Created with IntelliJ IDEA.
 * b
 * bingchaji
 * https://leetcode.cn/contest/sf-tech/problems/BN8jAm/
 * 顺丰05. 慧眼神瞳
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/10 16:15
 */
public class IsCompliance {

    private int[] arr;

    public boolean isCompliance(int[][] distance, int n) {
        int length = distance.length;
        this.arr = new int[length];
        for (int i = 0; i < length; i++) {
            arr[i] = i;
        }
        for (int i = 0; i < length; i++) {
            int[] items = distance[i];
            for (int j = 0; j < length; j++) {

                if (items[j] <= 2) {
                    int v1 = find(i);
                    int v2 = find(j);
                    if (v1 != v2) {
                        arr[v2] = v1;
                    }
                }
            }
        }
        boolean[] flags = new boolean[length];
        for (int i : arr) {
            flags[i] = true;
        }
        for (boolean flag : flags) {
            if (flag) {
                n--;
            }
        }
        return n >= 0;
    }

    private int find(int v) {
        if (arr[v] == v) {
            return v;
        }
        int t = find(arr[v]);
        arr[v] = t;
        return t;
    }

}
