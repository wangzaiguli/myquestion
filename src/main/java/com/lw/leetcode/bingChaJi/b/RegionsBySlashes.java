package com.lw.leetcode.bingChaJi.b;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * 959. 由斜杠划分区域
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/4 13:20
 */
public class RegionsBySlashes {


    public static void main(String[] args) {
        RegionsBySlashes test = new RegionsBySlashes();


        // 2
//        String[] arr = {" /", "/ "};

        // 1
        String[] arr = {" /", "  "};

        // 4
//        String[] arr = {"\\/", "/\\"};

        // 5
//        String[] arr = {"/\\", "\\/"};

        // 3
//        String[] arr = {"//", "/ "};

        // 2
//        String[] arr = {"   ", " \\/", " / "};

        int i = test.regionsBySlashes(arr);
        System.out.println(i);
    }

    private Map<Integer, Integer> map = new HashMap<>();

    public int regionsBySlashes(String[] grid) {
        int length = grid.length;
        for (int i = 0; i < length; i++) {
            String str = grid[i];
            for (int j = 0; j < length; j++) {
                char item = str.charAt(j);
                int a = (i << 12) + (j << 4);
                int b = a + 1;
                int c = a + 2;
                int d = a + 3;
                int hu = i == 0 ? a : find(((i - 1) << 12) + (j << 4) + 2);
                int hl = j == 0 ? d : find((i << 12) + ((j - 1) << 4) + 1);
                map.put(a, hu);
                if (item == ' ') {
                    map.put(b, hu);
                    map.put(c, hu);
                    map.put(d, hu);
                    map.put(hl, hu);
                } else if (item == '/') {
                    map.put(d, hu);
                    map.put(hl, hu);
                    map.put(c, b);
                    map.put(b, b);
                } else {
                    map.put(b, hu);
                    map.put(c, hl);
                    map.put(d, hl);
                }
            }
        }
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                int v = (i << 12) + (j << 4);
                set.add(find(v ));
                set.add(find(v + 1));
                set.add(find(v + 2));
                set.add(find(v + 3));
            }
        }
        return set.size();
    }

    private int find(int k) {
        Integer v = map.get(k);
        if (v == null) {
            map.put(k, k);
            return k;
        }
        if (v == k) {
            return k;
        }
        int r = find(v);
        map.put(k, r);
        return r;
    }

}

