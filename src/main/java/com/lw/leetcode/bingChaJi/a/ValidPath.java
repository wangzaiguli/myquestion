package com.lw.leetcode.bingChaJi.a;

/**
 * Created with IntelliJ IDEA.
 * 1971. 寻找图中是否存在路径
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/19 11:01
 */
public class ValidPath {

    private int[] arr;

    public boolean validPath(int n, int[][] edges, int source, int destination) {
        arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = i;
        }
        for (int[] edge : edges) {
            arr[find(edge[0])] = find(edge[1]);
        }
        return find(source) == find(destination);
    }

    private int find(int t) {
        if (arr[t] == t) {
            return t;
        }
        arr[t] = find(arr[t]);
        return arr[t];
    }

}
