package com.lw.leetcode.sort.a;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * sort
 * 1331. 数组序号转换
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/19 16:38
 */
public class ArrayRankTransform {
    public int[] arrayRankTransform(int[] arr) {
        int[] temp = arr.clone();
        Arrays.sort(temp);
        Map<Integer, Integer> map = new HashMap<>();
        int length = arr.length;
        int count = 1;
        for (int i = 0; i < length; ++i) {
            if (!map.containsKey(temp[i])) {
                map.put(temp[i], count++);
            }
        }
        for (int i = 0; i < length; ++i) {
            arr[i] = map.get(arr[i]);
        }
        return arr;
    }
}
