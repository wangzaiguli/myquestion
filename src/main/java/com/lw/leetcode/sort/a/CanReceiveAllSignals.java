package com.lw.leetcode.sort.a;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * zj-future01. 信号接收
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/11 11:44
 */
public class CanReceiveAllSignals {

    public boolean canReceiveAllSignals(int[][] intervals) {
        Arrays.sort(intervals, (a, b) -> Integer.compare(a[0], b[0]));
        int length = intervals.length;
        for (int i = 1; i < length; i++) {
            if (intervals[i][0] < intervals[i - 1][1]) {
                return false;
            }
        }
        return true;
    }

}
