package com.lw.leetcode.sort.a;

import java.util.PriorityQueue;

/**
 * Created with IntelliJ IDEA.
 * 题目-01. 化学反应
 *
 * @author liw
 * @version 1.0
 * @date 2022/9/26 9:36
 */
public class LastMaterial {

    public int lastMaterial(int[] material) {
        PriorityQueue<Integer> queue = new PriorityQueue<>((a, b) -> Integer.compare(b, a));
        for (int i : material) {
            queue.add(i);
        }
        while (queue.size() > 1) {
            queue.add(queue.poll() - queue.poll());
        }
        return queue.poll();
    }

}
