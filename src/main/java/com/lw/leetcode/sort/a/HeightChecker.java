package com.lw.leetcode.sort.a;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 1051. 高度检查器
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/25 20:47
 */
public class HeightChecker {
    public int heightChecker(int[] heights) {
        int count = 0;
        int[] temp = heights.clone();
        Arrays.sort(temp);
        for (int i = heights.length - 1; i >= 0; i--) {
            if (heights[i] != temp[i]) {
                count++;
            }
        }
        return count;
    }
}
