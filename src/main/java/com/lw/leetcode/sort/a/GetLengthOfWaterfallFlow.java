package com.lw.leetcode.sort.a;

import java.util.PriorityQueue;

/**
 * Created with IntelliJ IDEA.
 * a
 * sort
 * https://leetcode.cn/contest/autox2023/problems/l9HbCJ/
 * AutoX-1. 网页瀑布流
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/10 17:38
 */
public class GetLengthOfWaterfallFlow {

    public int getLengthOfWaterfallFlow(int num, int[] block) {
        if (num >= block.length) {
            int max = 0;
            for (int i : block) {
                max = Math.max(max, i);
            }
            return max;
        }
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        for (int i = 0; i < num; i++) {
            queue.add(block[i]);
        }
        int length = block.length;
        for (int i = num; i < length; i++) {
            queue.add(queue.poll() + block[i]);
        }
        for (int i = 1; i < num; i++) {
            queue.poll();
        }
        return queue.poll();
    }

}
