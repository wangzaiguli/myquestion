package com.lw.leetcode.sort.a;

import java.util.*;

/**
 * sort
 * 506. 相对名次
 *
 * @Author liw
 * @Date 2021/6/28 13:11
 * @Version 1.0
 */
public class FindRelativeRanks {

    public static void main(String[] args) {
        FindRelativeRanks test = new FindRelativeRanks();
        int[] arr = {5, 1, 4};
        String[] relativeRanks = test.findRelativeRanks(arr);
        System.out.println(Arrays.toString(relativeRanks));
    }

    public String[] findRelativeRanks(int[] score) {

        int n = score.length;
        String[] desc = {"Gold Medal", "Silver Medal", "Bronze Medal"};
        int[][] arr = new int[n][2];

        for (int i = 0; i < n; ++i) {
            arr[i][0] = score[i];
            arr[i][1] = i;
        }
        Arrays.sort(arr, (a, b) -> b[0] - a[0]);
        String[] ans = new String[n];
        for (int i = 0; i < n; ++i) {
            if (i >= 3) {
                ans[arr[i][1]] = Integer.toString(i + 1);
            } else {
                ans[arr[i][1]] = desc[i];
            }
        }
        return ans;
    }

}
