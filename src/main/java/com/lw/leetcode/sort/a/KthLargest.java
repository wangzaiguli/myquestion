package com.lw.leetcode.sort.a;

import java.util.PriorityQueue;

/**
 * 剑指 Offer II 059. 数据流的第 K 大数值
 * 703. 数据流中的第 K 大元素
 *
 * @Author liw
 * @Date 2021/8/18 21:23
 * @Version 1.0
 */
public class KthLargest {

    int k;
    PriorityQueue<Integer> minHeap = new PriorityQueue<>();

    public KthLargest(int k, int[] nums) {
        this.k = k;
        for (int x : nums) {
            minHeap.offer(x);
        }
    }

    public int add(int val) {
        minHeap.offer(val);
        while (minHeap.size() > k) {
            minHeap.poll();
        }
        return minHeap.peek();
    }
}
