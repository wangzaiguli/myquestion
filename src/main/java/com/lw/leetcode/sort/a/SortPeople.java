package com.lw.leetcode.sort.a;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 2418. 按身高排序
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/28 10:51
 */
public class SortPeople {

    public String[] sortPeople(String[] names, int[] heights) {
        int n = names.length;
        Map<Integer, String> map = new HashMap<>();
        for (int i = 0; i < n; i++) {
            map.put(heights[i], names[i]);
        }
        Arrays.sort(heights);
        for (int i = 0; i < n; i++) {
            names[n - 1 - i] = map.get(heights[i]);
        }
        return names;
    }

}
