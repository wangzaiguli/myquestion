package com.lw.leetcode.sort.a;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2389. 和有限的最长子序列
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/28 10:49
 */
public class AnswerQueries {

    public int[] answerQueries(int[] nums, int[] queries) {
        Arrays.sort(nums);
        int m = queries.length;
        int[] answer = new int[m];
        for (int i = 0; i < m; i++) {
            int query = queries[i];
            int size = 0;
            int sum = 0;
            for (int num : nums) {
                if (sum + num > query) {
                    break;
                }
                sum += num;
                size++;
            }
            answer[i] = size;
        }
        return answer;
    }

}
