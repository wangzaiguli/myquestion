package com.lw.leetcode.sort.a;

import java.util.PriorityQueue;

/**
 * 1502. 判断能否形成等差数列
 *
 * @Author liw
 * @Date 2021/6/14 19:59
 * @Version 1.0
 */
public class CanMakeArithmeticProgression {

    public static void main(String[] args) {
        CanMakeArithmeticProgression test = new CanMakeArithmeticProgression();
        int[] arr = {3, 5, 1};
        boolean b = test.canMakeArithmeticProgression(arr);
        System.out.println(b);
    }

    public boolean canMakeArithmeticProgression(int[] arr) {
        int length = arr.length;
        if (length < 2) {
            return false;
        }
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        for (int i : arr) {
            queue.add(i);
        }
        int a = queue.poll();
        int b = queue.poll();
        int c = b - a;
        while (!queue.isEmpty()) {
            int value = queue.poll();
            if (value - b != c) {
                return false;
            }
            b = value;
        }
        return true;
    }

}
