package com.lw.leetcode.sort.a;

import java.util.Arrays;

/**
 * create by idea
 * 976. 三角形的最大周长
 *
 * @author lmx
 * @version 1.0
 * @date 2022/1/22 14:15
 */
public class LargestPerimeter {

    public int largestPerimeter(int[] nums) {
        Arrays.sort(nums);
        for (int i = nums.length - 1; i >= 2; --i) {
            if (nums[i - 2] + nums[i - 1] > nums[i]) {
                return nums[i - 2] + nums[i - 1] + nums[i];
            }
        }
        return 0;
    }

}
