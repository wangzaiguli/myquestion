package com.lw.leetcode.sort.a;

import java.util.Arrays;
import java.util.PriorityQueue;

/**
 * Created with IntelliJ IDEA.
 * sort
 * 1619. 删除某些元素后的数组均值
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/23 13:36
 */
public class TrimMean {

    public double trimMean(int[] arr) {
        Arrays.sort(arr);
        int sum = 0;
        int a = arr.length/ 20;
        int b = arr.length - arr.length / 20;

        for (int i = a; i < b; i++) {
            sum += arr[i];
        }
        return sum / (arr.length - arr.length * 0.1);
    }

    public double trimMean2(int[] arr) {
        int length = arr.length;
        int l = length / 20;
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
        int sum = 0;
        for (int i : arr) {
            priorityQueue.add(i);
            sum += i;
        }
        int c = 0;
        for (int i = 0; i < l; i++) {
            c += priorityQueue.poll();
        }
        priorityQueue = new PriorityQueue<>((a, b) -> Integer.compare(b, a));
        for (int i : arr) {
            priorityQueue.add(i);
        }
        for (int i = 0; i < l; i++) {
            c += priorityQueue.poll();
        }
        return (double) (sum - c) / (18 * l);
    }
}
