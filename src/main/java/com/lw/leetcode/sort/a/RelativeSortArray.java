package com.lw.leetcode.sort.a;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * sort
 * 1122. 数组的相对排序
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/21 16:15
 */
public class RelativeSortArray {
    public int[] relativeSortArray(int[] arr1, int[] arr2) {
        int length = 1001;
        int[] nums = new int[length];
        int[] res = new int[arr1.length];
        for (int i : arr1) {
            nums[i]++;
        }
        int index = 0;
        for (int i : arr2) {
            while (nums[i] > 0) {
                res[index++] = i;
                nums[i]--;
            }
        }
        for (int i = 0; i < length; i++) {
            while (nums[i] > 0) {
                res[index++] = i;
                nums[i]--;
            }
        }
        return res;
    }
}
