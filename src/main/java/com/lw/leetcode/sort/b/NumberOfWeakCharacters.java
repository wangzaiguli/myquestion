package com.lw.leetcode.sort.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * sort
 * 1996. 游戏中弱角色的数量
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/29 16:46
 */
public class NumberOfWeakCharacters {

    public static void main(String[] args) {
        NumberOfWeakCharacters test = new NumberOfWeakCharacters();

        //  [[5,5],[6,3],[3,6]]
//        int[][] arr = {{5,5}, {6,3}, {3,6}};
        //  1
        int[][] arr = {{1, 1}, {2, 1}, {2, 2}, {1, 2}};
        int i = test.numberOfWeakCharacters(arr);
        System.out.println(i);
    }

    public int numberOfWeakCharacters(int[][] properties) {
        Arrays.sort(properties, (a, b) -> a[0] == b[0] ? Integer.compare(b[1], a[1]) : Integer.compare(b[0], a[0]));
        int a = properties[0][0];
        int length = properties.length;
        int count = 0;
        int i = 1;
        for (; i < length; i++) {
            if (properties[i][0] != a) {
                break;
            }
        }
        if (i == length) {
            return 0;
        }
        a = properties[i][0];
        int max = properties[0][1];
        int max2 = properties[0][1];
        for (; i < length; i++) {
            int a1 = properties[i][0];
            int b1 = properties[i][1];
            if (a != a1) {
                max2 = Math.max(max2, max);
                a = a1;
            }
            if (b1 < max2) {
                count++;
            }
            max = Math.max(max, b1);
        }
        return count;
    }

}
