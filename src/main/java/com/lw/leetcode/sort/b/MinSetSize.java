package com.lw.leetcode.sort.b;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * sort
 * 1338. 数组大小减半
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/13 16:00
 */
public class MinSetSize {

    public static void main(String[] args) {
        MinSetSize test = new MinSetSize();
        int[] arr = {1, 9};
        int i = test.minSetSize(arr);
        System.out.println(i);
    }

    public int minSetSize(int[] arr) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int num : arr) {
            map.merge(num, 1, (a, b) -> a + b);
        }
        int[] counts = new int[map.size()];
        int i = 0;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            counts[i++] = entry.getValue();
        }
        Arrays.sort(counts);
        int l = arr.length >> 1;
        i = counts.length - 1;

        while (l > 0) {
            l -= counts[i];
            i--;
        }

        return counts.length - i - 1;
    }
}
