package com.lw.leetcode.sort.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * a
 * sort
 * 2545. 根据第 K 场考试的分数排序
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/28 9:32
 */
public class SortTheStudents {

    public int[][] sortTheStudents(int[][] score, int k) {
        Arrays.sort(score, (a, b) -> Integer.compare(b[k], a[k]));
        return score;
    }

}
