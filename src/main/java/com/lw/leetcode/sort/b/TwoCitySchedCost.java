package com.lw.leetcode.sort.b;

import java.util.Arrays;

/**
 * arr
 * 1029. 两地调度
 *
 * @Author liw
 * @Date 2021/8/31 21:37
 * @Version 1.0
 */
public class TwoCitySchedCost {


    public static void main(String[] args) {
        TwoCitySchedCost test = new TwoCitySchedCost();

        // 1859
//        int[][] arr = {{259,770},{448,54},{926,667},{184,139},{840,118},{577,469}};

        // 3086
//        int[][] arr =   {{515,563},{451,713},{537,709},{343,819},{855,779},{457,60},{650,359},{631,42}};

        // 110
        int[][] arr = {{10, 20}, {30, 200}, {400, 50}, {30, 20}};

        int i = test.twoCitySchedCost(arr);
        System.out.println(i);

    }

    public int twoCitySchedCost(int[][] costs) {
        for (int[] cost : costs) {
            cost[0] = cost[1] - cost[0];
        }
        Arrays.sort(costs, (a, b) -> Integer.compare(a[0], b[0]));
        int length = costs.length;
        int m = length >> 1;
        int sum = 0;
        for (int i = 0; i < m; i++) {
            sum += costs[i][1];
        }
        for (int i = m; i < length; i++) {
            sum += costs[i][1] - costs[i][0];
        }
        return sum;
    }
}
