package com.lw.leetcode.sort.b;

import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * 729. 我的日程安排表 I
 * 剑指 Offer II 058. 日程表
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/8 12:38
 */
public class MyCalendar {
    TreeMap<Integer, Integer> calendar;

    MyCalendar() {
        calendar = new TreeMap<>();
    }

    public boolean book(int start, int end) {
        Integer prev = calendar.floorKey(start);
        Integer next = calendar.ceilingKey(start);
        if ((prev == null || calendar.get(prev) <= start) &&
                (next == null || end <= next)) {
            calendar.put(start, end);
            return true;
        }
        return false;
    }


}
