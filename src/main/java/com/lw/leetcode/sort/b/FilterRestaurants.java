package com.lw.leetcode.sort.b;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * sort
 * 1333. 餐厅过滤器
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/6 22:02
 */
public class FilterRestaurants {

    public List<Integer> filterRestaurants(int[][] restaurants, int veganFriendly, int maxPrice, int maxDistance) {
        List<int[]> collector = new ArrayList<>();
        for (int[] restaurant : restaurants) {
            if (veganFriendly == 1 && restaurant[2] == 0 || restaurant[3] > maxPrice || restaurant[4] > maxDistance) {
                continue;
            }
            collector.add(restaurant);
        }
        collector.sort((o1, o2) -> o1[1] == o2[1] ? o2[0] - o1[0] : o2[1] - o1[1]);
        List<Integer> res = new ArrayList<>();
        for (int[] rest : collector) {
            res.add(rest[0]);
        }
        return res;
    }

}
