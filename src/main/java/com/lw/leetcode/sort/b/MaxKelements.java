package com.lw.leetcode.sort.b;

import java.util.PriorityQueue;

/**
 * Created with IntelliJ IDEA.
 * 2530. 执行 K 次操作后的最大分数
 * b
 * sort
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/9 10:18
 */
public class MaxKelements {

    public long maxKelements(int[] nums, int k) {
        long sum = 0;
        PriorityQueue<Integer> queue = new PriorityQueue<>((a, b) -> Integer.compare(b, a));
        for (int num : nums) {
            queue.add(num);
        }
        while (k > 0) {
            Integer poll = queue.poll();
            sum += poll;
            queue.add((poll + 2) / 3);
            k--;
        }
        return sum;
    }

}
