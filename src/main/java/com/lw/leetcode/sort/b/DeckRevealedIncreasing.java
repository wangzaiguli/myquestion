package com.lw.leetcode.sort.b;

import java.util.Arrays;

/**
 * 950. 按递增顺序显示卡牌
 *
 * @Author liw
 * @Date 2021/6/21 17:39
 * @Version 1.0
 */
public class DeckRevealedIncreasing {


    public static void main(String[] args) {
        DeckRevealedIncreasing test = new DeckRevealedIncreasing();
        int[] arr = {17};
        int[] ints = test.deckRevealedIncreasing(arr);
        System.out.println(Arrays.toString(ints));
    }

    public int[] deckRevealedIncreasing(int[] deck) {
        Arrays.sort(deck);
        int length = deck.length;
        int[] arr = new int[length];
        int index = 0;
        int l = length;
        for (int num : deck) {
            arr[index] = num;
            l--;
            if (l == 0) {
                break;
            }
            int count = 0;
            while (count < 2) {
                index++;
                if (index == length) {
                    index = 0;
                }
                if (arr[index] == 0) {
                    count++;
                }
            }
        }
        return arr;
    }

}
