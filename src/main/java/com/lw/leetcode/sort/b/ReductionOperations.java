package com.lw.leetcode.sort.b;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/26 9:37
 */
public class ReductionOperations {

    public static void main(String[] args) {
        ReductionOperations test = new ReductionOperations();
        int[] arr = {3, 2, 5, 3, 6};
        int i = test.reductionOperations(arr);
        System.out.println(i);
    }

    public int reductionOperations(int[] nums) {
        Arrays.sort(nums);
        int length = nums.length;
        int item = 0;
        int sum = 0;
        for (int i = length - 2; i >= 0; i--) {
            item++;
            if (nums[i] != nums[i + 1]) {
                sum += item;
            }
        }
        return sum;
    }

    public int reductionOperations2(int[] nums) {
        Map<Integer, Integer> map = new TreeMap<>((a, b) -> Integer.compare(b, a));
        for (int num : nums) {
            map.merge(num, 1, (a, b) -> a + b);
        }
        int sum = 0;
        int size = map.size() - 1;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            sum += entry.getValue() * size--;
        }
        return sum;
    }

}
