package com.lw.leetcode.sort.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * sort
 * 1985. 找出数组中的第 K 大整数
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/18 15:20
 */
public class KthLargestNumber {

    public static void main(String[] args) {
        KthLargestNumber test = new KthLargestNumber();
        String[] str = {"23345340", "56", "8907"};

        String s = test.kthLargestNumber(str, 1);

        System.out.println(s);
    }

    public String kthLargestNumber(String[] nums, int k) {

        Arrays.sort(nums, (a, b) -> {
            if (a.length() == b.length()) {
                return a.compareTo(b);
            } else {
                return a.length() - b.length();
            }
        });
        return nums[nums.length - k];
    }
}
