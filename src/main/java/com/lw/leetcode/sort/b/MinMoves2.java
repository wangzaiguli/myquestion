package com.lw.leetcode.sort.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/26 10:37
 */
public class MinMoves2 {

    public int minMoves2(int[] nums) {
        Arrays.sort(nums);
        int sum = 0;
        int length = nums.length;
        int mid = length >> 1;
        int m = nums[mid];

        for (int i = 0; i < mid; i++) {
            sum += m - nums[i];
        }
        for (int i = mid + 1; i < length; i++) {
            sum += nums[i] - m;
        }
        return sum;
    }

    public int minMoves22(int[] nums) {
        Arrays.sort(nums);
        int sum = 0;
        int m = nums[nums.length >> 1];
        for (int num : nums) {
            sum += Math.abs(m - num);
        }
        return sum;
    }

}
