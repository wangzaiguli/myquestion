package com.lw.leetcode.sort.b;

import java.util.PriorityQueue;

/**
 * Created with IntelliJ IDEA.
 * 1962. 移除石子使总数最小
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/18 12:11
 */
public class MinStoneSum {
    public int minStoneSum(int[] piles, int k) {
        PriorityQueue<Integer> queue = new PriorityQueue<>((a, b) ->  b - a);
        for (int i = 0; i < piles.length; i++) {
            queue.offer(piles[i]);
        }
        for (int i = 0; i < k; i++) {
            queue.offer((queue.poll() + 1) >> 1);
        }
        int sum = 0;
        for (int x : queue) {
            sum += x;
        }
        return sum;
    }
}
