package com.lw.leetcode.sort.b;

import java.util.Arrays;

/**
 * sort
 * 646. 最长数对链
 *
 * @Author liw
 * @Date 2021/7/5 17:55
 * @Version 1.0
 */
public class FindLongestChain {


    public static void main(String[] args) {
        FindLongestChain test = new FindLongestChain();


    }

    public int findLongestChain(int[][] pairs) {
        Arrays.sort(pairs, (a, b) -> a[1] - b[1]);
        int res = 1;
        int tmp = pairs[0][1];
        for (int i = 1; i < pairs.length; i++) {
            if (pairs[i][0] > tmp) {
                res++;
                tmp = pairs[i][1];
            }
        }
        return res;
    }
}
