package com.lw.leetcode.sort.b;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

/**
 * Created with IntelliJ IDEA.
 * 6130. 设计数字容器系统
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/24 12:34
 */
public class NumberContainers {

    private Map<Integer, TreeSet<Integer>> map = new HashMap<>();
    private Map<Integer, Integer> counts = new HashMap<>();

    public NumberContainers() {

    }

    public void change(int index, int number) {
        Integer old = counts.get(index);
        if (old != null) {
            TreeSet<Integer> set = map.get(old);
            set.remove(index);
        }
        map.computeIfAbsent(number, v -> new TreeSet<>()).add(index);
        counts.put(index, number);
    }

    public int find(int number) {
        TreeSet<Integer> set = map.get(number);
        if (set == null || set.size() == 0) {
            return -1;
        }
        return set.ceiling(0);
    }
}
