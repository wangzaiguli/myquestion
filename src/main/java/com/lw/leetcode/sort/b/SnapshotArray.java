package com.lw.leetcode.sort.b;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * 1146. 快照数组
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/5 19:02
 */
class SnapshotArray {
    private List<TreeMap<Integer, Integer>> arr = new ArrayList<>();
    private int snap = 0;

    public SnapshotArray(int length) {
        for (int i = 0; i < length; i++) {
            arr.add(new TreeMap<>());
        }
    }

    public void set(int index, int val) {
        arr.get(index).put(snap, val);
    }

    public int snap() {
        return snap++;
    }

    public int get(int index, int snap_id) {
        TreeMap<Integer, Integer> tm = arr.get(index);
        Integer key = tm.floorKey(snap_id);
        return key == null ? 0 : tm.get(key);
    }
}
