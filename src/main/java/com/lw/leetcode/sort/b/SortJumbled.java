package com.lw.leetcode.sort.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2191. 将杂乱无章的数字排序
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/20 22:01
 */
public class SortJumbled {

    public static void main(String[] args) {
        SortJumbled test = new SortJumbled();

        // [338,38,991]
//        int[] arr1 = {8,9,4,0,2,1,3,5,7,6};
//        int[] arr2 = {991,338,38};

        // [123,456,789]
//        int[] arr1 = {0,1,2,3,4,5,6,7,8,9};
//        int[] arr2 = {789,456,123};

        //  [9,8,7,6,5,4,3,2,1,0]
//        int[] arr1 = {9,8,7,6,5,4,3,2,1,0};
//        int[] arr2 = {0,1,2,3,4,5,6,7,8,9};

        //  [454,95,404,47799,19021,162535,51890378]
        int[] arr1 = {7,9,4,1,0,3,8,6,2,5};
        int[] arr2 = {47799,19021,162535,454,95,51890378,404};

        int[] ints = test.sortJumbled(arr1, arr2);
        System.out.println(Arrays.toString(ints));
    }

    public int[] sortJumbled(int[] mapping, int[] nums) {
        int length = nums.length;
        int[][] arr = new int[length][2];
        for (int i = 0; i < length; i++) {
            int v = nums[i];
            arr[i][1] = v;
            if(v == 0) {
                arr[i][0] = mapping[0];
                continue;
            }
            int item = 0;
            int c = 0;
            while (v != 0) {
              item = item * 10 +  mapping[v % 10];
              v /= 10;
              c++;
            }
            while (c != 0) {
                c--;
                v = v * 10 + item % 10;
                item /= 10;
            }
            arr[i][0] = v;
        }
        Arrays.sort(arr, (a, b) -> Integer.compare(a[0], b[0]));
        for (int i = 0; i < length; i++) {
            nums[i] = arr[i][1];
        }
        return nums;
    }

}
