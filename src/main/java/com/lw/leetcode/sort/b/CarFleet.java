package com.lw.leetcode.sort.b;

import java.util.Map;
import java.util.TreeMap;

/**
 * sort
 * 853. 车队
 *
 * @Author liw
 * @Date 2021/8/2 21:21
 * @Version 1.0
 */
public class CarFleet {

    public static void main(String[] args) {
        CarFleet test = new CarFleet();
//        int target = 12;
//        int[] position = {10,8,0,5,3};
//        int[] speed = {2,4,1,1,3};

        // 10 [6,8] [3,2]  2
        int target = 10;
        int[] position = {6, 8};
        int[] speed = {3, 2};

        int i = test.carFleet(target, position, speed);
        System.out.println(i);
    }

    public int carFleet(int target, int[] position, int[] speed) {
        Map<Integer, Integer> map = new TreeMap<>((a, b) -> Integer.compare(b, a));
        int length = position.length;
        for (int i = 0; i < length; i++) {
            map.put(position[i], speed[i]);
        }
        int count = 0;
        double time = 0D;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            int value = entry.getValue();
            int key = entry.getKey();
            double t = (double) (target - key) / (double) value;
            if (t > time) {
                count++;
                time = t;
            }
        }
        return count;
    }
}
