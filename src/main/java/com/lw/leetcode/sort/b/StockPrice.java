package com.lw.leetcode.sort.b;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * 2034. 股票价格波动
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/23 22:34
 */
public class StockPrice {

    private TreeMap<Integer, Integer> map;
    private Map<Integer, Integer> m;
    private int p;
    private int t;

    public StockPrice() {
        map = new TreeMap<>();
        m = new HashMap<>();
    }

    public void update(int timestamp, int price) {
        if (m.containsKey(timestamp)) {
            Integer v = m.get(timestamp);
            int c = map.get(v);
            if (c == 1) {
                map.remove(v);
            } else {
                map.put(v, c - 1);
            }
        }
        map.merge(price, 1 , (a, b) -> a + b);
        m.put(timestamp, price);
        if (timestamp >= t) {
            p = price;
            t = timestamp;
        }
    }

    public int current() {
        return p;
    }

    public int maximum() {
        return map.lastKey();
    }

    public int minimum() {
        return map.firstKey();
    }

}
