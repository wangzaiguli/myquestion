package com.lw.leetcode.sort.b;

/**
 * Created with IntelliJ IDEA.
 * 2126. 摧毁小行星
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/5 13:25
 */
public class AsteroidsDestroyed {


    public static void main(String[] args) {
        AsteroidsDestroyed test = new AsteroidsDestroyed();


        // true
//        int n = 10;
//        int[] arr = {3,9,19,5,21};

        // false
        int n = 5;
        int[] arr = {4, 9, 23, 4};

        boolean b = test.asteroidsDestroyed(n, arr);
        System.out.println(b);
    }

    public boolean asteroidsDestroyed(int mass, int[] asteroids) {
        int length = asteroids.length;
        long item = mass;
        for (int i = 0; i < length; i++) {
            int l = length - 1;
            for (int j = i; j <= l; j++) {
                int v = asteroids[j];
                if (v <= item) {
                    item += v;
                    break;
                } else {
                    asteroids[j] = asteroids[l];
                    asteroids[l] = v;
                    l--;
                    if (j > l) {
                        return false;
                    }
                    j--;
                }
            }
        }
        return true;
    }

}
