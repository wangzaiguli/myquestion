package com.lw.leetcode.sort.b;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * 826. 安排工作以达到最大收益
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/9 11:39
 */
public class MaxProfitAssignment {

    public int maxProfitAssignment(int[] difficulty, int[] profit, int[] worker) {
        TreeMap<Integer, Integer> map = new TreeMap<>();
        int length = difficulty.length;
        for (int i = 0; i < length; i++) {
            Integer value = map.get(difficulty[i]);
            if (value == null || value < profit[i]) {
                map.put(difficulty[i], profit[i]);
            }
        }
        int max = 0;
        List<Integer> list = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() < max) {
                list.add(entry.getKey());
                continue;
            }
            max = entry.getValue();
        }
        for (Integer key : list) {
            map.remove(key);
        }
        int sum = 0;
        for (int i : worker) {
            Integer key = map.floorKey(i);
            if (key != null) {
                sum += map.get(key);
            }
        }
        return sum;
    }

}
