package com.lw.leetcode.sort.b;

import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * sort
 * 436. 寻找右区间
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/19 11:06
 */
public class FindRightInterval {

    public int[] findRightInterval(int[][] intervals) {
        TreeMap<Integer, Integer> map = new TreeMap<>();
        int length = intervals.length;
        for (int i = 0; i < length; i++) {
            map.put(intervals[i][0], i);
        }
        int[] values = new int[length];
        for (int i = 0; i < length; i++) {
            Integer key = map.ceilingKey(intervals[i][1]);
            if (key == null) {
                values[i] = -1;
            } else {
                values[i] = map.get(key);
            }
        }
        return values;
    }

}
