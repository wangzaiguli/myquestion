package com.lw.leetcode.sort.b;

/**
 * 1015. 可被 K 整除的最小整数
 *
 * @Author liw
 * @Date 2021/10/31 13:29
 * @Version 1.0
 */
public class SmallestRepunitDivByK {

    public int smallestRepunitDivByK(int k) {
        long item = 1;
        while (item < k) {
            item = item * 10 + 1;
        }
        while (item <= Integer.MAX_VALUE) {
            if (item % k == 0) {
                return (int) item;
            }
            item = item * 10 + 1;
        }
        return -1;
    }

}
