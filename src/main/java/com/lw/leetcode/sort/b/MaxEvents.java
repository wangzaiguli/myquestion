package com.lw.leetcode.sort.b;

import java.util.Arrays;
import java.util.PriorityQueue;

/**
 * Created with IntelliJ IDEA.
 * 1353. 最多可以参加的会议数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/6 20:58
 */
public class MaxEvents {


    public static void main(String[] args) {
        MaxEvents test = new MaxEvents();

        // 5
//        int[][] arr = {{1,2}, {1,2}, {3,3},{1,5},{1,5}};
        // 4
//        int[][] arr = { {3,3},{1,5},{1,5},{4,5}};
        // 1
//        int[][] arr = { {1,10000}};
        // 3
//        int[][] arr = { {1,2}, {1,2}, {1,6},{1,2},{1,2}};
        // 2
//        int[][] arr = { {2,2}, {2,2}, {1,10},{2,2},{2,2}};
        // 7
        int[][] arr = {{1, 1}, {1, 2}, {1, 3}, {1, 4}, {1, 5}, {1, 6}, {1, 7}};
        int i = test.maxEvents(arr);
        System.out.println(i);

    }

    public int maxEvents(int[][] events) {
        Arrays.sort(events, (a, b) -> a[0] == b[0] ? Integer.compare(a[1], b[1]) : Integer.compare(a[0], b[0]));
        int l = events.length;
        int count = 0;
        int length = 0;
        for (int[] event : events) {
            length = Math.max(length, event[1]);
        }
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        int index = 0;
        for (int i = 1; i <= length; i++) {
            while (index < l && events[index][0] <= i) {
                queue.add(events[index][1]);
                index++;
            }
            while (!queue.isEmpty()) {
                if (queue.poll() >= i) {
                    count++;
                    break;
                }
            }
        }
        return count;
    }


}
