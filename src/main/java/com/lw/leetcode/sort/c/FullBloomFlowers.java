package com.lw.leetcode.sort.c;

import java.util.Arrays;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * 2251. 花期内花的数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/8 15:58
 */
public class FullBloomFlowers {

    public static void main(String[] args) {
        FullBloomFlowers test = new FullBloomFlowers();

        // {1,2,2,2}
//        int[][] flowers = {{1, 6}, {3, 7}, {9, 12}, {4, 13}};
//        int[] persons = {2, 3, 7, 11};

        // {2,2,1}
//        int[][] flowers = {{1, 10}, {3, 3}};
//        int[] persons = {3, 3, 2};

        // [0,0,3,0,0,2,0,2,0,0]
        int[][] flowers = {{19, 37}, {19, 38}, {19, 35}};
        int[] persons = {6, 7, 21, 1, 13, 37, 5, 37, 46, 43};

        int[] ints = test.fullBloomFlowers(flowers, persons);
        System.out.println(Arrays.toString(ints));

    }

    public int[] fullBloomFlowers(int[][] flowers, int[] persons) {
        TreeMap<Integer, Integer> map = new TreeMap<>();
        map.put(0, 0);
        for (int[] flower : flowers) {
            int st = flower[0];
            int end = flower[1] + 1;
            map.merge(st, 1, (a, b) -> a + b);
            map.merge(end, -1, (a, b) -> a + b);
        }
        int item = 0;
        for (int key : map.keySet()) {
            item += map.get(key);
            map.put(key, item);
        }
        int length = persons.length;
        int[] arr = new int[length];
        for (int i = 0; i < length; i++) {
            arr[i] = map.floorEntry(persons[i]).getValue();
        }
        return arr;
    }

}
