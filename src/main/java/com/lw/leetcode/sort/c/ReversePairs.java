package com.lw.leetcode.sort.c;

import java.util.Arrays;

/**剑指 Offer 51. 数组中的逆序对
 * @Author liw
 * @Date 2021/6/30 17:08
 * @Version 1.0
 */
public class ReversePairs {

    public static void main(String[] args) {
        ReversePairs test = new ReversePairs();
        int length = 6;

//        int[] nums = new int[length];
//        for (int i = 0; i < length; i++) {
//            nums[i] = (int) (Math.random() * 100);
//        }

        int[] nums = {7};

        System.out.println(Arrays.toString(nums));
        int i = test.reversePairs(nums);
        System.out.println(i);

    }



    private int[] item;
    private int count = 0;

    public int reversePairs(int[] nums) {
        if (nums == null || nums.length < 2) {
            return 0;
        }
        int length = nums.length;
        item = new int[length];
        find(nums, 0, length - 1);
        return count;
    }

    private void find(int[] nums, int st, int end) {
        if (st >= end) {
            return;
        }
        int m = st + ((end - st) >> 1);
        find(nums, st, m);
        find(nums, m + 1, end);
        int i = st;
        int j = m + 1;
        int index = 0;
        while (i <= m && j <= end) {
            if (nums[i] > nums[j]) {
                item[index++] = nums[j++];
                count += (m - i + 1);
            } else {
                item[index++] = nums[i++];
            }
        }
        while (i <= m) {
            item[index++] = nums[i++];
        }
        while (j <= end) {
            item[index++] = nums[j++];
        }
        index = 0;
        for (int k = st; k <= end; k++) {
            nums[k] = item[index++];
        }
    }
}
