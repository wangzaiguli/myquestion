package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1041. 困于环中的机器人
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/7 21:25
 */
public class IsRobotBounded {
    public boolean isRobotBounded(String instructions) {
        int i = instructions.length();
        int direct = 0;
        int y = 0;
        int x = 0;
        for (int k = 0; k < 4; k++) {
            for (int j = 0; j < i; j++) {
                char c = instructions.charAt(j);
                if (c == 'G') {
                    if (direct == 0) {
                        y++;
                    } else if (direct == 1) {
                        x--;
                    } else if (direct == 2) {
                        y--;
                    } else {
                        x++;
                    }
                } else if (c == 'L') {
                    direct = (direct + 1) % 4;
                } else if (c == 'R') {
                    direct = (direct + 3) % 4;
                }
            }
        }

        return x == 0 && y == 0;
    }
}
