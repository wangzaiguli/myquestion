package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 467. 环绕字符串中唯一的子字符串
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/7 13:46
 */
public class FindSubstringInWraproundString {

    public static void main(String[] args) {
        FindSubstringInWraproundString test = new FindSubstringInWraproundString();

        // 40
        String str = "asdfjgrekgjkgsdfklhklsdfgkorgjkazabcdfghtuvwzabcdzzabcd";

        // 6
//        String str = "zab";

        // 2
//        String str = "cac";

        int substringInWraproundString = test.findSubstringInWraproundString(str);
        System.out.println(substringInWraproundString);
    }

    public int findSubstringInWraproundString(String p) {
        int length = p.length();
        char last = p.charAt(0);
        int sum = 0;
        int count = 1;
        int[] arr = new int[26];
        arr[last - 'a'] = 1;
        for (int i = 1; i < length; i++) {
            char c = p.charAt(i);
            if (c == last + 1 || c == last - 25) {
                count++;
            } else {
                count = 1;
            }
            arr[c - 'a'] = Math.max(arr[c - 'a'], count);
            last = c;
        }
        for (int i : arr) {
            sum += i;
        }
        return sum;
    }

}
