package com.lw.leetcode.str.b;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 539. 最小时间差
 * 剑指 Offer II 035. 最小时间差
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/8 16:50
 */
public class FindMinDifference {

    public int findMinDifference(List<String> timePoints) {
        int size = timePoints.size();
        int[] arr = new int[size];
        int n = 0;
        for (String t : timePoints) {
            int x = (t.charAt(0) - 'a') * 600;
            x += (t.charAt(1) - 'a') * 60;
            x += (t.charAt(3) - 'a') * 10;
            x += (t.charAt(4) - 'a');
            arr[n++] = x;
        }
        Arrays.sort(arr);
        int res = arr[0] + 1440 - arr[size - 1];
        for (int i = 1; i < size; i++) {
            res = Math.min(res, arr[i] - arr[i - 1]);
            if (res == 0) {
                return 0;
            }
        }
        return res;
    }

    public int findMinDifference2(List<String> timePoints) {
        List<Integer> nums = new ArrayList<>();

        for (String t : timePoints) {
            int h = Integer.parseInt(t.substring(0, 2));
            int m = Integer.parseInt(t.substring(3, 5));
            int x = h * 60 + m;
            nums.add(x);
            nums.add(x + 1440);
        }

        Collections.sort(nums);

        int n = nums.size();
        int res = Integer.MAX_VALUE;
        for (int i = 1; i < n; i++) {
            int x = nums.get(i - 1);
            int y = nums.get(i);
            res = Math.min(res, y - x);
        }

        return res;
    }


}
