package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 1946. 子字符串突变后可能得到的最大整数
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/2 11:32
 */
public class MaximumNumber {

    public String maximumNumber(String num, int[] change) {
        boolean flag = false;
        char[] arr = num.toCharArray();
        int length = arr.length;
        for (int i = 0; i < length; i++) {
            int tmp = arr[i] - '0';
            if (flag && tmp > change[tmp]) {
                break;
            }
            if (tmp < change[tmp]) {
                arr[i] = (char) ('0' + change[tmp]);
                flag = true;
            }
        }
        return String.valueOf(arr);
    }

}
