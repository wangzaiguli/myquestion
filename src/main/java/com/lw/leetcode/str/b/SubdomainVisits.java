package com.lw.leetcode.str.b;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * str
 * 811. 子域名访问计数
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/1 15:27
 */
public class SubdomainVisits {

    public List<String> subdomainVisits(String[] cpdomains) {
        Map<String, Integer> map = new HashMap<>();
        for (String domain : cpdomains) {
            String[] cpinfo = domain.split("\\s+");
            String[] frags = cpinfo[1].split("\\.");
            int count = Integer.parseInt(cpinfo[0]);
            String str = "";
            for (int i = frags.length - 1; i >= 0; --i) {
                str = frags[i] + (i < frags.length - 1 ? "." : "") + str;
                map.put(str, map.getOrDefault(str, 0) + count);
            }
        }

        List<String> list = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            list.add(entry.getValue() + " " + entry.getKey());
        }
        return list;
    }

}
