package com.lw.leetcode.str.b;

import java.math.BigDecimal;

/**
 * 306. 累加数
 *
 * @Author liw
 * @Date 2021/6/29 16:02
 * @Version 1.0
 */
public class IsAdditiveNumber {

    public static void main(String[] args) {
        IsAdditiveNumber test = new IsAdditiveNumber();
//        String str = "112358";
//        String str = "199100199";
//        String str = "1101111";
//        String str = "101";
        String str = "0235813";

//        String str = "11235813213455890144";


        boolean additiveNumber = test.isAdditiveNumber(str);
        System.out.println(additiveNumber);

        // 121474836472147483648 true
        //     if(num.equals("0")) return false;
        //    if(num.equals("1")) return false;
        //    if(num.equals("10")) return false;
        //    if(num.equals("111")) return false;
        //    if(num.equals("113")) return false;
        //    if(num.equals("1023")) return false;
        //    if(num.equals("1203")) return false;
        //    if(num.equals("0235813")) return false;
        //    if(num.equals("199001200")) return false;
        //    if(num.equals("120122436")) return false;
        //    if(num.equals("121202436")) return false;
        //    if(num.equals("121224036")) return false;
        //    if(num.equals("19910011992")) return false;
        //    if(num.equals("1991000199299498797")) return false;
        //    if(num.equals("2461016264268110179")) return false;
        //    if(num.equals("11235813213455890144")) return false;

    }

    private char[] arr;
    private int length;
    public boolean isAdditiveNumber(String num) {
        if (num == null || num.length() < 3) {
            return false;
        }
        length = num.length();
        arr = num.toCharArray();
        int b = length >> 1;
        BigDecimal a1 = BigDecimal.ZERO;
        BigDecimal a2;
        for (int i = 0; i < b; i++) {
            a1 = a1.multiply(BigDecimal.TEN).add(new BigDecimal(arr[i] - 48));
            a2 = BigDecimal.ZERO;
            if (i == 1 && arr[0] == '0') {
                return false;
            }
            for (int j = i + 1; j < length; j++) {
                if (j > i + 1 && arr[i + 1] == '0') {
                    break;
                }
                a2 = a2.multiply(BigDecimal.TEN).add(new BigDecimal(arr[j] - 48));
                if (find(a1, a2, j + 1)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean find(BigDecimal a1, BigDecimal a2, int st) {
        BigDecimal add = a1.add(a2);
        String str = add.toString();
        int l = str.length();
        if (st + l > this.length) {
            return false;
        }
        if (String.valueOf(arr, st, l).equals(str)) {
            if (st + l == this.length) {
                return true;
            }
            return find(a2, add, st + l);
        }
        return false;
    }


}
