package com.lw.leetcode.str.b;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 2452. 距离字典两次编辑以内的单词
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/31 10:45
 */
public class TwoEditWords {

    public static void main(String[] args) {
        TwoEditWords test = new TwoEditWords();

        // ["word","note","wood"]
//        String[] queries = {"word", "note", "ants", "wood"};
//        String[] dictionary = {"wood", "joke", "moat"};

        // []
        String[] queries = {"yes"};
        String[] dictionary = {"not"};

        List<String> list = test.twoEditWords(queries, dictionary);
        System.out.println(list);

    }

    public List<String> twoEditWords(String[] queries, String[] dictionary) {
        int n = dictionary.length;
        char[][] arr = new char[n][];
        for (int i = 0; i < n; i++) {
            arr[i] = dictionary[i].toCharArray();
        }
        List<String> list = new ArrayList<>();
        for (String query : queries) {
            char[] chars = query.toCharArray();
            for (int j = 0; j < n; j++) {
                if (find(chars, arr[j])) {
                    list.add(query);
                    break;
                }
            }
        }
        return list;
    }

    private boolean find(char[] arr1, char[] arr2) {
        int c = 0;
        for (int i = arr1.length - 1; i >= 0; i--) {
            if (arr1[i] != arr2[i]) {
                c++;
            }
        }
        return c <= 2;
    }

}
