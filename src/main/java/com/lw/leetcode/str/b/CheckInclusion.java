package com.lw.leetcode.str.b;

/**
 * 567. 字符串的排列
 * 剑指 Offer II 014. 字符串中的变位词
 *
 * @Author liw
 * @Date 2021/6/24 16:37
 * @Version 1.0
 */
public class CheckInclusion {

    public static void main(String[] args) {
        CheckInclusion test = new CheckInclusion();
        String s1 = "ab";
        String s2 = "eidboaoo";
        boolean b = test.checkInclusion(s1, s2);
        System.out.println(b);
    }

    public boolean checkInclusion(String s1, String s2) {
        int l1 = s1.length();
        int l2 = s2.length();
        if (l1 > l2) {
            return false;
        }
        int[] arr1 = new int[26];
        int[] arr2 = new int[26];

        byte[] bytes = s1.getBytes();
        for (int i = 0; i < l1; i++) {
            arr1[bytes[i] - 97] += 1;
        }
        bytes = s2.getBytes();
        for (int i = 0; i < l1; i++) {
            arr2[bytes[i] - 97] += 1;
        }

        int st = 0;
        for (int i = l1; i < l2; i++) {
            boolean b = find(arr1, arr2);
            if (b) {
                return b;
            }
            arr2[bytes[i] - 97] += 1;
            arr2[bytes[st++] - 97] -= 1;
        }

        return find(arr1, arr2);
    }

    private boolean find(int[] arr1, int[] arr2) {
        for (int i = 0; i < 26; i++) {
            if (arr1[i] != arr2[i]) {
                return false;
            }
        }
        return true;
    }
}
