package com.lw.leetcode.str.b;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * 1647. 字符频次唯一的最小删除次数
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/10 21:48
 */
public class MinDeletions {

    public int minDeletions(String s) {
        int[] arr = new int[26];
        for (char c : s.toCharArray()) {
            arr[c - 'a']++;
        }
        int count = 0;
        Set<Integer> set = new HashSet<>();
        for (int i = 25; i >= 0; i--) {
            int v = arr[i];
            while (v != 0 && set.contains(v)) {
                v--;
                count++;
            }
            if (v != 0) {
                set.add(v);
            }
        }
        return count;
    }

}
