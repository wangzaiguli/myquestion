package com.lw.leetcode.str.b;

/**
 * str
 * b
 * 777. 在LR字符串中交换相邻字符
 *
 * @Author liw
 * @Date 2021/5/11 11:18
 * @Version 1.0
 */
public class CanTransform {

    public static void main(String[] args) {
        CanTransform test = new CanTransform();

        // false
//        String a = "LXXLXRLXXL";
//        String b = "XLLXRXLXLX";

        // true
//        String a = "XXXXXLXXXX";
//        String b = "LXXXXXXXXX";

        // true
//        String a = "RXXLRXRXL";
//        String b = "XRLXXRRLX";

        // fasle
        String a = "XRXXXLXXXR";
        String b = "XXRLXXXRXX";



        System.out.println(test.canTransform(a, b));
    }

    public boolean canTransform(String start, String end) {
        if (!start.replace("X", "").equals(end.replace("X", ""))) {
            return false;
        }
        int length = start.length();
        int sl = 0;
        int sr = 0;
        int el = 0;
        int er = 0;
        for (int i = 0; i < length; i++) {
            char s = start.charAt(i);
            if (s == 'L') {
                sl++;
            } else if (s == 'R') {
                sr++;
            }
            char e = end.charAt(i);
            if (e == 'L') {
                el++;
            } else if (e == 'R') {
                er++;
            }
            if (sl > el || sr < er) {
                return false;
            }
        }
        return true;
    }

    public boolean canTransform2(String start, String end) {
        int length = start.length();
        int index = 0;
        char[] arr = start.toCharArray();
        for (int i = 0; i < length; i++) {
            char b = end.charAt(i);
            boolean flag = false;
            for (int j = index; j < length; j++) {
                char a = arr[j];
                if (a == 'A') {
                    continue;
                }
                if (b == 'L' && a == 'R' ) {
                    return false;
                }
                if (b == 'R' && a != 'R') {
                    return false;
                }
                if ((b == 'X' && a == 'L')) {
                    return false;
                }
                if (a == b) {
                    flag = true;
                    arr[j] = 'A';
                    break;
                }
            }
            if (!flag) {
                return false;
            }
        }
        return true;
    }
}
