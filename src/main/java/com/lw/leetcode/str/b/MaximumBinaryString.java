package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * str
 * 1702. 修改后的最大二进制字符串
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/8 22:36
 */
public class MaximumBinaryString {
    public String maximumBinaryString(String binary) {
        int a = 0;
        int b = 0;
        int length = binary.length();
        char[] arr = binary.toCharArray();
        boolean flag = false;
        for (int i = 0; i < length; i++) {
            if (arr[i] == '0') {
                flag = true;
                a++;
                arr[i] = '1';
            } else {
                if (flag) {
                    b++;
                }
            }
        }
        if (a == 0 || a == 1) {
            return binary;
        }
        arr[length - b - 1] = '0';
        return String.valueOf(arr);
    }
}
