package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 788. 旋转数字
 *
 * @author liw
 * @version 1.0
 * @date 2022/9/25 8:53
 */
public class RotatedDigits {

    public int rotatedDigits(int n) {
        int count = 0;
        for (int i = 2; i <= n; i++) {
            String s = String.valueOf(i);
            s = s.replaceAll("[+0,+1,+8]", "");
            if (!"".equals(s)) {
                s = s.replaceAll("[+2,+5,+6,+9]", "");
                if ("".equals(s)) {
                    count += 1;
                }
            }
        }
        return count;
    }

}
