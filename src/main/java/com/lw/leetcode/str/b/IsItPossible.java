package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 6284. 使字符串总不同字符的数目相等
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/9 11:12
 */
public class IsItPossible {

    public static void main(String[] args) {
        IsItPossible test = new IsItPossible();

        String a = "ab";
        String b = "abcc";

        boolean itPossible = test.isItPossible(a, b);
        System.out.println(itPossible);

    }

    public boolean isItPossible(String word1, String word2) {
        int[] as = new int[26];
        int[] bs = new int[26];
        int length = word1.length();
        for (int i = 0; i < length; i++) {
            as[word1.charAt(i) - 'a']++;
        }
        length = word2.length();
        for (int i = 0; i < length; i++) {
            bs[word2.charAt(i) - 'a']++;
        }
        int ac = 0;
        int bc = 0;
        for (int i = 0; i < 26; i++) {
            if (as[i] > 0) {
                ac++;
            }
            if (bs[i] > 0) {
                bc++;
            }
        }
        for (int i = 0; i < 26; i++) {
            if (as[i] == 0) {
                continue;
            }
            int a = ac;
            int b = bc;
            for (int j = 0; j < 26; j++) {
                if (bs[j] == 0) {
                    continue;
                }
                if (i == j) {
                    if (ac == bc) {
                        return true;
                    }
                    continue;
                }
                if (as[i] == 1) {
                    a--;
                }
                if (bs[j] == 1) {
                    b--;
                }
                if (bs[i] == 0) {
                    b++;
                }
                if (as[j] == 0) {
                    a++;
                }
                if (a == b) {
                    return true;
                }
                a = ac;
                b = bc;
            }
        }
        return false;
    }

}
