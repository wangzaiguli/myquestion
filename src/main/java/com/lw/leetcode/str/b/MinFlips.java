package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * str
 * 1529. 最少的后缀翻转次数
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/30 14:44
 */
public class MinFlips {

    public static void main(String[] args) {
        MinFlips test = new MinFlips();

        // 3
        String str = "10111";
        int i = test.minFlips(str);

        System.out.println(i);
    }

    public int minFlips(String target) {
        int count = 0;
        int length = target.length();
        char item = '0';
        for (int i = 0; i < length; i++) {
            char c = target.charAt(i);
            if (c != item) {
                count++;
                item = c;
            }
        }
        return count;
    }

}
