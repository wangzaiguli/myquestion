package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 1328. 破坏回文串
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/11 9:44
 */
public class BreakPalindrome {

    public String breakPalindrome(String palindrome) {
        int n = palindrome.length();
        if (n == 1) {
            return "";
        }
        StringBuilder sb = new StringBuilder(palindrome);
        for (int i = 0; i < n / 2; i++) {
            if (sb.charAt(i) != 'a') {
                sb.setCharAt(i, 'a');
                return sb.toString();
            }
        }
        sb.setCharAt(n - 1, 'b');
        return sb.toString();
    }

}
