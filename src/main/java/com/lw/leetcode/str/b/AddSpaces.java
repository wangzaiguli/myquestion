package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * str
 * 5957. 向字符串添加空格
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/19 15:55
 */
public class AddSpaces {

    public static void main(String[] args) {
        AddSpaces test = new AddSpaces();

        // Leetcode Helps Me Learn
        String str = "LeetcodeHelpsMeLearn";
        int[] arr = {8, 13, 15};
        String s = test.addSpaces(str, arr);
        System.out.println(s);
    }

    public String addSpaces(String s, int[] spaces) {
        int a = s.length();
        int b = spaces.length;
        StringBuilder sb = new StringBuilder(a + b);
        a--;
        b--;
        while (a >= 0) {
            sb.append(s.charAt(a--));
            if (b >= 0 && a + 1 == spaces[b]) {
                b--;
                sb.append(' ');
            }
        }
        return sb.reverse().toString();
    }

}
