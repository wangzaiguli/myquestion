package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 1754. 构造字典序最大的合并字符串
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/21 10:35
 */
public class LargestMerge {

    public String largestMerge(String word1, String word2) {
        int a = word1.length();
        int b = word2.length();
        StringBuilder s = new StringBuilder(a + b);
        int i = 0;
        int j = 0;
        while (i < a || j < b) {
            if (word1.substring(i).compareTo(word2.substring(j)) >= 0) {
                s.append(word1.charAt(i++));
            } else {
                s.append(word2.charAt(j++));
            }
        }
        return s.toString();
    }

}
