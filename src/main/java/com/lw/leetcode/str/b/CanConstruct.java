package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 1400. 构造 K 个回文字符串
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/10 20:40
 */
public class CanConstruct {
    public boolean canConstruct(String s, int k) {
        int length = s.length();
        if (k > length) {
            return false;
        }
        if (k == length) {
            return true;
        }
        int[] arr = new int[26];
        for (int i = 0; i < length; i++) {
            arr[s.charAt(i) - 'a']++;
        }
        int c = 0;
        for (int i : arr) {
            c += (i & 1);
        }
        return c <= k;
    }
}
