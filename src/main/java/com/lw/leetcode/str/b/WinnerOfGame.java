package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 2038. 如果相邻两个颜色均相同则删除当前颜色
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/22 9:19
 */
public class WinnerOfGame {

    public boolean winnerOfGame(String colors) {
        int length = colors.length();
        if (length < 3) {
            return false;
        }
        char a = colors.charAt(0);
        char b = colors.charAt(1);
        char c;
        int ac = 0;
        int bc = 0;
        for (int i = 2; i < length; i++) {
            c = colors.charAt(i);
            if ('A' == c && c == b && b == a) {
                ac++;
            } else if ('B' == c && c == b && b == a) {
                bc++;
            }
            a = b;
            b = c;
        }
        return ac > bc;
    }

}
