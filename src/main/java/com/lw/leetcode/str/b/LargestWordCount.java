package com.lw.leetcode.str.b;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * 6084. 最多单词数的发件人
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/30 11:08
 */
public class LargestWordCount {
    public String largestWordCount(String[] messages, String[] senders) {
        int n = senders.length;
        int max = 0;
        String res = "";
        HashMap<String, Integer> map = new HashMap<>();
        for (int i = 0; i < n; i++) {
            String sender = senders[i];
            String msg = messages[i];
            int cnt = map.getOrDefault(sender, 0) + msg.split(" ").length;
            if (cnt >= max) {
                if (cnt > max || sender.compareTo(res) > 0) {
                    res = sender;
                }
                max = cnt;
            }
            map.put(sender, cnt);
        }
        return res;
    }
}
