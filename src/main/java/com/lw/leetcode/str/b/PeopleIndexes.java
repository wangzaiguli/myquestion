package com.lw.leetcode.str.b;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * str
 * 1452. 收藏清单
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/9 13:55
 */
public class PeopleIndexes {
    public List<Integer> peopleIndexes(List<List<String>> favoriteCompanies) {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < favoriteCompanies.size(); i++) {
            List<String> checkOne = favoriteCompanies.get(i);
            boolean flag = true;
            for (int j = 0; j < favoriteCompanies.size(); j++) {
                if (j == i) {
                    continue;
                }
                List<String> second = favoriteCompanies.get(j);
                Set<String> tmpSecond = new HashSet<>(second);
                if (tmpSecond.containsAll(checkOne)) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                result.add(i);
            }
        }
        return result;
    }

}
