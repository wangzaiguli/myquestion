package com.lw.leetcode.str.b;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * str
 * 890. 查找和替换模式
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/25 22:00
 */
public class FindAndReplacePattern {

    public static void main(String[] args) {
        FindAndReplacePattern test = new FindAndReplacePattern();
        // ["mee","aqq"]
        String[] arr = {"abc", "deq", "mee", "aqq", "dkd", "ccc"};
        String str = "abb";
        List<String> andReplacePattern = test.findAndReplacePattern(arr, str);
        System.out.println(andReplacePattern);
    }

    public List<String> findAndReplacePattern(String[] words, String pattern) {
        int[] arr1 = new int[27];
        int[] arr2 = new int[27];
        int length = pattern.length();
        int[] chars = new int[length];
        for (int i = 0; i < length; i++) {
            chars[i] = pattern.charAt(i) - 96;
        }
        List<String> list = new ArrayList<>();
        a:
        for (String word : words) {
            Arrays.fill(arr1, 0);
            Arrays.fill(arr2, 0);
            for (int i = 0; i < length; i++) {
                int c1 = word.charAt(i) - 96;
                int c2 = chars[i];
                if (arr1[c2] == 0 && arr2[c1] == 0) {
                    arr1[c2] = c1;
                    arr2[c1] = c2;
                }
                if (arr1[c2] == 0 || arr2[c1] == 0 || arr1[c2] != c1 || arr2[c1] != c2) {
                    continue a;
                }
            }
            list.add(word);
        }
        return list;
    }

}
