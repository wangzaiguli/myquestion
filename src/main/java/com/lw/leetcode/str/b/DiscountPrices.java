package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 6079. 价格减免
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/30 11:04
 */
public class DiscountPrices {
    public String discountPrices(String sentence, int discount) {
        double d = 1.0 * (100 - discount) / 100;
        String[] ss = sentence.split(" ");
        StringBuilder ans = new StringBuilder();
        for (int i = 0; i < ss.length; ++i) {
            if (ss[i].charAt(0) != '$') {
                ans.append(ss[i]).append(' ');
                continue;
            }
            try {
                double val = Double.valueOf(ss[i].substring(1));
                val *= d;
                ans.append(String.format("$%.2f ", val));
            } catch (NumberFormatException e) {
                ans.append(ss[i]).append(' ');
                continue;
            }
        }
        return ans.substring(0, ans.length() - 1);
    }
}
