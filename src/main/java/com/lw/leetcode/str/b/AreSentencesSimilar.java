package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 1813. 句子相似性 III
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/31 10:05
 */
public class AreSentencesSimilar {

    public static void main(String[] args) {
        AreSentencesSimilar test = new AreSentencesSimilar();
        // “My is is Haley is", "My Haley is” 输出了false
        String str1 = "My is is Haley is";
        String str2 = "My Haley is";

        boolean b = test.areSentencesSimilar(str1, str2);
        System.out.println(b);
    }

    public boolean areSentencesSimilar(String s, String t) {
        if (s == null || s.length() == 0 || t == null || t.length() == 0) {
            return true;
        }
        if (s.length() < t.length()) {
            return areSentencesSimilar(t, s);
        }

        String[] arr1 = s.split(" ");
        String[] arr2 = t.split(" ");
        int m = arr1.length;
        int n = arr2.length;
        int i = 0;
        while (i < n && arr1[i].equals(arr2[i])) {
            i++;
        }
        if (i == n) {
            return true;
        }
        int j = 0;
        while (j < n && arr1[m - 1 - j].equals(arr2[n - 1 - j])) {
            j++;
        }
        if (j == n) {
            return true;
        }
        return (i + j) >= n;
    }


}
