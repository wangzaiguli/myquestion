package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 1347. 制造字母异位词的最小步骤数
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/11 11:34
 */
public class MinSteps {
    public int minSteps(String s, String t) {

        int[] a = new int[26];
        int[] b = new int[26];

        for (char c : s.toCharArray()) {
            a[c - 'a']++;
        }
        for (char c : t.toCharArray()) {
            b[c - 'a']++;
        }
        int sum = 0;
        for (int i = 25; i >= 0; i--) {
            sum += Math.abs(a[i] - b[i]);
        }
        return sum >> 1;
    }
}
