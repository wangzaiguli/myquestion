package com.lw.leetcode.str.b;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Created with IntelliJ IDEA.
 * 2343. 裁剪数字后查询第 K 小的数字
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/18 11:12
 */
public class SmallestTrimmedNumbers {

    public int[] smallestTrimmedNumbers(String[] nums, int[][] queries) {
       int[] ans =  new int[queries.length];
        int m = nums[0].length();
        for (int p = 0; p < queries.length; p++) {
            int[] q = queries[p];
            List<Integer> idx = new ArrayList<>(Arrays.asList(IntStream.range(0, nums.length).boxed().toArray(Integer[]::new)));
            Collections.sort(idx, (i, j) -> nums[i].substring(m - q[1]).compareTo(nums[j].substring(m - q[1])));
            ans[p] = idx.get(q[0] - 1);
        }
        return ans;
    }

}
