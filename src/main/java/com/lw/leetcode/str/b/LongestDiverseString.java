package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * str
 * 1405. 最长快乐字符串
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/7 9:44
 */
public class LongestDiverseString {


    public static void main(String[] args) {
        LongestDiverseString test = new LongestDiverseString();

        // abbaabaac
//        int a = 5;
//        int b = 3;
//        int c = 1;

        // aabaa
//        int a = 7;
//        int b = 1;
//        int c = 0;

        // aabaa
        int a = 2;
        int b = 2;
        int c = 1;

        String test1 = test.longestDiverseString(a, b, c);

        System.out.println(test1);
    }

    public String longestDiverseString(int a, int b, int c) {
        int max = Math.max(a, Math.max(b, c));
        char a1;
        char b1;
        char c1;
        if (max == a) {
            a1 = 'a';
            b1 = 'b';
            c1 = 'c';
        } else if (max == b) {
            b = a;
            a = max;
            a1 = 'b';
            b1 = 'a';
            c1 = 'c';
        } else {
            c = a;
            a = max;
            a1 = 'c';
            b1 = 'b';
            c1 = 'a';
        }
        char[] arr = new char[a + b + c];
        int i = 0;
        while (a < (b + c) * 2) {
            arr[i++] = a1;
            a--;
            if (b > 0) {
                b--;
                arr[i++] = b1;
                if (b > 0) {
                    b--;
                    arr[i++] = b1;
                } else {
                    c--;
                    arr[i++] = c1;
                }
            } else {
                if (c == 0) {
                    break;
                } else if (c == 1) {
                    c--;
                    arr[i++] = c1;
                } else {
                    c -= 2;
                    arr[i++] = c1;
                    arr[i++] = c1;
                }

            }
        }
        while (b + c > 0) {
            arr[i++] = a1;
            arr[i++] = a1;
            a -= 2;
            if (b > 0) {
                b--;
                arr[i++] = b1;

            } else {
                c--;
                arr[i++] = c1;
            }
        }
        if (a == 1) {
            arr[i++] = a1;
        } else if (a >= 2) {
            arr[i++] = a1;
            arr[i++] = a1;
        }
        return String.valueOf(arr, 0, i);
    }

}
