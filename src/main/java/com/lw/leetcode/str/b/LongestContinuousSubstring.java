package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * str
 * 2414. 最长的字母序连续子字符串的长度
 *
 * @author liw
 * @version 1.0
 * @date 2022/9/18 20:57
 */
public class LongestContinuousSubstring {

    public static void main(String[] args) {
        LongestContinuousSubstring test = new LongestContinuousSubstring();

        // 5
//        String str = "abcde";

        // 2
//        String str = "abacaba";

        // 1
        String str = "a";

        int i = test.longestContinuousSubstring(str);
        System.out.println(i);
    }

    public int longestContinuousSubstring(String s) {
        char[] arr = s.toCharArray();
        int item = 1;
        int max = 0;
        int length = s.length();
        for (int i = 1; i < length; i++) {
            if (arr[i] == arr[i - 1] + 1) {
                item++;
            } else {
                max = Math.max(max, item);
                item = 1;
            }
        }
        return Math.max(max, item);
    }

}
