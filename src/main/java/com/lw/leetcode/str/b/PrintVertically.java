package com.lw.leetcode.str.b;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * str
 * 1324. 竖直打印单词
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/13 16:16
 */
public class PrintVertically {

    public static void main(String[] args) {
        PrintVertically test = new PrintVertically();
        String str = "TO BE OR NOT TO BE";
        List<String> list = test.printVertically(str);
        System.out.println(list);
    }

    public List<String> printVertically(String s) {
        List<String> res = new ArrayList<>();
        String[] split = s.split(" ");
        int col = 0;
        for (String str : split) {
            col = Math.max(col, str.length());
        }
        for (int c = 0; c < col; c++) {
            StringBuilder sb = new StringBuilder();
            for (String str : split) {
                sb.append(c < str.length() ? str.charAt(c) : " ");
            }
            res.add(trim(sb.toString()));
        }
        return res;
    }

    private String trim(String s) {

        int i = s.length() - 1;
        char[] chars = s.toCharArray();
        while (chars[i] == ' ') {
            i--;
        }
        return String.valueOf(chars, 0, i + 1);
    }
}
