package com.lw.leetcode.str.b;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * str
 * b
 * 1451. 重新排列句子中的单词
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/9 13:30
 */
public class ArrangeWords {

    public String arrangeWords(String text) {
        String[] s = text.split(" ");
        s[0] = s[0].toLowerCase();
        Arrays.sort(s, Comparator.comparingInt(String::length));
        s[0] = s[0].substring(0, 1).toUpperCase() + s[0].substring(1);
        return String.join(" ", s);
    }


}
