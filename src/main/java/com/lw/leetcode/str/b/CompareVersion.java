package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 165. 比较版本号
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/1 9:19
 */
public class CompareVersion {

    public int compareVersion(String version1, String version2) {
        String[] v1 = version1.split("\\.");
        String[] v2 = version2.split("\\.");
        int a = v1.length;
        int b = v2.length;
        int c = Math.min(a, b);
        for (int i = 0; i < c; ++i) {
            int x = Integer.parseInt(v1[i]);
            int y = Integer.parseInt(v2[i]);
            if (x > y) {
                return 1;
            }
            if (x < y) {
                return -1;
            }
        }
        for (int i = c; i < a; ++i) {
            if (Integer.parseInt(v1[i]) > 0) {
                return 1;
            }
        }
        for (int i = a; i < b; ++i) {
            if (Integer.parseInt(v2[i]) > 0) {
                return -1;
            }
        }
        return 0;
    }

    public int compareVersion2(String version1, String version2) {
        String[] v1 = version1.split("\\.");
        String[] v2 = version2.split("\\.");
        for (int i = 0; i < v1.length || i < v2.length; ++i) {
            int x = 0, y = 0;
            if (i < v1.length) {
                x = Integer.parseInt(v1[i]);
            }
            if (i < v2.length) {
                y = Integer.parseInt(v2[i]);
            }
            if (x > y) {
                return 1;
            }
            if (x < y) {
                return -1;
            }
        }
        return 0;
    }


}
