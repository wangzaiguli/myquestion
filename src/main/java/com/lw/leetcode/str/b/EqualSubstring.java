package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 1208. 尽可能使字符串相等
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/30 13:14
 */
public class EqualSubstring {

    public static void main(String[] args) {
        EqualSubstring test = new EqualSubstring();

        // 3
//        String s = "abcd";
//        String b = "bcdf";
//        int k = 3;

        // 3
//        String s = "abcd";
//        String b = "cdef";
//        int k = 3;

        // 3
//        String s = "abcd";
//        String b = "acde";
//        int k = 0;

        // 4
        String s = "krpgjbjjznpzdfy";
        String b = "nxargkbydxmsgby";
        int k = 14;

        int i = test.equalSubstring(s, b, k);
        System.out.println(i);
    }

    public int equalSubstring(String s, String t, int maxCost) {
        char[] as = s.toCharArray();
        char[] bs = t.toCharArray();
        int length = s.length();
        for (int i = 0; i < length; i++) {
            as[i] = (char) Math.abs(as[i] - bs[i]);
        }
        int st = 0;
        int end = 0;
        int max = 0;
        int item = 0;
        while (end < length) {
            if (item <= maxCost) {
                max = Math.max(max, end - st);
                maxCost -= as[end++];
            } else {
                maxCost += as[st++];
            }
        }
        if (item <= maxCost) {
            max = Math.max(max, end - st);
        }
        return max;
    }

}
