package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * str
 * 1750. 删除字符串两端相同字符后的最短长度
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/8 20:43
 */
public class MinimumLength {


    public static void main(String[] args) {
        MinimumLength test = new MinimumLength();

        // 1
        String str = "a";
        // 1
//        String str = "aba";
        // 0
//        String str = "aa";
        // 0
//        String str = "cabaabac";
        // 3
//        String str = "aabccabba";

        int i = test.minimumLength(str);
        System.out.println(i);


    }

    public int minimumLength(String s) {
        int length = s.length();
        int j = length - 1;
        int i = 0;
        char[] arr = s.toCharArray();
        while (i < j) {
            if (arr[i] != arr[j]) {
                return j - i + 1;
            }
            i++;
            while (i < length && arr[i] == arr[i - 1]) {
                i++;
            }
            j--;
            while (j >= 0 && arr[j] == arr[j + 1]) {
                j--;
            }
        }
        if (i > j) {
            return 0;
        }
        return j - i + 1;
    }
}
