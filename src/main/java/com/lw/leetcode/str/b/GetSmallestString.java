package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 1663. 具有给定数值的最小字符串
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/8 11:39
 */
public class GetSmallestString {

    public String getSmallestString(int n, int k) {
        char[] arr = new char[n];
        for (int i = n - 1; i > 0; i--) {
            int c = (k + i - 2) / (i);
            if (c <= 26) {
                arr[n - i - 1] = 'a';
            } else {
                arr[n - i - 1] = (char) (k - 26 * i + 96);
            }
            k -= (arr[n - i - 1] - 96);
        }
        arr[n - 1] = (char) (k + 96);
        return String.valueOf(arr);
    }

}
