package com.lw.leetcode.str.b;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/31 9:23
 */
public class Evaluate {

    public String evaluate(String s, List<List<String>> knowledge) {
        Map<String, String> map = new HashMap<>();
        for (List<String> list : knowledge) {
            map.put(list.get(0), list.get(1));
        }
        StringBuilder sb = new StringBuilder();
        StringBuilder item = new StringBuilder();
        int keyCount = 0;
        for (char c : s.toCharArray()) {
            if (c == '(') {
                keyCount++;
            } else if (c == ')') {
                keyCount--;
                sb.append(map.getOrDefault(item.toString(), "?"));
                item.setLength(0);
            } else if (keyCount > 0) {
                item.append(c);
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }


}
