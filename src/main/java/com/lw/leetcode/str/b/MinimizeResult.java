package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 2232. 向表达式添加括号后的最小结果
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/15 9:54
 */
public class MinimizeResult {


    public static void main(String[] args) {
        MinimizeResult test = new MinimizeResult();

        // 170
        String str = "247+38";
        String s = test.minimizeResult(str);

        System.out.println(s);

    }

    public String minimizeResult(String expression) {
        String[] split = expression.split("\\+");
        String a = split[0];
        String b = split[1];
        int al = a.length();
        int bl = b.length();
        int min = Integer.MAX_VALUE;
        int s1 = 1;
        String values = null;
        for (int i = 0; i < al; i++) {
            if (i > 0) {
                s1 = Integer.parseInt(a.substring(0, i));
            }
            int s2 = Integer.parseInt(a.substring(i));
            for (int j = 1; j <= bl; j++) {
                int s3 = Integer.parseInt(b.substring(0, j));
                int s4 = 1;
                if (j != bl) {
                    s4 = Integer.parseInt(b.substring(j));
                }
                int s = s1 * (s2 + s3) * s4;
                if (s < min) {
                    min = s;
                    String ai = "";
                    if (i > 0) {
                        ai = String.valueOf(s1);
                    }
                    String bi = "";
                    if (j < bl) {
                        bi = String.valueOf(s4);
                    }
                    values = ai + "(" + s2 + "+" + s3 + ")" + bi;
                }
            }
        }
        return values;
    }

}