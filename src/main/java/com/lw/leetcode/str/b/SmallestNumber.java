package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 2375. 根据模式串构造最小数字
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/15 9:11
 */
public class SmallestNumber {

    public static void main(String[] args) {
        SmallestNumber test = new SmallestNumber();

        // "12"
        String str = "I";

        // "21"
//        String str = "D";

        // 123549876
//        String str = "IIIDIDDD";

        // 4321
//        String str = "DDD";

        String s = test.smallestNumber(str);
        System.out.println(s);
    }

    public String smallestNumber(String pattern) {
        pattern = "I" + pattern;
        int length = pattern.length();
        char[] arr = new char[length];
        char[] chars = pattern.toCharArray();
        int i = 0;
        char st = '1';
        while (i < length) {
            char c = pattern.charAt(i);
            if (c == 'I') {
                int count = find(i + 1, chars);
                st += count;
                char st2 = st;
                while (count > 0) {
                    arr[i] = st2;
                    i++;
                    st2--;
                    count--;
                }
                st++;
                arr[i] = st2;
                i++;
            }
        }
        return String.valueOf(arr);
    }

    private int find(int index, char[] chars) {
        int length = chars.length;
        int count = 0;
        while (index < length) {
            if (chars[index] == 'D') {
                count++;
                index++;
            } else {
                break;
            }
        }
        return count;
    }
}
