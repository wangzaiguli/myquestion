package com.lw.leetcode.str.b;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * 966. 元音拼写检查器
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/13 20:20
 */
public class Spellchecker {

    private Set<String> set;
    private Map<String, String> mapa;
    private Map<String, String> mapb;

    public String[] spellchecker(String[] wordlist, String[] queries) {
        set = new HashSet<>();
        mapa = new HashMap<>();
        mapb = new HashMap<>();

        for (String word : wordlist) {
            String wordlow = word.toLowerCase();
            set.add(word);
            mapa.putIfAbsent(wordlow, word);
            mapb.putIfAbsent(devowel(wordlow), word);
        }

        String[] ans = new String[queries.length];
        int t = 0;
        for (String query : queries) {
            ans[t++] = solve(query);
        }
        return ans;
    }

    public String solve(String query) {
        if (set.contains(query)) {
            return query;
        }
        String a = query.toLowerCase();
        if (mapa.containsKey(a)) {
            return mapa.get(a);
        }
        String b = devowel(a);
        if (mapb.containsKey(b)) {
            return mapb.get(b);
        }
        return "";
    }

    public String devowel(String word) {
        StringBuilder ans = new StringBuilder(word.length());
        for (char c : word.toCharArray()) {
            ans.append((c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') ? '*' : c);
        }
        return ans.toString();
    }


}
