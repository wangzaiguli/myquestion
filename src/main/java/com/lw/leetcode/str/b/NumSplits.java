package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * str
 * 1525. 字符串的好分割数目
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/15 22:21
 */
public class NumSplits {


    public static void main(String[] args) {
        NumSplits test = new NumSplits();


        // 2
//        String str = "aacaba";

        // 1
//        String str = "abcd";

        // 2
        String str = "acbadbaada";

        //
        int i = test.numSplits(str);
        System.out.println(i);
    }

    public int numSplits(String s) {
        int[] arr = new int[26];
        int a = 0;
        int b = 0;
        int count = 0;
        char[] chars = s.toCharArray();
        for (char c : chars) {
            arr[c - 'a']++;
        }
        for (int i = 0; i < 26; i++) {
            if (arr[i] != 0) {
                b++;
            }
        }
        int flag = 0;
        for (char c : chars) {
            int i = c - 'a';
            arr[i]--;
            if (arr[i] == 0) {
                b--;
            }
            int k = 1 << i;
            if ((k & flag) == 0) {
                flag |= k;
                a++;
            }
            if (a == b) {
                count++;
            }
        }
        return count;
    }
}
