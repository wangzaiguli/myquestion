package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * str
 * 1881. 插入后的最大值
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/16 20:42
 */
public class MaxValue {

    public String maxValue(String n, int x) {
        int length = n.length();
        StringBuilder sb = new StringBuilder(length + 1);
        if (n.charAt(0) == '-') {
            for (int i = 1; i < length; i++) {
                if (n.charAt(i) - 48 > x) {
                    sb.append(n, 0, i).append(x).append(n.substring(i));
                    return sb.toString();
                }
            }
        } else {
            for (int i = 0; i < length; i++) {
                if (n.charAt(i) - 48 < x) {
                    sb.append(n, 0, i).append(x).append(n.substring(i));
                    return sb.toString();
                }
            }
        }
        return n + x;
    }

}
