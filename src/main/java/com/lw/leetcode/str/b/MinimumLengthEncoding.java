package com.lw.leetcode.str.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 820. 单词的压缩编码
 * 剑指 Offer II 065. 最短的单词编码
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/21 11:02
 */
public class MinimumLengthEncoding {


    public static void main(String[] args) {
        MinimumLengthEncoding test = new MinimumLengthEncoding();

//        String[] arr = {"time", "me", "bell"};
//        String[] arr = {"t"};
        String[] arr = {"p", "grah", "qwosp"};

        int i = test.minimumLengthEncoding(arr);
        System.out.println(i);
    }

    public int minimumLengthEncoding(String[] words) {
        int length = words.length;
        for (int i = 0; i < length; i++) {
            words[i] = new StringBuilder(words[i]).reverse().toString();
        }
        Arrays.sort(words);
        int n = words[length - 1].length() + 1;
        for (int i = length - 2; i >= 0; i--) {
            String word = words[i];
            String word1 = words[i + 1];
            if (word.length() > word1.length()) {
                n = n + word.length() + 1;
            } else {
                if (!word.equals(words[i + 1].substring(0, word.length()))) {
                    n = n + word.length() + 1;
                }
            }
        }
        return n;
    }
}
