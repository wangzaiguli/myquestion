package com.lw.leetcode.str.b;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * str
 * 816. 模糊坐标
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/22 20:45
 */
public class AmbiguousCoordinates {

    public static void main(String[] args) {
        AmbiguousCoordinates test = new AmbiguousCoordinates();


        //  ["(1, 23)", "(12, 3)", "(1.2, 3)", "(1, 2.3)"]
        String str = "(123)";

        //   ["(0.001, 1)", "(0, 0.011)"]
//        String str = "(00011)";

        //  ["(0, 123)", "(0, 12.3)", "(0, 1.23)", "(0.1, 23)", "(0.1, 2.3)", "(0.12, 3)"]
//        String str = "(0123)";

        //  [(10, 0)]
//        String str = "(100)";

        List<String> list = test.ambiguousCoordinates(str);
        System.out.println(list);

    }

    public List<String> ambiguousCoordinates(String s) {
        s = s.substring(1, s.length() - 1);
        int length = s.length();
        List<String> list = new ArrayList<>();
        List<String> a = new ArrayList<>();
        List<String> b = new ArrayList<>();
        StringBuilder sb = new StringBuilder(length + 6);
        for (int i = 1; i < length; i++) {
            a.clear();
            b.clear();
            find(s.substring(0, i), a);
            find(s.substring(i), b);
            if (a.isEmpty() || b.isEmpty()) {
                continue;
            }
            for (String s1 : a) {
                sb.setLength(0);
                sb.append('(');
                sb.append(s1);
                sb.append(',');
                sb.append(' ');
                int l = s1.length();
                for (String s2 : b) {
                    sb.setLength(l + 3);
                    sb.append(s2);
                    sb.append(')');
                    list.add(sb.toString());
                }
            }
        }
        return list;
    }

    private void find(String str, List<String> list) {
        int length = str.length();
        if (length == 1) {
            list.add(str);
            return;
        }
        char a = str.charAt(0);
        char b = str.charAt(length - 1);
        if (a == '0') {
            if (b == '0') {
                return;
            }
            list.add("0." + str.substring(1));
            return;
        }
        list.add(str);
        if (b == '0') {
            return;
        }
        for (int i = 1; i < length; i++) {
            list.add(str.substring(0, i) + "." + str.substring(i));
        }
    }

}
