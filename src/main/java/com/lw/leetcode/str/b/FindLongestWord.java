package com.lw.leetcode.str.b;

import java.util.List;

/**
 * 524. 通过删除字母匹配到字典里最长单词
 *
 * @Author liw
 * @Date 2021/6/23 16:38
 * @Version 1.0
 */
public class FindLongestWord {
    public boolean isSubsequence(String x, String y) {
        int j = 0;
        for (int i = 0; i < y.length() && j < x.length(); i++) {
            if (x.charAt(j) == y.charAt(i)) {
                j++;
            }
        }
        return j == x.length();
    }

    public String findLongestWord(String s, List<String> d) {
        String value = "";
        for (String str : d) {
            if (isSubsequence(str, s)) {
                if (str.length() > value.length()) {
                    value = str;
                } else if (str.length() == value.length()) {
                    if (str.compareTo(value) < 0) {
                        value = str;
                    }
                }
            }
        }
        return value;
    }


}
