package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 面试题 01.05. 一次编辑
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/11 20:46
 */
public class OneEditAway {
    public boolean oneEditAway(String first, String second) {
        int a = first.length();
        int b = second.length();
        int len = a - b;
        if (len > 1 || len < -1) {
            return false;
        }
        int count = 1;
        for (int i = 0, j = 0; i < a && j < b; i++, j++) {
            if (first.charAt(i) != second.charAt(j)) {
                if (len == 1) {
                    j--;
                } else if (len == -1) {
                    i--;
                }
                count--;
            }
            if (count < 0) {
                return false;
            }
        }
        return true;
    }
}
