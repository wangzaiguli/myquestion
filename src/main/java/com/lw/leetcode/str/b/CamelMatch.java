package com.lw.leetcode.str.b;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 1023. 驼峰式匹配
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/25 11:12
 */
public class CamelMatch {

    public static void main(String[] args) {
        CamelMatch test = new CamelMatch();

        // false,true,false,false,false
        String[] arr = {"FooBar", "FooBarTest", "FootBall", "FrameBuffer", "ForceFeedBack"};
        String str = "FoBaT";

        List<Boolean> list = test.camelMatch(arr, str);
        System.out.println(list);
    }

    public List<Boolean> camelMatch(String[] queries, String pattern) {
        char[] arr = pattern.toCharArray();
        int pl = arr.length;
        int all = queries.length;
        List<Boolean> list = new ArrayList<>(all);
        for (String str : queries) {
            int length = str.length();
            int i = 0;
            int j = 0;
            boolean flag = true;
            while (i < pl && j < length) {
                char c = str.charAt(j);
                if (arr[i] == c) {
                    i++;
                    j++;
                } else {
                    if (c > 'Z') {
                        j++;
                    } else {
                        flag = false;
                        break;
                    }
                }
            }
            if (i < pl) {
                flag = false;
            } else {
                while (j < length) {
                    if (str.charAt(j) > 'Z') {
                        j++;
                    } else {
                        flag = false;
                        break;
                    }
                }
            }
            list.add(flag);
        }
        return list;
    }

}
