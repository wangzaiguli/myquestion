package com.lw.leetcode.str.b;

/**
 * create by idea
 * str
 * 1930. 长度为 3 的不同回文子序列
 *
 * @author lmx
 * @version 1.0
 * @date 2021/11/28 19:54
 */
public class CountPalindromicSubsequence {

    public static void main(String[] args) {
        CountPalindromicSubsequence test = new CountPalindromicSubsequence();

        // 3
//        String str = "aabca";

        // 0
//        String str = "adc";

        // 0
        String str = "bbcbaba";

        int i = test.countPalindromicSubsequence(str);
        System.out.println(i);
    }

    public int countPalindromicSubsequence(String s) {
        int count = 0;
        char[] arr = s.toCharArray();
        int length = s.length();
        int[] items = new int[26];
        for (char i = 'a'; i <= 'z'; i++) {
            int st = -1;
            int end = 0;
            for (int j = 0; j < length; j++) {
                if (arr[j] == i) {
                    st = j;
                    break;
                }
            }
            for (int j = length - 1; j >= 0; j--) {
                if (arr[j] == i) {
                    end = j;
                    break;
                }
            }
            if (end - st > 1) {
                for (int j = st + 1; j < end; j++) {
                    items[arr[j] - 'a'] = 1;
                }
                for (int j = 0; j < 26; j++) {
                    if (items[j] == 1) {
                        items[j] = 0;
                        count++;
                    }
                }
            }
        }
        return count;
    }

}
