package com.lw.leetcode.str.b;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 1233. 删除子文件夹
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/19 16:25
 */
public class RemoveSubfolders {

    public static void main(String[] args) {
        RemoveSubfolders test = new RemoveSubfolders();

        // ["/a","/c/d","/c/f"]
//        String[] arr = {"/a", "/a/b", "/c/d", "/c/d/e", "/c/f"};

        // ["/a"]
//        String[] arr = {"/a", "/a/b/c", "/a/b/d"};

        // ["/a/b/c","/a/b/ca","/a/b/d"]
        String[] arr = {"/a/b/c", "/a/b/ca", "/a/b/d"};

        List<String> list = test.removeSubfolders(arr);

        System.out.println(list);
    }

    public List<String> removeSubfolders(String[] folder) {
        Arrays.sort(folder);
        List<String> list = new ArrayList<>();
        String path = folder[0];
        String item = path + "/";
        int length = folder.length;
        for (int i = 1; i < length; i++) {
            if (!folder[i].startsWith(item)) {
                list.add(path);
                path = folder[i];
                item = path + "/";
            }
        }
        if (list.isEmpty() || !path.equals(list.get(list.size() - 1))) {
            list.add(path);
        }
        return list;
    }

}
