package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 1358. 包含所有三种字符的子字符串数目
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/11 14:18
 */
public class NumberOfSubstrings {

    public static void main(String[] args) {
        NumberOfSubstrings test = new NumberOfSubstrings();

        // 3
//        String str = "abca";

        // 10
        String str = "abcabc";

        int i = test.numberOfSubstrings(str);
        System.out.println(i);

    }

    public int numberOfSubstrings(String s) {
        int[] counts = new int[3];
        char[] arr = s.toCharArray();
        int length = s.length();
        int i = 0;
        int j = 0;
        int count = 0;
        while (i <= length) {
            if (counts[0] > 0 && counts[1] > 0 && counts[2] > 0) {
                count += (length - i + 1);
                counts[arr[j] - 'a']--;
                j++;
            } else {
                if (i == length) {
                    break;
                }
                counts[arr[i] - 'a']++;
                i++;
            }
        }
        return count;
    }

}
