package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 面试题 17.11. 单词距离
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/17 17:54
 */
public class FindClosest {

    public int findClosest(String[] words, String word1, String word2) {
        int a = -1;
        int b = -1;
        int i = 0;
        int min = Integer.MAX_VALUE;
        for (String word : words) {
            if (word.equals(word1)) {
                a = i;
                if (b != -1) {
                    min = Math.min(min, a - b);
                }
            } else if (word.equals(word2)) {
                b = i;
                if (a != -1) {
                    min = Math.min(min, b - a);
                }
            }
            i++;
        }
        return min;
    }

}
