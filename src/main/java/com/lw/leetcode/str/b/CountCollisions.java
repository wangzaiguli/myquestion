package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 2211. 统计道路上的碰撞次数
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/21 13:55
 */
public class CountCollisions {

    public static void main(String[] args) {
        CountCollisions test = new CountCollisions();

        // 5
//        String str = "RLRSLL";

        // 4
//        String str = "RRLL";

        // 0
        String str = "LLRR";

        int i = test.countCollisions(str);

        System.out.println(i);
    }

    public int countCollisions(String directions) {
        int f = -1;
        int length = directions.length();
        int count = 0;
        int rc = 0;
        for (int i = 0; i < length; i++) {
            char c = directions.charAt(i);
            if (c == 'S') {
                f = 0;
                count += rc;
                rc = 0;
            } else if (c == 'R') {
                f = 1;
                rc++;
            } else {
                if (f == 0) {
                    count++;
                } else if (f == 1) {
                    count += (rc + 1);
                    rc = 0;
                    f = 0;
                }
            }
        }
        return count;
    }

}
