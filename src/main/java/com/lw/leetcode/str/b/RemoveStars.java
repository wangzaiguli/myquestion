package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 2390. 从字符串中移除星号
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/29 13:19
 */
public class RemoveStars {

    public static void main(String[] args) {
        RemoveStars test = new RemoveStars();

        // "lecoe"
//        String str = "leet**cod*e";

        // ""
        String str = "erase*****";

        String s = test.removeStars(str);
        System.out.println(s + "&&&&&");
    }

    public String removeStars(String s) {
        char[] arr = s.toCharArray();
        int i = 0;
        int j = 0;
        int length = s.length();
        while (j < length) {
            if (arr[j] == '*') {
                j++;
                i--;
            } else {
                arr[i++] = arr[j++];
            }
        }
        return String.valueOf(arr, 0, i);
    }

}
