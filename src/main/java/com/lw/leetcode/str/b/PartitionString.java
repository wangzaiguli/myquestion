package com.lw.leetcode.str.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2405. 子字符串的最优划分
 *
 * @author liw
 * @version 1.0
 * @date 2022/9/11 20:51
 */
public class PartitionString {

    public static void main(String[] args) {
        PartitionString test = new PartitionString();

        // 4
//        String str = "abacaba";

        // 6
//        String str = "ssssss";

        // 1
//        String str = "abcdef";

        // 2
        String str = "abcdefa";

        int i = test.partitionString(str);
        System.out.println(i);
    }

    public int partitionString(String s) {
        char[] chars = s.toCharArray();
        int length = s.length();
        int[] arr = new int[26];
        int count = 1;
        for (int i = 0; i < length; i++) {
            int v = chars[i] - 'a';
            if (arr[v] == 1) {
                Arrays.fill(arr, 0);
                count++;
            }
            arr[v] = 1;
        }
        return count;
    }

}
