package com.lw.leetcode.str.b;

/**
 * create by idea
 * 1247. 交换字符使得字符串相同
 *
 * @author lmx
 * @version 1.0
 * @date 2022/1/26 21:22
 */
public class MinimumSwap {
    public int minimumSwap(String s1, String s2) {
        int length = s1.length();
        if (s2.length() != length) {
            return -1;
        }
        int a = 0;
        int b = 0;
        for (int i = 0; i < length; i++) {
            char c1 = s1.charAt(i);
            char c2 = s2.charAt(i);
            if (c1 == c2) {
                continue;
            }
            if (c1 == 'x') {
                a++;
            } else {
                b++;
            }
        }
        if ((b & 1) == (a & 1)) {
            return ((a + b) >> 1) + (a & 1);
        }
        return -1;
    }
}
