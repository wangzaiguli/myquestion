package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 2023. 连接后等于目标字符串的字符串对
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/24 21:03
 */
public class NumOfPairs {

    public int numOfPairs(String[] nums, String target) {
        int sum = 0;
        int length = nums.length;
        int l = target.length();
        for (int i = 0; i < length; i++) {
            for (int j = i + 1; j < length; j++) {
                if (nums[i].length() + nums[j].length() == l) {
                    if (target.equals(nums[j] + nums[i])) {
                        sum++;
                    }
                    if (target.equals(nums[i] + nums[j])) {
                        sum++;
                    }
                }
            }
        }
        return sum;
    }

}
