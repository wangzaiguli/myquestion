package com.lw.leetcode.str.b;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 916. 单词子集
 *
 * @Author liw
 * @Date 2021/9/1 22:00
 * @Version 1.0
 */
public class WordSubsets {

    public List<String> wordSubsets(String[] A, String[] B) {
        int[] all = new int[26];
        int[] item = new int[26];
        int[] counts = new int[26];
        for (String b : B) {
            Arrays.fill(item, 0);
            for (char c : b.toCharArray()) {
                item[c - 'a']++;
            }
            for (int i = 0; i < 26; ++i) {
                all[i] = Math.max(all[i], item[i]);
            }
        }
        List<String> list = new ArrayList<>();
        for (String a : A) {
            Arrays.fill(counts, 0);
            for (char c : a.toCharArray()) {
                counts[c - 'a']++;
            }
            int i;
            for (i = 0; i < 26; ++i) {
                if (counts[i] < all[i]) {
                    break;
                }
            }
            if (i == 26) {
                list.add(a);
            }
        }
        return list;
    }


}
