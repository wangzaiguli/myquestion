package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * str
 * 1737. 满足三条件之一需改变的最少字符数
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/9 16:12
 */
public class MinCharacters {

    public static void main(String[] args) {
        MinCharacters test = new MinCharacters();

        // 2
//        String a = "aba";
//        String b = "caa";
        // 3
//        String a = "dabadd";
//        String b = "cda";
        // 0
        String a = "dee";
        String b = "a";

        int i = test.minCharacters(a, b);
        System.out.println(i);
    }

    public int minCharacters(String a, String b) {
        int[] arr1 = new int[26];
        int[] arr2 = new int[26];
        int l1 = a.length();
        int l2 = b.length();
        for (int i = 0; i < l1; i++) {
            arr1[a.charAt(i) - 'a']++;
        }
        for (int i = 0; i < l2; i++) {
            arr2[b.charAt(i) - 'a']++;
        }
        int min = Integer.MAX_VALUE;
        int s1 = 0;
        int s2 = 0;
        for (int i = 0; i < 25; i++) {
            s1 += arr1[i];
            s2 += arr2[i];
            min = Math.min(Math.min(min, l1 - arr1[i] + l2 - arr2[i]), Math.min(l2 - s2 + s1, l1 - s1 + s2));
        }
        return Math.min(min, l1 - arr1[25] + l2 - arr2[25]);
    }

}
