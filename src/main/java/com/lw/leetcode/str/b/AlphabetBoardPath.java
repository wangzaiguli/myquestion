package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * str
 * 1138. 字母板上的路径
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/7 21:55
 */
public class AlphabetBoardPath {


    public static void main(String[] args) {
        AlphabetBoardPath test = new AlphabetBoardPath();

        // "DDR!UURRR!!DDD!"
//        String str = "leet";

        // "RR!DDRR!UUL!R!"
        String str = "code";

        String s = test.alphabetBoardPath(str);
        System.out.println(s);
    }


    public String alphabetBoardPath(String target) {
        int x1 = 0;
        int y1 = 0;
        int x2;
        int y2;
        int k = -1;
        int length = target.length();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int v = target.charAt(i) - 'a';
            if (v == k) {
                sb.append('!');
                continue;
            }
            k = v;
            x2 = v / 5;
            y2 = v % 5;
            int x = x2 - x1;
            if (x > 0) {
                if (v == 25) {
                    x--;
                }
                for (int j = 0; j < x; j++) {
                    sb.append('D');
                }
            } else {
                for (int j = 0; j > x; j--) {
                    sb.append('U');
                }
            }
            int y = y2 - y1;
            if (y > 0) {
                for (int j = 0; j < y; j++) {
                    sb.append('R');
                }
            } else {
                for (int j = 0; j > y; j--) {
                    sb.append('L');
                }
            }
            if (v == 25 && x >= 0) {
                sb.append('D');
            }
            sb.append('!');
            x1 = x2;
            y1 = y2;
        }
        return sb.toString();
    }

}
