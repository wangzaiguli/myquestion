package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 2186. 使两字符串互为字母异位词的最少步骤数
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/28 10:13
 */
public class MinSteps2 {

    public static void main(String[] args) {
        MinSteps2 test = new MinSteps2();
        // "leetcode"
        //"coats"
        String a = "leetcode";
        String b = "coats";
        int i = test.minSteps(a, b);
        System.out.println(i);
    }


    public int minSteps(String s, String t) {
        int[] arr = new int[26];
        for (int i = s.length() - 1; i >= 0; i--) {
            arr[s.charAt(i) - 'a']++;
        }
        for (int i = t.length() - 1; i >= 0; i--) {
            arr[t.charAt(i) - 'a']--;
        }
        int sum = 0;
        for (int i : arr) {
            sum += Math.abs(i);
        }
        return sum;
    }
}
