package com.lw.leetcode.str.b;

/**
 * 779. 第K个语法符号
 *
 * @Author liw
 * @Date 2021/5/22 12:30
 * @Version 1.0
 */
public class KthGrammar {

    public static void main(String[] args) {
        KthGrammar test = new KthGrammar();
        for (int i = 1; i <= 8; i++) {
            int i1 = test.kthGrammar(4, i);
            System.out.println( i + "  " + i1);
        }
    }

    public int kthGrammar(int n, int k) {
        k--;
        int d = n - 1;
        int item = 0;
        while (d >= 0) {
            int i = k >> d;
            item = item ^ (i & 1);
            d--;
        }
        return item;
    }
}
