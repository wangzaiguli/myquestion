package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * str
 * 2486. 追加字符以获得子序列
 *
 * @author liw
 * @version 1.0
 * @date 2022/11/28 20:35
 */
public class AppendCharacters {

    public int appendCharacters(String s, String t) {
        int m = s.length();
        int n = t.length();
        int i = 0;
        int j = 0;
        while (i < m && j < n) {
            if (s.charAt(i) == t.charAt(j)) {
                j++;
            }
            i++;
        }
        return n - j;
    }

}
