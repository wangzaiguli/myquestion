package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * 面试题 16.18. 模式匹配
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/2 20:33
 */
public class PatternMatching {

    public static void main(String[] args) {
        PatternMatching test = new PatternMatching();

        // true
        String a = "abba";
        String b = "dogcatcatdog";

        boolean b1 = test.patternMatching(a, b);
        System.out.println(b1);
    }

    public boolean patternMatching(String pattern, String value) {
        int l = value.length();
        if (l == 0) {
            return pattern.length() == 1;
        }
        char[] arr = pattern.toCharArray();
        int a = 0;
        int b = 0;
        for (char c : arr) {
            if (c == 'a') {
                a++;
            } else {
                b++;
            }
        }

        boolean f = find(value, a);
        if (f) {
            return true;
        }
        f = find(value, b);
        if (f) {
            return true;
        }
        if (a == 0 || b == 0) {
            return false;
        }
        int t = (l - b) / a;
        int length = pattern.length();
        item:
        for (int i = 1; i <= t; i++) {
            if ((l - a * i) % b != 0) {
                continue;
            }
            int c = (l - a * i) / b;
            int a1 = 0;
            int b1 = 0;
            String sa = "";
            String sb = "";
            int st = 0;

            for (int j = 0; j < length; j++) {
                if (arr[j] == 'a') {
                    String str = value.substring(st, st + i);
                    if ("".equals(sa)) {
                        sa = str;
                        if (sa.equals(sb)) {
                            continue item;
                        }
                    } else {
                        if (!str.equals(sa)) {
                            continue item;
                        }
                    }
                    a1++;
                } else {
                    String str = value.substring(st, st + c);
                    if ("".equals(sb)) {
                        sb = str;
                        if (sa.equals(sb)) {
                            continue item;
                        }
                    } else {
                        if (!str.equals(sb)) {
                            continue item;
                        }
                    }
                    b1++;
                }
                st = a1 * i + b1 * c;
                if (st == l) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean find(String value, int a) {
        if (a == 0) {
            return false;
        }
        int l = value.length();
        if (l % a != 0) {
            return false;
        }
        int c = l / a;
        String s = value.substring(0, c);
        for (int i = 1; i < a; i++) {
            if (!s.equals(value.substring(i * c, i * c + c))) {
                return false;
            }
        }
        return true;

    }

}
