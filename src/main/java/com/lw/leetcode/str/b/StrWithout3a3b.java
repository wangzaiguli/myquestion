package com.lw.leetcode.str.b;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * str
 * 984. 不含 AAA 或 BBB 的字符串
 *
 * @author liw
 * @version 1.0
 * @date 2021/8/6 15:05
 */
public class StrWithout3a3b {

    public static void main(String[] args) {
        StrWithout3a3b test = new StrWithout3a3b();
        String s = test.strWithout3a3b(3, 3);
        System.out.println(s);
    }


    public String strWithout3a3b(int a, int b) {
        StringBuilder sb = new StringBuilder(a + b);

        int f = 'b';
        while (a > 0 || b > 0) {
            if (a == 0) {
                for (int i = 0; i < b; i++) {
                    sb.append('b');
                }
                break;
            }
            if (b == 0) {
                for (int i = 0; i < a; i++) {
                    sb.append('a');
                }
                break;
            }
            if (a == 1 && b == 1) {
                if (f == 'b') {
                    sb.append('a');
                    sb.append('b');
                } else {
                    sb.append('b');
                    sb.append('a');
                }
                break;
            }
            if (a == b) {
                a -= 2;
                b -= 2;
                if (f == 'b') {
                    sb.append('a');
                    sb.append('a');
                    sb.append('b');
                    sb.append('b');
                } else {
                    sb.append('b');
                    sb.append('b');
                    sb.append('a');
                    sb.append('a');
                }

            } else if (a > b) {
                a -= 2;
                b -= 1;
                sb.append('a');
                sb.append('a');
                sb.append('b');
                f = 'b';
            } else {
                a -= 1;
                b -= 2;
                sb.append('b');
                sb.append('b');
                sb.append('a');
                f = 'a';
            }
        }
        return sb.toString();
    }

}
