package com.lw.leetcode.str.c;

import com.lw.test.util.Utils;

/**
 * Created with IntelliJ IDEA.
 * 899. 有序队列
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/9 16:30
 */
public class OrderlyQueue {

    public static void main(String[] args) {
        OrderlyQueue test = new OrderlyQueue();

        // acb
//        String str = "cba";
//        int k = 1;

        // aaabc
//        String str = "baaca";
//        int k = 3;

        //
        String str = Utils.getStr(1000, 'a', 'z');
        int k = 125;


        String s = test.orderlyQueue(str, k);
        System.out.println(s);
    }

    public String orderlyQueue(String s, int k) {
        int size = 26;
        int[] as = new int[size];
        int length = s.length();
        char[] chars = s.toCharArray();
        for (int i = 0; i < length; i++) {
            as[chars[i] - 'a']++;
        }
        StringBuilder sb = new StringBuilder(length);
        if (k > 1) {
            for (int i = 0; i < size; i++) {
                char c = (char) (i + 'a');
                for (int j = as[i] - 1; j >= 0; j--) {
                    sb.append(c);
                }
            }
            return sb.toString();
        }

        String min = s;
        for (int i = 1; i < length; i++) {
            String item = s.substring(i) + s.substring(0, i);
            if (min.compareTo(item) > 0) {
                min = item;
            }
        }
        return min;
    }

    private int getMin(int[] arr) {
        for (int i = 0; i < 26; i++) {
            if (arr[i] != 0) {
                return i;
            }
        }
        return 0;
    }

    private int getMax(int[] arr, int t) {
        for (int i = t + 1; i < 26; i++) {
            if (arr[i] != 0) {
                return i;
            }
        }
        return -1;
    }

}
