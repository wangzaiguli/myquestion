package com.lw.leetcode.str.c;

/**
 * Created with IntelliJ IDEA.
 * 6097. 替换字符后匹配
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/12 12:57
 */
public class MatchReplacement {

    public static void main(String[] args) {
        MatchReplacement test = new MatchReplacement();

        // true
        String s = "fool3e7bar";
        String sub = "leet";
        char[][] mapping = {{'e', '3'}, {'t', '7'}, {'t', '8'}};

        boolean b = test.matchReplacement(s, sub, mapping);
        System.out.println(b);
    }

    public boolean matchReplacement(String s, String sub, char[][] mappings) {
        int[][] arr = new int[128][128];
        for (char[] mapping : mappings) {
            arr[mapping[0]][mapping[1]] = 1;
        }
        char[] as = s.toCharArray();
        char[] bs = sub.toCharArray();
        int n = s.length();
        int m = sub.length();
        int i = 0;
        int j = 0;
        int st = 0;
        while (i < n && j < m) {
            if (arr[bs[j]][as[i]] == 1 || bs[j] == as[i]) {
                i++;
                j++;
            } else {
                i = ++st;
                j = 0;
            }
        }
        return j == m;
    }

}
