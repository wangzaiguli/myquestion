package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * str
 * 面试题 01.01. 判定字符是否唯一
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/29 16:13
 */
public class IsUnique {
    public boolean isUnique(String astr) {
        int[] hash = new int[26];
        for (char ch : astr.toCharArray()) {
            hash[ch - 'a']++;
            if (hash[ch - 'a'] > 1) {
                return false;
            }
        }
        return true;
    }
}
