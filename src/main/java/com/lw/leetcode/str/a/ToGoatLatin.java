package com.lw.leetcode.str.a;

import java.util.Arrays;
import java.util.List;

/**
 * 824. 山羊拉丁文
 *
 * @Author liw
 * @Date 2021/6/5 22:37
 * @Version 1.0
 */
public class ToGoatLatin {
    public String toGoatLatin(String sentence) {
        StringBuilder sb = new StringBuilder();
        int k = 1;
        List<Character> list = Arrays.asList('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U');
        for (String s : sentence.split(" ")) {
            char c = s.charAt(0);
            if (list.contains(c)) {
                sb.append(s);
            } else {
                sb.append(s.substring(1)).append(c);
            }

            //2.添加"ma"、"a"、空格
            sb.append("ma");
            for (int i = 0; i < k; i++) {
                sb.append("a");
            }
            k++;
            sb.append(" ");
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }
}
