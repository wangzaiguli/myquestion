package com.lw.leetcode.str.a;

/**
 * str
 * 434. 字符串中的单词数
 *
 * @Author liw
 * @Date 2021/6/28 11:45
 * @Version 1.0
 */
public class CountSegments {
    public int countSegments(String s) {
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            if ((i == 0 || s.charAt(i - 1) == ' ') && s.charAt(i) != ' ') {
                count++;
            }
        }
        return count;
    }
}
