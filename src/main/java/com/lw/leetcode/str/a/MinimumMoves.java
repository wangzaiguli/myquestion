package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 2027. 转换字符串的最少操作次数
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/27 9:40
 */
public class MinimumMoves {

    public int minimumMoves(String s) {
        int length = s.length();
        int count = 0;
        int index = 0;
        while (index < length) {
            if (s.charAt(index) == 'X') {
                count++;
                index += 2;
            }
            index++;
        }
        return count;
    }

}
