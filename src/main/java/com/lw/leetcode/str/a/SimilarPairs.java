package com.lw.leetcode.str.a;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * a
 * str
 * 2506. 统计相似字符串对的数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/19 11:12
 */
public class SimilarPairs {

    public int similarPairs(String[] words) {
        Map<Integer, Integer> map = new HashMap<>();
        for (String word : words) {
            int length = word.length();
            int key = 0;
            for (int i = 0; i < length; i++) {
                key |= 1 << (word.charAt(i) - 'a');
            }
            map.merge(key, 1, (a, b) -> a + b);
        }
        int sum = 0;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            int value = entry.getValue() - 1;
            sum += ((value * (value + 1)) >> 1);
        }
        return sum;
    }

}
