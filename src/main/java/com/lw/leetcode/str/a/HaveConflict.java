package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 6214. 判断两个事件是否存在冲突
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/24 11:21
 */
public class HaveConflict {

    public boolean haveConflict(String[] event1, String[] event2) {
        int as = find(event1[0]);
        int ae = find(event1[1]);
        int bs = find(event2[0]);
        int be = find(event2[1]);
        return ((as <= bs && ae >= bs) || (ae >= be && as <= be) || (as >= bs && ae <= be));
    }

    private int find(String str) {
        int a = str.charAt(0);
        int b = str.charAt(1);
        int c = str.charAt(3);
        int d = str.charAt(4);
        return a * 600 + b * 60 + c * 10 + d;
    }

}
