package com.lw.leetcode.str.a;

/**
 * 521. 最长特殊序列 Ⅰ
 *
 * @Author liw
 * @Date 2021/5/11 11:00
 * @Version 1.0
 */
public class FindLUSlength {
    public int findLUSlength(String a, String b) {
        if (a.equals(b)) {
            return -1;
        }
        return Math.min(a.length(), b.length());
    }
}
