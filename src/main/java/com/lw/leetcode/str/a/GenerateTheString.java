package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 1374. 生成每种字符都是奇数个的字符串
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/1 14:54
 */
public class GenerateTheString {

    public String generateTheString(int n) {
        StringBuilder sb = new StringBuilder(n);
        int st = 0;
        if ((n & 1) == 0) {
            sb.append('b');
            st = 1;
        }
        for (int i = st; i < n; i++) {
            sb.append('a');
        }
        return sb.toString();
    }

}
