package com.lw.leetcode.str.a;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 6225. 差值数组不同的字符串
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/31 10:56
 */
public class SddString {

    public static void main(String[] args) {
        SddString test = new SddString();

        // abc
//        String[] arr = {"adc", "wzy", "abc"};

        // bob
        String[] arr = {"aaa", "bob", "ccc", "ddd"};

        String s = test.oddString(arr);
        System.out.println(s);
    }

    public String oddString(String[] words) {

        int length = words.length;
        Map<String, Integer> indexMap = new HashMap<>();
        Map<String, Integer> countMap = new HashMap<>();

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            String word = words[i];
            String s = find(word, sb);
            indexMap.put(s, i);
            countMap.put(s, countMap.getOrDefault(s, 0) + 1);
        }
        for (Map.Entry<String, Integer> entry : countMap.entrySet()) {
            if (entry.getValue() == 1) {
                return words[indexMap.get(entry.getKey())];
            }
        }
        return null;

    }

    private String find(String str, StringBuilder sb) {
        int length = str.length();
        sb.setLength(0);
        for (int i = 1; i < length; i++) {
            sb.append(str.charAt(i) - str.charAt(i - 1));
            sb.append("&");
        }
        return sb.toString();
    }

}
