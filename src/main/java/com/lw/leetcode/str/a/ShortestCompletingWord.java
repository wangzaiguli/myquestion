package com.lw.leetcode.str.a;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 748. 最短补全词
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/10 10:34
 */
public class ShortestCompletingWord {

    public static void main(String[] args) {
        ShortestCompletingWord test = new ShortestCompletingWord();
        String str = "1s3 PSt";
         String[] arr =        {"step","steps","stripe","stepple"};
        String s = test.shortestCompletingWord(str, arr);
        System.out.println(s);
    }

    public String shortestCompletingWord(String licensePlate, String[] words) {
        int[] cnt = new int[26];
        int length = licensePlate.length();
        for (int i = 0; i < length; ++i) {
            char ch = licensePlate.charAt(i);
            if (ch >= 'A') {
                cnt[(ch | 0X20) - 'a']++;
            }
        }
        int[] c = new int[26];
        int idx = -1;
        length = words.length;
        for (int i = 0; i < length; ++i) {

            for (int j = 0; j < words[i].length(); ++j) {
                char ch = words[i].charAt(j);
                ++c[ch - 'a'];
            }
            boolean ok = true;
            for (int j = 0; j < 26; ++j) {
                if (c[j] < cnt[j]) {
                    ok = false;
                    break;
                }
            }
            if (ok && (idx < 0 || words[i].length() < words[idx].length())) {
                idx = i;
            }
            Arrays.fill(c, 0);
        }
        return words[idx];
    }

}
