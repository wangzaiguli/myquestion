package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * str
 * 1961. 检查字符串是否为数组前缀
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/20 22:11
 */
public class IsPrefixString {
    public boolean isPrefixString(String s, String[] words) {
        int sn = s.length();
        int l = 0;
        for (String word : words) {
            int length = word.length();
            if (l + length > sn) {
                return false;
            }

            if (s.substring(l, l + length).equals(word)) {
                l += length;
                if (l == sn) {
                    return true;
                }
            } else {
                return false;
            }
        }
        return l == sn;
    }

}
