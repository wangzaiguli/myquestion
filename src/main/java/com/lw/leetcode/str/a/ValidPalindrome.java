package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 680. 验证回文字符串 Ⅱ
 * 剑指 Offer II 019. 最多删除一个字符得到回文
 *
 * @author liw
 * @version 1.0
 * @date 2021/8/26 13:42
 */
public class ValidPalindrome {

    public static void main(String[] args) {
        ValidPalindrome test = new ValidPalindrome();
        // true
//        String str = "abca";

        // false
        String str = "abc";

        boolean b = test.validPalindrome(str);
        System.out.println(b);
    }

    public boolean validPalindrome(String s) {
        if (s == null) {
            return true;
        }
        int length = s.length();
        if (length < 3) {
            return true;
        }
        int st = 0;
        int end = length - 1;
        int flag = 0;
        int st1 = 0;
        int end1 = 0;
        while (st < end) {
            if (s.charAt(st) == s.charAt(end)) {
                st++;
                end--;
                continue;
            }
            if (flag == 2) {
                return false;
            }
            if (flag == 0) {
                st1 = st;
                end1 = end;
                st++;
            } else {
                st = st1;
                end = end1;
                end--;
            }
            flag++;
        }
        return true;
    }

}
