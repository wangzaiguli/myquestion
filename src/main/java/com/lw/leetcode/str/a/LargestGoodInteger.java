package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 2264. 字符串中最大的 3 位相同数字
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/18 10:45
 */
public class LargestGoodInteger {
    public String largestGoodInteger(String num) {
        String res = "";
        int n = num.length();
        char[] cs = num.toCharArray();
        for (int i = 0; i + 2 < n; i++) {
            if (cs[i] == cs[i + 1] && cs[i + 1] == cs[i + 2]) {
                if (num.substring(i, i + 3).compareTo(res) > 0) {
                    res = num.substring(i, i + 3);
                }
            }
        }
        return res;
    }
}
