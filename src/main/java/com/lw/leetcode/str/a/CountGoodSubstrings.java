package com.lw.leetcode.str.a;

/**
 * str
 * a
 * 1876. 长度为三且各字符不同的子字符串
 *
 * @Author liw
 * @Date 2021/11/6 22:39
 * @Version 1.0
 */
public class CountGoodSubstrings {
    public int countGoodSubstrings(String s) {
        int ans = 0;
        for (int i = 0; i < s.length() - 2; i++) {
            char a = s.charAt(i);
            char b = s.charAt(i + 1);
            char c = s.charAt(i + 2);
            if (a != b && a != c && b != c) {
                ans++;
            }
        }
        return ans;
    }
}
