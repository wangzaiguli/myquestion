package com.lw.leetcode.str.a;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * str
 * 面试题 01.02. 判定是否互为字符重排
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/29 16:03
 */
public class CheckPermutation {

    public static void main(String[] args) {
        CheckPermutation test = new CheckPermutation();
        String s1 = "ad";
        String s2 = "bc";
        System.out.println(test.CheckPermutation(s1, s2));
    }

    public boolean CheckPermutation(String s1, String s2) {
        char[] s1Chars = s1.toCharArray();
        char[] s2Chars = s2.toCharArray();
        Arrays.sort(s1Chars);
        Arrays.sort(s2Chars);
        return new String(s1Chars).equals(new String(s2Chars));
    }
}
