package com.lw.leetcode.str.a;

/**
 * 1309. 解码字母到整数映射
 *
 * @Author liw
 * @Date 2021/5/25 16:44
 * @Version 1.0
 */
public class FreqAlphabets {

    public static void main(String[] args) {
        FreqAlphabets test = new FreqAlphabets();
        String s = test.freqAlphabets("10#11#12");
        System.out.println(s);
    }

    public String freqAlphabets(String s) {
        int length = s.length();
        char[] arr = new char[length];
        int limit = length - 2;
        int index = 0;
        for (int i = 0; i < limit; i++) {
            if (s.charAt(i + 2) == '#') {
                arr[index++] = (char) ((s.charAt(i)- 48) * 10 + s.charAt(i + 1) + 48);
                i += 2;
            } else {
                arr[index++] = (char) ( s.charAt(i) + 48);
            }
        }

        if (s.charAt(length - 1) != '#') {
            if (s.charAt(length - 2) != '#') {
                arr[index++] = (char) ( s.charAt(length - 2) + 48);
            }
            arr[index++] = (char) ( s.charAt(length - 1) + 48);
        }
        return new String(arr, 0, index);
    }
}
