package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * str
 * 面试题 01.03. URL化
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/29 16:09
 */
public class ReplaceSpaces {
    public String replaceSpaces(String S, int length) {
        char[] s = S.toCharArray();
        int count = 0;
        int j = 0;

        for (int i = 0; i < length; i++) {
            if (s[i] == ' ') {
                count++;
            }
        }
        char[] arr = new char[(count << 1) + length];
        for (int i = 0; i < length; i++) {
            if (s[i] == ' ') {
                arr[j++] = '%';
                arr[j++] = '2';
                arr[j++] = '0';
            } else {
                arr[j++] = s[i];
            }
        }
        return new String(arr);
    }
}
