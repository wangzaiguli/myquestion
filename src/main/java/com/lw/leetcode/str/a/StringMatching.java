package com.lw.leetcode.str.a;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 1408. 数组中的字符串匹配
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/24 13:42
 */
public class StringMatching {

    public List<String> stringMatching(String[] words) {
        List<String> list = new ArrayList<>();
        Arrays.sort(words, (s1, s2) -> s1.length() - s2.length());
        for (int i = 0; i < words.length; i++) {
            for (int j = i + 1; j < words.length; j++) {
                if (words[j].contains(words[i])) {
                    list.add(words[i]);
                    break;
                }
            }
        }
        return list;
    }

}
