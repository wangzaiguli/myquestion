package com.lw.leetcode.str.a;

/**
 * 844. 比较含退格的字符串
 */
public class BackspaceCompare {

    public static void main(String[] args) {

        BackspaceCompare test = new BackspaceCompare();
//        boolean b = test.backspaceCompare("##a#b#c", "ad##c");
        boolean b = test.backspaceCompare("y#fo##f", "y#f#o##f");
        System.out.println(b);
    }

    public boolean backspaceCompare(String s, String t) {
        return find(s).equals(find(t));
    }

    private String find(String s) {
        int a = s.length();
        StringBuilder sb = new StringBuilder(a);
        for (int i = 0; i < a; i++) {
            char c = s.charAt(i);
            if (c == '#') {
                if (sb.length() > 0) {

                    sb.deleteCharAt(sb.length() - 1);
                }
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }
}
