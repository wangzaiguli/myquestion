package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 2000. 反转单词前缀
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/18 13:39
 */
public class ReversePrefix {

    public String reversePrefix(String word, char ch) {
        int length = word.length();
        char[] chars = word.toCharArray();
        for (int i = 0; i < length; i++) {
            if (ch == chars[i]) {
                int j = 0;
                while (j < i) {
                    ch = chars[i];
                    chars[i--] = chars[j];
                    chars[j++] = ch;
                }
                return String.valueOf(chars);
            }
        }
        return word;
    }

}
