package com.lw.leetcode.str.a;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * str
 * a
 * 1002. 查找共用字符
 *
 * @Author liw
 * @Date 2021/11/6 22:15
 * @Version 1.0
 */
public class CommonChars {
    public List<String> commonChars(String[] words) {
        int[] minfreq = new int[26];
        Arrays.fill(minfreq, Integer.MAX_VALUE);
        int[] freq = new int[26];
        for (String word : words) {
            Arrays.fill(freq, 0);
            int length = word.length();
            for (int i = 0; i < length; ++i) {
                char ch = word.charAt(i);
                ++freq[ch - 'a'];
            }
            for (int i = 0; i < 26; ++i) {
                minfreq[i] = Math.min(minfreq[i], freq[i]);
            }
        }

        List<String> ans = new ArrayList<>();
        for (int i = 0; i < 26; ++i) {
            for (int j = 0; j < minfreq[i]; ++j) {
                ans.add(String.valueOf((char) (i + 'a')));
            }
        }
        return ans;
    }
}
