package com.lw.leetcode.str.a;

/**
 * str
 * 1880. 检查某单词是否等于两单词之和
 *
 * @Author liw
 * @Date 2021/6/18 16:01
 * @Version 1.0
 */
public class IsSumEqual {

    public static void main(String[] args) {
        IsSumEqual test = new IsSumEqual();
        boolean sumEqual = test.isSumEqual("d", "b", "aaaaae");
        System.out.println(sumEqual);
    }

    public boolean isSumEqual(String firstWord, String secondWord, String targetWord) {
        int length = firstWord.length();
        int v1 = 0;
        for (int i = 0; i < length; i++) {
            v1 = v1 * 10 + firstWord.charAt(i) - 97;
        }

        length = secondWord.length();
        int v2 = 0;
        for (int i = 0; i < length; i++) {
            v2 = v2 * 10 + secondWord.charAt(i) - 97;
        }

        length = targetWord.length();
        int v3 = 0;
        for (int i = 0; i < length; i++) {
            v3 = v3 * 10 + targetWord.charAt(i) - 97;
        }
        return v1 + v2 == v3;
    }


}
