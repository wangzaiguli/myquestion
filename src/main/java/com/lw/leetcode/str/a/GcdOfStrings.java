package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 1071. 字符串的最大公因子
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/25 20:30
 */
public class GcdOfStrings {
    public String gcdOfStrings(String str1, String str2) {
        int a = str1.length();
        int b = str2.length();
        if (a == b) {
            return str1.equals(str2) ? str1 : "";
        }
        if (a > b) {
            if (str1.substring(0, b).equals(str2)) {
                return gcdOfStrings(str1.substring(b), str2);
            } else {
                return "";
            }
        } else {
            if (str2.substring(0, a).equals(str1)) {
                return gcdOfStrings(str2.substring(a), str1);
            } else {
                return "";
            }
        }
    }
}
