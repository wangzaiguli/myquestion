package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 2309. 兼具大小写的最好英文字母
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/20 14:40
 */
public class GreatestLetter {

    public static void main(String[] args) {
        GreatestLetter test = new GreatestLetter();

        // E
        String str = "lEeTcOdE";

        // R
//        String str = "arRAzFif";

        String s = test.greatestLetter(str);
        System.out.println(s);
    }

    public String greatestLetter(String s) {
        int[] arr = new int[128];
        for (int i = s.length() - 1; i >= 0; i--) {
            arr[s.charAt(i)]++;
        }
        for (int i = 122; i > 96; i--) {
            if (arr[i] > 0 && arr[i - 32] > 0) {
                return String.valueOf((char) (i - 32));
            }
        }
        return "";
    }

}
