package com.lw.leetcode.str.a;

/**
 * 58. 最后一个单词的长度
 *
 * @Author liw
 * @Date 2021/9/21 17:59
 * @Version 1.0
 */
public class LengthOfLastWord {
    public int lengthOfLastWord(String s) {
        if (s == null || "".equals(s)) {
            return 0;
        }
        char[] chars = s.toCharArray();
        int length = chars.length;
        int n = 0;
        boolean f = true;
        for (int i = length - 1; i >= 0; i--) {
            char aChar = chars[i];
            if (aChar == ' ' && !f) {
                return n;
            }
            if (aChar != ' ') {
                n++;
                f = false;
            }
        }
        return n;
    }
}
