package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 剑指 Offer II 018. 有效的回文
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/10 21:29
 */
public class IsPalindrome {
    public boolean isPalindrome(String s) {
        StringBuilder sgood = new StringBuilder();
        int length = s.length();
        for (int i = 0; i < length; i++) {
            char ch = s.charAt(i);
            if (Character.isLetterOrDigit(ch)) {
                sgood.append(Character.toLowerCase(ch));
            }
        }
        StringBuilder sb = new StringBuilder(sgood).reverse();
        return sgood.toString().equals(sb.toString());
    }

}
