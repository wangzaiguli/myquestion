package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 925. 长按键入
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/30 17:35
 */
public class IsLongPressedName {


    public static void main(String[] args) {
        IsLongPressedName test = new IsLongPressedName();

        // true
//        String a = "alex";
//        String b = "aaleex";

        // false
        String a = "alex";
        String b = "alexxr";
        boolean longPressedName = test.isLongPressedName(a, b);
        System.out.println(longPressedName);

    }

    public boolean isLongPressedName(String name, String typed) {
        int m = name.length();
        int n = typed.length();
        if (m > n) {
            return false;
        }
        char[] a = name.toCharArray();
        char[] b = typed.toCharArray();
        if (a[0] != b[0]) {
            return false;
        }
        int i = 1;
        int j = 1;
        while (i < m && j < n) {
            if (a[i] == b[j]) {
                i++;
                j++;
            } else {
                if (b[j] == b[j - 1]) {
                    j++;
                } else {
                    return false;
                }
            }
        }
        if (i < m) {
            return false;
        }
        for (int k = j; k < n; k++) {
            if (b[k] != b[k - 1]) {
                return false;
            }
        }
        return true;
    }
}
