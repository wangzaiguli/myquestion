package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * a
 * str
 * https://leetcode.cn/contest/cmbchina-2022spring/problems/fWcPGC/
 * 招商银行-01. 文本编辑程序设计
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/10 15:14
 */
public class DeleteText {

    public static void main(String[] args) {
        DeleteText test = new DeleteText();

        String str = "abwfqcd";
        int index = 3;

        String s = test.deleteText(str, index);
        System.out.println("#" + s + "#");
    }

    public String deleteText(String article, int index) {
        if (article.charAt(index) == ' ') {
            return article;
        }
        int length = article.length();
        int st = -1;
        int end = length;
        int item = index;
        while (item >= 0) {
            if (article.charAt(item) == ' ') {
                st = item;
                break;
            }
            item--;
        }
        st++;
        item = index;
        while (item < length) {
            if (article.charAt(item) == ' ') {
                end = item;
                break;
            }
            item++;
        }
        end--;
        if (st == 0 && end == length - 1) {
            return "";
        }
        if (st == 0) {
            return article.substring(end + 2);
        }
        if (end == length - 1) {
            return article.substring(0, length - end + st - 2);
        }
        return article.substring(0, st) + article.substring(end + 2);
    }

}
