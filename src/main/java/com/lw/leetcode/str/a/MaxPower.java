package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 1446. 连续字符
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/1 9:33
 */
public class MaxPower {

    public static void main(String[] args) {
        MaxPower test = new MaxPower();

        // 5
        String str = "abbcccddddeeeeedcbaaaaaa";

        int i = test.maxPower(str);
        System.out.println(i);
    }

    public int maxPower(String s) {
        int count = 0;
        int max = 0;
        char c;
        char item = ' ';
        int length = s.length();
        for (int i = 0; i < length; i++) {
            c = s.charAt(i);
            if (c == item) {
                count++;
            } else {
                max = Math.max(max, count);
                count = 1;
                item = c;
            }
        }
        return Math.max(max, count);
    }

}
