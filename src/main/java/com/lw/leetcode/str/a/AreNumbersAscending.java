package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 *2042. 检查句子中的数字是否递增
 * @author liw
 * @version 1.0
 * @date 2023/1/3 8:51
 */
public class AreNumbersAscending {

    public boolean areNumbersAscending(String s) {
        String[] ss = s.split(" ");
        int last = 0;
        for (String str : ss) {
            char c = str.charAt(0);
            if (c <= '9') {
                int i = Integer.parseInt(str);
                if (i <= last) {
                    return false;
                }
                last = i;
            }
        }
        return true;
    }

}
