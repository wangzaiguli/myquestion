package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 1684. 统计一致字符串的数目
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/17 10:33
 */
public class CountConsistentStrings {
    public int countConsistentStrings(String allowed, String[] words) {
        int[] res = new int[26];
        for (int i = 0; i < allowed.length(); i++) {
            res[allowed.charAt(i) - 97] = 1;
        }
        int result = 0;
        for (String now : words) {
            for (int i = 0; i < now.length(); i++) {
                if (res[now.charAt(i) - 97] == 0) {
                    result--;
                    break;
                }
            }
            result++;
        }
        return result;
    }

}
