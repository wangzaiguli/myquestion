package com.lw.leetcode.str.a;

/**
 * str
 * 1704. 判断字符串的两半是否相似
 *
 * @Author liw
 * @Date 2021/6/18 16:29
 * @Version 1.0
 */
public class HalvesAreAlike {
    public boolean halvesAreAlike(String s) {
        int len = s.length();
        int plen = len >> 1;
        int count = 0;
        for (int i = 0; i < plen; ++i) {
            char f = s.charAt(i);
            if (f == 'a' || f == 'e' || f == 'o' || f == 'i' || f == 'u' || f == 'A' || f == 'E' || f == 'O' || f == 'I' || f == 'U') {
                count++;
            }
        }
        for (int i = plen; i < len; ++i) {
            char f = s.charAt(i);
            if (f == 'a' || f == 'e' || f == 'o' || f == 'i' || f == 'u' || f == 'A' || f == 'E' || f == 'O' || f == 'I' || f == 'U') {
                count--;
            }
            if (count < 0) {
                return false;
            }
        }
        return count == 0;
    }
}
