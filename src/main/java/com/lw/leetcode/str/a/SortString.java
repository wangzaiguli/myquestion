package com.lw.leetcode.str.a;

/**
 * 1370. 上升下降字符串
 *
 * @Author liw
 * @Date 2021/6/18 17:25
 * @Version 1.0
 */
public class SortString {
    public String sortString(String s) {
        int length = s.length();
        StringBuilder sb = new StringBuilder(length);
        int[] arr = new int[26];
        for (char c : s.toCharArray()) {
            arr[c - 97]++;
        }
        while (length > 0) {
            for (int i = 0; i < 26; i++) {
                if (arr[i] > 0) {
                    sb.append((char) (i + 97));
                    arr[i]--;
                    length--;
                }
            }
            for (int i = 25; i >= 0; i--) {
                if (arr[i] > 0) {
                    sb.append((char) (i + 97));
                    arr[i]--;
                    length--;
                }
            }
        }
        return sb.toString();
    }
}
