package com.lw.leetcode.str.a;

import java.util.ArrayList;
import java.util.List;

/**
 * str
 * 412. Fizz Buzz
 *
 * @Author liw
 * @Date 2021/6/12 21:20
 * @Version 1.0
 */
public class FizzBuzz {
    public List<String> fizzBuzz(int n) {
        List<String> li = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            boolean a = i % 3 == 0;
            boolean b = i % 5 == 0;
            if (a && b) {
                li.add("FizzBuzz");
            } else if (a) {
                li.add("Fizz");
            } else if (b) {
                li.add("Buzz");
            } else {
                li.add(String.valueOf(i));
            }
        }
        return li;
    }
}
