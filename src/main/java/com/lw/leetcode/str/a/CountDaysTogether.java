package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * str
 * 2409. 统计共同度过的日子数
 *
 * @author liw
 * @version 1.0
 * @date 2022/9/18 21:26
 */
public class CountDaysTogether {

    private int[] mm = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public int countDaysTogether(String arriveAlice, String leaveAlice, String arriveBob, String leaveBob) {
        for (int i = 1; i < 13; i++) {
            mm[i] += mm[i - 1];
        }
        int a= getDay(arriveAlice);
        int b= getDay(leaveAlice);
        int c= getDay(arriveBob);
        int d= getDay(leaveBob);
        if (b <= d && b >= c) {
            return b - Math.max(a, c) + 1;
        }

        if (d <= b && d >= a) {
            return d - Math.max(a, c) + 1;
        }
        return 0;
    }

    private int getDay(String str) {
        int a = (str.charAt(0) - '0') * 10 + str.charAt(1) - '0';
        int b = (str.charAt(3) - '0') * 10 + str.charAt(4) - '0';
        return mm[a - 1] + b;
    }

}
