package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 383. 赎金信
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/4 16:28
 */
public class CanConstruct {
    public boolean canConstruct(String ransomNote, String magazine) {
        if (ransomNote == null || ransomNote.length() == 0) {
            return true;
        }
        if (magazine == null || magazine.length() == 0) {
            return false;
        }
        int a = ransomNote.length();
        int b = magazine.length();

        if (a > b) {
            return false;
        }
        int[] arr = new int[26];
        for (int i = 0; i < b; i++) {
            arr[magazine.charAt(i) - 97] += 1;
        }
        for (int i = 0; i < a; i++) {
            int index = ransomNote.charAt(i) - 97;
            int count = arr[index];
            if (count == 0) {
                return false;
            }
            arr[index] -= 1;
        }
        return true;
    }
}
