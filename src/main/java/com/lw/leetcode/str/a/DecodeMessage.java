package com.lw.leetcode.str.a;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 2325. 解密消息
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/4 10:57
 */
public class DecodeMessage {
    public String decodeMessage(String key, String message) {
        Map<Character, Character> map = new HashMap<>();
        for (int i = 0; i < key.length() && map.size() < 26; i++) {
            char c = key.charAt(i);
            if (map.containsKey(c) || c == ' ') {
                continue;
            }
            map.put(c, (char) ('a' + map.size()));
        }
        map.put(' ', ' ');
        StringBuilder sb = new StringBuilder(message.length());
        for (char c : message.toCharArray()) {
            sb.append(map.get(c));
        }
        return sb.toString();
    }
}
