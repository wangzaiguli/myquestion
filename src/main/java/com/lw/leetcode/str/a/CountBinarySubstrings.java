package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 696. 计数二进制子串
 *
 * @author liw
 * @version 1.0
 * @date 2021/8/26 14:43
 */
public class CountBinarySubstrings {

    public static void main(String[] args) {
        CountBinarySubstrings test = new CountBinarySubstrings();
        // 6
//        String str = "00110011";

        // 4
//        String str = "10101";

        // 4
        String str = "101110001";

        System.out.println(test.countBinarySubstrings(str));
    }

    public int countBinarySubstrings(String s) {
        int length = s.length();
        char item = s.charAt(0);
        int a = 1;
        int b = 0;
        int sum = 0;
        for (int i = 1; i < length; i++) {
            char c = s.charAt(i);
            if (c == item) {
                a++;
                continue;
            }
            item = c;
            sum += Math.min(a, b);
            b = a;
            a = 1;
        }
        sum += Math.min(a, b);
        return sum;

    }

}
