package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 面试题 01.09. 字符串轮转
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/29 15:14
 */
public class IsFlipedString {
    public boolean isFlipedString(String s1, String s2) {
        if (s1.length() != s2.length()) {
            return false;
        }
        if (s1.equals(s2)) {
            return true;
        }
        return (s1 + s1).replace(s2, "").equals(s1);
    }
}
