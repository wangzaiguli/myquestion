package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * str
 * 2108. 找出数组中的第一个回文字符串
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/19 16:05
 */
public class FirstPalindrome {

    public String firstPalindrome(String[] words) {
        int length = words.length;
        flag:
        for (int i = 0; i < length; i++) {
            String word = words[i];
            int end = word.length() - 1;
            int st = 0;
            while (st < end) {
                if (word.charAt(st++) != word.charAt(end--)) {
                    continue flag;
                }
            }
            return word;
        }
        return "";
    }

}
