package com.lw.leetcode.str.a;

import java.util.HashMap;
import java.util.Map;

/**819. 最常见的单词
 * @Author liw
 * @Date 2021/7/9 11:39
 * @Version 1.0
 */
public class MostCommonWord {
    public String mostCommonWord(String paragraph, String[] banned) {
        Map<String, Integer> banSet = new HashMap<>();
        for(String s : banned){
            banSet.put(s, 1);
        }
        HashMap<String,Integer> map = new HashMap<>();
        paragraph = paragraph.replace(",", " ").replace(".", " ").replace("!", " ").replace("?", " ").replace(";", " ").replace("'", " ");
        for(String s : paragraph.split("\\s+")){
            s = s.toLowerCase();
            if(banSet.get(s) != null) {
                continue;
            }
            if(map.containsKey(s)){
                map.put(s,map.get(s) + 1);
            }else{
                map.put(s, 1);
            }
        }
        int max = 0;
        String maxString = "";
        for(String s : map.keySet()){
            if(map.get(s) > max){
                max = map.get(s);
                maxString = s;
            }
        }
        return maxString;
    }
}
