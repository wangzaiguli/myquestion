package com.lw.leetcode.str.a;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * str
 * 2299. 强密码检验器 II
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/12 13:41
 */
public class StrongPasswordCheckerII {

    public boolean strongPasswordCheckerII(String password) {
        int len = password.length();
        if (len < 8) {
            return false;
        }
        int xx = 0, dx = 0, sz = 0, zf = 0;
        Set<Character> set = new HashSet<>();
        for (char c : "!@#$%^&*()-+".toCharArray()) {
            set.add(c);
        }
        for (int i = 0; i < len; ++i) {
            char c = password.charAt(i);
            if (i + 1 < len) {
                if (c == password.charAt(i + 1)) return false;
            }
            if (Character.isDigit(c)) {
                sz++;
            } else if (Character.isLowerCase(c)) {
                xx++;
            } else if (Character.isUpperCase(c)) {
                dx++;
            } else if (set.contains(c)) {
                zf++;
            } else {
                return false;
            }
        }
        return sz > 0 && xx > 0 && dx > 0 && zf > 0;
    }


    public boolean strongPasswordCheckerII2(String password) {
        int length = password.length();
        if (length < 8) {
            return false;
        }
        boolean au = false;
        boolean al = false;
        boolean n = false;
        boolean t = false;
        Set<Character> set = new HashSet<>();
        char item = ' ';
        for (int i = 0; i < length; i++) {
            char c = password.charAt(i);
            if (c == item) {
                return false;
            }
            set.add(c);
            item = c;
            if (c >= 'a' && c <= 'z') {
                al = true;
            } else if (c >= 'A' && c <= 'Z') {
                au = true;
            } else if (c >= '0' && c <= '9') {
                n = true;
            } else if (c == '!') {
                t = true;
            } else if (c == '@') {
                t = true;
            } else if (c == '#') {
                t = true;
            } else if (c == '$') {
                t = true;
            } else if (c == '%') {
                t = true;
            } else if (c == '^') {
                t = true;
            } else if (c == '&') {
                t = true;
            } else if (c == '*') {
                t = true;
            } else if (c == '(') {
                t = true;
            } else if (c == ')') {
                t = true;
            } else if (c == '-') {
                t = true;
            } else if (c == '+') {
                t = true;
            }
        }
        return au && al && n && t && set.size() > 7;
    }

}
