package com.lw.leetcode.str.a;

/**
 * str
 * 504. 七进制数
 *
 * @Author liw
 * @Date 2021/6/28 13:09
 * @Version 1.0
 */
public class ConvertToBase7 {
    public String convertToBase7(int num) {
        StringBuilder sb = new StringBuilder();
        if (num == 0) {
            return "0";
        }
        boolean flag = num < 0;
        num = Math.abs(num);
        while (num != 0) {
            sb.append(num % 7);
            num /= 7;
        }
        if (flag) {
            sb.append("-");
        }
        return sb.reverse().toString();
    }
}
