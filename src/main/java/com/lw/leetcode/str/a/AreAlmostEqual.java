package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 1790. 仅执行一次字符串交换能否使两个字符串相等
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/11 9:07
 */
public class AreAlmostEqual {

    public boolean areAlmostEqual(String s1, String s2) {
        int length = s1.length();
        int count = 0;
        char a = ' ';
        char b = ' ';
        char c = ' ';
        char d = ' ';
        for (int i = 0; i < length; i++) {
            if (s1.charAt(i) != s2.charAt(i)) {
                count++;
                if (count == 1) {
                    a = s1.charAt(i);
                    b = s2.charAt(i);
                } else if (count == 2) {
                    c = s1.charAt(i);
                    d = s2.charAt(i);
                } else {
                    return false;
                }
            }
        }
        if (count == 0) {
            return true;
        }
        if (count == 1) {
            return false;
        }
        return a == d && b == c;
    }

}
