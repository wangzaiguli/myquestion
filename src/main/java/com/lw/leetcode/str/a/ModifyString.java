package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 1576. 替换所有的问号
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/16 11:21
 */
public class ModifyString {
    public String modifyString(String s) {
        int n = s.length();
        char[] sb = s.toCharArray();
        for (int i = 0; i < n; i++) {
            if (s.charAt(i) == '?') {
                char a = 'a';
                while ((i > 0 && sb[i - 1] == a) || (i < n - 1 && sb[i + 1] == a)) {
                    a++;
                }
                sb[i] = a;
            }
        }
        return new String(sb);
    }
}
