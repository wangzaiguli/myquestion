package com.lw.leetcode.str.a;

import java.util.HashSet;
import java.util.Set;

/**
 * 1935. 可以输入的最大单词数
 *
 * @Author liw
 * @Date 2021/11/6 22:11
 * @Version 1.0
 */
public class CanBeTypedWords {

    public int canBeTypedWords(String text, String brokenLetters) {
        String[] words = text.split(" ");
        Set<Character> buset = new HashSet<>();
        for (int i = 0; i < brokenLetters.length(); i++)
            buset.add(brokenLetters.charAt(i));
        int res = 0;
        for (String word : words) {
            boolean ok = true;
            for (int i = 0; i < word.length(); i++) {
                if (buset.contains(word.charAt(i))) {
                    ok = false;
                    break;
                }
            }
            if (ok)
                res++;
        }
        return res;
    }


}
