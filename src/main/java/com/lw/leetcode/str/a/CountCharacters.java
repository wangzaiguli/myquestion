package com.lw.leetcode.str.a;

import java.util.Arrays;

/**
 * 1160. 拼写单词
 *
 * @Author liw
 * @Date 2021/6/11 13:13
 * @Version 1.0
 */
public class CountCharacters {
    public int countCharacters(String[] words, String chars) {
        int[] hash = new int[26];
        for (char ch : chars.toCharArray()) {
            hash[ch - 'a'] += 1;
        }
        int[] map = new int[26];
        int len = 0;
        for (String word : words) {
            Arrays.fill(map, 0);
            boolean flag = true;
            for (char ch : word.toCharArray()) {
                map[ch - 'a']++;
                if (map[ch - 'a'] > hash[ch - 'a']) {
                    flag = false;
                }
            }
            len += flag ? word.length() : 0;
        }
        return len;
    }
}
