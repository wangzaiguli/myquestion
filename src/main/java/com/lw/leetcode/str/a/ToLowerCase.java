package com.lw.leetcode.str.a;

/**
 * str
 * 709. 转换成小写字母
 *
 * @Author liw
 * @Date 2021/7/10 15:45
 * @Version 1.0
 */
public class ToLowerCase {
    public String toLowerCase(String str) {
        char[] arr = str.toCharArray();
        for (int i = str.length() - 1; i >= 0; i--) {
            char c = arr[i];
            if (c >= 'A' && c <= 'Z') {
                arr[i] = (char) (c + 32);
            }
        }
        return String.valueOf(arr);
    }
}
