package com.lw.leetcode.str.a;

/**
 * str
 * 1859. 将句子排序
 *
 * @Author liw
 * @Date 2021/6/18 16:24
 * @Version 1.0
 */
public class SortSentence {
    public String sortSentence(String s) {
        String[] strs = s.split(" ");
        String[] res = new String[strs.length];
        for (String str : strs) {
            int l = str.length() - 1;
            res[ str.charAt(l) - 49] = str.substring(0, l);
        }
        return String.join(" ", res);
    }
}
