package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * a
 * str
 * 2259. 移除指定数字得到的最大结果
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/3 22:13
 */
public class RemoveDigit {

    public static void main(String[] args) {
        RemoveDigit test = new RemoveDigit();

        // 231
//        String str = "1231";
//        char c = '1';

        // 623
//        String str = "6236";
//        char c = '6';

        // 51
//        String str = "551";
//        char c = '5';

        // 12
        String str = "123";
        char c = '3';

        String s = test.removeDigit(str, c);
        System.out.println(s);

    }

    public String removeDigit(String number, char digit) {
        int length = number.length();
        char last = number.charAt(0);
        int index = 0;
        for (int i = 1; i < length; i++) {
            char c = number.charAt(i);
            if (last == digit) {
                if (c > digit) {
                    return number.substring(0, i - 1) + number.substring(i);
                }
                index = i - 1;
            }
            last = c;
        }
        if (last == digit) {
            return number.substring(0, length - 1);
        }
        return number.substring(0, index) + number.substring(index + 1);
    }

}
