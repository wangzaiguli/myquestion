package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * str
 * 1108. IP 地址无效化
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/21 16:11
 */
public class DefangIPaddr {

    public String defangIPaddr(String address) {
        int length = address.length();
        StringBuilder sb = new StringBuilder(length + 6);
        for (int i = 0; i < length; i++) {
            if (address.charAt(i) == '.') {
                sb.append("[.]");
                continue;
            }
            sb.append(address.charAt(i));
        }
        return sb.toString();
    }

}
