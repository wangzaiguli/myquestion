package com.lw.leetcode.str.a;

import java.util.HashSet;

/**
 * Created with IntelliJ IDEA.
 * 804. 唯一摩尔斯密码词
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/18 9:37
 */
public class UniqueMorseRepresentations {

    private static String[] map = {
            ".-",
            "-...",
            "-.-.",
            "-..",
            ".",
            "..-.",
            "--.",
            "....",
            "..",
            ".---",
            "-.-",
            ".-..",
            "--",
            "-.",
            "---",
            ".--.",
            "--.-",
            ".-.",
            "...",
            "-",
            "..-",
            "...-",
            ".--",
            "-..-",
            "-.--",
            "--.."
    };

    public int uniqueMorseRepresentations(String[] words) {
        if (words == null) {
            return 0;
        }
        HashSet<String> set = new HashSet<>();
        StringBuilder sb = new StringBuilder();
        for (String s : words) {
            sb.setLength(0);
            for (char c : s.toCharArray()) {
                sb.append(map[c - 'a']);
            }
            set.add(sb.toString());
        }
        return set.size();
    }

}
