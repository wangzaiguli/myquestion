package com.lw.leetcode.str.a;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * str
 * 1805. 字符串中不同整数的数目
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/28 15:26
 */
public class NumDifferentIntegers {

    public static void main(String[] args) {
        NumDifferentIntegers test = new NumDifferentIntegers();
        String str = "035985750011523523129774573439111590559325a1554234973";
//        String str = "a123b00c34d8ef034";
        int i = test.numDifferentIntegers(str);
        System.out.println(i);
    }

    public int numDifferentIntegers(String word) {

        int length = word.length();
        StringBuilder item = new StringBuilder();
        char[] chars = word.toCharArray();

        int index;
        char c;
        for (index = 0; index < length; index++) {
             c = chars[index];
            if (c >= '0' && c <= '9') {
                item.append(c);
                break;
            }
        }
        if (item.length() == 0) {
            return 0;
        }
        Set<String> set = new HashSet<>();
        for (int i = index + 1; i < length; i++) {
            c = chars[i];
            if (c >= '0' && c <= '9') {
                if (chars[i - 1] < '0' || chars[i - 1] > '9') {
                    int l = item.length();
                    int k = 0;
                    for (; k < l; k++) {
                        if (item.charAt(k) != '0') {
                            break;
                        }
                    }
                    if (k == l) {
                        set.add("0");
                    } else {
                        set.add(item.substring(k));
                    }
                    item.setLength(0);
                }
                item.append(c);

            }
        }
        int l = item.length();
        int k = 0;
        for (; k < l; k++) {
            if (item.charAt(k) != '0') {
                break;
            }
        }
        if (k == l) {
            set.add("0");
        } else {
            set.add(item.substring(k));
        }
        return set.size();
    }

}
