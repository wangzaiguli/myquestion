package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 6070. 计算字符串的数字和
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/18 9:18
 */
public class DigitSum {

    public String digitSum(String s, int k) {
        int length = s.length();
        if (length <= k) {
            return s;
        }
        int c = (length + k - 1) / k;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < c; i++) {
            String str = find(s.substring(i * k, Math.min((i + 1) * k, length)));
            sb.append(str);
        }
        return digitSum(sb.toString(), k);
    }

    private String find(String str) {
        int sum = 0;
        for (int length = str.length() - 1; length >= 0; length--) {
            sum += str.charAt(length) - '0';
        }
        return String.valueOf(sum);
    }

}
