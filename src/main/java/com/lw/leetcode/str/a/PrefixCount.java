package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 2185. 统计包含给定前缀的字符串
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/8 11:47
 */
public class PrefixCount {

    public int prefixCount(String[] words, String pref) {
        int res = 0;
        for (String word : words) {
            if (word.startsWith(pref)) {
                res++;
            }
        }
        return res;
    }

}
