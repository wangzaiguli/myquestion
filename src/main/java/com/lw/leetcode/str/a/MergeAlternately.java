package com.lw.leetcode.str.a;

/**
 * str
 * 1768. 交替合并字符串
 *
 * @Author liw
 * @Date 2021/6/18 15:54
 * @Version 1.0
 */
public class MergeAlternately {
    public String mergeAlternately(String word1, String word2) {
        int n = word1.length();
        int m = word2.length();
        int min = Math.min(n, m);
        StringBuilder sb = new StringBuilder(n + m);
        int i;
        for (i = 0; i < min; i++) {
            sb.append(word1.charAt(i));
            sb.append(word2.charAt(i));
        }

        while (i < n) {
            sb.append(word1.charAt(i++));
        }
        while (i < m) {
            sb.append(word2.charAt(i++));
        }
        return sb.toString();
    }
}
