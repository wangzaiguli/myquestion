package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 551. 学生出勤记录 I
 *
 * @author liw
 * @version 1.0
 * @date 2021/8/17 9:39
 */
public class CheckRecord {

    public boolean checkRecord(String str) {
        int length = str.length();
        int count  = 0;
        int last = 0;
        boolean flag = false;
        char[] arr = str.toCharArray();
        for (int i = 0; i < length; i++) {
            if (arr[i] == 'L') {
                if (!flag) {
                    flag = true;
                    last = 1;
                } else {
                    last++;
                    if (last > 2) {
                        return false;
                    }
                }
                continue;
            }
            if (arr[i] == 'A') {
                count++;
                if (count > 1) {
                    return false;
                }
            }
            flag = false;
        }
        return true;
    }
}
