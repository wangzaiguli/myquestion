package com.lw.leetcode.str.a;

import java.util.HashSet;
import java.util.Set;

/**
 * create by idea
 * str
 * 929. 独特的电子邮件地址
 *
 * @author lmx
 * @version 1.0
 * @date 2022/1/22 13:57
 */
public class NumUniqueEmails {

    public int numUniqueEmails(String[] emails) {
        Set<String> seen = new HashSet<>();
        for (String email : emails) {
            int i = email.indexOf('@');
            String local = email.substring(0, i);
            String rest = email.substring(i);
            if (local.contains("+")) {
                local = local.substring(0, local.indexOf('+'));
            }
            local = local.replaceAll("\\.", "");
            seen.add(local + rest);
        }
        return seen.size();
    }


}
