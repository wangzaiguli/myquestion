package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 2129. 将标题首字母大写
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/28 10:42
 */
public class CapitalizeTitle {

    public String capitalizeTitle(String title) {
        String[] arr = title.split(" ");
        StringBuilder sb = new StringBuilder(title.length() + 1);
        for (String s : arr) {
            if (s.length() <= 2) {
                sb.append(s.toLowerCase());
            } else {
                sb.append(s.substring(0, 1).toUpperCase());
                sb.append(s.substring(1).toLowerCase());
            }
            sb.append(" ");
        }
        return sb.deleteCharAt(sb.length() - 1).toString();
    }

}
