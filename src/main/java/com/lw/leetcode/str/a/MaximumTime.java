package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 1736. 替换隐藏数字得到的最晚时间
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/24 7:09
 */
public class MaximumTime {

    public static void main(String[] args) {
        MaximumTime test = new MaximumTime();
        String str = "2?:?0";
        String s = test.maximumTime(str);
        System.out.println(s);
    }

    public String maximumTime(String time) {

        char a = time.charAt(0);
        char b = time.charAt(1);
        char c = time.charAt(3);
        char d = time.charAt(4);

        if (a == '?') {
            if (b == '?') {
                a = '2';
                b = '3';
            } else if (b < '4') {
                a = '2';
            } else {
                a = '1';
            }
        } else {
            if (b == '?') {
                if (a < '2') {
                    b = '9';
                } else {
                    b = '3';
                }
            }
        }
        if (c == '?') {
            c = '5';
        }
        if (d == '?') {
            d = '9';
        }
        return String.valueOf(new char[]{a, b, ':', c, d});
    }
}
