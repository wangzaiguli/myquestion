package com.lw.leetcode.str.a;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 *
 * str
 * 面试题 01.04. 回文排列
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/29 15:35
 */
public class CanPermutePalindrome {
    public boolean canPermutePalindrome(String s) {
        if(s == null){
            return false;
        }
        char[] chars = s.toCharArray();
        Set<Character> set = new HashSet<>();
        for(char c : chars){
            if(set.contains(c)){
                set.remove(c);
            }else{
                set.add(c);
            }
        }
        return set.size() <= 1;
    }
}
