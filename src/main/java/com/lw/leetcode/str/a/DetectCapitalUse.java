package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 520. 检测大写字母
 *
 * @author liw
 * @version 1.0
 * @date 2021/8/26 13:15
 */
public class DetectCapitalUse {

    public static void main(String[] args) {
        DetectCapitalUse test = new DetectCapitalUse();
        String a = "uSD";

        System.out.println(test.detectCapitalUse(a));
    }

    public boolean detectCapitalUse(String word) {

        int length = word.length();
        if (length < 2) {
            return true;
        }
        char c = word.charAt(1);
        if (word.charAt(0) >= 'a' && c < 'a') {
            return false;
        }
        char st = 'a';
        char end = 'z';
        if (c <= 'Z' && c >= 'A') {
            st = 'A';
            end = 'Z';
        }
        for (int i = 2; i < length; i++) {
            if (word.charAt(i) > end || word.charAt(i) < st) {
                return false;
            }
        }
        return true;
    }
}
