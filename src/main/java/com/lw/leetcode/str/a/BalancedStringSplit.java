package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 1221. 分割平衡字符串
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/7 17:31
 */
public class BalancedStringSplit {
    public int balancedStringSplit(String s) {
        if (s == null) {
            return 0;
        }
        int length = s.length();
        if (length == 0) {
            return 0;
        }
        char[] chars = s.toCharArray();

        int a = 0;
        int b = 0;
        int n = 0;
        for (int i = 0; i < length; i++) {
            if (chars[i] == 'L') {
                a++;
            } else {
                b++;
            }
            if (a == b) {
                a = 0;
                b = 0;
                n++;
            }
        }
        return n;
    }
}
