package com.lw.leetcode.str.a;

import java.util.ArrayList;
import java.util.List;

/**
 * str
 * 830. 较大分组的位置
 *
 * @Author liw
 * @Date 2021/7/9 11:45
 * @Version 1.0
 */
public class LargeGroupPositions {

    public List<List<Integer>> largeGroupPositions(String S) {
        S = S + "A";
        List<List<Integer>> result = new ArrayList<>();
        int begin = 0;
        for (int i = 1; i < S.length(); i++) {
            if (S.charAt(i) != S.charAt(i - 1)) {
                if (i - begin >= 3) {
                    List<Integer> temp = new ArrayList<>();
                    temp.add(begin);
                    temp.add(i - 1);
                    result.add(temp);
                }
                begin = i;
            }
        }
        return result;
    }

}
