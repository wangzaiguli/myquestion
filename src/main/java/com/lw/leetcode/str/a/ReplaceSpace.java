package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 *剑指 Offer 05. 替换空格
 * @author liw
 * @version 1.0
 * @date 2021/9/18 9:33
 */
public class ReplaceSpace {

    public String replaceSpace(String s) {
        int length = s.length();
        char[] array = new char[length * 3];
        int size = 0;
        for (int i = 0; i < length; i++) {
            char c = s.charAt(i);
            if (c == ' ') {
                array[size++] = '%';
                array[size++] = '2';
                array[size++] = '0';
            } else {
                array[size++] = c;
            }
        }
        return new String(array, 0, size);
    }

}
