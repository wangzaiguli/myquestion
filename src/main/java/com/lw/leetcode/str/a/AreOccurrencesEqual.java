package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 1941. 检查是否所有字符出现次数相同
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/18 9:41
 */
public class AreOccurrencesEqual {

    public boolean areOccurrencesEqual(String s) {
        int[] arr = new int[26];
        for (char c : s.toCharArray()) {
            arr[c - 'a']++;
        }
        int cnt = arr[s.charAt(0) - 'a'];
        for (int i : arr) {
            if (i != 0 && cnt != i) {
                return false;
            }
        }
        return true;
    }

}
