package com.lw.leetcode.str.a;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 1624. 两个相同字符之间的最长子字符串
 *
 * @author liw
 * @version 1.0
 * @date 2022/9/17 15:37
 */
public class MaxLengthBetweenEqualCharacters {

    public int maxLengthBetweenEqualCharacters(String s) {
        int[] arr = new int[26];
        Arrays.fill(arr, -1);
        int max = -1;
        int length = s.length();
        char[] as = s.toCharArray();
        for (int i = 0; i < length; i++) {
            int t = as[i] - 'a';

            if (arr[t] == -1) {
                arr[t] = i;
            } else {
                max = Math.max(max, i - arr[t] - 1);
            }
        }
        return max;
    }

}
