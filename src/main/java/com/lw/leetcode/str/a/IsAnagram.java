package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 剑指 Offer II 032. 有效的变位词
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/10 21:17
 */
public class IsAnagram {
    public boolean isAnagram(String s, String t) {
        int length = s.length();
        if (length != t.length() || s.equals(t)) {
            return false;
        }
        int[] a = new int[26];
        int[] b = new int[26];
        byte[] ab = s.getBytes();
        byte[] bb = t.getBytes();
        for (int i = 0; i < length; i++) {
            a[ab[i] - 97] = a[ab[i] - 97] + 1;
            b[bb[i] - 97] = b[bb[i] - 97] + 1;
        }
        for (int i = 0; i < 26; i++) {
            if (a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }
}
