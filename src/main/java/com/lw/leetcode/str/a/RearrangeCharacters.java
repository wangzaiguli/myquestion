package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 2287. 重排字符形成目标字符串
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/13 9:37
 */
public class RearrangeCharacters {

    public int rearrangeCharacters(String s, String target) {
        int[] arr1 = new int[26];
        int[] arr2 = new int[26];
        for (int i = s.length() - 1; i >= 0; i--) {
            arr1[s.charAt(i) - 'a']++;
        }
        for (int i = target.length() - 1; i >= 0; i--) {
            arr2[target.charAt(i) - 'a']++;
        }
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < 26; i++) {
            if (arr2[i] != 0) {
                min = Math.min(min, arr1[i] / arr2[i]);
            }
        }
        return min;
    }

}
