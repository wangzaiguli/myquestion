package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 1556. 千位分隔数
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/16 11:01
 */
public class ThousandSeparator {

    public static void main(String[] args) {
        ThousandSeparator test = new ThousandSeparator();
        String s = test.thousandSeparator(1);
        System.out.println(s);

    }

    public String thousandSeparator(int n) {
        if (n == 0) {
            return "0";
        }
        char[] arr = new char[20];
        int index = 19;
        int count = -1;
        while (n != 0) {
            count++;
            if (count % 3 == 0) {
                arr[index--] = '.';
            }
            arr[index--] = (char) ((n % 10) + 48);
            n = n / 10;
        }
        return String.valueOf(arr, index + 1, 18 - index);
    }
}
