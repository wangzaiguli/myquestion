package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 1957. 删除字符使字符串变好
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/20 9:18
 */
public class MakeFancyString {

    public String makeFancyString(String s) {
        StringBuilder q = new StringBuilder(s.length());
        int count = 0;
        char item = ' ';
        for (char c : s.toCharArray()) {
            if (item == c) {
                count++;
            } else {
                count = 1;
            }
            if (count < 3) {
                q.append(c);
            }
            item = c;
        }
        return q.toString();
    }


}
