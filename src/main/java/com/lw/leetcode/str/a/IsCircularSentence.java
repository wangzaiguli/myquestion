package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * str
 * 2490. 回环句
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/4 12:15
 */
public class IsCircularSentence {

    public boolean isCircularSentence(String sentence) {
        int length = sentence.length();
        char t = ' ';
        for (int i = 0; i < length; i++) {
            char c = sentence.charAt(i);
            if (c == ' ') {
                c = sentence.charAt(++i);
                if (c != t) {
                    return false;
                }
            }
            t = c;
        }
        return t == sentence.charAt(0);
    }

}
