package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 1758. 生成交替二进制字符串的最少操作数
 *
 * @author liw
 * @version 1.0
 * @date 2022/11/29 18:06
 */
public class MinOperations {

    public int minOperations(String s) {
        int length = s.length();
        int a = 0;
        int b = 0;
        char v = '0';
        for (int i = 0; i < length; i++) {
            if (s.charAt(i) == v) {
                b++;
            } else {
                a++;
            }
            v ^= 1;
        }
        return Math.min(a, b);
    }

}
