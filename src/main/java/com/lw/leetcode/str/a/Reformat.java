package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * str
 * 1417. 重新格式化字符串
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/27 17:16
 */
public class Reformat {

    public static void main(String[] args) {
        Reformat test = new Reformat();
        String str = "covid2019";
        String reformat = test.reformat(str);

        System.out.println(reformat);
    }

    public String reformat(String s) {
        int a = 0;
        int b = 0;
        char[] chars = s.toCharArray();
        for (char c : chars) {
            if (c >= '0' && c <= '9') {
                a += 1;
            } else {
                b += 1;
            }
        }
        if (b - a > 1 || a - b > 1) {
            return "";
        }
        char[] ans = new char[s.length()];
        int i = a >= b ? 0 : 1;
        int j = i == 0 ? 1 : 0;
        for (char c : chars) {
            if (c >= '0' && c <= '9') {
                ans[i] = c;
                i += 2;
            } else {
                ans[j] = c;
                j += 2;
            }
        }
        return new String(ans);
    }

}
