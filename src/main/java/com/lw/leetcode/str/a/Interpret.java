package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * 1678. 设计 Goal 解析器
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/17 10:04
 */
public class Interpret {
    public String interpret(String str) {
        int n = str.length();
        StringBuilder ans = new StringBuilder();
        for (int i = 0; i < n; ++i) {
            if (str.charAt(i) == '(' && str.charAt(i + 1) == ')') {
                ans.append("o");
                ++i;
            } else if (str.charAt(i) == 'G') {
                ans.append("G");
            } else {
                ans.append("al");
                i += 3;
            }
        }
        return ans.toString();
    }


}
