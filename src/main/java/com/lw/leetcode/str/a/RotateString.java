package com.lw.leetcode.str.a;

/**
 * 796. 旋转字符串
 */
public class RotateString {

    public boolean rotateString(String A, String B) {
        int a = A.length();
        int b = B.length();
        if (a != b) {
            return false;
        }
        if (a == 0) {
            return true;
        }
        char c = B.charAt(0);
        for (int i = 0; i < a; i++) {
            char f = A.charAt(i);
            if (f == c && B.equals(A.substring(i) + A.substring(0, i))) {
                return true;
            }
        }
        return false;
    }

}
