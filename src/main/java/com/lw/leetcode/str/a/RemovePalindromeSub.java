package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * str
 * 1332. 删除回文子序列
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/23 12:49
 */
public class RemovePalindromeSub {
    public int removePalindromeSub(String s) {
        if (s.length() == 0) {
            return 0;
        }
        int i = 0;
        int j = s.length() - 1;
        while (j > i) {
            if (s.charAt(i++) != s.charAt(j--)) {
                return 2;
            }
        }
        return 1;
    }
}
