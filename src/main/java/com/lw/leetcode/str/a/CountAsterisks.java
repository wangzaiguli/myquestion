package com.lw.leetcode.str.a;

/**
 * Created with IntelliJ IDEA.
 * str
 * 2315. 统计星号
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/26 18:57
 */
public class CountAsterisks {

    public int countAsterisks(String s) {
        int length = s.length();
        int sum = 0;
        boolean flag = true;
        for (int i = 0; i < length; i++) {
            char c = s.charAt(i);
            if (c == '|') {
                flag = !flag;
            }

            sum += (flag && c == '*' ? 1 : 0);

        }
        return sum;
    }

}
