package com.lw.leetcode.add.n;

import com.lw.leetcode.linked.ListNode;

/**
 * Created with IntelliJ IDEA.
 * a
 * link
 * https://leetcode.cn/contest/cnunionpay2022/problems/VLNEbD/
 * 银联-1. 重构链表
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/16 15:24
 */
public class ReContruct {

    public ListNode reContruct(ListNode head) {
        ListNode root = new ListNode(0);
        root.next = head;
        ListNode last = root;
        ListNode item = head;
        while (item != null) {
            if ((item.val & 1) == 0) {
                last.next = item.next;
            } else {
                last = item;
            }
            item = item.next;
        }
        return root.next;
    }

}
