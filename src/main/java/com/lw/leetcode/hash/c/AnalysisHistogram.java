package com.lw.leetcode.add.n;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * b
 * arr
 * https://leetcode.cn/contest/ccbft-2021fall/problems/9Rs2aO/
 * 建信02. 柱状图分析
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/16 13:44
 */
public class AnalysisHistogram {

    public int[] analysisHistogram(int[] heights, int cnt) {
        Arrays.sort(heights);
        int min = heights[cnt - 1] - heights[0];
        int st = 0;
        int length = heights.length;
        for (int i = cnt; i < length; i++) {
            int t = heights[i] - heights[i - cnt + 1];
            if (t < min) {
                min = t;
                st = i - cnt + 1;
            }
        }
        int[] arr = new int[cnt];
        System.arraycopy(heights, st, arr, 0, cnt);
        return arr;
    }

}
