package com.lw.leetcode.hash.b;

import java.util.HashMap;
import java.util.Map;

/**
 * create by idea
 * 面试题 16.02. 单词频率
 *
 * @author lmx
 * @version 1.0
 * @date 2022/1/3 16:41
 */
public class WordsFrequency {
    private Map<String, Integer> map;

    public WordsFrequency(String[] book) {
        map = new HashMap<>();
        for (String word : book) map.put(word, map.getOrDefault(word, 0) + 1);
    }

    public int get(String word) {
        return map.getOrDefault(word, 0);
    }

}
