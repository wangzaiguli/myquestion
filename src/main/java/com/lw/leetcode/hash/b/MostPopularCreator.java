package com.lw.leetcode.hash.b;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * 6221. 最流行的视频创作者
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/31 10:13
 */
public class MostPopularCreator {

    public List<List<String>> mostPopularCreator(String[] creators, String[] ids, int[] views) {
        int length = creators.length;
        Map<String, Integer> all = new HashMap<>();
        Map<String, Integer> max = new HashMap<>();
        Map<String, String> idMap = new HashMap<>();
        for (int i = 0; i < length; i++) {
            String c = creators[i];
            String id = ids[i];
            int view = views[i];

            all.put(c, all.getOrDefault(c, 0) + view);
            int d = max.getOrDefault(c, 0);
            if (d < view || (d == view && id.compareTo(idMap.get(c)) < 0)) {
                max.put(c, view);
                idMap.put(c, id);
            }
        }
        int m = 0;
        for (Map.Entry<String, Integer> entry : all.entrySet()) {
            m = Math.max(m, entry.getValue());
        }
        List<List<String>> list = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : all.entrySet()) {
            if (m == entry.getValue()) {
                List<String> li = Arrays.asList(entry.getKey(), idMap.get(entry.getKey()));
                list.add(li);
            }
        }
        return list;
    }

}
