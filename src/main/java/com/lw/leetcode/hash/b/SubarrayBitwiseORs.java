package com.lw.leetcode.hash.b;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * 898. 子数组按位或操作
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/1 17:27
 */
public class SubarrayBitwiseORs {

    public int subarrayBitwiseORs(int[] arr) {
        Set<Integer> all = new HashSet<>();
        Set<Integer> as = new HashSet<>();
        Set<Integer> bs = new HashSet<>();
        for (int v : arr) {
            for (int a : as) {
                bs.add(a | v);
            }
            bs.add(v);
            all.addAll(bs);
            as.clear();
            as.addAll(bs);
            bs.clear();
        }
        return all.size();
    }

}
