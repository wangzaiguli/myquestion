package com.lw.leetcode.hash.b;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 609. 在系统中查找重复文件
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/23 11:56
 */
public class FindDuplicate {

    public List<List<String>> findDuplicate(String[] paths) {
        HashMap<String, List<String>> map = new HashMap<>();
        for (String path : paths) {
            String[] values = path.split(" ");
            for (int i = 1; i < values.length; i++) {
                String[] nameCont = values[i].split("\\(");
                nameCont[1] = nameCont[1].replace(")", "");
                map.computeIfAbsent(nameCont[1], v -> new ArrayList<>()).add(values[0] + "/" + nameCont[0]);
            }
        }
        List<List<String>> res = new ArrayList<>();
        for (Map.Entry<String, List<String>> entry : map.entrySet()) {
            if (entry.getValue().size() > 1) {
                res.add(entry.getValue());
            }
        }
        return res;
    }

}
