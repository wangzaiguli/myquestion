package com.lw.leetcode.hash.b;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * hash
 * b
 * LCP 62. 交通枢纽
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/9 10:29
 */
public class TransportationHub {

    public int transportationHub(int[][] path) {
        Map<Integer, Integer> map = new HashMap<>();
        int[] arr = new int[1001];
        for (int[] ints : path) {
            arr[ints[1]]++;
            map.merge(ints[0], 1, (a, b) -> a + b);
            if (!map.containsKey(ints[1])) {
                map.put(ints[1], 0);
            }
        }
        int i = map.size() - 1;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() == 0 && arr[entry.getKey()] == i) {
                return entry.getKey();
            }
        }
        return -1;
    }

}
