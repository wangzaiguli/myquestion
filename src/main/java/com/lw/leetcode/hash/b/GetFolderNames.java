package com.lw.leetcode.hash.b;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 1487. 保证文件名唯一
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/17 21:45
 */
public class GetFolderNames {

    public static void main(String[] args) {
        GetFolderNames test = new GetFolderNames();

        // ["kaido","kaido(1)","kaido","kaido(1)"]
        String[] arr = {"kaido", "kaido(1)", "kaido", "kaido(1)"};

        String[] folderNames = test.getFolderNames(arr);
        System.out.println(Arrays.toString(folderNames));
    }

    public String[] getFolderNames(String[] names) {
        Map<String, Integer> map = new HashMap<>();
        int length = names.length;
        for (int i = 0; i < length; i++) {
            String name = names[i];

            if (!map.containsKey(name)) {
                map.put(name, 0);
                continue;
            }
            for (int j = map.get(name) + 1; ; j++) {
                String key = name + '(' + j + ')';
                if (!map.containsKey(key)) {
                    map.put(name, j);
                    map.put(key, 0);
                    names[i] = key;
                    break;
                }
            }
        }
        return names;
    }

}
