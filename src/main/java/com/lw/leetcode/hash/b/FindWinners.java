package com.lw.leetcode.hash.b;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 2225. 找出输掉零场或一场比赛的玩家
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/15 18:09
 */
public class FindWinners {

    public List<List<Integer>> findWinners(int[][] matches) {
        Map<Integer, Integer> a = new HashMap<>();
        Map<Integer, Integer> b = new HashMap<>();
        for (int[] match : matches) {
            a.put(match[0], 1);
            b.merge(match[1], 1, (i, j) -> i + j);
        }
        List<List<Integer>> all = new ArrayList<>();
        List<Integer> al = new ArrayList<>();
        List<Integer> bl = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : a.entrySet()) {
            Integer key = entry.getKey();
            if (!b.containsKey(key)) {
                al.add(key);
            }
        }
        for (Map.Entry<Integer, Integer> entry : b.entrySet()) {
            if (entry.getValue() == 1) {
                bl.add(entry.getKey());
            }
        }
        al.sort(Integer::compareTo);
        bl.sort(Integer::compareTo);
        all.add(al);
        all.add(bl);
        return all;
    }
    
}
