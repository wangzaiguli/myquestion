package com.lw.leetcode.hash.b;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 2295. 替换数组中的元素
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/7 9:14
 */
public class ArrayChange {

    public int[] arrayChange(int[] nums, int[][] operations) {

        int length = nums.length;
        Map<Integer, Integer> map = new HashMap<>(length << 1);
        for (int num : nums) {
            map.put(num, num);
        }
        Map<Integer, Integer> items = new HashMap<>(length << 1);

        for (int[] operation : operations) {
            int a = operation[0];
            int b = operation[1];
            Integer v = items.get(a);
            if (v == null) {
                map.put(a, b);
                items.put(b, a);
            } else {
                map.put(v, b);
                items.remove(a);
                items.put(b, v);
            }
        }

        for (int i = 0; i < length; i++) {
            nums[i] = map.get(nums[i]);
        }
        return nums;
    }

}
