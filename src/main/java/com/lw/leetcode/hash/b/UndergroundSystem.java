package com.lw.leetcode.hash.b;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 1396. 设计地铁系统
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/2 12:13
 */
public class UndergroundSystem {

    private Map<String, Long> map = new HashMap<>();
    private Map<Integer, String> stationMap = new HashMap<>();
    private Map<Integer, Integer> timeMap = new HashMap<>();

    public UndergroundSystem() {

    }

    public void checkIn(int id, String stationName, int t) {
        stationMap.put(id, stationName);
        timeMap.put(id, t);
    }

    public void checkOut(int id, String stationName, int t) {
        String key = stationMap.get(id) + "&" + stationName;
        long v = map.getOrDefault(key, 0L);
        map.put(key, v + (1L << 32) + t - timeMap.get(id));
        stationMap.remove(id);
        timeMap.remove(id);
    }

    public double getAverageTime(String startStation, String endStation) {
        long v = map.get(startStation + "&" + endStation);
        double c = v >> 32;
        double s = v & 0XFFFFFFFFL;
        return s / c;
    }

}
