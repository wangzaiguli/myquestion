package com.lw.leetcode.hash.b;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 面试题 17.05.  字母与数字
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/25 21:38
 */
public class FindLongestSubarray {

    public String[] findLongestSubarray(String[] array) {
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0, -1);
        int st = 0;
        int max = -1;
        int item = 0;
        int length = array.length;
        for (int i = 0; i < length; i++) {
            if (array[i] .charAt(0) < 65) {
                item++;
            } else {
                item--;
            }
            Integer c = map.get(item);
            if (c == null) {
                map.put(item, i);
            } else {
                int l = i - c;
                if (l > max) {
                    st = c;
                    max = l;
                }
            }
        }
        if (max == -1) {
            return new String[0];
        }
        return Arrays.copyOfRange(array, st + 1, st + max + 1);
    }

}
