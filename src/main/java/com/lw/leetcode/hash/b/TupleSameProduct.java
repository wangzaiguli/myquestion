package com.lw.leetcode.hash.b;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 1726. 同积元组
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/10 13:11
 */
public class TupleSameProduct {

    public static void main(String[] args) {
        TupleSameProduct test = new TupleSameProduct();

        // 8
//        int[] arr = {2,3,4,6};

        // 16
//        int[] arr = {1,2,4,5,10};

        // 40
//        int[] arr = {2,3,4,6,8,12};

        // 0
        int[] arr = {2, 3, 5, 7};

        int i = test.tupleSameProduct(arr);
        System.out.println(i);
    }

    public int tupleSameProduct(int[] nums) {
        int length = nums.length;
        Map<Integer, Integer> map = new HashMap<>();
        int sum = 0;
        for (int i = 0; i < length; i++) {
            int a = nums[i];
            for (int j = i + 1; j < length; j++) {
                int b = nums[j] * a;
                int c = map.getOrDefault(b, 0);
                sum += (c << 3);
                map.put(b, c + 1);
            }
        }
        return sum;
    }

}
