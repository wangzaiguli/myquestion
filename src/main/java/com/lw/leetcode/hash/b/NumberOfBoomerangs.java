package com.lw.leetcode.hash.b;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 447. 回旋镖的数量
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/13 11:15
 */
public class NumberOfBoomerangs {

    public int numberOfBoomerangs(int[][] points) {
        int ans = 0;
        for (int[] p : points) {
            Map<Integer, Integer> cnt = new HashMap<>();
            int a = p[0];
            int b = p[1];
            for (int[] q : points) {
                int dis = (a - q[0]) * (a - q[0]) + (b - q[1]) * (b - q[1]);
                cnt.put(dis, cnt.getOrDefault(dis, 0) + 1);
            }
            for (Map.Entry<Integer, Integer> entry : cnt.entrySet()) {
                int m = entry.getValue();
                ans += m * (m - 1);
            }
        }
        return ans;
    }

}
