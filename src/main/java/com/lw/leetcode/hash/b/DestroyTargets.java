package com.lw.leetcode.hash.b;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 6226. 摧毁一系列目标
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/31 10:26
 */
public class DestroyTargets {

    public static void main(String[] args) {
        DestroyTargets test = new DestroyTargets();

        // 1
//        int[] nums = {3, 7, 8, 1, 1, 5};
//        int space = 2;

        // 1
//        int[] nums = {1, 3, 5, 2, 4, 6};
//        int space = 2;

        // 2
        int[] nums = {6, 2, 5};
        int space = 100;

        int i = test.destroyTargets(nums, space);
        System.out.println(i);
    }

    public int destroyTargets(int[] nums, int space) {
        Map<Integer, Long> map = new HashMap<>();
        long s = 1L << 32;
        for (int num : nums) {
            int t = num % space;
            Long v = map.get(t);
            if (v == null) {
                map.put(t, s + num);
            } else {
                int l = v.intValue();
                if (num < l) {
                    map.put(t, v + s - l + num);
                } else {
                    map.put(t, v + s);
                }
            }
        }
        int v = 0;
        int c = 0;
        for (Map.Entry<Integer, Long> entry : map.entrySet()) {
            Long value = entry.getValue();
            int v1 = value.intValue();
            int c1 = (int) (value >> 32);
            if (c1 > c || (c1 == c && v1 < v)) {
                c = c1;
                v = v1;
            }
        }
        return v;
    }

}
