package com.lw.leetcode.hash.b;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.TreeSet;

/**
 * Created with IntelliJ IDEA.
 * 6126. 设计食物评分系统
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/25 10:23
 */
public class FoodRatings {


    public static void main(String[] args) {


        // [null,null,"tjokfmxg",null,"tjokfmxg","tjokfmxg",null,"cduc","cduc",null,"cduc",null,"mnij"]

        // ["changeRating","highestRated","changeRating","highestRated","highestRated","changeRating","highestRated","highestRated","changeRating","highestRated","changeRating","highestRated"]
        //[[["tjokfmxg",19],["waxlau"],["uqklk",7],["waxlau"],["waxlau"],["tjokfmxg",14],["waxlau"],["waxlau"],["tjokfmxg",4],["waxlau"],["mnij",18],["waxlau"]]

        String[] foods = {"tjokfmxg","xmiuwozpmj","uqklk","mnij","iwntdyqxi","cduc","cm","mzwfjk"};
        String[] cuisines = {"waxlau","ldpiabqb","ldpiabqb","waxlau","ldpiabqb","waxlau","waxlau","waxlau"};
        int[] arr = {9,13,7,16,10,17,16,17};

        FoodRatings test = new FoodRatings(foods, cuisines, arr);

        test.changeRating("tjokfmxg",19);
        System.out.println(  test.highestRated("waxlau"));
        test.changeRating("uqklk",7);
        System.out.println(   test.highestRated("waxlau"));
        System.out.println(   test.highestRated("waxlau"));
        test.changeRating("tjokfmxg",14);
        System.out.println(     test.highestRated("waxlau"));
        System.out.println(    test.highestRated("waxlau"));
        test.changeRating("tjokfmxg",4);
        System.out.println(    test.highestRated("waxlau"));
        test.changeRating("mnij",18);
        System.out.println(test.highestRated("waxlau")) ;

    }

    private Map<String, Node> map;
    private Map<String, TreeSet<Node>> items;

    public FoodRatings(String[] foods, String[] cuisines, int[] ratings) {
        map = new HashMap<>();
        items = new HashMap<>();
        int length = foods.length;
        for (int i = 0; i < length; i++) {
            Node node = new Node(foods[i], cuisines[i], ratings[i]);

            items.computeIfAbsent(cuisines[i], v -> new TreeSet<>((a, b) -> a.rating == b.rating ? a.food.compareTo(b.food) : Integer.compare(b.rating, a.rating)))
                    .add(node);


            map.put(foods[i], node);
        }

    }

    public void changeRating(String food, int newRating) {
        Node node = map.get(food);
        TreeSet<Node> nodes = items.get(node.cuisine);
        nodes.remove(node);
        Node node1 = new Node(food, node.cuisine, newRating);
        map.put(food, node1);
        nodes.add(node1);
    }

    public String highestRated(String cuisine) {
        return items.get(cuisine).first().food;
    }

    private static class Node {
        private String food;
        private int rating;
        private String cuisine;

        public Node(String food, String cuisine, int rating) {
            this.food = food;
            this.rating = rating;
            this.cuisine = cuisine;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            Node node = (Node) o;
            return Objects.equals(food, node.food);
        }

        @Override
        public int hashCode() {
            return food.hashCode();
        }
    }

}
