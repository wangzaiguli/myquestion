package com.lw.leetcode.hash.b;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * hash
 * 1282. 用户分组
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/15 20:53
 */
public class GroupThePeople {

    public static void main(String[] args) {
        GroupThePeople test = new GroupThePeople();

        // [[5],[0,1,2],[3,4,6]]
        int[] arr = {3, 3, 3, 3, 3, 1, 3};

        List<List<Integer>> lists = test.groupThePeople(arr);
        System.out.println(lists);
    }

    public List<List<Integer>> groupThePeople(int[] groupSizes) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        int length = groupSizes.length;
        List<List<Integer>> list = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            int n = groupSizes[i];
            List<Integer> li = map.computeIfAbsent(n, v -> new ArrayList<>(n));
            li.add(i);
            if (li.size() == n) {
                list.add(new ArrayList<>(li));
                li.clear();
            }
        }
        return list;
    }

}
