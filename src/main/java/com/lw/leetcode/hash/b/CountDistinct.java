package com.lw.leetcode.hash.b;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * hash
 * 2261. 含最多 K 个可整除元素的子数组
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/3 22:08
 */
public class CountDistinct {

    public int countDistinct(int[] nums, int k, int p) {
        Set<String> set = new HashSet<>();
        StringBuilder sb = new StringBuilder();
        int length = nums.length;
        for (int i = 0; i < length; i++) {
            int cnt = 0;
            sb.setLength(0);
            for (int j = i; j < length; j++) {
                sb.append(nums[j]).append("+");
                if (nums[j] % p == 0) {
                    cnt++;
                }
                if (cnt > k) {
                    break;
                }
                set.add(sb.toString());
            }
        }
        return set.size();
    }

}
