package com.lw.leetcode.hash.a;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 2053. 数组中第 K 个独一无二的字符串
 *
 * @Author liw
 * @Date 2021/11/6 21:44
 * @Version 1.0
 */
public class KthDistinct {
    public String kthDistinct(String[] arr, int k) {
        Map<String, Long> countMap = Arrays.stream(arr)
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
        for (String s : arr) {
            if (countMap.get(s) == 1) {
                k--;
            }
            if (k == 0 && countMap.get(s) == 1) {
                return s;
            }
        }
        return "";
    }
}
