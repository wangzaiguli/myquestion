package com.lw.leetcode.hash.a;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * hash
 * 884. 两句话中的不常见单词
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/29 14:00
 */
public class UncommonFromSentences {

    public String[] uncommonFromSentences(String s1, String s2) {
        Map<String, Integer> map = new HashMap<>();
        for (String word : s1.split(" ")) {
            map.merge(word, 1, (a, b) -> a + b);
        }
        for (String word : s2.split(" ")) {
            map.merge(word, 1, (a, b) -> a + b);
        }
        List<String> list = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getValue() == 1) {
              list.add(entry.getKey());
            }
        }
        return list.toArray(new String[list.size()]);
    }


}
