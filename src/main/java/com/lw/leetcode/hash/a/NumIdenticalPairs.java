package com.lw.leetcode.hash.a;

/**
 * Created with IntelliJ IDEA.
 * 1512. 好数对的数目
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/15 11:49
 */
public class NumIdenticalPairs {
    public int numIdenticalPairs(int[] nums) {
        int[] cnt = new int[101];
        int ans = 0;
        for (int num : nums) {
            ans += cnt[num];
            cnt[num]++;
        }
        return ans;
    }
}
