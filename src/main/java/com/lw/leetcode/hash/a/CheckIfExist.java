package com.lw.leetcode.hash.a;

import java.util.HashMap;
import java.util.Map;

/**
 * hash
 * 1346. 检查整数及其两倍数是否存在
 *
 * @Author liw
 * @Date 2021/8/3 21:56
 * @Version 1.0
 */
public class CheckIfExist {
    public boolean checkIfExist(int[] arr) {
        int length = arr.length;
        Map<Integer, Integer> map = new HashMap<>(length << 1);
        for (int i = 0; i < length; i++) {
            int value = arr[i];
            if (map.get(value << 1) != null || ((value & 1) == 0 && map.get(value >> 1) != null)) {
                return true;
            }
            map.put(value, 1);
        }
        return false;
    }
}
