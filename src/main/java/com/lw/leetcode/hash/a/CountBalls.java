package com.lw.leetcode.hash.a;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 1742. 盒子中小球的最大数量
 *
 * @author liw
 * @version 1.0
 * @date 2022/11/23 9:11
 */
public class CountBalls {

    public int countBalls(int lowLimit, int highLimit) {
        Map<Integer, Integer> map = new HashMap<>();
        int max = 0;
        for (int i = lowLimit; i <= highLimit; i++) {
            int sum = 0;
            int item = i;
            while (item != 0) {
                sum += item % 10;
                item /= 10;
            }
            map.merge(sum, 1, (a, b) -> a + b);
            max = Math.max(max, map.get(sum));
        }
        return max;
    }


}
