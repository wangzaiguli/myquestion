package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 419. 甲板上的战舰
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/15 10:53
 */
public class CountBattleships {

    public int countBattleships(char[][] board) {
        if (board == null || board.length == 0 || board[0].length == 0) {
            return 0;
        }
        int a = board.length;
        int b = board[0].length;
        int count = 0;
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                if (board[i][j] == 'X' && (i == a - 1 || board[i + 1][j] == '.') && (j == b - 1 || board[i][j + 1] == '.')) {
                    count++;
                }
            }
        }
        return count;
    }

}
