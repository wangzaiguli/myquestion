package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 885. 螺旋矩阵 III
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/11 10:20
 */
public class SpiralMatrixIII {

    public static void main(String[] args) {
        SpiralMatrixIII test = new SpiralMatrixIII();

        // [[1,4],[1,5],[2,5],[2,4],[2,3],[1,3],[0,3],
        // [0,4],[0,5],[3,5],[3,4],[3,3],[3,2],[2,2],
        // [1,2],[0,2],[4,5],[4,4],[4,3],[4,2],[4,1],
        // [3,1],[2,1],[1,1],[0,1],[4,0],[3,0],[2,0],[1,0],[0,0]]

        // [1,4],[1,5],[2,5],[2,4],[2,3],[1,3],[0,3],
        // [0,4],[0,5],[3,5],[3,4],[3,3],[3,2],[2,2],
        // [1,2],[0,2],[4,5],[4,4],[4,3],[4,2],[4,1],
        // [3,1],[2,1],[1,1],[0,1],[4,0],[3,0],[2,0],[1,0],[0,0],
        int a = 5;
        int b = 6;
        int c = 1;
        int d = 4;
        int[][] arr = test.spiralMatrixIII(a, b, c, d);
        for (int[] anInt : arr) {
            System.out.print("["+anInt[0]+","+anInt[1]+"],");
        }
    }




    public int[][] spiralMatrixIII(int rows, int cols, int rStart, int cStart) {
        int sum = rows * cols;
        int[][] arr = new int[sum][2];
        int x = rStart;
        int y = cStart;
        int i = 0;
        arr[i++] = new int[]{x, y};
        y++;
        if (y < cols ) {
            arr[i++] = new int[]{x, y};
        }
        x++;
        if (x < rows && y < cols) {
            arr[i++] = new int[]{x, y};
        }
        y--;
        if (x < rows) {
            arr[i++] = new int[]{x, y};
        }
        y--;
        int step = 3;
        while (i < sum ) {
            if (y >= 0 && y < cols) {
                for (int j = 0; j < step; j++) {
                    if (x >= 0 && x < rows) {
                        arr[i++] = new int[]{x, y};
                    }
                    x--;
                }
            } else {
                x -= step;
            }
            x++;
            y++;
            if (x >= 0 && x < rows) {
                for (int j = 0; j < step; j++) {
                    if (y >= 0 && y < cols) {
                        arr[i++] = new int[]{x, y};
                    }
                    y++;
                }
            }else {
                y += step;
            }
            y--;
            x++;
            if (y >= 0 && y < cols) {
                for (int j = 0; j < step; j++) {
                    if (x >= 0 && x < rows) {
                        arr[i++] = new int[]{x, y};
                    }
                    x++;
                }
            }else {
                x += step;
            }
            x--;
            y--;
            if (x >= 0 && x < rows) {
                for (int j = 0; j < step; j++) {
                    if (y >= 0 && y < cols) {
                        arr[i++] = new int[]{x, y};
                    }
                    y--;
                }
            }else {
                y -= step;
            }
            step += 2;
        }
        return arr;
    }




}
