package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * arr
 * b
 * 1465. 切割后面积最大的蛋糕
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/9 14:29
 */
public class MaxArea {

    public int maxArea(int h, int w, int[] horizontalCuts, int[] verticalCuts) {
        Arrays.sort(horizontalCuts);
        Arrays.sort(verticalCuts);
        int horizonMax = horizontalCuts[0];
        int verticalMax = verticalCuts[0];

        for (int i = 1; i < horizontalCuts.length; i++) {
            horizonMax = Math.max(horizonMax, horizontalCuts[i] - horizontalCuts[i - 1]);
        }
        horizonMax = Math.max(horizonMax, h - horizontalCuts[horizontalCuts.length - 1]);

        for (int i = 1; i < verticalCuts.length; i++) {
            verticalMax = Math.max(verticalMax, verticalCuts[i] - verticalCuts[i - 1]);
        }
        verticalMax = Math.max(verticalMax, w - verticalCuts[verticalCuts.length - 1]);
        return (int) (((long)horizonMax * verticalMax) % 1000000007);
    }


}
