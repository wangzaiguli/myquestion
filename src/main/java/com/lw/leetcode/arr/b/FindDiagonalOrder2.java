package com.lw.leetcode.arr.b;

import java.util.ArrayList;
import java.util.List;

/**
 * arr
 * b
 * 1424. 对角线遍历 II
 *
 *
 * @Author liw
 * @Date 2021/5/21 17:15
 * @Version 1.0
 */
public class FindDiagonalOrder2 {

    public int[] findDiagonalOrder(List<List<Integer>> nums) {
        List<int[]> list = new ArrayList<>();
        int length = nums.size();
        for (int i = 0; i < length; i++) {
            List<Integer> li = nums.get(i);
            int size = li.size();
            for (int j = 0; j < size; j++) {
                int[] arr = {i + j, j, li.get(j)};
                list.add(arr);
            }
        }
        list.sort((a, b) -> a[0] == b[0] ? Integer.compare(a[1], b[1]) : Integer.compare(a[0], b[0]));
        int[] arr = new int[list.size()];
        int i = 0;
        for (int[] ints : list) {
            arr[i++] = ints[2];
        }
        return arr;
    }

}
