package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 1144. 递减元素使数组呈锯齿状
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/31 11:21
 */
public class MovesToMakeZigzag {

    public int movesToMakeZigzag(int[] nums) {
        int length = nums.length - 1;
        if(length < 2) {
            return 0;
        }
        int a = 0;
        int b = 0;
        for (int i = 1; i < length; i++) {
            int l = Math.max(nums[i] - nums[i - 1] + 1, 0);
            int r = Math.max(nums[i] - nums[i + 1] + 1, 0);
            if ((i & 1) == 0) {
                b += Math.max(l, r);
            } else {
                a += Math.max(l, r);
            }
        }
        b += Math.max(nums[0] - nums[1] + 1, 0);
        if (length % 2 == 0) {
            b += Math.max(nums[length] - nums[length - 1] + 1, 0);
        } else {
            a += Math.max(nums[length] - nums[length - 1] + 1, 0);
        }
        return Math.min(a, b);
    }

}
