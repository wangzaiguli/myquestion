package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 2342. 数位和相等数对的最大和
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/18 10:56
 */
public class MaximumSum {

    public static void main(String[] args) {
        MaximumSum test = new MaximumSum();

        // 54
//        int[] arr = {18, 43, 36, 13, 7};

        // -1
        int[] arr = {10, 12, 19, 14};

        int i = test.maximumSum(arr);
        System.out.println(i);
    }

    public int maximumSum(int[] nums) {
        int l = 82;
        int[] as = new int[l];
        int[] bs = new int[l];
        for (int num : nums) {
            int c = 0;
            int t = num;
            while (t != 0) {
                c += t % 10;
                t /= 10;
            }
            if (num > as[c]) {
                bs[c] = as[c];
                as[c] = num;
            } else if (num > bs[c]) {
                bs[c] = num;
            }
        }
        int max = -1;
        for (int i = 1; i < l; i++) {
            if (bs[i] > 0) {
                max = Math.max(max, as[i] + bs[i]);
            }
        }
        return max;
    }

}
