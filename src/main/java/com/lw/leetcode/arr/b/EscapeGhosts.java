package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 789. 逃脱阻碍者
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/23 13:48
 */
public class EscapeGhosts {

    public boolean escapeGhosts2(int[][] ghosts, int[] target) {
        int[] source = new int[]{0, 0};
        for (int[] ghost: ghosts) {
            if (taxi(ghost, target) <= taxi(source, target)) {
                return false;
            }
        }
        return true;
    }

    public int taxi(int[] P, int[] Q) {
        return Math.abs(P[0] - Q[0]) + Math.abs(P[1] - Q[1]);
    }


    public boolean escapeGhosts(int[][] ghosts, int[] target) {
        int a = target[0];
        int b = target[1];
        int s = Math.abs(0 - a) + Math.abs(0 - b);
        for (int[] ghost : ghosts) {
            if (Math.abs(ghost[0] - a) + Math.abs(ghost[1] - b) <= s) {
                return false;
            }
        }
        return true;
    }

}
