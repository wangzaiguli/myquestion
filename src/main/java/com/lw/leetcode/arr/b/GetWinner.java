package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 1535. 找出数组游戏的赢家
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/20 16:32
 */
public class GetWinner {

    public static void main(String[] args) {
        GetWinner test = new GetWinner();

        // 5
//        int[] arr = {2,1,3,5,4,6,7};
//        int k = 2;

        // 3
//        int[] arr = {3,2,1};
//        int k = 10;

        // 9
//        int[] arr = {1,9,8,2,3,7,6,4,5};
//        int k = 7;

        // 99
        int[] arr = {1, 11, 22, 33, 44, 55, 66, 77, 88, 99};
        int k = 1000000000;

        int winner = test.getWinner(arr, k);
        System.out.println(winner);
    }

    public int getWinner(int[] arr, int k) {
        if(k == 1) {
            return Math.max(arr[0], arr[1]);
        }
        int length = arr.length;
        int count = k;
        int item = arr[0];
        for (int i = 1; i < length; i++) {
            if (arr[i] < item) {
                count--;
                if (count == 0) {
                    return item;
                }
            } else {
                item = arr[i];
                count = k - 1;
            }
        }
        return item;
    }

}
