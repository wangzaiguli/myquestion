package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2567. 修改两个元素的最小分数
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/19 12:05
 */
public class MinimizeSum {

    public int minimizeSum(int[] nums) {
        int end = nums.length - 1;
        if (end == 2) {
            return 0;
        }
        Arrays.sort(nums);
        int max = nums[end] - nums[0];
        max = Math.min(max, nums[end] - nums[2]);
        max = Math.min(max, nums[end - 1] - nums[1]);
        return Math.min(max, nums[end - 2] - nums[0]);
    }

}
