package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 1497. 检查数组对是否可以被 k 整除
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/10 13:49
 */
public class CanArrange {

    public static void main(String[] args) {
        CanArrange test = new CanArrange();

        // true
        int[] arr = {-10, 10};
        int k = 2;

        // true
//        int[] arr = {-1,1,-2,2,-3,3,-4,4};
//        int k = 3;

        // false
//        int[] arr = {1,2,3,4,5,6};
//        int k = 10;

        // false
//        int[] arr = {-1,-1,-1,-1,2,2,-2,-2};
//        int k = 3;

        boolean b = test.canArrange(arr, k);
        System.out.println(b);
    }

    public boolean canArrange(int[] arr, int k) {
        int[] ints = new int[k];
        for (int i : arr) {
            int v = i % k;
            if (v < 0) {
                ints[v + k]++;
            } else {
                ints[v]++;
            }
        }
        if ((ints[0] & 1) == 1) {
            return false;
        }
        int l = (k - 1) >> 1;
        for (int i = 1; i <= l; i++) {
            if (ints[i] != ints[k - i]) {
                return false;
            }
        }
        if ((k & 1) == 0 && (ints[k >> 1] & 1) == 1) {
            return false;
        }
        return true;
    }
}
