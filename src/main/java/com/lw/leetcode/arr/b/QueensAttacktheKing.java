package com.lw.leetcode.arr.b;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 1222. 可以攻击国王的皇后
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/19 15:29
 */
public class QueensAttacktheKing {
    public List<List<Integer>> queensAttacktheKing(int[][] queens, int[] king) {
        char[][] arr = new char[8][8];
        for (int[] q : queens) {
            arr[q[0]][q[1]] = 'Q';
        }
        int[][] dir = new int[][]{{-1, 0}, {-1, 1}, {0, 1}, {1, 1}, {1, 0}, {1, -1}, {0, -1}, {-1, -1}};
        List<List<Integer>> ans = new ArrayList<>();
        for (int[] path : dir) {
            dfs(ans, arr, king[0], king[1], path[0], path[1]);
        }
        return ans;
    }

    private static boolean dfs(List<List<Integer>> ans, char[][] arr, int x, int y, int increX, int increY) {
        if (x == 8 || x < 0 || y == 8 || y < 0) {
            return false;
        }
        if (arr[x][y] == 'Q') {
            ans.add(Arrays.asList(x, y));
            return true;
        }
        return dfs(ans, arr, x + increX, y + increY, increX, increY);
    }

}
