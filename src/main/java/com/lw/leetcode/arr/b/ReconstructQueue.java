package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * 406. 根据身高重建队列
 *
 * @Author liw
 * @Date 2021/5/6 11:13
 * @Version 1.0
 */
public class ReconstructQueue {

    public static void main(String[] args) {
        ReconstructQueue test = new ReconstructQueue();

//        int[][] people = {{7,0},{4,4},{7,1},{5,0},{6,1},{5,2}};
        // [[2,4],[3,4],[9,0],[0,6],[7,1],[6,0],[7,3],[2,5],[1,1],[8,0]]
        // [[6,0],[1,1],[8,0],[7,1],[9,0],[2,4],[0,6],[2,5],[3,4],[7,3]]
        int[][] people = {{2,4},{3,4},{9,0},{0,6},{7,1},{6,0}
                ,{7,3},{2,5},{1,1},{8,0}};

        int[][] ints = test.reconstructQueue(people);
        print(ints);
    }

    public int[][] reconstructQueue(int[][] people) {
        int length = people.length;
        if (length == 1) {
            return people;
        }
        Arrays.sort(people, (b, a) -> b[0] == a[0] ? a[1] - b[1] : b[0] - a[0]);
        int[][] arr = new int[length][2];
        for (int i = 0; i < length; i++) {
            arr[i][0] = -1;
        }
        for (int[] p : people) {
            int n = -1;
            for (int i = 0; i < length; i++) {
                int[] ints = arr[i];
                if (ints[0] == -1) {
                    n++;
                }
                if (n == p[1]) {
                    arr[i] = p;
                    break;
                }
            }
        }
        return arr;
    }

    private static void print (int[][] arr) {

        for (int[] ints : arr) {
            System.out.print(Arrays.toString(ints));

        }
        System.out.println();

    }
}
