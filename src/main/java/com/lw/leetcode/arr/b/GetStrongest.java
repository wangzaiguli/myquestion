package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 1471. 数组中的 k 个最强值
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/6 9:24
 */
public class GetStrongest {

    public int[] getStrongest(int[] arr, int k) {
        Arrays.sort(arr);
        int m = arr[(arr.length - 1) >> 1];
        int[] values = new int[k];
        int st = 0;
        int end = arr.length - 1;
        int i = 0;
        while (i < k) {
            if (m - arr[st] > arr[end] - m) {
                values[i] = arr[st++];
            } else {
                values[i] = arr[end--];
            }
            i++;
        }
        return values;
    }

}
