package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 2017. 网格游戏
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/24 20:20
 */
public class GridGame {

    public static void main(String[] args) {
        GridGame test = new GridGame();
        int[][] arr = {{2, 5, 4}, {1, 5, 1}};

        long l = test.gridGame(arr);

        System.out.println(l);
    }

    public long gridGame(int[][] grid) {
        long sum1 = 0L;
        long sum2 = 0L;
        int length = grid[0].length;
        for (int i = length - 1; i > 0; i--) {
            sum2 += grid[0][i];
        }
        long min = sum2;
        for (int i = 1; i < length; i++) {
            sum1 += grid[1][i - 1];
            sum2 -= grid[0][i];
            min = Math.min(min, Math.max(sum1, sum2));
        }
        return min;
    }

}
