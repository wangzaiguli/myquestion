package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 1583. 统计不开心的朋友
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/31 21:51
 */
public class UnhappyFriends {


    public static void main(String[] args) {
        UnhappyFriends test = new UnhappyFriends();

        // 5
        int n = 6;
        int[][] preferences = {{1, 4, 3, 2, 5}, {0, 5, 4, 3, 2}, {3, 0, 1, 5, 4}, {2, 1, 4, 0, 5}, {2, 1, 0, 3, 5}, {3, 4, 2, 0, 1}};
        int[][] pairs = {{3, 1}, {2, 0}, {5, 4}};

        int i = test.unhappyFriends(n, preferences, pairs);
        System.out.println(i);

    }

    public int unhappyFriends(int n, int[][] preferences, int[][] pairs) {
        int[][] arr = new int[n][n];
        for (int i = 0; i < n; i++) {
            int[] ints = preferences[i];
            for (int j = 0; j < n - 1; j++) {
                arr[i][ints[j]] = j;
            }
        }
        int l = pairs.length;
        int[] counts = new int[n];
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < l; j++) {
                if (i == j) {
                    continue;
                }
                int[] as = pairs[i];
                int[] bs = pairs[j];
                int a0 = as[0];
                int a1 = as[1];
                int b0 = bs[0];
                int b1 = bs[1];
                if (counts[a0] == 0) {
                    if ((arr[a0][a1] > arr[a0][b0] && arr[b0][b1] > arr[b0][a0]) ||
                            (arr[a0][a1] > arr[a0][b1] && arr[b1][b0] > arr[b1][a0])) {
//                        System.out.println(i + "  a " + j + "  " + a0);
                        counts[a0] = 1;
                    }
                }
                if (counts[a1] == 0) {
                    if ((arr[a1][a0] > arr[a1][b0] && arr[b0][b1] > arr[b0][a1])
                            || (arr[a1][a0] > arr[a1][b1] && arr[b1][b0] > arr[b1][a1])) {
//                        System.out.println(i + "  b " + j + "  " + a1);
                        counts[a1] = 1;
                    }
                }
            }
        }

        int count = 0;
        for (int i : counts) {
            count += i;
        }
        return count;
    }

}
