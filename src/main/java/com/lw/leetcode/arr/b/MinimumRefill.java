package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 2105. 给植物浇水 II
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/24 9:46
 */
public class MinimumRefill {

    public int minimumRefill(int[] plants, int capacityA, int capacityB) {
        int l = plants.length - 1;
        int length = plants.length >> 1;
        int count = 0;
        int a = capacityA;
        int b = capacityB;
        for (int i = 0; i < length; i++) {
            a -= plants[i];
            if (a < 0) {
                count++;
                a = capacityA - plants[i];
            }
            b -= plants[l - i];
            if (b < 0) {
                count++;
                b = capacityB - plants[l - i];
            }
        }
        if ((l & 1) == 0 && Math.max(a, b) < plants[length]) {
            count++;
        }
        return count;
    }

}
