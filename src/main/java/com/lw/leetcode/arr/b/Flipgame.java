package com.lw.leetcode.arr.b;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 822. 翻转卡片游戏
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/21 11:35
 */
public class Flipgame {
    public int flipgame(int[] fronts, int[] backs) {
        Map<Integer, Integer> map = new HashMap<>();
        int length = fronts.length;
        for (int i = 0; i < length; i++) {
            if (fronts[i] == backs[i]) {
                map.put(fronts[i], 1);
            }
        }
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < length; i++) {
            int front = fronts[i];
            int back = backs[i];
            if (front == back) {
                continue;
            }
            if (front < min && map.get(front) == null) {
                min = front;
            }
            if (back < min && map.get(back) == null) {
                min = back;
            }
        }
        return min == Integer.MAX_VALUE ? 0 : min;
    }
}
