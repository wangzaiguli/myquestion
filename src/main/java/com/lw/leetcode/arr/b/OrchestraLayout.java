package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * LCP 29. 乐团站位
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/30 11:16
 */
public class OrchestraLayout {

    public static void main(String[] args) {
        OrchestraLayout test = new OrchestraLayout();

        // 5
//        int a = 5;
//        int b = 3;
//        int c = 1;


//        int a = 4;
//        int b = 0;
//        int c = 0;

        // 1
//        int a = 449572;
//        int b = 209397;
//        int c = 306801;

        // 1
        int a = 30;
        int b = 4;
        int c = 20;

        int i = test.orchestraLayout(a, b, c);
        System.out.println(i);
    }

    public int orchestraLayout(long n, long xPos, long yPos) {
        long k = Math.min(Math.min(xPos, n - 1 - xPos), Math.min(yPos, n - 1 - yPos));
        long sum = (n * k - k * k) << 2;
        long l = n - (k << 1) - 1;
        if (xPos == k) {
            sum += yPos - k + 1;
        } else if (n - 1 - yPos == k) {
            sum += l;
            sum += xPos - k + 1;
        } else if (n - 1 - xPos == k) {
            sum += l << 1;
            sum += n - k - yPos;
        } else {
            sum += l * 3;
            sum += n - k - xPos;
        }
        sum %= 9;
        return sum == 0 ? 9 : (int) sum;
    }

}
