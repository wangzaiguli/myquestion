package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 1828. 统计一个圆中点的数目
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/24 17:12
 */
public class CountPoints {

    public int[] countPoints(int[][] points, int[][] queries) {
        int length = queries.length;
        int[] arr = new int[length];
        int tmp = 0;
        for (int i = 0; i < length; i++) {
            int[] query = queries[i];
            for (int[] point : points) {
                if ((query[0] - point[0]) * (query[0] - point[0]) + (query[1] - point[1]) * (query[1] - point[1]) <= query[2] * query[2]) {
                    tmp++;
                }
            }
            arr[i] = tmp;
            tmp = 0;
        }
        return arr;
    }

}
