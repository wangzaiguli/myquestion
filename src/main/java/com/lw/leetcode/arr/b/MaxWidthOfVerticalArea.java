package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1637. 两点之间不包含任何点的最宽垂直面积
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/19 22:29
 */
public class MaxWidthOfVerticalArea {

    public int maxWidthOfVerticalArea(int[][] points) {
        int length = points.length;
        int[] arr = new int[length];
        for (int i = 0; i < length; i++) {
            arr[i] = points[i][0];
        }
        Arrays.sort(arr);
        int max = 0;
        for (int i = 0; i < length - 1; i++) {
            max = Math.max(max, arr[i + 1] - arr[i]);
        }
        return max;
    }

}
