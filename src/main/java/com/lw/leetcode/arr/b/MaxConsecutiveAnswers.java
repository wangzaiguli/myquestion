package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/19 10:18
 */
public class MaxConsecutiveAnswers {
    
    public static void main(String[] args) {
        MaxConsecutiveAnswers test = new MaxConsecutiveAnswers();

        // 5
//        String str = "TTFTTFTT";
//        int k = 1;

        // 3
//        String str = "TFFT";
//        int k = 1;

        // 4
        String str = "TTFF";
        int k = 2;

        int i = test.maxConsecutiveAnswers(str, k);
        System.out.println(i);


    }

    public int maxConsecutiveAnswers(String answerKey, int k) {
        char[] chars = answerKey.toCharArray();
        int length = answerKey.length();
        int max = 0;
        int a = 0;
        int b = 0;
        int st = 0;
        int end = 0;
        while (st < length) {
            if (Math.min(a, b) > k ) {
                if (chars[end++] == 'T') {
                    a--;
                } else {
                    b--;
                }
            } else {
                if (chars[st++] == 'T') {
                    a++;
                } else {
                    b++;
                }
                max = Math.max(max, st - end - 1);
            }
        }
        while (end + max < st) {
            if (Math.min(a, b) > k) {
                if (chars[end++] == 'T') {
                    a--;
                } else {
                    b--;
                }
            } else {
                max = Math.max(max, st - end);
                break;
            }
        }
        return max;
    }

}
