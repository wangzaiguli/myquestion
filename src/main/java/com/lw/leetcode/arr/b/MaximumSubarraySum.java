package com.lw.leetcode.arr.b;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 6230. 长度为 K 子数组中的最大和
 *
 * @author liw
 * @version 1.0
 * @date 2022/11/7 9:36
 */
public class MaximumSubarraySum {

    public static void main(String[] args) {
        MaximumSubarraySum test = new MaximumSubarraySum();

        // 15
//        int[] nums = {1, 5, 4, 2, 9, 9, 9};
//        int k = 3;

        // 0
        int[] nums = {4, 4, 4};
        int k = 3;

        long sum = test.maximumSubarraySum(nums, k);
        System.out.println(sum);
    }

    public long maximumSubarraySum(int[] nums, int k) {
        long sum = 0;
        int length = nums.length;
        long max = 0;
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < k; i++) {
            sum += nums[i];
            map.merge(nums[i], 1, (a, b) -> a + b);
        }
        if (map.size() == k) {
            max = sum;
        }
        for (int i = k; i < length; i++) {
            sum += nums[i];
            sum -= nums[i - k];
            map.merge(nums[i], 1, (a, b) -> a + b);
            int c = map.get(nums[i - k]);
            if (c == 1) {
                map.remove(nums[i - k]);
            } else {
                map.put(nums[i - k], c - 1);
            }
            if (map.size() == k) {
                max = Math.max(max, sum);
            }
        }
        return max;
    }

}
