package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 1744. 你能在你最喜欢的那天吃到你最喜欢的糖果吗？
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/21 17:24
 */
public class CanEat {
    public boolean[] canEat(int[] candiesCount, int[][] queries) {
        int n = candiesCount.length;
        long[] sum = new long[n];
        sum[0] = candiesCount[0];
        for (int i = 1; i < n; ++i) {
            sum[i] = sum[i - 1] + candiesCount[i];
        }

        int l = queries.length;
        boolean[] arr = new boolean[l];
        for (int i = 0; i < l; ++i) {
            int[] query = queries[i];
            int favoriteType = query[0];
            long x1 = query[1] + 1L;
            long y1 = x1 * query[2];
            long x2 = favoriteType == 0 ? 1 : sum[favoriteType - 1] + 1;
            long y2 = sum[favoriteType];
            arr[i] = !(x1 > y2 || y1 < x2);
        }
        return arr;
    }

}
