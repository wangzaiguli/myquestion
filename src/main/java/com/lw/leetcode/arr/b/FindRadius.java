package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 475. 供暖器
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/8 14:17
 */
public class FindRadius {

    public static void main(String[] args) {
        FindRadius test = new FindRadius();
        // [1,2,3]
        //[2]
        int[] arr1 = {1, 2, 300};
        int[] arr2 = {2};
        int radius = test.findRadius(arr1, arr2);
        System.out.println(radius);
    }

    public int findRadius(int[] houses, int[] heaters) {
        Arrays.sort(houses);
        Arrays.sort(heaters);
        int i = 0;
        int j = 0;
        long item = Integer.MIN_VALUE;
        int a = houses.length;
        int b = heaters.length;
        long max = Long.MIN_VALUE;
        while (i < a && j < b) {
            if (houses[i] >= heaters[j]) {
                item = heaters[j];
                j++;
                continue;
            }
            long m = houses[i] - item;
            long n = heaters[j] - houses[i];
            max = Math.max(max, Math.min(m, n));
            i++;
        }
        if (i != a) {
            max = Math.max(max, Math.abs(houses[a - 1] - heaters[b - 1]));
        }
        return (int) max;
    }

}
