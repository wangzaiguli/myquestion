package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 1620. 网络信号最好的坐标
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/19 16:27
 */
public class BestCoordinate {

    public int[] bestCoordinate(int[][] towers, int radius) {
        int[] ans = new int[2];
        int max = 0;
        int radius2 = radius * radius;
        int ax = Integer.MAX_VALUE;
        int ay = Integer.MAX_VALUE;
        int bx = 0;
        int by = 0;
        for (int[] tower : towers) {
            ax = Math.min(tower[0], ax);
            bx = Math.max(tower[0], bx);
            ay = Math.min(tower[1], ay);
            by = Math.max(tower[1], by);
        }
        for (int i = ax; i <= bx; i++) {
            for (int j = ay; j <= by; j++) {
                int signal = 0;
                for (int[] tower : towers) {
                    int distance2 = (tower[0] - i) * (tower[0] - i) + (tower[1] - j) * (tower[1] - j);
                    if (distance2 <= radius2) {
                        signal += Math.floor(tower[2] / (1 + Math.sqrt(distance2)));
                    }
                }
                if (signal > max) {
                    max = signal;
                    ans[0] = i;
                    ans[1] = j;
                }
            }
        }
        return ans;
    }

}
