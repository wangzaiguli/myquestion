package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1578. 避免重复字母的最小删除成本
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/3 17:43
 */
public class MinCost {
    public int minCost(String s, int[] cost) {
        int length = s.length();
        int all = 0;
        int sum = cost[0];
        int max = sum;
        int item = s.charAt(0);
        for (int i = 1; i < length; i++) {
            int n = cost[i];
            if (item == s.charAt(i)) {
                max = Math.max(n, max);
                sum += n;
            } else {
                all += (sum - max);
                sum = n;
                max = n;
                item = s.charAt(i);
            }
        }
        all += (sum - max);
        return all;
    }
}
