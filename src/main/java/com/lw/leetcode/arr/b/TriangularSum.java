package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 2221. 数组的三角和
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/7 9:53
 */
public class TriangularSum {

    public static void main(String[] args) {
        TriangularSum test = new TriangularSum();

        // 8
        int[] arr = {1, 2, 3, 4, 5};

        // 5
//        int[] arr = {5};

        int i = test.triangularSum(arr);
        System.out.println(i);

    }

    public int triangularSum(int[] nums) {
        for (int i = nums.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                nums[j] = (nums[j] + nums[j + 1]) % 10;
            }
        }
        return nums[0];
    }
}
