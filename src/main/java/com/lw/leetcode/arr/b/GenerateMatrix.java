package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 59. 螺旋矩阵 II
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/1 16:48
 */
public class GenerateMatrix {
    public int[][] generateMatrix(int n) {

        int[][] arr = new int[n][n];
        int value = 1;
        int a = n >> 1;
        for (int i = 0; i < a; i++) {
            int end = n - 1 - i;
            generateMatrix(arr, i, end, value);
            value += ((end - i) << 2);
        }
        if ((n & 1) == 1) {
            arr[a][a] = n * n;
        }
        return arr;
    }

    public void generateMatrix(int[][] arr, int st, int end, int value) {
        for (int i = st; i < end; i++) {
            arr[st][i] = value++;
        }
        for (int i = st; i < end; i++) {
            arr[i][end] = value++;
        }
        for (int i = end; i > st; i--) {
            arr[end][i] = value++;
        }
        for (int i = end; i > st; i--) {
            arr[i][st] = value++;
        }
    }
}


