package com.lw.leetcode.arr.b;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 1865. 找出和为指定值的下标对
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/7 22:46
 */
class FindSumPairs {

    private int[] nums1;
    private int[] nums2;
    private Map<Integer, Integer> map;

    public FindSumPairs(int[] nums1, int[] nums2) {
        this.nums1 = nums1;
        this.nums2 = nums2;
        map = new HashMap<>();
        for (int v : nums2) {
            map.merge(v, 1, (a, b) -> a + b);
        }
    }

    public void add(int index, int val) {
        int num = nums2[index];
        int size = map.get(num);
        map.put(num, size - 1);

        nums2[index] += val;
        map.merge(nums2[index], 1, (a, b) -> a + b);
    }

    public int count(int tot) {
        int count = 0;
        for (int v : nums1) {
            count += map.getOrDefault(tot - v, 0);
        }
        return count;
    }

}

