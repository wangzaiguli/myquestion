package com.lw.leetcode.arr.b;

/**
 * 238. 除自身以外数组的乘积
 *
 * @Author liw
 * @Date 2021/5/6 15:00
 * @Version 1.0
 */
public class ProductExceptSelf {

    public int[] productExceptSelf(int[] nums) {
        int length = nums.length;
        int l = length - 1;
        int[] a = new int[length];
        int[] b = new int[length];
        a[0] = 1;
        b[l] = 1;
        for (int i = 0; i < l; i++) {
            a[i + 1] = a[i] * nums[i];
        }
        for (int i = l; i > 0; i--) {
            b[i - 1] = b[i] * nums[i];
        }

        for (int i = 0; i < length; i++) {
            a[i] = a[i] * b[i];
        }
        return a;
    }

}
