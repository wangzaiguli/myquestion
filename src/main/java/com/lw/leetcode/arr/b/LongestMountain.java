package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * arr
 * 845. 数组中的最长山脉
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/30 11:27
 */
public class LongestMountain {


    public static void main(String[] args) {
        LongestMountain test = new LongestMountain();

        // 5
//        int[] arr = {2,1,4,7,3,2,5};
        // 0
        int[] arr = {2,2,1};
        int i = test.longestMountain(arr);
        System.out.println(i);

    }


    public int longestMountain(int[] arr) {
        boolean flag = true;
        int count = 1;
        int max = 0;
        for (int i = arr.length - 2; i >= 0; i--) {
            if (arr[i] > arr[i + 1]) {
                if (flag) {
                    count++;
                } else {
                    max = Math.max(max, count);
                    count = 2;
                    flag = true;
                }
            } else if (arr[i] < arr[i + 1]) {
                if (flag && count == 1) {
                    continue;
                }
                count++;
                flag = false;
            } else {
                if (!flag) {
                    max = Math.max(max, count);
                }
                count = 1;
                flag = true;
            }
        }
        if (!flag) {
            max = Math.max(max, count);
        }
        return max;
    }



}
