package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * arr
 * 985. 查询后的偶数和
 *
 * @author liw
 * @version 1.0
 * @date 2021/8/6 13:40
 */
public class SumEvenAfterQueries {

    public int[] sumEvenAfterQueries(int[] nums, int[][] queries) {
        int sum = 0;
        for (int num : nums) {
            if ((num & 1) == 0) {
                sum += num;
            }
        }
        int length = queries.length;
        int[] arr = new int[length];
        for (int i = 0; i < length; i++) {
            int[] query = queries[i];
            int index = query[1];
            int value = query[0];
            int item = nums[index];
            if ((item & 1) == 0 ) {
                sum -= item;
            }
            item += value;
            if ((item & 1) == 0 ) {
                sum += item;
            }
            nums[index] = item;
            arr[i] = sum;
        }
        return arr;
    }
}
