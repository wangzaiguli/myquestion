package com.lw.leetcode.arr.b;

/**
 * create by idea
 * arr
 * 1936. 新增的最少台阶数
 *
 * @author lmx
 * @version 1.0
 * @date 2021/11/28 19:45
 */
public class AddRungs {

    public int addRungs(int[] rungs, int dist) {
        int sum = (rungs[0] - 1) / dist;
        int length = rungs.length;
        for (int i = 1; i < length; i++) {
             sum += (rungs[i] - rungs[i - 1] - 1) / dist;
        }
        return sum;
    }

}
