package com.lw.leetcode.arr.b;

/**
 * @Author liw
 * @Date 2021/9/25 14:31
 * @Version 1.0
 */
public class ReachNumber {
    public static void main(String[] args) {
        ReachNumber test = new ReachNumber();
        for (int i = -1000000000; i <= -1000000000; i++) {
            int a = test.reachNumber2(i);
            int b = test.reachNumber(i);

            if (a != b) {
                System.out.println(i + "   " + a + "   " + b);
            }
        }
        System.out.println("OK");
    }

    public int reachNumber(int target) {
        int i = 1;
        int sum = 0;
        target = target < 0 ? ~target + 1 : target;
        while (true) {
            sum += i;
            if (target <= sum && ((sum - target) & 1) == 0){
                return i;
            }
            i++;
        }
    }

    public int reachNumber2(int target) {
        target = Math.abs(target);
        int k = 0;
        while (target > 0)
            target -= ++k;
        return target % 2 == 0 ? k : k + 1 + k % 2;
    }


}
