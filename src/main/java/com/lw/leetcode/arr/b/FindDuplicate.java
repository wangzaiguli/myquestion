package com.lw.leetcode.arr.b;

/**
 * 287. 寻找重复数
 *
 * @Author liw
 * @Date 2021/4/30 16:03
 * @Version 1.0
 */
public class FindDuplicate {


    public static int findDuplicate(int[] nums) {
        int length = nums.length;
        for (int i = 0; i < length; i++) {
            int value = nums[i];
            if (value == i + 1) {
                continue;
            }
            int item = value - 1;
            if (nums[item] == value) {
                return value;
            }
            nums[i] = nums[item];
            nums[item] = value;
            i--;
        }
        return 0;
    }

}