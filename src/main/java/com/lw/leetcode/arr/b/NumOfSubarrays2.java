package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1343. 大小为 K 且平均值大于等于阈值的子数组数目
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/14 21:59
 */
public class NumOfSubarrays2 {

    public int numOfSubarrays(int[] arr, int k, int threshold) {
        threshold *= k;
        int sum = 0;
        for (int i = 0; i < k; i++) {
            sum += arr[i];
        }
        int count = sum >= threshold ? 1 : 0;
        int length = arr.length;
        for (int i = k; i < length; i++) {
            sum = sum + arr[i] - arr[i - k];
            if (sum >= threshold) {
                count++;
            }
        }
        return count;
    }

}
