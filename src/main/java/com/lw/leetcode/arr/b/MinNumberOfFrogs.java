package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1419. 数青蛙
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/16 14:23
 */
public class MinNumberOfFrogs {

    public static void main(String[] args) {
        MinNumberOfFrogs test = new MinNumberOfFrogs();

        // 1
//        String str = "croakcroak";

        // 2
//        String str = "crcoakroak";

        // -1
        String str = "croakcrook";

        int i = test.minNumberOfFrogs(str);
        System.out.println(i);
    }

    public int minNumberOfFrogs(String croakOfFrogs) {
        int[] arr = new int[4];
        int count = 0;
        int max = 0;
        int length = croakOfFrogs.length();
        for (int i = 0; i < length; i++) {
            char c = croakOfFrogs.charAt(i);
            switch (c) {
                case 'c':
                    count++;
                    max = Math.max(max, count);
                    arr[0]++;
                    break;
                case 'r':
                    if (arr[0] == 0) {
                        return -1;
                    }
                    arr[0]--;
                    arr[1]++;
                    break;
                case 'o':
                    if (arr[1] == 0) {
                        return -1;
                    }
                    arr[1]--;
                    arr[2]++;
                    break;
                case 'a':
                    if (arr[2] == 0) {
                        return -1;
                    }
                    arr[2]--;
                    arr[3]++;
                    break;
                default:
                    if (arr[3] == 0) {
                        return -1;
                    }
                    arr[3]--;
                    count--;
            }
        }
        for (int i = 0; i < 4; i++) {
            if (arr[i] != 0) {
                return -1;
            }
        }
        return max;
    }

}
