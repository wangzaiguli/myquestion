package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 2381. 字母移位 II
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/21 9:39
 */
public class ShiftingLetters2381 {

    public String shiftingLetters(String s, int[][] shifts) {
        int length = s.length();
        int[] arr = new int[length + 1];
        for (int[] shift : shifts) {
            if (shift[2] == 0) {
                arr[shift[0]]--;
                arr[shift[1] + 1]++;
            } else {
                arr[shift[0]]++;
                arr[shift[1] + 1]--;
            }
        }
        for (int i = 1; i <= length; i++) {
            arr[i] += arr[i - 1];
        }
        char[] chars = s.toCharArray();
        for (int i = 0; i < length; i++) {
            int t = arr[i] % 26;
            if (chars[i] + t > 'z') {
                chars[i] = (char) (chars[i] + t - 26);
            } else {
                if (chars[i] + t < 'a') {
                    chars[i] = (char) (chars[i] + t + 26);
                } else {
                    chars[i] = (char) (chars[i] + t);
                }
            }
        }
        return String.valueOf(chars);
    }

}
