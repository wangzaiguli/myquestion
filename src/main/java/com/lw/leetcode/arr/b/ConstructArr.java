package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 剑指 Offer 66. 构建乘积数组
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/15 11:59
 */
public class ConstructArr {

    public int[] constructArr(int[] a) {
        int length = a.length;
        int[] arr = new int[length];
        if (length == 0) {
            return arr;
        }
        int item = 1;
        for (int i = length - 1; i >= 0; i--) {
            item *= a[i];
            arr[i] = item;
        }
        item = 1;
        for (int i = 0; i < length - 1; i++) {
            int t = a[i];
            arr[i] = item * arr[i + 1];
            item *= t;
        }
        arr[length - 1] = item;
        return arr;
    }

}
