package com.lw.leetcode.arr.b;

/**1094. 拼车
 * @Author liw
 * @Date 2021/10/7 13:51
 * @Version 1.0
 */
public class CarPooling {
    public boolean carPooling(int[][] trips, int capacity) {
        int[] arr = new int[1001];
        for (int[] trip : trips) {
            arr[trip[1]] += trip[0];
            arr[trip[2]] -= trip[0];
        }
        if (arr[0] > capacity) {
            return false;
        }
        int sum = 0;
        for (int i = 1; i < 1001; i++) {
            sum += arr[i];
            if (sum > capacity) {
                return false;
            }
        }
        return true;
    }
}
