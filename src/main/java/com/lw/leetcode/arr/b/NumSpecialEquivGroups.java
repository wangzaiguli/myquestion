package com.lw.leetcode.arr.b;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * b
 * arr
 * 893. 特殊等价字符串组
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/9 13:51
 */
public class NumSpecialEquivGroups {

    public static void main(String[] args) {
        NumSpecialEquivGroups test = new NumSpecialEquivGroups();

        // 3
//        String[] arr = {"abcd","cdab","cbad","xyzz","zzxy","zzyx"};

        // 3
        String[] arr = {"abc", "acb", "bac", "bca", "cab", "cba"};

        int i = test.numSpecialEquivGroups(arr);
        System.out.println(i);


    }

    public int numSpecialEquivGroups(String[] words) {
        int length = words[0].length();
        Map<String, Integer> map = new HashMap<>();
        char[] arr = new char[length];
        int mid = (length + 1) >> 1;
        for (String word : words) {
            int j = 0;
            for (int i = 0; i < length; i += 2) {
                arr[j++] = word.charAt(i);
            }
            for (int i = 1; i < length; i += 2) {
                arr[j++] = word.charAt(i);
            }
            Arrays.sort(arr, 0, mid);
            Arrays.sort(arr, mid, length);
            map.put(String.valueOf(arr), 1);
        }
        return map.size();
    }

}
