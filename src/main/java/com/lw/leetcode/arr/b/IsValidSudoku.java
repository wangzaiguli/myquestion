package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 36. 有效的数独
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/17 9:00
 */
public class IsValidSudoku {
    public boolean isValidSudoku(char[][] board) {
        return bb(board, 0, 0);
    }

    public boolean bb(char[][] matrix, int x, int y) {

        if (x == 8 && y == 9) {
            return true;
        }
        if (y == 9) {
            y = 0;
            x++;
        }
        boolean flag = cc(matrix, x, y);
        if (!flag) {
            return false;
        } else {
            return bb(matrix, x, y + 1);
        }
    }


    public boolean cc(char[][] matrix, int x, int y) {
        char c = matrix[x][y];
        if (c == '.') {
            return true;
        }
        for (int i = 0; i < 9; i++) {
            if (matrix[x][i] == c && i != y) {
                return false;
            }
            if (matrix[i][y] == c && i != x) {
                return false;
            }
        }
        int i = x / 3 * 3;
        int j = y / 3 * 3;
        for (int k = 0; k < 3; k++) {
            for (int m = 0; m < 3; m++) {
                if (k + i != x && m + j != y) {
                    if (matrix[k + i][m + j] == c) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
