package com.lw.leetcode.arr.b;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/8 14:52
 */
public class MaxScoreIndices {

    public List<Integer> maxScoreIndices(int[] nums) {
        int n = nums.length;
        List<Integer> list = new ArrayList<>();
        int max = 0;
        for (int x : nums) {
            if (x == 1) {
                max++;
            }
        }
        int item = max;
        list.add(0);
        for (int i = 0; i < n; i++) {
            item += nums[i] == 0 ? 1 : -1;
            if (item == max) {
                list.add(i + 1);
            } else if (item > max) {
                list.clear();
                list.add(i + 1);
                max = item;
            }
        }
        return list;
    }

}
