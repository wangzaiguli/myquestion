package com.lw.leetcode.arr.b;

import java.util.HashMap;
import java.util.Map;

/**
 * 523. 连续的子数组和
 *
 * @Author liw
 * @Date 2021/6/2 13:10
 * @Version 1.0
 */
public class CheckSubarraySum {

    public static void main(String[] args) {
        CheckSubarraySum test = new CheckSubarraySum();
//        int[] arr = {23, 4, 6, 4, 7};
//        int[] arr = {23, 2, 6, 4, 7};
        int[] arr = {2, 4};
        boolean b = test.checkSubarraySum(arr, 6);
        System.out.println(b);
    }

    public boolean checkSubarraySum(int[] nums, int k) {
        int length = nums.length;
        int sum = 0;
        Map<Integer, Integer> map = new HashMap<>(length << 1);
        map.put(0, -1);
        for (int i = 0; i < length; i++) {
            int key = (sum + nums[i]) % k;
            Integer value = map.get(key);
            if (value != null) {
                if (i - value > 1) {
                    return true;
                }
            } else {
                map.put(key, i);
            }
            sum = key;
        }
        return false;
    }
}
