package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2294. 划分数组使最大差为 K
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/7 9:10
 */
public class PartitionArray {

    public static void main(String[] args) {
        PartitionArray test = new PartitionArray();

        // 2
//        int[] arr = {3, 6, 1, 2, 5};
//        int k = 2;

        // 2
//        int[] arr = {1, 2, 3};
//        int k = 1;

        // 3
        int[] arr = {2, 2, 4, 5};
        int k = 0;

        int i = test.partitionArray(arr, k);
        System.out.println(i);
    }

    public int partitionArray(int[] nums, int k) {
        Arrays.sort(nums);
        int count = 0;
        int limit = -1;
        for (int num : nums) {
            if (num > limit) {
                count++;
                limit = num + k;
            }
        }
        return count;
    }

}
