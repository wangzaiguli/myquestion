package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 2012. 数组美丽值求和
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/18 16:38
 */
public class SumOfBeauties {

    public static void main(String[] args) {
        SumOfBeauties test = new SumOfBeauties();

        // 2
//        int[] arr = {1,2,3};
        // 14
        int[] arr = {1, 2, 3, 4, 5, 7, 8, 9, 10};

        int i = test.sumOfBeauties(arr);
        System.out.println(i);
    }

    public int sumOfBeauties(int[] nums) {
        int length = nums.length;
        int max = nums[0];
        int[] bs = new int[length];

        bs[length - 1] = nums[length - 1];

        for (int i = length - 2; i > 0; i--) {
            bs[i] = Math.min(bs[i + 1], nums[i]);
        }
        int sum = 0;
        for (int i = 1; i < length - 1; i++) {
            int v = nums[i];
            if (max < v && bs[i + 1] > v) {
                sum += 2;
            } else if (nums[i - 1] < v && nums[i + 1] > v) {
                sum++;
            }
            max = Math.max(max, v);
        }
        return sum;
    }

}
