package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 2028. 找出缺失的观测数据
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/18 12:03
 */
public class MissingRolls {
    public int[] missingRolls(int[] rolls, int mean, int n) {
        int len = rolls.length;
        int sum = (len + n) * mean;
        for (int i = 0; i < len; i++) {
            sum -= rolls[i];
        }
        int a = sum / n;
        int b = sum % n;
        if (a > 6 || a <= 0 || a == 6 && b != 0) {
            return new int[0];
        }
        int[] re = new int[n];
        for (int i = 0; i < n; i++) {
            if (b-- > 0) {
                re[i] = 1;
            }
            re[i] += a;
        }
        return re;
    }
}
