package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 6347. 统计范围内的元音字符串数
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/6 9:57
 */
public class VowelStrings {

    public int[] vowelStrings(String[] words, int[][] queries) {
        int length = words.length;
        int[] arr = new int[length + 1];
        int item = 0;
        for (int i = 0; i < length; i++) {
            String word = words[i];
            if (check(word.charAt(0)) && check(word.charAt(word.length() - 1))) {
                item++;
            }
            arr[i + 1] = item;
        }
        int size = queries.length;
        int[] values = new int[size];
        for (int i = 0; i < size; i++) {
            int[] query = queries[i];
            values[i] = arr[query[1] + 1] - arr[query[0]];
        }
        return values;
    }

    private boolean check(char c) {
        return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u';
    }

}
