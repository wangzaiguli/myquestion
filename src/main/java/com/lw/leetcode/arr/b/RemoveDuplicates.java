package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 80. 删除有序数组中的重复项 II
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/23 9:35
 */
public class RemoveDuplicates {
    public int removeDuplicates(int[] nums) {
        if (nums == null) {
            return 0;
        }
        int length = nums.length;
        if (length < 3) {
            return length;
        }
        int n = 0;
        int c = 0;
        for (int i = 1; i < length; i++) {
            if (nums[i] == nums[i - 1]) {
                c++;
            } else {
                if (c >= 2) {
                    n = n + c - 1;
                }
                c = 0;
            }
            nums[i - n] = nums[i];
        }
        if (c > 0) {
            n = n + c - 1;
            nums[length - n - 1] = nums[length - 1];
        }
        return length - n;

    }
}
