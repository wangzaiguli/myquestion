package com.lw.leetcode.arr.b;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 1276. 不浪费原料的汉堡制作方案
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/31 11:36
 */
public class NumOfBurgers {

    public List<Integer> numOfBurgers(int tomatoSlices, int cheeseSlices) {
        List<Integer> ansList = new ArrayList<>();
        if ((tomatoSlices & 1) == 1) {
            return ansList;
        }
        int x = (tomatoSlices >> 1) - cheeseSlices;
        if (x < 0 || cheeseSlices - x < 0) {
            return ansList;
        }
        ansList.add(x);
        ansList.add(cheeseSlices - x);
        return ansList;
    }


}
