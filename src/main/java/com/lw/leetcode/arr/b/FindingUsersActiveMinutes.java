package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 1817. 查找用户活跃分钟数
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/6 19:28
 */
public class FindingUsersActiveMinutes {


    public static void main(String[] args) {
        FindingUsersActiveMinutes test = new FindingUsersActiveMinutes();

        // [0, 2, 0, 0, 0]
        int[][] arr = {{0,5},{1,2},{0,2},{0,5},{1,3}};
        int k = 5;

        int[] ints = test.findingUsersActiveMinutes(arr, k);
        System.out.println(Arrays.toString(ints));
    }

    public int[] findingUsersActiveMinutes(int[][] logs, int k) {
        Arrays.sort(logs, (a, b) -> a[0] == b[0] ? Integer.compare(a[1], b[1]) : Integer.compare(a[0], b[0]));
        int id = logs[0][0];
        int c = -1;
        int m = -1;
        int[] arr = new int[k];
        for (int[] log : logs) {
            if (log[0] == id) {
                if (log[1] != m) {
                    c++;
                    m = log[1];
                }
            } else {
                arr[c]++;
                id = log[0];
                c = 0;
                m = log[1];
            }
        }
        arr[c]++;
        return arr;
    }

}
