package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1975. 最大方阵和
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/18 15:53
 */
public class MaxMatrixSum {

    public long maxMatrixSum(int[][] matrix) {
        long sum = 0;
        int count = 0;
        long max = Integer.MAX_VALUE;
        for (int[] ints : matrix) {
            for (long item : ints) {
                if (item < 0) {
                    sum -= item;
                    count++;
                    max = Math.min(item * -1, max);
                } else {
                    max = Math.min(item, max);
                    sum += item;
                }
            }
        }
        if ((count & 1) == 1) {
            sum -=(max << 1);
        }
        return sum;
    }

}
