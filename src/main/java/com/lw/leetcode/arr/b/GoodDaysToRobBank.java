package com.lw.leetcode.arr.b;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 2100. 适合打劫银行的日子
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/2 13:30
 */
public class GoodDaysToRobBank {

    public List<Integer> goodDaysToRobBank(int[] security, int time) {
        int n = security.length;
        List<Integer> ls = new ArrayList<>();
        if (n < 2 * time + 1) {
            return ls;
        }
        int[] right = new int[n];
        right[n - 1] = 0;
        int cnt = 0;
        for (int i = n - 2; i >= 0; --i) {
            cnt = security[i + 1] >= security[i] ? cnt + 1 : 0;
            right[i] = cnt;
        }
        cnt = 0;
        if (cnt >= time && right[0] >= time) {
            ls.add(0);
        }
        for (int i = 1; i < n; ++i) {
            cnt = security[i - 1] >= security[i] ? cnt + 1 : 0;
            if (cnt >= time && right[i] >= time) {
                ls.add(i);
            }
        }
        return ls;
    }

}
