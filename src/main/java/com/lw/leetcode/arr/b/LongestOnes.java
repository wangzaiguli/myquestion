package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1004. 最大连续1的个数 III
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/5 18:33
 */
public class LongestOnes {

    public static void main(String[] args) {
        LongestOnes test = new LongestOnes();

         // 6
//        int[] arr = {1,1,1,0,0,0,1,1,1,1,0};
//        int k = 2;

         // 3
        int[] arr = {0,0,1,1,1,0,0};
        int k = 0;

        // 10
//        int[] arr = {0,0,1,1,0,0,1,1,1,0,1,1,0,0,0,1,1,1,1};
//        int k = 3;

        int i = test.longestOnes(arr, k);
        System.out.println(i);
    }

    public int longestOnes(int[] nums, int k) {
        int length = nums.length;
        int st = 0;
        int end = 0;
        int max = 0;
        int count = 0;
        while (end < length) {
            if (nums[end] == 0) {
                if (count == k) {
                    max = Math.max(max, end - st);
                    while (st <= end) {
                        if (nums[st] == 0) {
                            st++;
                            count--;
                            break;
                        }
                        st++;
                    }
                }
                count++;
            }
            end++;
        }
        return Math.max(max, end - st);
    }

}