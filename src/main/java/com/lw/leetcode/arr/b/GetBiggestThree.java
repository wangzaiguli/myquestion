package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1878. 矩阵中最大的三个菱形和
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/12 18:01
 */
public class GetBiggestThree {

    public static void main(String[] args) {
        GetBiggestThree test = new GetBiggestThree();

//        int[][] arr = {{3,4,5,1,3},{3,3,4,2,3},{20,30,200,40,10},{1,5,5,4,1},{4,3,2,2,5}};

        // [[1,2,3],[4,5,6],[7,8,9]]
//        int[][] arr = {{1,2,3},{4,5,6},{7,8,9}};


        int[][] arr = {{7,7,8}};
        int[] biggestThree = test.getBiggestThree(arr);

        System.out.println(Arrays.toString(biggestThree));
    }

    private int[][] grid ;
    private int a;
    private int b;
    private int c;

    public int[] getBiggestThree(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        this.grid = grid;
        int length = Math.min(((m  + 1) >> 1), ((n  + 1) >> 1));
        for (int[] ints : grid) {
            for (int j = 0; j < n; j++) {
                int v = ints[j];
                if (v == a || v == b) {
                    continue;
                }
                if (v > a) {
                    c = b;
                    b = a;
                    a = v;
                } else if (v > b) {
                    c = b;
                    b = v;
                } else if (v > c) {
                    c = v;
                }
            }
        }
        for (int i = 2; i <= length; i++) {
            find(i);
        }
        if (b == 0) {
            return new int[]{a};
        } else if (c == 0) {
            return new int[]{a, b};
        }
        return new int[]{a, b, c};
    }

    private void find (int length) {
        int m = grid.length;
        int n = grid[0].length;
        int end1 = m - (length << 1) + 1;
        int end2 = n - length;
        int l = length - 1;
        int ll = l << 1;
        int sum = 0;
        for (int i = 0; i <= end1; i++) {
            for (int j = length - 1; j <= end2; j++) {
                sum = grid[i][j] + grid[i + l][j - l] + grid[i + l][j + l] + grid[i + ll][j];
                for (int k = 1; k < l; k++) {
                    sum = sum + grid[i + k][j - k] + grid[i + k][j + k] + grid[i + ll - k][j - k] + grid[i + ll - k][j + k];
                }
                if (sum == a || sum == b) {
                    continue;
                }
                if (sum > a) {
                    c = b;
                    b = a;
                    a = sum;
                } else if (sum > b) {
                    c = b;
                    b = sum;
                } else if (sum > c) {
                    c  = sum;
                }
            }
        }
    }

}
