package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 *667. 优美的排列 II
 * @author liw
 * @version 1.0
 * @date 2021/11/8 22:08
 */
public class ConstructArray {

    public static void main(String[] args) {
        ConstructArray test = new ConstructArray();

        // 3,1,2
        int n = 3;
        int k = 2;

        // 1,2,3
//        int n = 3;
//        int k = 1;

        int[] ints = test.constructArray(n, k);
        System.out.println(Arrays.toString(ints));
    }

    public int[] constructArray(int n, int k) {
        int[] arr = new int[n];
        for (int i = k; i < n; i++) {
            arr[i] = i + 1;
        }
        int c = k;
        for (int i = k - 1; i >= 0; i--) {
            arr[i] = arr[i + 1] - c;
            c = c > 0 ? ~c + 2 : ~c;
        }
        return arr;
    }
}
