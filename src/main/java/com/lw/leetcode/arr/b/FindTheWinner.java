package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 1823. 找出游戏的获胜者
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/6 19:42
 */
public class FindTheWinner {


    public static void main(String[] args) {
        FindTheWinner test = new FindTheWinner();

        // 3
        int n = 5;
        int k = 2;

        int theWinner = test.findTheWinner(n, k);
        System.out.println(theWinner);
    }

    public int findTheWinner(int n, int k) {
        int[] arr = new int[n];
        int t = n;
        int c = 0;
        int i = 0;
        while (n != 1) {
            i = i == t ? 0 : i;
            int v = arr[i];
            if (v == -1) {
                i++;
                continue;
            }
            c++;
            if (c == k) {
                arr[i] = -1;
                c = 0;
                n--;
            }
            i++;
        }
        for (int j = 0; j < t; j++) {
            if (arr[j] == 0) {
                return j + 1;
            }
        }
        return -1;
    }

}
