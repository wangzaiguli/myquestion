package com.lw.leetcode.arr.b;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * hash
 * 2260. 必须拿起的最小连续卡牌数
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/3 21:53
 */
public class MinimumCardPickup {

    public int minimumCardPickup(int[] cards) {
        int v = Integer.MAX_VALUE;
        Map<Integer, Integer> map = new HashMap<>();
        int length = cards.length;
        for (int i = 0; i < length; i++) {
            if (map.get(cards[i]) != null) {
                v = Math.min(v, i - map.get(cards[i]) + 1);
            }
            map.put(cards[i], i);
        }
        return v == Integer.MAX_VALUE ? -1 : v;
    }

}
