package com.lw.leetcode.arr.b;

/**
 * 1953. 你可以工作的最大周数
 *
 * @Author liw
 * @Date 2021/10/6 19:47
 * @Version 1.0
 */
public class NumberOfWeeks {
    public long numberOfWeeks(int[] milestones) {
        long sum = 0;
        long max = 0;
        for (int milestone : milestones) {
            sum += milestone;
            max = Math.max(max, milestone);
        }
        sum = sum - max;
        if (max > sum) {
            return (sum << 1) + 1;
        }
        return sum + max;
    }
}
