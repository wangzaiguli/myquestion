package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 1785. 构成特定和需要添加的最少元素
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/27 21:47
 */
public class MinElements {

    public int minElements(int[] nums, int limit, int goal) {
        long sum = 0;
        for (int x : nums) {
            sum += x;
        }
        return (int) ((Math.abs(sum - goal) + limit - 1) / limit);
    }

}
