package com.lw.leetcode.arr.b;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 6125. 相等行列对
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/24 12:57
 */
public class EqualPairs {

    public int equalPairs(int[][] grid) {
        int length = grid.length;
        Map<String, Integer> map = new HashMap<>();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            for (int[] ints : grid) {
                sb.append(ints[i]);
                sb.append("#");
            }
            map.merge(sb.toString(), 1, (a, b) -> a + b);
            sb.setLength(0);
        }
        int sum = 0;
        for (int[] ints : grid) {
            for (int c : ints) {
                sb.append(c);
                sb.append("#");
            }
            sum += map.getOrDefault(sb.toString(), 0);
            sb.setLength(0);
        }
        return sum;
    }

}
