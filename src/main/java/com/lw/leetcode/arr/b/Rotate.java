package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 48. 旋转图像
 * 面试题 01.07. 旋转矩阵
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/24 17:37
 */
public class Rotate {
    public void rotate(int[][] matrix) {
        if (matrix == null) {
            return;
        }
        int length = matrix.length;
        if (length <= 1) {
            return;
        }
        int st = 0;
        int end = length - 1;
        int item;
        while (end - st > 0) {
            for (int i = st; i < end; i++) {
                item = matrix[st][i];
                matrix[st][i] = matrix[st + end - i][st];
                matrix[st + end - i][st] = matrix[end][st + end - i];
                matrix[end][st + end - i] = matrix[i][end];
                matrix[i][end] = item;
            }
            st++;
            end--;
        }
    }
}
