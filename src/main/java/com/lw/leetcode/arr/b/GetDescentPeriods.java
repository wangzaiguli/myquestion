package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 5958. 股票平滑下跌阶段的数目
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/19 15:51
 */
public class GetDescentPeriods {

    public static void main(String[] args) {
        GetDescentPeriods test = new GetDescentPeriods();

        // 7
//        int[] arr =  {3,2,1,4};

        // 4
        int[] arr =  {8,6,7,7};

        long descentPeriods = test.getDescentPeriods(arr);
        System.out.println(descentPeriods);
    }

    public long getDescentPeriods(int[] prices) {

        long sum = 1;
        int length = prices.length;

        int item = 1;
        for (int i = 1; i < length; i++) {
            if (prices[i] == prices[i - 1] - 1) {
                item++;
            } else {
                item = 1;
            }
            sum += item;
        }
        return sum;

    }
}
