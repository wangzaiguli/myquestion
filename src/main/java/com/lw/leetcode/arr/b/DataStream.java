package com.lw.leetcode.arr.b;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 2526. 找到数据流中的连续整数
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/8 11:55
 */
public class DataStream {


    public static void main(String[] args) {
        DataStream test = new DataStream(4, 3);

        System.out.println(test.consec(4));
        System.out.println(test.consec(4));
        System.out.println(test.consec(4));
        System.out.println(test.consec(3));

    }

    private int value = 0;
    private int k = 0;
    private int count = 0;
    private List<Integer> list = new ArrayList<>();

    public DataStream(int value, int k) {
        this.value = value;
        this.k = k;
    }

    public boolean consec(int num) {
        list.add(num);
        if (num == value) {
            count++;
        }
        if (list.size() <= k) {
            return count == k;
        }
        if (list.get(list.size() - k - 1) == value) {
            count--;
        }
        return count == k;
    }
}
