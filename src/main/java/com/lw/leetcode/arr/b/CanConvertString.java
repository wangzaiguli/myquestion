package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1540. K 次操作转变字符串
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/6 22:43
 */
public class CanConvertString {
    public boolean canConvertString(String s, String t, int k) {


        int length = s.length();
        if (length != t.length()) {
            return false;
        }
        int[] arr = new int[26];
        for (int i = 0; i < length; i++) {
            int c = t.charAt(i) - s.charAt(i);
            if (c < 0) {
                c += 26;
            }
            arr[c]++;
        }

        int max = 0;

        for (int i = 1; i < 26; i++) {
            max = Math.max(max, (arr[i] - 1) * 26 + i);
        }

        return max <= k;

    }
}
