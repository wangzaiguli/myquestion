package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * b
 * arr
 * https://leetcode.cn/contest/cnunionpay2022/problems/wMGN0t/
 * 银联-3. 风能发电
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/16 16:02
 */
public class StoredEnergy {

    public int storedEnergy(int storeLimit, int[] power, int[][] supply) {
        int length = power.length;
        int index = 0;
        int l = supply.length;
        int all = 0;
        int[] arr = supply[0];
        for (int i = 0; i < length; i++) {
            if (index + 1 < l && supply[index + 1][0] == i) {
                arr = supply[++index];
            }
            int p = power[i];
            if (p > arr[2]) {
                all = (all + p - arr[2]) > storeLimit ? storeLimit : (all + p - arr[2]);
            } else if (p < arr[1]) {
                all = (all - arr[1] + p) < 0 ? 0 : (all - arr[1] + p);
            }
        }
        return all;
    }

}
