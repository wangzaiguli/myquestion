package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1759. 统计同构子字符串的数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/8 21:32
 */
public class CountHomogenous {



    public int countHomogenous(String s) {
        int length = s.length();
        char item = ' ';
        long sum = 0;
        int count = 0;
        for (int i = 0; i < length; i++) {
            char c = s.charAt(i);
            if (c == item) {
                count++;
            } else {
                count = 1;
                item = c;
            }
            sum = (sum + count) % 1000000007;
        }
        return (int) sum;
    }

}
