package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 *738. 单调递增的数字
 * @author liw
 * @version 1.0
 * @date 2021/7/23 22:55
 */
public class MonotoneIncreasingDigits {

    public static void main(String[] args) {
        MonotoneIncreasingDigits test = new MonotoneIncreasingDigits();
        int n = 123486;
        int i = test.monotoneIncreasingDigits(n);
        System.out.println(i);
    }

    public int monotoneIncreasingDigits(int n) {
        if (n < 10) {
            return n;
        }
        int[] arr = new int[11];
        int l = 10;
        while (n > 0) {
            arr[l--] = n % 10;
            n /= 10;
        }
        for (int i = 9; i > l; i--) {
            if (arr[i] > arr[i + 1]) {
                for (int j = 10; j > i; j--) {
                    arr[j] = 9;
                }
                arr[i]--;
            }
        }
        n = arr[l + 1];
        for (int i = l + 2; i < 11; i++) {
            n = n * 10 + arr[i];
        }
        return n;
    }
}
