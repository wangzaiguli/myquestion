package com.lw.leetcode.arr.b;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * 面试题 16.21. 交换和
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/28 17:02
 */
public class FindSwapValues {

    public static void main(String[] args) {
        FindSwapValues test = new FindSwapValues();

        // [1, 3]
        int[] arr1 = {4, 1, 2, 1, 1, 2};
        int[] arr2 = {3, 6, 3, 3};
        int[] swapValues = test.findSwapValues(arr1, arr2);

        System.out.println(Arrays.toString(swapValues));
    }

    public int[] findSwapValues(int[] array1, int[] array2) {
        long sum = 0L;
        Set<Integer> set = new HashSet<>();
        for (int i : array1) {
            sum += i;
        }
        long a = sum;
        for (int i : array2) {
            sum += i;
            set.add(i);
        }
        if (sum % 2L != 0) {
            return new int[0];
        }
        int b = (int) (sum / 2 - a);
        for (int i : array1) {
            if (set.contains(i + b)) {
                return new int[]{i, i + b};
            }
        }
        return new int[0];
    }

}
