package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 881. 救生艇
 *
 * @author liw
 * @version 1.0
 * @date 2021/8/26 8:57
 */
public class NumRescueBoats {
    public int numRescueBoats(int[] people, int limit) {
        Arrays.sort(people);
        int count = 0;
        int st = 0;
        int end = people.length - 1;
        while (st <= end) {
            if (people[st] + people[end] <= limit) {
                st++;
            }
            end--;
            count++;
        }
        return count;
    }
}
