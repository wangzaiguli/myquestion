package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * b
 * 滑动窗口示例
 * 1456. 定长子串中元音的最大数目
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/9 13:59
 */
public class MaxVowels {
    public int maxVowels(String s, int k) {
        int n = s.length();
        int count = 0;
        for (int i = 0; i < k; ++i) {
            count += isVowel(s.charAt(i));
        }
        int ans = count;
        for (int i = k; i < n; ++i) {
            count += isVowel(s.charAt(i)) - isVowel(s.charAt(i - k));
            ans = Math.max(ans, count);
        }
        return ans;
    }

    private int isVowel(char ch) {
        return ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' ? 1 : 0;
    }

}
