package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 6129. 全 0 子数组的数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/24 12:29
 */
public class ZeroFilledSubarray {

    public long zeroFilledSubarray(int[] nums) {
        long sum = 0;
        int count = 0;
        for (int num : nums) {
            if (num == 0) {
                count++;
                sum += count;
            } else {
                count = 0;
            }
        }
        return sum;
    }

}
