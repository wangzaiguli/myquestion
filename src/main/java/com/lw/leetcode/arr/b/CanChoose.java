package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 1764. 通过连接另一个数组的子数组得到一个数组
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/21 17:50
 */
public class CanChoose {


    public boolean canChoose(int[][] gs, int[] nums) {
        int n = nums.length, m = gs.length;
        int cnt = 0;
        for (int i = 0, j = 0; i < n && j < m; ) {
            if (check(gs[j], nums, i)) {
                i += gs[j++].length;
                cnt++;
            } else {
                i++;
            }
        }
        return cnt == m;
    }

    private boolean check(int[] g, int[] nums, int i) {
        int j = 0;
        for (; j < g.length && i < nums.length; j++, i++) {
            if (g[j] != nums[i]) {
                return false;
            }
        }
        return j == g.length;
    }


//    public boolean canChoose1(int[][] gs, int[] nums) {
//
//        int st = 0;
//        int length = nums.length;
//
//        for (int[] g : gs) {
//
//
//
//
//        }
//
//        int n = nums.length, m = gs.length;
//        int cnt = 0;
//        for (int i = 0, j = 0; i < n && j < m; ) {
//            if (check(gs[j], nums, i)) {
//                i += gs[j++].length;
//                cnt++;
//            } else {
//                i++;
//            }
//        }
//        return cnt == m;
//    }
//
//    private boolean find (int[] nums, int st, int[] g) {
//        int[] ints = find(g);
//        int l = g.length;
//        int i = 0;
//        int j = -1;
//        while (i < length && j < l) {
//            if (j == -1 || nums[i] == g[j]) {
//                i++;
//                j++;
//            } else {
//                j = ints[j];
//            }
//        }
//    }
//
//    private int[] getNext (int[] nums) {
//        int i = 0;
//        int j = -1;
//        int length = nums.length;
//        int[] arr = new int[length];
//        while (i < length){
//            if (j == -1 || nums[i] == nums[j]) {
//                i++;
//                j++;
//                arr[i] = j;
//            } else {
//                j = arr[j];
//            }
//        }
//        return arr;
//    }

}
