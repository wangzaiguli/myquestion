package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 2086. 从房屋收集雨水需要的最少水桶数
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/27 21:02
 */
public class MinimumBuckets {

    public int minimumBuckets(String street) {
        int length = street.length() - 1;
        char[] chars = street.toCharArray();
        if (length == 0) {
            return chars[0] == 'H' ? -1 : 0;
        }
        if ((chars[0] == 'H' && chars[1] == 'H') || (chars[length - 1] == 'H' && chars[length] == 'H')) {
            return -1;
        }
        for (int i = 3; i <= length; i++) {
            if (chars[i] == 'H' && chars[i - 1] == 'H' && chars[i - 2] == 'H') {
                return -1;
            }
        }
        int a = chars[0] == 'H' ? 1 : 0;
        for (int i = 1; i < length; i++) {
            char c = chars[i];
            if (c == 'H') {
                a++;
            } else {
                if (chars[i - 1] == 'H' && chars[i + 1] == 'H') {
                    chars[i + 1] = '.';
                }
            }
        }
        return a + (chars[length] == 'H' ? 1 : 0);
    }

}
