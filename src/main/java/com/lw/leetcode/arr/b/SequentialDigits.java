package com.lw.leetcode.arr.b;

import java.util.ArrayList;
import java.util.List;

/**
 * 1291. 顺次数
 *
 * @Author liw
 * @Date 2021/10/2 19:42
 * @Version 1.0
 */
public class SequentialDigits {

    public static void main(String[] args) {
        SequentialDigits test = new SequentialDigits();

        // [123,234]
//        int a = 100;
//        int b = 300;


        // [1234,2345,3456,4567,5678,6789,12345]
        int a = 1000;
        int b = 13000;

        List<Integer> list = test.sequentialDigits(a, b);

        System.out.println(list);
    }

    public List<Integer> sequentialDigits(int low, int high) {
        List<Integer> list = new ArrayList<>();
        int c = 0;
        int lc = low;
        int a = 0;
        while (lc != 0) {
            c++;
            a = lc;
            lc /= 10;
        }
        int f = a;
        int p = 1;
        for (int i = 1; i < c; i++) {
            p *= 10;
            f = f * 10 + a + i;
        }
        while (f <= high) {
            if (a + c > 10) {
                a = 1;
                c++;
                p *= 10;
                f = 1;
                for (int i = 1; i < c; i++) {
                    f = f * 10 + a + i;
                }
            } else {
                if (f >= low) {
                    list.add(f);
                }
                f = f % p * 10 + a + c;
                a++;
            }
        }
        return list;
    }


}
