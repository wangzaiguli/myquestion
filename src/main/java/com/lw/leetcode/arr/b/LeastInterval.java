package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 621. 任务调度器
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/9 10:22
 */
public class LeastInterval {

    public static void main(String[] args) {
        LeastInterval test = new LeastInterval();
        char[] arr = {'A', 'A', 'B', 'B', 'C', 'D', 'E'};
        int i = test.leastInterval(arr, 3);
        System.out.println(i);
    }

    public int leastInterval(char[] tasks, int n) {
        int length = tasks.length;
        if (n == 0) {
            return length;
        }
        int[] arr = new int[26];
        for (char c : tasks) {
            arr[c - 'A']++;
        }
        int max = 0;
        for (int i = 0; i < 26; i++) {
            max = Math.max(max, arr[i]);
        }
        int count = 0;
        for (int i = 0; i < 26; i++) {
            if (arr[i] == max) {
                count++;
            }
        }
        int m = (length - max) / n + 1;
        if (m > max) {
            return length;
        }
        return Math.max((max - 1) * (n + 1) + count, length);
    }

}
