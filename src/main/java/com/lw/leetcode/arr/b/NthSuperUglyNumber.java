package com.lw.leetcode.arr.b;

/**
 * 313. 超级丑数
 *
 * @Author liw
 * @Date 2021/7/6 10:56
 * @Version 1.0
 */
public class NthSuperUglyNumber {


    public static void main(String[] args) {
        NthSuperUglyNumber test = new NthSuperUglyNumber();
        int[] arr = {2};
        int i = test.nthSuperUglyNumber(12, arr);
        System.out.println(i);
    }

    public int nthSuperUglyNumber(int n, int[] primes) {
        int length = primes.length;
        int[] values = new int[n];
        values[0] = 1;
        int[] counts = new int[length];
        for (int i = 1; i < n; i++) {
            long min = Long.MAX_VALUE;
            for (int j = 0; j < length; j++) {
                min = Math.min(min, values[counts[j]] * primes[j]);
            }
            values[i] = (int) min;
            for (int j = 0; j < length; j++) {
                if (values[counts[j]] * primes[j] == min) {
                    counts[j] += 1;
                }
            }
        }
        return values[n - 1];
    }

}
