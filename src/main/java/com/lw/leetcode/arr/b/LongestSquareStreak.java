package com.lw.leetcode.arr.b;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 2501. 数组中最长的方波
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/11 13:45
 */
public class LongestSquareStreak {

    public int longestSquareStreak(int[] nums) {
        Arrays.sort(nums);
        Map<Integer, Integer> map = new HashMap<>();
        int max = 1;
        for (int num : nums) {
            int t = (int) Math.pow(num, 0.5);
            if (t * t == num) {
                int c = map.getOrDefault(t, 0) + 1;
                max = Math.max(max, c);
                map.put(num, c);
            } else {
                map.put(num, 1);
            }
        }
        return max > 1 ? max : -1;
    }

}
