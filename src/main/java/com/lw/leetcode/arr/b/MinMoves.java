package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 1674. 使数组互补的最少操作次数
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/30 11:05
 */
public class MinMoves {


    public static void main(String[] args) {
        MinMoves test = new MinMoves();

        // 1
//        int[] arr = {1, 2, 4, 3};
//        int k = 4;

        // 2
//        int[] arr = {1,2,2,1};
//        int k = 2;

        // 0
        int[] arr = {1, 2, 1, 2};
        int k = 2;

        int i = test.minMoves(arr, k);
        System.out.println(i);

    }


    public int minMoves(int[] nums, int limit) {
        int l1 = nums.length;
        int l2 = l1 >> 1;
        int maxLength = Integer.MAX_VALUE;
        for (int i = 0; i < l2; i++) {
            int max = Math.max(nums[l1 - 1 - i], nums[i]);
            int min = Math.min(nums[l1 - 1 - i], nums[i]);
            nums[i] = min;
            nums[l1 - 1 - i] = max;
            maxLength = Math.min(maxLength, Math.max(limit, max) + Math.max(limit, min));
        }
        int[] arr = new int[maxLength + 1];
        for (int i = 0; i < l2; i++) {
            int max = nums[l1 - 1 - i];
            int min = nums[i];
            int v = min + 1;
            if (v <= maxLength) {
                arr[v]--;
            }
            v = max + min;
            if (v <= maxLength) {
                arr[v]--;
            }
            v++;
            if (v <= maxLength) {
                arr[v]++;
            }
            v = Math.max(limit, min) + max + 1;
            if (v <= maxLength) {
                arr[v]++;
            }
        }
        arr[2] += l1;
        int m = arr[2];
        for (int i = 3; i < maxLength; i++) {
            arr[i] += arr[i - 1];
            m = Math.min(m, arr[i]);
        }
        return m;
    }


}
