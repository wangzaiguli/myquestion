package com.lw.leetcode.arr.b;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 90. 子集 II
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/31 19:16
 */
public class SubsetsWithDup {
    public List<List<Integer>> subsetsWithDup(int[] nums) {
        List<List<Integer>> all = new ArrayList<>();
        List<List<Integer>> item = new ArrayList<>();
        List<List<Integer>> item2;
        List<Integer> list = new ArrayList<>();
        Arrays.sort(nums);
        int length = nums.length;
        list.add(nums[0]);
        all.add(new ArrayList<>());
        all.add(list);
        item.add(list);
        for (int i = 1; i < length; i++) {
            int num = nums[i];
            item2 = new ArrayList<>();
            if (num == nums[i - 1]) {
                for (List<Integer> value : item) {
                    list = new ArrayList<>(value);
                    list.add(num);
                    item2.add(list);
                }
            } else {
                for (List<Integer> value : all) {
                    list = new ArrayList<>(value);
                    list.add(num);
                    item2.add(list);
                }
            }
            all.addAll(item2);
            item = item2;
        }
        return all;
    }
}
