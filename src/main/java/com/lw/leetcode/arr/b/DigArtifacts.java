package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 2201. 统计可以提取的工件
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/21 14:37
 */
public class DigArtifacts {

    public int digArtifacts(int n, int[][] artifacts, int[][] dig) {
        boolean[][] visit = new boolean[n][n];
        for (int[] ints : dig) {
            visit[ints[0]][ints[1]] = true;
        }
        int res = 0;
        for (int[] cur : artifacts) {
            int lx = cur[0];
            int ly = cur[1];
            int rx = cur[2];
            int ry = cur[3];
            boolean flag = true;
            for (int m = lx; m <= rx; ++m) {
                for (int k = ly; k <= ry; ++k) {
                    if (!visit[m][k]) {
                        flag = false;
                        break;
                    }
                }
            }
            if (flag) {
                ++res;
            }
        }
        return res;
    }

}
