package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * zj-future02. 黑白棋游戏
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/11 11:49
 */
public class MinSwaps_zj {

    public static void main(String[] args) {
        MinSwaps_zj test = new MinSwaps_zj();

        // 1
        int[] arr = {1, 0, 1, 0, 1, 0};

        // 0
//        int[] arr = {0, 0, 0, 1, 0};

        // 2
//        int[] arr = {1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1};

        // 3
//        int[] arr = {1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1};

        int i = test.minSwaps(arr);
        System.out.println(i);
    }

    public int minSwaps(int[] chess) {
        int count = chess[0];
        int length = chess.length;
        for (int i = 1; i < length; i++) {
            count += chess[i];
            chess[i] = chess[i - 1] + chess[i];
        }
        if (count == 0) {
            return 0;
        }
        int min = chess[count - 1];
        for (int i = count; i < length; i++) {
            min = Math.max(min, chess[i] - chess[i - count]);
        }
        return count - min;
    }

}
