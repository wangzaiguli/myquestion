package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * 1033. 移动石子直到连续
 *
 * @Author liw
 * @Date 2021/5/11 10:45
 * @Version 1.0
 */
public class NumMovesStones {

    public int[] numMovesStones(int a, int b, int c) {
        int[] item = {a, b, c};
        Arrays.sort(item);
        int min = 0;
        int max = 0;
        int i = item[1] - item[0];
        int j = item[2] - item[1];
        if (i == 1 && j == 1) {
            return new int[]{min, max};
        }
        if (i == 1) {
            return new int[]{1, j - 1};
        }
        if (j == 1) {
            return new int[]{1, i - 1};
        }
        if (i == 2) {
            return new int[]{1, j };
        }
        if (j == 2) {
            return new int[]{1, i};
        }
        return new int[]{2, i+ j - 2};
    }

}
