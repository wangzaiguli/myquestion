package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 849. 到最近的人的最大距离
 *
 * @author liw
 * @version 1.0
 * @date 2021/8/2 17:10
 */
public class MaxDistToClosest {

    public static void main(String[] args) {
        MaxDistToClosest test = new MaxDistToClosest();
        int[] arr = {0, 1};
        int i = test.maxDistToClosest(arr);
        System.out.println(i);
    }

    public int maxDistToClosest(int[] seats) {
        int length = seats.length;
        int[] arr = new int[length];
        int index = 0;
        int count = 0;
        int last = 0;
        for (int seat : seats) {
            if (seat == 1) {
                if (last == 0) {
                    arr[index++] = count;
                    count = 0;
                }
            } else {
                count++;
            }
            last = seat;
        }
        arr[index] = count;
        int max = Math.max(arr[0], arr[index]) - 1;
        for (int i = 1; i < index; i++) {
            max = Math.max(max, (arr[i] - 1) / 2);
        }
        return max + 1;
    }
}
