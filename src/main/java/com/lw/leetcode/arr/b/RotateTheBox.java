package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 1861. 旋转盒子
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/16 10:36
 */
public class RotateTheBox {

    public char[][] rotateTheBox(char[][] box) {
        int a = box.length;
        int b = box[0].length;
        char[][] arr = new char[b][a];
        for (char[] chars : arr) {
            Arrays.fill(chars, '.');
        }
        for (int i = a - 1; i >= 0; i--) {
            char[] items = box[i];
            int st = b - 1;
            int end = b - 1;
            while (st >= 0) {
                char c = items[st];
                if (c == '*') {
                    arr[st][a - 1 - i] = '*';
                    end = st - 1;
                }
                if (c == '#') {
                    arr[end][a - 1 - i] = c;
                    end--;
                }
                st--;
            }
        }
        return arr;
    }

}
