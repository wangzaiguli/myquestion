package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1124. 表现良好的最长时间段
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/29 21:13
 */
public class LongestWPI {


    public static void main(String[] args) {
        LongestWPI test = new LongestWPI();

        // 3
//        int[] arr = {9,9,6,0,6,6,9};

        // 3
//        int[] arr = {14,12,0,6,3,7,10,6};

        // 3
        int[] arr = {8, 10, 6, 16, 5};

        int i = test.longestWPI(arr);
        System.out.println(i);
    }

    public int longestWPI(int[] hours) {
        int length = hours.length;
        int i = 0;
        int j = 0;
        int item = 0;
        int max = 0;
        for (int k = 0; k < length; k++) {
            int hour = hours[k];
            if (hour > 8) {
                item++;
            } else {
                item--;
            }
            if (item > 0) {
                max = k + 1;
            } else if (item == 0) {
                if (j != 0) {
                    max = Math.max(max, k - hours[(item - 1) * -1 - 1]);
                }
            } else if (item < j) {
                hours[i] = k;
                i++;
                j--;
            } else if (item != j) {
                max = Math.max(max, k - hours[(item - 1) * -1 - 1]);
            }
        }
        return max;
    }

}
