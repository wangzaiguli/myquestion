package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 2369. 检查数组是否存在有效划分
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/8 15:28
 */
public class ValidPartition {

    public static void main(String[] args) {
        ValidPartition test = new ValidPartition();

        // true
//        int[] arr = {4, 4, 4, 5, 6};

        // false
        int[] arr = {1, 1, 1, 2};

        boolean b = test.validPartition(arr);
        System.out.println(b);
    }

    public boolean validPartition(int[] nums) {
        int length = nums.length;
        boolean[] arr = new boolean[length + 1];
        arr[0] = true;
        arr[1] = false;
        arr[2] = nums[0] == nums[1];
        for (int i = 2; i < length; i++) {
            boolean b3 = arr[i - 2];
            boolean b2 = arr[i - 1];
            int n2 = nums[i - 2];
            int n1 = nums[i - 1];
            int n = nums[i];
            arr[i + 1] = (b3 && ((n2 + 1 == n1 && n1 + 1 == n) || (n2 == n1 && n1 == n))) || (b2 && n1 == n);
        }
        return arr[length];
    }

}
