package com.lw.leetcode.arr.b;

import java.util.HashMap;
import java.util.Map;

/**
 * 454. 四数相加 II
 *
 * @Author liw
 * @Date 2021/6/23 9:25
 * @Version 1.0
 */
public class FourSumCount {
    public int fourSumCount(int[] A, int[] B, int[] C, int[] D) {
        int res = 0;
        Map<Integer, Integer> map = new HashMap<>();
        for (int a : A) {
            for (int b : B) {
                map.merge(a + b, 1, (k, v) -> k + v);
            }
        }
        for (int c : C) {
            for (int d : D) {
                res += map.getOrDefault(-c - d, 0);
            }
        }
        return res;
    }
}
