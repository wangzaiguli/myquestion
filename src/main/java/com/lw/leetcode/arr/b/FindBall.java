package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 1706. 球会落何处
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/24 22:39
 */
public class FindBall {


    public static void main(String[] args) {
        FindBall test = new FindBall();

        // {1,-1,-1,-1,-1}
//        int[][] arr = {{1,1,1,-1,-1},{1,1,1,-1,-1},{-1,-1,-1,1,1},{1,1,1,1,-1},{-1,-1,-1,-1,-1}};

        // {-1}
        int[][] arr = {{-1}};

        // {0,1,2,3,4,-1}
//        int[][] arr = {{1,1,1,1,1,1},{-1,-1,-1,-1,-1,-1},{1,1,1,1,1,1},{-1,-1,-1,-1,-1,-1}};



        int[] ball = test.findBall(arr);
        System.out.println(Arrays.toString(ball));
    }


    private int[][] grid;
    private int a;
    private int b;
    public int[] findBall(int[][] grid) {
        a = grid.length;
        b = grid[0].length;
        this.grid = grid;
        int[] arr = new int[b];
        for (int i = 0; i < b; i++) {
            arr[i] = find(0, i) ;
        }
        return arr;
    }

    private int find(int x, int y) {
        if (x == a) {
            return y;
        }
        if (y < 0 || y == b) {
            return -1;
        }
        int v = grid[x][y];
        y += v;
        if (y < 0 || y == b) {
            return -1;
        }
        if (v != grid[x][y]) {
            return -1;
        }
        return find(x + 1, y);
    }


}
