package com.lw.leetcode.arr.b;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * arr
 * b
 * 2420. 找到所有好下标
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/9 10:46
 */
public class GoodIndices {

    public static void main(String[] args) {
        GoodIndices test = new GoodIndices();

        // [2,3]
        int[] nums = {2, 1, 1, 1, 3, 4, 1};
        int k = 2;

        // []
//        int[] nums = {2, 1, 1, 2};
//        int k = 2;

        // [4,5]
//        int[] nums = {878724, 201541, 179099, 98437, 35765, 327555, 475851, 598885, 849470, 943442};
//        int k = 4;

        List<Integer> list = test.goodIndices(nums, k);
        System.out.println(list);
    }

    public List<Integer> goodIndices(int[] nums, int k) {
        int length = nums.length;
        int[] arr1 = new int[length];
        int[] arr2 = new int[length];

        Arrays.fill(arr1, 1);
        Arrays.fill(arr2, 1);
        for (int i = 1; i < length; i++) {
            if (nums[i] <= nums[i - 1]) {
                arr1[i] = arr1[i - 1] + 1;
            }
        }
        for (int i = length - 2; i >= 0; i--) {
            if (nums[i] <= nums[i + 1]) {
                arr2[i] = arr2[i + 1] + 1;
            }
        }
        List<Integer> list = new ArrayList<>();
        for (int i = 1; i < length - 1; i++) {
            if (arr1[i - 1] >= k && arr2[i + 1] >= k) {
                list.add(i);
            }
        }
        return list;
    }

}
