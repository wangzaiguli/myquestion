package com.lw.leetcode.arr.b;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 1630. 等差子数组
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/24 21:18
 */
public class CheckArithmeticSubarrays {

    public static void main(String[] args) {
        CheckArithmeticSubarrays test = new CheckArithmeticSubarrays();

        // [true,false,true]
//        int[] arr1 = {4,6,5,9,3,7};
//        int[] arr2 = {0,0,2};
//        int[] arr3 = {2,3,5};


        int[] arr1 = {52, 104, 234, 156, 208, -171, -513, -114, -57, -285, -570, -456, -228, -342, -399, 344, 86, 43, 215, 172, 301, -784, -490, -686, -196, -980, -392, -98, 280, 336, 112, 504, 280, 56, 448, 392, 266, 114, 304, 72, 144, 288, 108, 324, 252, 180, 360, 36, -365, -511, -584, -438, -146, -730, -219, -73, -292, -657, 310, 55, 165, 550, 385, 110, 495, 275, 440, 220, 330, 185, -282, -329, -188, -470, -235, -47, -376, -94, -141, -423, -462, -396, -330, -66, -305, -427, -366, -183, -122, -488, -61, -610, -549, -244, 950, 50, 15, 35, 30, 10, 40, 20, 45, 154, 88, 44, 198, 110, 220, 176, 22, 66, 132, 69, 230, 80, 40, 360, 320, 240, 280, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -438, -730, -511, 260, 208, 52, 130, 156, 234, 78, 26, 104, 182, 154, 462, -768, -288, -192, -960, -96, -384, -864, -576, -672, -480, 469, 67, -18, -36, -54, 86, -440, -495, 140, 700, 490, 280, 350, 630, 420, 70, 560, 210, 352, -198, -660, -264, -66, 405, 324, 550, 220, 385, 330, 55, 495, 165, 440, 275, 110, 77, 33, 110, 66, 88, 99, 44, 11, 55, 22, 21, 9, 15, -588, -784, -98, -392, -196, -882, -686, -294, -980, -490, 60, 36, 42, 12, -490, -140, -700, -350, -420, -560, -70, -630, -280, 610, 61, 244, -700, -210, -70, -140, -630, -350, -420, -490, -280, -560, -910, -546, -728, -91, -273, -637, -819, -364, -182, -14, -20, -16, -6, -116, 42, 14, 84, 70, 98, 112, 140, 56, 126, 28, -664, -166, -332, -415, -498, -830, -48, -96, -120, -240, -145, -261, -344, -172, -86, -30, -40, -20, 792, 198, 99, 594, 990, 693, 495, 891, 396, 297, -329, -594};
        int[] arr2 = {116};
        int[] arr3 = {125};

        for (int i = 116; i < 126; i++) {
            System.out.print(arr1[i] + ",");
        }


        List<Boolean> list = test.checkArithmeticSubarrays(arr1, arr2, arr3);
        System.out.println(list);
    }


    public List<Boolean> checkArithmeticSubarrays(int[] nums, int[] l, int[] r) {
        int length = l.length;
        List<Boolean> list = new ArrayList<>(length);
        int[] arr = new int[nums.length];
        for (int i = 0; i < length; i++) {
            int a = l[i];
            int b = r[i];
            int max = nums[b];
            int min = max;
            for (int j = a; j < b; j++) {
                max = Math.max(max, nums[j]);
                min = Math.min(min, nums[j]);
            }
            if (min == max) {
                list.add(true);
                continue;
            }
            int d = b - a;
            int c = (max - min) / d;
            if (c * d != max - min) {
                list.add(false);
                continue;
            }
            boolean flag = true;
            for (int j = a; j <= b; j++) {
                int num = nums[j];
                int k = (num - min) / c;
                if (c * k != num - min) {
                    flag = false;
                    break;
                }
                arr[k]++;
            }
            for (int j = 0; j <= d; j++) {
                if (arr[j] != 1) {
                    flag = false;
                }
                arr[j] = 0;
            }
            list.add(flag);
        }
        return list;
    }

}
