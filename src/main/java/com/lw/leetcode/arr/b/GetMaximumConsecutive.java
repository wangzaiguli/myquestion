package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 1798. 你能构造出连续值的最大数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/27 22:03
 */
public class GetMaximumConsecutive {

    public int getMaximumConsecutive(int[] coins) {
        Arrays.sort(coins);
        int max = 0;
        for (int coin : coins) {
            if (coin > max + 1) {
                return max + 1;
            }
            max += coin;
        }
        return max + 1;
    }

}
