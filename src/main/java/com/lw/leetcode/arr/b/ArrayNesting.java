package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * arr
 * 565. 数组嵌套
 *
 * @author liw
 * @version 1.0
 * @date 2021/8/12 17:27
 */
public class ArrayNesting {

    public static void main(String[] args) {
        ArrayNesting test = new ArrayNesting();
        int[] arr = {5,4,0,3,1,6,2};
        int i = test.arrayNesting(arr);
        System.out.println(i);
    }

    public int arrayNesting(int[] nums) {
        int length = nums.length;
        int[] arr = new int[length];
        int max = 0;
        int num;
        int value;
        for (int i = 0; i < length; i++) {
            if (arr[i] != 0) {
                continue;
            }
            arr[i] = 1;
            num = nums[i];
            int c = 1;
            while (arr[num] == 0) {
                value = nums[num];
                arr[num] = 1;
                num = value;
                c++;
            }
            max = Math.max(max, c);
        }
        return max;
    }
}
