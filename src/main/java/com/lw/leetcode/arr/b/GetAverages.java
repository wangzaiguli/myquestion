package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 2090. 半径为 k 的子数组平均值
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/2 13:17
 */
public class GetAverages {
    public int[] getAverages(int[] nums, int k) {
        int n = nums.length;
        long[] pre = new long[n + 1];
        int[] ans = new int[n];
        for (int i = 1; i <= n; i++) {
            pre[i] = pre[i - 1] + nums[i - 1];
        }
        Arrays.fill(ans, -1);
        int l = 2 * k + 1;
        int end = n - k;
        for (int i = k; i < end; i++) {
            ans[i] = (int) ((pre[i + k + 1] - pre[i - k]) / l);
        }
        return ans;
    }

}
