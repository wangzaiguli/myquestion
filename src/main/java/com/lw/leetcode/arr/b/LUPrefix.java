package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 2424. 最长上传前缀
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/8 9:43
 */
public class LUPrefix {

    private int[] arr;
    private int st = 0;
    private int n;

    public LUPrefix(int n) {
        this.arr = new int[n + 2];
        this.n = n + 2;
        arr[0] = 1;
    }

    public void upload(int video) {
        arr[video] = 1;
    }

    public int longest() {
        while (st < n) {
            if (arr[st] == 0) {
                st--;
                break;
            }
            st++;
        }
        return st;
    }

}
