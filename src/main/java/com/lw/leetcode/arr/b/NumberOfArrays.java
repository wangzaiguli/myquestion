package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 2145. 统计隐藏数组数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/9 10:31
 */
public class NumberOfArrays {

    public static void main(String[] args) {
        NumberOfArrays test = new NumberOfArrays();

        // 2
//        int[] arr = {1,-3,4};
//        int lower = 1;
//        int upper = 6;

        // 4
//        int[] arr = {3,-4,5,1,-2};
//        int lower = -4;
//        int upper = 5;

        // 0
//        int[] arr = {4,-7,2};
//        int lower = 3;
//        int upper = 6;

        // 60
        int[] arr = {-40};
        int lower = -46;
        int upper = 53;

        int i = test.numberOfArrays(arr, lower, upper);
        System.out.println(i);
    }

    public int numberOfArrays(int[] differences, int lower, int upper) {
        long item = 0L;
        long max = 0L;
        long min = 0L;
        for (int difference : differences) {
            item += difference;
            max = Math.max(max, item);
            min = Math.min(min, item);
        }
        return upper - max + min - lower + 1 > 0 ? (int) (upper - max + min - lower + 1) : 0;
    }

}
