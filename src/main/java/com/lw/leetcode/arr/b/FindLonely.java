package com.lw.leetcode.arr.b;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 2150. 找出数组中的所有孤独数字
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/8 13:11
 */
public class FindLonely {

    public List<Integer> findLonely(int[] nums) {
        List<Integer> list = new ArrayList<>();
        Arrays.sort(nums);
        int length = nums.length - 1;
        if (length == 0) {
            list.add(nums[0]);
            return list;
        }
        if (nums[length] > nums[length - 1] + 1) {
            list.add(nums[length]);
        }
        if (nums[1] > nums[0] + 1) {
            list.add(nums[0]);
        }
        for (int i = 1; i < length; i++) {
            if (nums[i] > nums[i - 1] + 1 && nums[i + 1] > nums[i] + 1) {
                list.add(nums[i]);
            }
        }
        return list;
    }

}
