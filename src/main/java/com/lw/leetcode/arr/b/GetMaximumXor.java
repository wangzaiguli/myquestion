package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1829. 每个查询的最大异或值
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/9 9:50
 */
public class GetMaximumXor {

    public static void main(String[] args) {
        GetMaximumXor test = new GetMaximumXor();

        // [0,3,2,3]
//        int[] arr = {0,1,1,3};
//        int k = 2;

        // [5,2,6,5]
//        int[] arr = {2,3,4,7};
//        int k = 3;

        // [4,3,6,4,6,7]
        int[] arr = {0, 1, 2, 2, 5, 7};
        int k = 3;

        int[] maximumXor = test.getMaximumXor(arr, k);
        System.out.println(Arrays.toString(maximumXor));
    }

    public int[] getMaximumXor(int[] nums, int maximumBit) {
        int length = nums.length;
        int item = 0;
        for (int num : nums) {
            item ^= num;
        }
        int k = (2 << (maximumBit - 1)) - 1;
        int[] arr = new int[length];
        for (int i = 0; i < length; i++) {
            arr[i] = item ^ k;
            item ^= nums[length - i - 1];
        }
        return arr;
    }

}
