package com.lw.leetcode.arr.b;

import java.util.HashMap;
import java.util.Map;

/**
 * 781. 森林中的兔子
 *
 * @Author liw
 * @Date 2021/5/22 12:00
 * @Version 1.0
 */
public class NumRabbits {
    // [1,0,1,0,0]
    public int numRabbits(int[] answers) {
        if (answers == null || answers.length == 0) {
            return 0;
        }
        Map<Integer, Integer> map = new HashMap<>();
        for (int answer : answers) {
            map.merge(answer, 1, (a, b) -> a + b);
        }
        int sum = 0;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            Integer key = entry.getKey() + 1;
            Integer count = entry.getValue();
            int i = key * (count / key);
            sum += i;
            if (i != count) {
                sum += key;
            }
        }
        return sum;
    }

}
