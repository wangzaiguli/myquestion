package com.lw.leetcode.arr.b;

/**
 * 面试题 10.09. 排序矩阵查找
 * 240. 搜索二维矩阵 II
 * 剑指 Offer 04. 二维数组中的查找
 * 74. 搜索二维矩阵
 *
 * @Author liw
 * @Date 2021/5/10 9:58
 * @Version 1.0
 */
public class SearchMatrix {
    public boolean searchMatrix(int[][] matrix, int target) {
        int a = matrix.length;
        if (a == 0) {
            return false;
        }
        int x = 0;
        int y = matrix[0].length - 1;
        while (x < a && y >= 0) {
            if (matrix[x][y] == target) {
                return true;
            }
            if (matrix[x][y] > target) {
                y--;
            } else {
                x++;
            }
        }
        return false;
    }
}
