package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1524. 和为奇数的子数组数目
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/15 21:37
 */
public class NumOfSubarrays {

    public static void main(String[] args) {
        NumOfSubarrays test = new NumOfSubarrays();
//        int[] arr = {1,3,5};
//        int[] arr = {2,4,6};
        int[] arr = {1, 2, 3, 4, 5, 6, 7};
        int i = test.numOfSubarrays(arr);
        System.out.println(i);
    }

    public int numOfSubarrays(int[] arr) {
        int v;
        int a = 0;
        int b = 0;
        int sum = 0;
        int length = arr.length;
        for (int i = 0; i < length; i++) {
            v = arr[i];
            if ((v & 1) == 0) {
                sum += a;
                b++;
            } else {
                sum += b;
                sum++;
                int c = b;
                b = a;
                a = c + 1;
            }
            sum %= 1000000007;
        }
        return sum;
    }
}
