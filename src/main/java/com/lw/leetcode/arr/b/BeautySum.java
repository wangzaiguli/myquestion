package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 1781. 所有子字符串美丽值之和
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/27 21:39
 */
public class BeautySum {

    public int beautySum(String s) {
        int sum = 0;
        char[] arr = s.toCharArray();
        int length = arr.length;
        int[] counts = new int[26];
        for (int i = 0; i < length; i++) {
            Arrays.fill(counts, 0);
            for (int j = i; j < length; j++) {
                counts[arr[j] - 'a']++;
                int max = Integer.MIN_VALUE;
                int min = Integer.MAX_VALUE;
                for (int count : counts) {
                    if (count > 0) {
                        max = Math.max(max, count);
                        min = Math.min(min, count);
                    }
                }
                sum += (max - min);
            }
        }
        return sum;
    }

}
