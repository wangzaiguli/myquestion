package com.lw.leetcode.arr.b;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 2365. 任务调度器 II
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/8 16:31
 */
public class TaskSchedulerII {

    public static void main(String[] args) {
        TaskSchedulerII test = new TaskSchedulerII();

        // 9
//        int[] arr = {1, 2, 1, 2, 3, 1};
//        int k = 3;

        // 6
        int[] arr = {5, 8, 8, 5};
        int k = 2;

        long l = test.taskSchedulerII(arr, k);
        System.out.println(l);
    }

    public long taskSchedulerII(int[] tasks, int space) {
        long item = 0;
        space++;
        Map<Integer, Long> map = new HashMap<>();
        for (int task : tasks) {
            item++;
            Long v = map.get(task);
            if (v != null && item - v < space) {
                item = space + v;
            }
            map.put(task, item);
        }
        return item;
    }

}
