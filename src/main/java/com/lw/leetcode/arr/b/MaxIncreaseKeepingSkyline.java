package com.lw.leetcode.arr.b;

/**
 * arr
 * 807. 保持城市天际线
 *
 * @Author liw
 * @Date 2021/7/6 14:28
 * @Version 1.0
 */
public class MaxIncreaseKeepingSkyline {

    public int maxIncreaseKeepingSkyline(int[][] grid) {
        int a = grid.length;
        int[] arr1 = new int[a];
        int[] arr2 = new int[a];
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                arr1[i] = Math.max(arr1[i], grid[j][i]);
                arr2[i] = Math.max(arr2[i], grid[i][j]);
            }
        }
        int sum = 0;
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                int temp = Math.min(arr1[j], arr2[i]);
                sum += temp - grid[i][j];
            }
        }
        return sum;
    }
}
