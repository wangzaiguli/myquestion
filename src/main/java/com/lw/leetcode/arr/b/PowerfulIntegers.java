package com.lw.leetcode.arr.b;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 970. 强整数
 *
 * @Author liw
 * @Date 2021/8/31 21:51
 * @Version 1.0
 */
public class PowerfulIntegers {

    public static void main(String[] args) {
        PowerfulIntegers test = new PowerfulIntegers();
        // [2,3,4,5,7,9,10]
//        int x = 2;
//        int y = 3;
//        int bound = 10;
        // [2,4,6,8,10,14]
//        int x = 3;
//        int y = 5;
//        int bound = 15;
        // [2,4,6,8,10,14]
        int x = 2;
        int y = 1;
        int bound = 10;

        List<Integer> list = test.powerfulIntegers(x, y, bound);
        System.out.println(list);
    }

    public List<Integer> powerfulIntegers(int x, int y, int bound) {
        List<Integer> values = new ArrayList<>();
        if (bound < 2) {
            return values;
        }
        int a = x;
        int b = y;
        if (x < y) {
            a = y;
            b = x;
        }
        if (a == 1) {
            values.add(2);
            return values;
        }
        List<Integer> list = new ArrayList<>();
        x = 1;
        while (x < bound) {
            list.add(x + 1);
            x *= a;
        }
        if (b == 1) {
            return list;
        }
        Set<Integer> set = new HashSet<>();
        y = 1;
        while (y <= bound) {
            for (Integer integer : list) {
                if (y + integer - 1 <= bound) {
                    set.add(y + integer - 1);
                } else {
                    break;
                }
            }
            y *= b;
        }
        values.addAll(set);
        return values;
    }

}
