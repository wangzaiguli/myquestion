package com.lw.leetcode.arr.b;

/**
 * 1385. 两个数组间的距离值
 *
 * @Author liw
 * @Date 2021/6/11 12:35
 * @Version 1.0
 */
public class FindTheDistanceValue {
    public int findTheDistanceValue(int[] arr1, int[] arr2, int d) {
        int cnt = 0;
        for (int x : arr1) {
            boolean ok = true;
            for (int y : arr2) {
                ok &= Math.abs(x - y) > d;
            }
            cnt += ok ? 1 : 0;
        }
        return cnt;
    }


    int findTheDistanceValue2(int[] arr1, int[] arr2, int d) {
        int result = arr1.length;
        for (int i : arr1) {
            for (int j : arr2) {
                if (Math.abs(i - j) <= d) {
                    result--;
                    break;
                }
            }
        }
        return result;
    }

}
