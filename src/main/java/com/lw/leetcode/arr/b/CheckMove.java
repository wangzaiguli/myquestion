package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 1958. 检查操作是否合法
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/17 11:20
 */
public class CheckMove {

    public boolean checkMove(char[][] board, int rMove, int cMove, char color) {
        int[][] dirs = new int[][]{{0, -1}, {0, 1}, {-1, 0}, {1, 0}, {-1, -1}, {1, 1}, {-1, 1}, {1, -1}};
        for (int[] dir : dirs) {
            int x = dir[0];
            int y = dir[1];
            int r = rMove + x;
            int c = cMove + y;
            int cnt = 0;
            while (0 <= r && r < 8 && 0 <= c && c < 8) {
                char item = board[r][c];
                if (item == color) {
                    if (cnt > 0) {
                        return true;
                    }
                    break;
                } else if (item == '.') {
                    break;
                }
                cnt++;
                r += x;
                c += y;
            }
        }
        return false;
    }

}
