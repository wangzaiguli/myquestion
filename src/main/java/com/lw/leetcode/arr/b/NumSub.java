package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 *1513. 仅含 1 的子串数
 * @author liw
 * @version 1.0
 * @date 2021/11/13 20:59
 */
public class NumSub {

    public int numSub(String s) {
        int sum = 0;
        int count = 0;
        for (char c : s.toCharArray()) {
            if (c == '0') {
                count= 0;
            } else {
                count++;
                sum = (count + sum) % 1000000007;
            }
        }
        return sum;
    }

}
