package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1753. 移除石子的最大得分
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/8 21:23
 */
public class MaximumScore {

    public int maximumScore(int a, int b, int c) {
        int m1 = Math.max(a, Math.max(b, c));
        int m3 = Math.min(a, Math.min(b, c));
        long sum = (long) a + b + c;
        int m2 = (int) (sum - m1 - m3);
        if (m2 + m3 <= m1) {
            return m2 + m3;
        }
        return (int) (sum >> 1);
    }
}
