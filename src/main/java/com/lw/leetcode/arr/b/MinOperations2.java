package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1769. 移动所有球到每个盒子所需的最小操作数
 *
 * @author liw
 * @version 1.0
 * @date 2021/8/19 15:21
 */
public class MinOperations2 {

    public static void main(String[] args) {
        MinOperations2 test = new MinOperations2();
        String str = "110";

        int[] ints = test.minOperations(str);
        System.out.println(Arrays.toString(ints));
    }

    /*
    1：创建一个数组ints来记录返回值。
    2：遍历字符串，先计算出ints[0] 的值， 和 1 出现的次数 count。
    3：再遍字符串，ints[i] 是由 ints[i - 1] 以及其前后 1的个数决定的。
    4：当计算 i 的时候 , i 之前有 m  个 1， i 之后有 n 个 1 。
    5：则 ints[i] = ints[i - 1] - n + m 。
    6：通过上面的公式，即可计算出结果。
    */
    public int[] minOperations(String boxes) {
        byte[] arr = boxes.getBytes();
        int length = boxes.length();
        int sum = 0;
        int count = 0;
        for (int i = 0; i < length; i++) {
            if (arr[i] == 49) {
                count++;
                sum += i;
            }
        }
        int[] ints = new int[length];
        ints[0] = sum;
        int item = 0;
        if (arr[0] == 49) {
            item++;
            count--;
        }
        for (int i = 1; i < length; i++) {
            int n = arr[i] - 48;
            ints[i] = ints[i - 1] - count + item;
            count -= n;
            item += n;
        }
        return ints;
    }


}
