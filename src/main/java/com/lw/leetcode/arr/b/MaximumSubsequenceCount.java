package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 6021. 字符串中最多数目的子字符串
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/21 9:38
 */
public class MaximumSubsequenceCount {

    public long maximumSubsequenceCount(String text, String pattern) {
        char a = pattern.charAt(0);
        char b = pattern.charAt(1);
        int length = text.length();
        long sum = 0;
        int ac = 0;
        int bc = 0;
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if (c == a) {
                ac++;
            } else if (c == b) {
                bc++;
                sum += ac;
            }
        }
        if (a == b) {
            long n = ac + bc + 1L;
            return (n - 1) * n / 2;
        }
        return sum + Math.max(ac, bc);
    }

}
