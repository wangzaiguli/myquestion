package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2171. 拿出最少数目的魔法豆
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/5 11:13
 */
public class MinimumRemoval {

    public long minimumRemoval(int[] beans) {
        long sum = 0;
        for (int bean : beans) {
            sum += bean;
        }
        Arrays.sort(beans);
        int length = beans.length;
        long v = beans[0];
        long min = sum - v * length;
        sum -= v;
        long s = v;
        for (int i = 1; i < length; i++) {
            v = beans[i];
            if (v == beans[i - 1]) {
                continue;
            }
            min = Math.min(min, s + sum - v * (length - i));
            s += v;
            sum -= v;
        }
        return min;
    }

}
