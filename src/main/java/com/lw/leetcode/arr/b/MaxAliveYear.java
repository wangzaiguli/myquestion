package com.lw.leetcode.arr.b;

/**
 * create by idea
 * other
 * 面试题 16.10. 生存人数
 *
 * @author lmx
 * @version 1.0
 * @date 2022/1/3 17:31
 */
public class MaxAliveYear {

    public static void main(String[] args) {
        MaxAliveYear test = new MaxAliveYear();

    }

    public int maxAliveYear(int[] birth, int[] death) {
        int[] arr = new int[102];
        int length = birth.length;
        for (int i = 0; i < length; i++) {
            arr[birth[i] - 1900] += 1;
            arr[death[i] - 1899] -= 1;
        }
        int item = 0;
        int max = 0;
        int y = 1900;
        for (int i = 0; i < 101; i++) {
            item += arr[i];
            if (max < item) {
                max = item;
                y = i + 1900;
            }
        }
        return y;
    }

}
