package com.lw.leetcode.arr.b;

import java.util.ArrayList;
import java.util.List;

/**
 * 386. 字典序排数
 *
 * @Author liw
 * @Date 2021/6/21 22:06
 * @Version 1.0
 */
public class LexicalOrder {

    public static void main(String[] args) {
        LexicalOrder test = new LexicalOrder();
        List<Integer> integers = test.lexicalOrder(5);
        System.out.println(integers);
    }

    private List<Integer> list = new ArrayList<>();
    private int n;

    public List<Integer> lexicalOrder(int n) {
        this.n = n;
        for (int i = 1; i < 10; i++) {
            if (i <= n) {
                list.add(i);
                find(i);
            }
        }
        return list;
    }

    private void find(int value) {
        value *= 10;
        for (int i = 0; i < 10; i++) {
            int val = value + i;
            if (val <= n) {
                list.add(val);
                find(val);
            } else {
                return;
            }
        }
    }

}
