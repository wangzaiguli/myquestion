package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1052. 爱生气的书店老板
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/23 13:52
 */
public class MaxSatisfied {

    public static void main(String[] args) {
        MaxSatisfied test = new MaxSatisfied();

        // 16
        int[] a = {1, 0, 1, 2, 1, 1, 7, 5};
        int[] b = {0, 1, 0, 1, 0, 1, 0, 1};
        int c = 3;

        int i = test.maxSatisfied(a, b, c);
        System.out.println(i);

    }

    public int maxSatisfied(int[] customers, int[] grumpy, int minutes) {
        int length = customers.length;
        int sum = 0;
        int item = 0;
        for (int i = 0; i < minutes; i++) {
            if (grumpy[i] == 1) {
                item += customers[i];
            } else {
                sum += customers[i];
            }
        }
        int max = item;
        int i = 0;
        while (minutes < length) {
            if (grumpy[minutes] == 1) {
                item += customers[minutes];
            } else {
                sum += customers[minutes];
            }
            if (grumpy[i] == 1) {
                item -= customers[i];
            }
            i++;
            minutes++;
            max = Math.max(max, item);
        }
        return sum + max;
    }

}
