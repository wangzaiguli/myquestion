package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 5229. 拼接数组的最大分数
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/26 18:31
 */
public class MaximumsSplicedArray {

    public static void main(String[] args) {
        MaximumsSplicedArray test = new MaximumsSplicedArray();

        // 210
//        int[] arr1 = {60, 60, 60};
//        int[] arr2 = {10, 90, 10};

        // 220
//        int[] arr1 = {20, 40, 20, 70, 30};
//        int[] arr2 = {50, 20, 50, 40, 20};

        // 31
        int[] arr1 = {7, 11, 13};
        int[] arr2 = {1, 1, 1};

        int i = test.maximumsSplicedArray(arr1, arr2);
        System.out.println(i);
    }

    public int maximumsSplicedArray(int[] nums1, int[] nums2) {
        int length = nums1.length;
        int as = 0;
        int bs = 0;
        int[] nums3 = new int[length];
        for (int i = 0; i < length; i++) {
            as += nums1[i];
            bs += nums2[i];
            nums3[i] = nums2[i] - nums1[i];
            nums1[i] -= nums2[i];
        }
        int suma = 0;
        int sumb = 0;
        int maxa = Integer.MIN_VALUE;
        int maxb = Integer.MIN_VALUE;
        for (int i = 0; i < length; i++) {
            suma += nums1[i];
            maxa = Math.max(maxa, suma);
            if (suma < 0) {
                suma = 0;
            }
            sumb += nums3[i];
            maxb = Math.max(maxb, sumb);
            if (sumb < 0) {
                sumb = 0;
            }
        }
        return Math.max(maxa + bs, maxb + as);
    }

}
