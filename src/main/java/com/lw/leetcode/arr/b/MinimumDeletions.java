package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 2091. 从数组中移除最大值和最小值
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/2 9:10
 */
public class MinimumDeletions {

    public int minimumDeletions(int[] nums) {
        int len = nums.length;
        if (len == 1) {
            return 1;
        }
        int maxIdx = 0, minIdx = 0;
        for (int i = 0; i < len; i++) {
            if (nums[i] > nums[maxIdx]) {
                maxIdx = i;
            }
            if (nums[i] < nums[minIdx]) {
                minIdx = i;
            }
        }
        if (maxIdx < minIdx) {
            int temp = maxIdx;
            maxIdx = minIdx;
            minIdx = temp;
        }
        int ans = Math.min(maxIdx + 1, len - minIdx);
        ans = Math.min(ans, minIdx + 1 + len - maxIdx);
        return ans;
    }


}
