package com.lw.leetcode.arr.b;

/**
 * arr
 * 838. 推多米诺
 *
 * @Author liw
 * @Date 2021/8/8 18:55
 * @Version 1.0
 */
public class PushDominoes {


    public static void main(String[] args){
        PushDominoes test = new PushDominoes();
        // LL.RR.LLRRLL..
//        String str = ".L.R...LR..L..";

        // "RRR.L"
        String str = "R.R.L";

        // RR.L
//        String str = "RR.L";

        //
//        String str = "";
        String s = test.pushDominoes(str);
        System.out.println(s);
    }

    public String pushDominoes(String dominoes) {
        char[] arr = dominoes.toCharArray();
        char last = '.';
        int st = -1;
        int length = arr.length;

        for (int i = 0; i < length; i++) {
            char value = arr[i];
            if (value == 'L') {
                int end = i;
                if (last == 'R') {
                    while (st < end) {
                        arr[st++] = 'R';
                        arr[end--] = 'L';
                    }
                } else {
                    while (st < end) {
                        arr[end--] = 'L';
                    }
                }
                st = i;
                last = '.';
            } else if (value == 'R'){
                if (last == 'R') {
                    while (st < i) {
                        arr[st++] = 'R';
                    }
                }
                st = i;
                last = 'R';
            }
        }
        if (last == 'R') {
            for (int i = st; i < length; i++) {
                arr[i] = 'R';
            }
        }
        return new String(arr);
    }
}
