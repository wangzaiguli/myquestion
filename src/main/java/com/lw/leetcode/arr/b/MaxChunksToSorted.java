package com.lw.leetcode.arr.b;

/**
 * 769. 最多能完成排序的块
 *
 * @Author liw
 * @Date 2021/6/18 9:26
 * @Version 1.0
 */
public class MaxChunksToSorted {


    public static void main(String[] args) {
        MaxChunksToSorted test = new MaxChunksToSorted();
//        int[] arr = {4,3,1,2,0};
//        int[] arr = {1,0,2,3,4};
//        int[] arr = {0};
//        int[] arr = {0,1};
        int[] arr = {1, 0};
        int i = test.maxChunksToSorted(arr);
        System.out.println(i);

    }

    public int maxChunksToSorted(int[] arr) {
        int st = 0;
        int count = 0;
        int max = 0;
        boolean flag = false;
        int length = arr.length;
        for (int i = 0; i < length; i++) {
            int value = arr[i];
            max = Math.max(value, max);
            if (st == value) {
                flag = true;
            }
            if (flag && max == i) {
                count++;
                flag = false;
                st = i + 1;
            }
        }
        return count;
    }


}
