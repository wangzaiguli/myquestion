package com.lw.leetcode.arr.b;

import java.util.*;

/**
 * arr
 * 491. 递增子序列
 * @Author liw
 * @Date 2021/7/1 11:35
 * @Version 1.0
 */
public class FindSubsequences {

    public static void main(String[] args) {
        FindSubsequences test = new FindSubsequences();

        List<Integer> a = new ArrayList<>();
        List<Integer> b = new ArrayList<>();
        a.add(1);
        b.add(1);
        Set<List<Integer>> set = new HashSet<>();
        set.add(a);
        set.add(b);
        System.out.println(set);

        int[] arr = {4,6};
        List<List<Integer>> subsequences = test.findSubsequences(arr);
        System.out.println(subsequences);
    }

    public List<List<Integer>> findSubsequences(int[] nums) {
        if (nums == null || nums.length < 2) {
            return Collections.emptyList();
        }
        Map<List<Integer>, Integer> map = new HashMap<>();
        List<List<Integer>> all = new ArrayList<>();
        for (int num : nums) {
            int size = all.size();
            for (int i = 0; i < size; i++) {
                List<Integer> list = all.get(i);
                Integer value = list.get(list.size() - 1);
                if (value <= num) {
                    List<Integer> it = new ArrayList<>(list);
                    it.add(num);
                    if (map.get(it) == null) {
                        all.add(it);
                        map.put(it, 0);
                    }
                }
            }
            List<Integer> item = new ArrayList<>();
            item.add(num);
            if (map.get(item) == null) {
                all.add(item);
                map.put(item, 0);
            }
        }
        List<List<Integer>> returnList = new ArrayList<>(all.size());
        for (List<Integer> list : all) {
            if (list.size() > 1) {
                returnList.add(list);
            }
        }
        return returnList;
    }




}
