package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 2256. 最小平均差
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/4 20:32
 */
public class MinimumAverageDifference {

    public static void main(String[] args) {
        MinimumAverageDifference test = new MinimumAverageDifference();

        // 2
        int[] arr = {4, 2, 0};
        int i = test.minimumAverageDifference(arr);
        System.out.println(i);
    }

    public int minimumAverageDifference(int[] nums) {
        int length = nums.length;
        if (length == 0) {
            return 0;
        }
        long a = 0;
        long b = 0;
        for (int num : nums) {
            b += num;
        }
        long b1;
        long a1;
        int min = Integer.MAX_VALUE;
        length--;
        int index = 0;
        for (int i = 0; i < length; i++) {
            a += nums[i];
            b -= nums[i];
            a1 = a / (i + 1);
            b1 = b / (length - i);
            int abs = (int) Math.abs(a1 - b1);
            if (min > abs) {
                min = abs;
                index = i;
            }
        }
        if (min > ((a + nums[length]) / nums.length)) {
            return length;
        }
        return index;
    }

}
