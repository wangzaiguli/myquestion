package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2161. 根据给定数字划分数组
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/8 12:27
 */
public class PivotArray {

    public int[] pivotArray(int[] nums, int pivot) {
        int[] ans = new int[nums.length];
        int pos = 0;
        Arrays.fill(ans, pivot);
        for (int num : nums) {
            if (num < pivot) {
                ans[pos++] = num;
            }
        }
        pos = nums.length - 1;
        for (int i = nums.length - 1; i >= 0; i--) {
            if (nums[i] > pivot) {
                ans[pos--] = nums[i];
            }
        }
        return ans;
    }

}
