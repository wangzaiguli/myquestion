package com.lw.leetcode.arr.b;

/**
 * 面试题 17.09. 第 k 个数
 *
 * @Author liw
 * @Date 2021/5/19 11:30
 * @Version 1.0
 */
public class GetKthMagicNumber {

    public static void main(String[] args) {
        GetKthMagicNumber test = new GetKthMagicNumber();
        for (int i = 1; i < 10; i++) {
            int kthMagicNumber = test.getKthMagicNumber(i);
            System.out.println(kthMagicNumber);
        }

    }

    public int getKthMagicNumber(int k) {
        int[] arr = new int[k];
        arr[0] = 1;
        int a = 0;
        int b = 0;
        int c = 0;
        for (int i = 1; i < k; i++) {
            int resultN = Math.min(Math.min(arr[a] * 3, arr[b] * 5), arr[c] * 7);
            if (resultN == arr[a] * 3) {
                a++;
            }
            if (resultN == arr[b] * 5) {
                b++;
            }
            if (resultN == arr[c] * 7) {
                c++;
            }
            arr[i] = resultN;
        }
        return arr[k - 1];
    }
}
