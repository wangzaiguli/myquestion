package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * arr
 * 396. 旋转函数
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/30 14:11
 */
public class MaxRotateFunction {

    public static void main(String[] args) {
        MaxRotateFunction test = new MaxRotateFunction();
        int[] arr = {4, 3, 2, 6};
        int aa = test.maxRotateFunction(arr);
        System.out.println(aa);
    }

    public int maxRotateFunction(int[] nums) {
        int length = nums.length;
        int sum = 0;
        int mul = 0;
        for (int i = 0; i < length; i++) {
            sum += nums[i];
            mul += (nums[i] * i);
        }
        int max = mul;
        for (int i = 1; i < length; i++) {
            mul = mul - sum + length * nums[i - 1];
            max = Math.max(max, mul);
        }
        return max;
    }

}
