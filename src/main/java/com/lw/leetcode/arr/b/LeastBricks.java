package com.lw.leetcode.arr.b;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 554. 砖墙
 *
 * @Author liw
 * @Date 2021/8/22 22:32
 * @Version 1.0
 */
public class LeastBricks {

    public int leastBricks(List<List<Integer>> wall) {
        int size = wall.size();
        int max = 0;
        Map<Integer, Integer> map = new HashMap<>();
        for (List<Integer> list : wall) {
            int sum = 0;
            int l = list.size() - 1;
            for (int j = 0; j < l; j++) {
                sum += list.get(j);
                int value = map.getOrDefault(sum, 0) + 1;
                map.put(sum, value);
                max = Math.max(max, value);
            }
        }
        return size - max;
    }

}
