package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 75. 颜色分类
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/20 9:37
 */
public class SortColors {

    public void sortColors(int[] nums) {
        if (nums == null || nums.length < 2) {
            return;
        }
        int length = nums.length;
        int st = 0;
        int end = length - 1;
        int index = 0;
        while (index <= end) {
            int value = nums[index];
            if (value == 1) {
                index++;
            } else if (value == 0) {
                nums[index] = nums[st];
                nums[st] = value;
                st++;
                index++;
            } else {
                nums[index] = nums[end];
                nums[end] = value;
                end--;
            }
        }
    }

}
