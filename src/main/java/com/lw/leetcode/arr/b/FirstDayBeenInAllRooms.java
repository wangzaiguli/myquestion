package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1997. 访问完所有房间的第一天
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/24 12:14
 */
public class FirstDayBeenInAllRooms {

    public int firstDayBeenInAllRooms(int[] nextVisit) {
        int length = nextVisit.length;
        long[] arr = new long[length];
        for (int i = 1; i < length; i++) {
            arr[i] = (((arr[i - 1] + 1000000007L) << 1) - arr[nextVisit[i - 1]] + 2) % 1000000007;
        }
        return (int) (arr[length - 1] % 1000000007);
    }

}
