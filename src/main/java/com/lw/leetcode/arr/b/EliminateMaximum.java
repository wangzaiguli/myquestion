package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 1921. 消灭怪物的最大数量
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/6 19:30
 */
public class EliminateMaximum {

    public int eliminateMaximum(int[] dist, int[] speed) {
        int len = dist.length;
        int[] div = new int[len];
        for (int i = 0; i < len; i++) {
            div[i] = (int) Math.ceil((float) dist[i] / speed[i]);
        }
        Arrays.sort(div);
        int index = 0;
        int count = 0;
        int round = 1;
        while (index < len) {
            if (round++ <= div[index++]) {
                count++;
            } else {
                return count;
            }
        }
        return count;
    }

}
