package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 2202. K 次操作后最大化顶端元素
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/21 9:27
 */
public class MaximumTop {

    public int maximumTop(int[] nums, int k) {
        int length = nums.length;
        if (length == 1 && (k & 1) == 1) {
            return -1;
        }
        int l = length;
        if (k <= length) {
            l = k - 1;
        }
        int max = -1;
        for (int i = 0; i < l; i++) {
            max = Math.max(max, nums[i]);
        }
        if (k < length) {
            max = Math.max(max, nums[k]);
        }
        return max;
    }

}
