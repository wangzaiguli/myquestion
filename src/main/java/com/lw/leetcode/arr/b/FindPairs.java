package com.lw.leetcode.arr.b;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 532. 数组中的 k-diff 数对
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/7 16:18
 */
public class FindPairs {

    public int findPairs(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        int sum = 0;
        for (int num : nums) {
            Integer v = map.get(num);
            if (v != null) {
                if (k == 0 && v < 2) {
                    sum++;
                }
                map.put(num, v + 1);
                continue;
            }
            v = 1;
            if (map.containsKey(num - k)) {
                sum++;
            }
            if (map.containsKey(num + k)) {
                sum++;
            }
            map.put(num, v);
        }
        return sum;
    }

}
