package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1664. 生成平衡数组的方案数
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/25 22:56
 */
public class WaysToMakeFair {

    public int waysToMakeFair(int[] nums) {
        int length = nums.length;
        int[] as = new int[length];
        int[] bs = new int[length];
        bs[0] = nums[0];
        for (int i = 1; i < length; i++) {
            if ((i & 1) == 0) {
                as[i] = as[i - 1];
                bs[i] = bs[i - 1] + nums[i];
            } else {
                as[i] = as[i - 1] + nums[i];
                bs[i] = bs[i - 1];
            }
        }
        int a = as[length - 1];
        int b = bs[length - 1];
        int c = 0;
        for (int i = 0; i < length; i++) {
            int aa = 0;
            int ba = 0;
            if ((i & 1) == 0) {
                aa = as[i] + b - bs[i];
                ba = bs[i] - nums[i] + a - as[i];
            } else {

                aa = as[i] - nums[i] + b - bs[i];
                ba = bs[i] + a - as[i];
            }
            if (aa == ba) {
                c++;
            }
        }
        return c;
    }

}
