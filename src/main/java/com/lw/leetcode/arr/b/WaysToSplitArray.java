package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 2270. 分割数组的方案数
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/15 21:33
 */
public class WaysToSplitArray {

    public int waysToSplitArray(int[] nums) {
        int length = nums.length;
        long sum = 0;
        for (int num : nums) {
            sum += num;
        }
        sum = (sum + 1) >> 1;
        long item = 0;
        int count = 0;
        length--;
        for (int i = 0; i < length; i++) {
            item += nums[i];
            if (item >= sum) {
                count++;
            }
        }
        return count;
    }

}
