package com.lw.leetcode.arr.b;

/**
 * 1503. 所有蚂蚁掉下来前的最后一刻
 *
 * @Author liw
 * @Date 2021/5/11 10:28
 * @Version 1.0
 */
public class GetLastMoment {

    public int getLastMoment(int n, int[] left, int[] right) {
        int max = 0;
        if (left != null && left.length != 0) {
            for (int i : left) {
                max = Math.max(i, max);
            }
        }
        if (right != null && right.length != 0) {
            for (int i : right) {
                max = Math.max(n - i, max);
            }
        }
        return max;
    }

}