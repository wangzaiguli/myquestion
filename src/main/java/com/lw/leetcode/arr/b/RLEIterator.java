package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 900. RLE 迭代器
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/4 10:40
 */
public class RLEIterator {

    private int[] encoding;
    private int index = 0;
    private int length;

    public RLEIterator(int[] encoding) {
        this.encoding = encoding;
        this.length = encoding.length;
    }

    public int next(int n) {
        while (index < length) {
            if (encoding[index] < n) {
                n -= encoding[index];
                index += 2;
            } else {
                encoding[index] -= n;
                return encoding[index + 1];
            }
        }
        return -1;
    }

}
