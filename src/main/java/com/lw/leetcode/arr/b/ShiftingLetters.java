package com.lw.leetcode.arr.b;

/**
 * 848. 字母移位
 *
 * @Author liw
 * @Date 2021/6/2 13:47
 * @Version 1.0
 */
public class ShiftingLetters {

    public static void main(String[] args) {
        ShiftingLetters test = new ShiftingLetters();
//        int[] arr = {3, 5, 29};
        int[] arr = {98,63,13,56,58,89};
        String abc = test.shiftingLetters("kfekcu", arr);
        System.out.println(abc);
    }

    // "kfekcu"
    //[98,63,13,56,58,89]
    //输出：
    //"xymfZf"
    //预期结果：
    //"xymftf"

    public String shiftingLetters(String s, int[] shifts) {
        int length = s.length();
        byte[] arr = s.getBytes();
        int sum = 0;
        for (int i = length - 1; i >= 0; i--) {
            sum = (shifts[i] % 26 + sum);
            int value = (arr[i] - 97 + sum);
            if (value >= 26) {
                arr[i] = (byte) (97 + value % 26);
            } else {
                arr[i] = (byte) ( 97 + value);
            }
        }
        return new String(arr);
    }
}
