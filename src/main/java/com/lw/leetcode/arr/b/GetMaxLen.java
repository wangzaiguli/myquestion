package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 1567. 乘积为正数的最长子数组长度
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/29 22:12
 */
public class GetMaxLen {

    public int getMaxLen(int[] nums) {
        int length = nums.length;
        int st = 0;
        int index = -1;
        int count = 0;
        int max = 0;
        for (int i = 0; i < length; i++) {
            int num = nums[i];
            if (num == 0) {
                st = i + 1;
                count = 0;
                index = -1;
                continue;
            }
            if (num < 0) {
                count++;
                if (index == -1) {
                    index = i;
                }
            }
            if ((count & 1) == 0) {
                max = Math.max(max, i - st + 1);
            } else {
                max = Math.max(max, i - index);
            }
        }
        return max;
    }

}
