package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1423. 可获得的最大点数
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/14 20:57
 */
public class MaxScore {

    public int maxScore(int[] cardPoints, int k) {
        int len = cardPoints.length;
        int sum = 0;
        for (int i = 0; i < k; i++) {
            sum += cardPoints[i];
        }
        int max = sum;
        for (int i = k - 1; i >= 0; i--) {
            sum = sum - cardPoints[i] + cardPoints[--len];
            max = Math.max(max, sum);
        }
        return max;
    }

}
