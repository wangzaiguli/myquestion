package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1395. 统计作战单位数
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/9 11:51
 */
public class NumTeams {


    public static void main(String[] args) {
        NumTeams test = new NumTeams();

        // 3
        int[] arr = {2, 5, 3, 4, 1};

        int i = test.numTeams(arr);
        System.out.println(i);
    }

    public int numTeams(int[] rating) {
        int length = rating.length;
        int[][] arr = new int[length][2];
        int sum = 0;
        for (int i = 1; i < length; i++) {
            int item = rating[i];
            int g = 0;
            int l = 0;
            for (int j = i - 1; j >= 0; j--) {
                int v = rating[j];
                if (v > item) {
                    g++;
                    sum += arr[j][0];
                } else if (v < item) {
                    l++;
                    sum += arr[j][1];
                }
                arr[i][0] = g;
                arr[i][1] = l;
            }
        }
        return sum;
    }

}
