package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1689. 十-二进制数的最少数目
 *
 * @author liw
 * @version 1.0
 * @date 2021/8/19 15:04
 */
public class MinPartitions {

    public int minPartitions(String n) {
        int ans = 0;
        for (int i = n.length() - 1; i >= 0; i--) {
            ans = Math.max(ans, n.charAt(i) - '0');
        }
        return ans;
    }

}
