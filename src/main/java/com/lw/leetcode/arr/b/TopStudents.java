package com.lw.leetcode.arr.b;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * 2512. 奖励最顶尖的 K 名学生
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/26 10:14
 */
public class TopStudents {
    public List<Integer> topStudents(String[] positive_feedback, String[] negative_feedback, String[] report, int[] student_id, int k) {
        Map<String, Integer> map = new HashMap<>();
        for (String s : positive_feedback) {
            map.put(s, 3);
        }
        for (String s : negative_feedback) {
            map.put(s, -1);
        }
        int[] ans = new int[report.length];
        for (int i = 0; i < report.length; i++) {
            int res = 0;
            for (String s : report[i].split(" ")) {
                res += map.getOrDefault(s, 0);
            }
            ans[i] = res;
        }
        Integer[] id = new Integer[report.length];
        for (int i = 0; i < report.length; i++) {
            id[i] = i;
        }
        Arrays.sort(id, (a, b) -> ans[a] == ans[b] ? student_id[a] - student_id[b] : ans[b] - ans[a]);
        List<Integer> arr = new ArrayList<>(k);
        for (int i = 0; i < k; i++) {
            arr.add(student_id[id[i]]);
        }
        return arr;
    }

}
