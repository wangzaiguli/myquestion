package com.lw.leetcode.arr.b;

/**
 * 795. 区间子数组个数
 *
 * @Author liw
 * @Date 2021/9/7 22:38
 * @Version 1.0
 */
public class NumSubarrayBoundedMax {

    public static void main(String[] args) {
        NumSubarrayBoundedMax test = new NumSubarrayBoundedMax();
        int[] arr = {6, 1, 1, 2, 3, 2, 3, 1, 1, 7, 2};
//        int[] arr = {2,1,4,3,5};
        int i = test.numSubarrayBoundedMax(arr, 2, 3);
        System.out.println(i);
    }

    public int numSubarrayBoundedMax(int[] nums, int left, int right) {
        int all = 0;
        int length = nums.length;
        int count = 0;
        int l = 0;
        int r = -1;
        for (int i = 0; i < length; i++) {
            int num = nums[i];
            if (num > right) {
                count = 0;
                l = i + 1;
                r = -1;
                continue;
            }
            count++;
            if (num < left) {
                if (r != -1) {
                    all += (r - l + 1);
                }
            } else {
                r = i;
                all += count;
            }
        }
        return all;
    }

}
