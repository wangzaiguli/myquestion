package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 6277. 行和列中一和零的差值
 *
 * @author liw
 * @version 1.0
 * @date 2022/11/27 19:44
 */
public class OnesMinusZeros {

    public int[][] onesMinusZeros(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        int[] as = new int[m];
        int[] bs = new int[n];
        for (int i = 0; i < m; i++) {
            int[] arr = grid[i];
            for (int j = 0; j < n; j++) {
                int v = arr[j];
                as[i] += v;
                bs[j] += v;
            }
        }
        for (int i = 0; i < m; i++) {
            int[] arr = grid[i];
            for (int j = 0; j < n; j++) {

                arr[j] = ((as[i] + bs[j]) << 1) - m - n;
            }
        }
        return grid;
    }

}
