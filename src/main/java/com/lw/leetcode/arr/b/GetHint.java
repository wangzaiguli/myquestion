package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 299. 猜数字游戏
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/8 15:49
 */
public class GetHint {
    public String getHint(String secret, String guess) {
        byte[] as = secret.getBytes();
        byte[] bs = guess.getBytes();
        int[] a = new int[10];
        int[] b = new int[10];
        int length = as.length;
        int n = 0;
        int m = 0;
        for (int i = 0; i < length; i++) {
            if (as[i] == bs[i]) {
                n++;
            } else {
                a[as[i] - 48] += 1;
                b[bs[i] - 48] += 1;
            }
        }
        for (int i = 0; i < 10; i++) {
            m += Math.min(a[i], b[i]);
        }
        return n + "A" + m + "B";
    }
}
