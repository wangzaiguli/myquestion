package com.lw.leetcode.arr.b;

/**
 * 477. 汉明距离总和
 *
 * @Author liw
 * @Date 2021/5/28 11:53
 * @Version 1.0
 */
public class TotalHammingDistance {
    public int totalHammingDistance(int[] nums) {
        int length = nums.length;
        int sum = 0;
        for (int i = 0; i < 32; i++) {
            int n = 0;
            for (int j = 0; j < length; j++) {
                n += ((nums[j] >>> i) & 1);
            }
            sum += ((length - n) * n);
        }
        return sum;
    }
}
