package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 1657. 确定两个字符串是否接近
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/5 19:49
 */
public class CloseStrings {

    public static void main(String[] args) {
        CloseStrings test = new CloseStrings();

        // true
//        String str1 = "abc";
//        String str2 = "bca";

        // true
//        String str1 = "cabbba";
//        String str2 = "abbccc";

        // false
//        String str1 = "cabbba";
//        String str2 = "aabbss";

        // false
//        String str1 = "a";
//        String str2 = "aa";

        // false
//        String str1 = "abbzzca";
//        String str2 = "babzzcz";

        // false
        String str1 = "uau";
        String str2 = "ssx";

        boolean b = test.closeStrings(str1, str2);
        System.out.println(b);
    }

    public boolean closeStrings(String word1, String word2) {
        if (word1.length() != word2.length()) {
            return false;
        }
        char[] arr1 = word1.toCharArray();
        char[] arr2 = word2.toCharArray();
        int[] item1 = new int[26];
        int[] item2 = new int[26];
        int length = arr1.length;
        for (int i = 0; i < length; i++) {
            item1[arr1[i] - 'a']++;
            item2[arr2[i] - 'a']++;
        }
        int n = 0;
        int m = 0;
        for (int i = 0; i < 26; i++) {
            if (item1[i] != 0) {
                n |= (1 << i);
            }
            if (item2[i] != 0) {
                m |= (1 << i);
            }
        }
        if (m != n) {
            return false;
        }
        Arrays.sort(item1);
        Arrays.sort(item2);
        for (int i = 0; i < 26; i++) {
            if (item1[i] != item2[i]) {
                return false;
            }
        }
        return true;
    }

}
