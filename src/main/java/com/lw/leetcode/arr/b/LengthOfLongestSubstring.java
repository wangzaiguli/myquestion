package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * 3. 无重复字符的最长子串
 * 剑指 Offer II 016. 不含重复字符的最长子字符串
 *
 * @Author liw
 * @Date 2021/9/20 10:15
 * @Version 1.0
 */
public class LengthOfLongestSubstring {
    public int lengthOfLongestSubstring(String s) {
        if (s == null) {
            return 0;
        }
        int length = s.length();
        if (length < 2) {
            return length;
        }
        int max = 1;
        int st = -1;
        int[] arr = new int[128];
        Arrays.fill(arr, -1);
        char[] chars = s.toCharArray();
        int index = chars[0];
        arr[index] = 0;
        for (int i = 1; i < length; i++) {
            index = chars[i];
            if (arr[index] != -1 && st <= arr[index]) {
                st = arr[index];
            } else {
                max = Math.max(i - st, max);
            }
            arr[index] = i;
        }
        return max;
    }
}
