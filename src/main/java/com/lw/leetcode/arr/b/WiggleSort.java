package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 面试题 10.11. 峰与谷
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/2 20:24
 */
public class WiggleSort {

    public void wiggleSort(int[] nums) {
        int length = nums.length;
        for (int i = 1; i < length; i++) {
            if (i % 2 == 0) {
                if (nums[i] > nums[i - 1]) {
                    int t = nums[i];
                    nums[i] = nums[i - 1];
                    nums[i - 1] = t;
                }
            } else {
                if (nums[i] < nums[i - 1]) {
                    int t = nums[i];
                    nums[i] = nums[i - 1];
                    nums[i - 1] = t;
                }
            }
        }
    }

}
