package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * b
 * arr
 * 2541. 使数组中所有元素相等的最小操作数 II
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/28 10:21
 */
public class MinOperations2541 {

    public long minOperations(int[] nums1, int[] nums2, int k) {
        int length = nums1.length;
        if (k == 0) {
            for (int i = 0; i < length; i++) {
                if (nums1[i] != nums2[i]) {
                    return -1;
                }
            }
            return 0;
        }
        long a = 0;
        long b = 0;
        for (int i = 0; i < length; i++) {
            int v = nums1[i] - nums2[i];
            if (v % k != 0) {
                return -1;
            }
            int t = v / k;
            b += t;
            if (t > 0) {
                a += t;
            }
        }
        if (b != 0) {
            return -1;
        }
        return a;
    }

}
