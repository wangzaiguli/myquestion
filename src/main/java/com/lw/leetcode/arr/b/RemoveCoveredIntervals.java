package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * sort
 * 1288. 删除被覆盖区间
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/20 20:55
 */
public class RemoveCoveredIntervals {

    public static void main(String[] args) {
        RemoveCoveredIntervals test = new RemoveCoveredIntervals();

        // 2
//        int[][] arr = {{1, 4}, {3, 6}, {2, 8}};

        // 1
        int[][] arr = {{1, 2}, {1, 4}, {3, 4}};
        int i = test.removeCoveredIntervals(arr);
        System.out.println(i);
    }

    public int removeCoveredIntervals(int[][] intervals) {
        Arrays.sort(intervals, (a, b) -> a[0] == b[0] ? Integer.compare(b[1], a[1]) : Integer.compare(a[0], b[0]));
        int length = intervals.length;
        int b = intervals[0][1];
        int count = 1;
        for (int i = 1; i < length; i++) {
            if (b < intervals[i][1]) {
                b = intervals[i][1];
                count++;
            }
        }
        return count;
    }

}
