package com.lw.leetcode.arr.b;

/**
 * 926. 将字符串翻转到单调递增
 * 剑指 Offer II 092. 翻转字符
 *
 * @Author liw
 * @Date 2021/6/23 13:40
 * @Version 1.0
 */
public class MinFlipsMonoIncr {

    public static void main(String[] args) {
        MinFlipsMonoIncr test = new MinFlipsMonoIncr();
//        String str = "00110";
//        String str = "010110";
//        String str = "00011000";
//        String str = "0";
        String str = "1";
//        String str = "010110001";
        System.out.println(test.minFlipsMonoIncr(str));
    }

    public int minFlipsMonoIncr(String s) {

        int length = s.length();
        int[] arr = new int[length];

        int l = 0;
        char[] chars = s.toCharArray();
        for (int i = 0; i < length; i++) {
            if (chars[i] == '1') {
                arr[l++] = i;
            }
        }
        if (l == 0 || l == length) {
            return 0;
        }
        int len = length - l;
        int min = l;
        for (int i = 0; i < l; i++) {
            min = Math.min(min, len - arr[i] + (i << 1));
        }
        return min;
    }
}
