package com.lw.leetcode.arr.b;

/**
 * 695. 岛屿的最大面积
 * 剑指 Offer II 105. 岛屿的最大面积
 *
 * @Author liw
 * @Date 2021/6/17 9:25
 * @Version 1.0
 */
public class MaxAreaOfIsland {
    private int[][] grid;
    private int max = 0;
    private int item = 0;
    private int a;
    private int b;

    public int maxAreaOfIsland(int[][] grid) {
        if (grid == null || (a = grid.length - 1) < 0 || (b = grid[0].length - 1) < 0) {
            return 0;
        }
        this.grid = grid;
        for (int i = 0; i <= a; i++) {
            for (int j = 0; j <= b; j++) {
                if (grid[i][j] == 1) {
                    item = 0;
                    find(i, j);
                    max = Math.max(item, max);
                }
            }
        }
        return max;
    }

    private void find(int x, int y) {
        if (x < 0 || x > a || y < 0 || y > b) {
            return;
        }
        if (grid[x][y] == 1) {
            grid[x][y] = 0;
            item++;
            find(x + 1, y);
            find(x - 1, y);
            find(x, y + 1);
            find(x, y - 1);
        }
    }
}
