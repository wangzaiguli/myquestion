package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 357. 统计各位数字都不同的数字个数
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/11 9:15
 */
public class CountNumbersWithUniqueDigits {
    public int countNumbersWithUniqueDigits(int n) {
        n = Math.min(10, n);
        int[] arr = new int[n + 2];
        arr[0] = 1;
        arr[1] = 10;
        int sum = 81;
        for (int i = 2; i <= n; i++) {
            arr[i] = arr[i - 1] + sum;
            sum *= (10 - i);
        }
        return arr[n];
    }
}
