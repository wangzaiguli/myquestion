package com.lw.leetcode.arr.b;

/**
 * create by idea
 * 1980. 找出不同的二进制字符串
 *
 * @author lmx
 * @version 1.0
 * @date 2021/11/28 19:34
 */
public class FindDifferentBinaryString {

    public String findDifferentBinaryString(String[] nums) {
        int length = nums.length;
        char[] arr = new char[length];
        for (int i = 0; i < length; i++) {
            arr[i] = nums[i].charAt(i) == '0' ? '1' : '0';
        }
        return String.valueOf(arr);
    }

}
