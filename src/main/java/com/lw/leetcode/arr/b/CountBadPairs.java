package com.lw.leetcode.arr.b;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 2364. 统计坏数对的数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/7 20:55
 */
public class CountBadPairs {

    public long countBadPairs(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        long sum = 0;
        int length = nums.length;
        for (int i = 0; i < length; i++) {
            int k = nums[i] - i;
            int c = map.getOrDefault(k, 0);
            sum += (i - c);
            map.put(k, c + 1);
        }
        return sum;
    }

}
