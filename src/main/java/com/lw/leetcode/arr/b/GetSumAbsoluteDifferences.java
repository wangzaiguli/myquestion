package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 1685. 有序数组中差绝对值之和
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/30 10:39
 */
public class GetSumAbsoluteDifferences {
    public int[] getSumAbsoluteDifferences(int[] nums) {
        int length = nums.length;
        int[] sums = new int[length];
        sums[0] = nums[0];
        for (int i = 1; i < length; i++) {
            sums[i] = sums[i - 1] + nums[i];
        }
        int[] arr = new int[length];
        for (int i = 0; i < length; i++) {
            arr[i] = (((i + 1) << 1) - length) * nums[i] - (sums[i] << 1) + sums[length - 1];
        }
        return arr;
    }
}
