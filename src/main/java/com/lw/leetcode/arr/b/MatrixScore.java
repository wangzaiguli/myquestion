package com.lw.leetcode.arr.b;

import javax.sound.midi.Soundbank;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/19 10:58
 */
public class MatrixScore {


    public static void main(String[] args) {
        MatrixScore test = new MatrixScore();
        int[][] arr = {{0, 1}, {1,1}};
        int i = test.matrixScore(arr);
        System.out.println(i);
    }

    public int matrixScore(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        for (int[] arr : grid) {
            if (arr[0] == 0) {
                for (int j = 0; j < n; j++) {
                    arr[j] ^= 1;
                }
            }
        }
        int l = (m - 1) >> 1;
        for (int i = 1; i < n; i++) {
            int c = 0;
            for (int[] ints : grid) {
                c += ints[i];
            }
            if (c <= l) {
                for (int j = 0; j < m; j++) {
                    grid[j][i] ^= 1;
                }
            }
        }
        int sum = 0;
        for (int[] arr : grid) {
            for (int j = 0; j < n; j++) {
                sum += (arr[j] << (n - j - 1));
            }
        }
        return sum;
    }


    public int matrixScore1(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        int ret = m * (1 << (n - 1));

        for (int j = 1; j < n; j++) {
            int nOnes = 0;
            for (int i = 0; i < m; i++) {
                if (grid[i][0] == 1) {
                    nOnes += grid[i][j];
                } else {
                    nOnes += (1 - grid[i][j]); // 如果这一行进行了行反转，则该元素的实际取值为 1 - grid[i][j]
                }
            }
            int k = Math.max(nOnes, m - nOnes);
            ret += k * (1 << (n - j - 1));
        }
        return ret;
    }

}
