package com.lw.leetcode.arr.b;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1695. 删除子数组的最大得分
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/14 21:37
 */
public class MaximumUniqueSubarray {

    public static void main(String[] args) {
        MaximumUniqueSubarray test = new MaximumUniqueSubarray();

        // 17
        int[] arr = {4, 2, 4, 5, 6};

        // 16911
//        int[] arr = {187,470,25,436,538,809,441,167,477,110,275,133,666,345,411,459,490,266,987,965,429,166,809,340,467,318,125,165,809,610,31,585,970,306,42,189,169,743,78,810,70,382,367,490,787,670,476,278,775,673,299,19,893,817,971,458,409,886,434};
        int i = test.maximumUniqueSubarray(arr);

        System.out.println(i);
    }

    public int maximumUniqueSubarray(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        int length = nums.length;
        int max = 0;
        int st = 0;
        int item = 0;
        for (int i = 0; i < length; i++) {
            int num = nums[i];
            item += num;
            Integer value = map.get(num);
            if (value != null && value != 0) {
                while (nums[st] != num) {
                    item -= nums[st];
                    map.put(nums[st], 0);
                    st++;
                }
                st++;
                item -= num;
            }
            max = Math.max(max, item);
            map.put(num, 1);
        }
        return max;
    }

}
