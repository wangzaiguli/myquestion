package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 6064. 不含特殊楼层的最大连续楼层数
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/15 19:32
 */
public class MaxConsecutive {

    public static void main(String[] args) {
        MaxConsecutive test = new MaxConsecutive();

        // 3
//        int a = 2;
//        int b = 9;
//        int[] arr = {4, 6};

        // 0
//        int a = 6;
//        int b = 8;
//        int[] arr = {7, 6, 8};

        // 0
        int a = 6;
        int b = 8;
        int[] arr = {1, 9};

        int i = test.maxConsecutive(a, b, arr);
        System.out.println(i);
    }

    public int maxConsecutive(int bottom, int top, int[] special) {
        Arrays.sort(special);
        int index = 0;
        int length = special.length;
        if (bottom > special[0]) {
            if (bottom > special[length - 1]) {
                return top - bottom + 1;
            }
            int st = 0;
            int end = length - 1;
            while (st < end) {
                int m = st + ((end - st) >> 1);
                if (special[m] > bottom) {
                    end = m;
                } else {
                    st = m + 1;
                }
            }
            index = st;
        }
        int max = 0;
        int i = index;
        int value = bottom;
        for (; i < length; i++) {
            int v = special[i];
            if (v > top) {
                max = Math.max(max, top - value + 1);
                break;
            }
            max = Math.max(max, v - value);
            value = v + 1;
        }
        if (special[length - 1] < top) {
            max = Math.max(max, top - value + 1);
        }
        return max;
    }

}
