package com.lw.leetcode.arr.b;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 1329. 将矩阵按对角线排序
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/11 9:53
 */
public class DiagonalSort {
    public int[][] diagonalSort(int[][] mat) {
        int n = mat.length;
        int m = mat[0].length;

        for (int i = n - 1; i >= 1 - m; --i) {
            int x = i > 0 ? i : 0;
            int y = i < 0 ? -i : 0;
            int[] arr = new int[Math.min(n - x, m - y)];
            for (int j = 0; j < arr.length; ++j) {
                arr[j] = mat[x + j][y + j];
            }
            Arrays.sort(arr);
            for (int j = 0; j < arr.length; ++j) {
                mat[x + j][y + j] = arr[j];
            }
        }
        return mat;
    }
}
