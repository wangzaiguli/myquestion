package com.lw.leetcode.arr.b;

/**
 * 209. 长度最小的子数组
 * 剑指 Offer II 008. 和大于等于 target 的最短子数组
 *
 * @Author liw
 * @Date 2021/5/24 11:15
 * @Version 1.0
 */
public class MinSubArrayLen {
    public int minSubArrayLen(int target, int[] nums) {
        int st = 0;
        int end = 0;
        int sum = 0;
        int min = Integer.MAX_VALUE;
        int length = nums.length;
        while (end < length) {
            sum += nums[end++];
            while (sum >= target && st <= end) {
                min = Math.min(end - st, min);
                sum -= nums[st++];
            }
        }
        return min == Integer.MAX_VALUE ? 0 : min;
    }
}
