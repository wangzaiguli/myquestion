package com.lw.leetcode.arr.b;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 1814. 统计一个数组中好对子的数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/6 19:07
 */
public class CountNicePairs {

    public static void main(String[] args) {
        CountNicePairs test = new CountNicePairs();

        // 2
//        int[] arr = {42,11,1,97};

        // 4
        int[] arr = {13,10,35,24,76};

        int i = test.countNicePairs(arr);

        System.out.println(i);
    }

    public int countNicePairs(int[] nums) {
        Map<Long, Integer> map = new HashMap<>();
        long sum = 0L;
        for (long num : nums) {
            long k = num - find(num);
            int c = map.getOrDefault(k, 0);
            sum += c;
            sum %= 1000000007;
            map.put(k, c + 1);
        }
        return (int) sum;
    }

    private long find(long n) {
        long v = 0L;
        while (n != 0L) {
            v = v * 10 + n % 10L;
            n /= 10L;
        }
        return v;
    }

}
