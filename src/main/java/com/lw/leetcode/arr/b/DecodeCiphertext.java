package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 2075. 解码斜向换位密码
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/23 21:02
 */
public class DecodeCiphertext {

    public static void main(String[] args) {
        DecodeCiphertext test = new DecodeCiphertext();

        // cipher
//        String str = "ch   ie   pr";
//        int k = 3;

        // coding
//        String str = "coding";
//        int k = 1;

        // " abc"
//        String str = " b  ac";
//        int k = 2;

        // "i love leetcode"
        String str = "iveo    eed   l te   olc";
        int k = 4;

        String s = test.decodeCiphertext(str, k);
        System.out.println("#" + s + "#");
    }

    public String decodeCiphertext(String encodedText, int rows) {
        int length = encodedText.length();
        int cell = length / rows;
        StringBuilder sb = new StringBuilder(length);
        int e = 0;
        int c = 0;
        for (int i = 0; i < cell; i++) {
            int end = Math.min(rows, cell - i);
            for (int j = 0; j < end; j++) {
                int index = j * cell + j + i;
                c++;
                char charAt = encodedText.charAt(index);
                if (charAt != ' ') {
                    e = c;
                }
                sb.append(charAt);
            }
        }
        return sb.substring(0, e);
    }

}
