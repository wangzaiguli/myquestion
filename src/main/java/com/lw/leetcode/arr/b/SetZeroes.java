package com.lw.leetcode.arr.b;

/**
 * create by idea
 * 73. 矩阵置零
 * 面试题 01.08. 零矩阵
 *
 * @author lmx
 * @version 1.0
 * @date 2021/11/21 17:38
 */
public class SetZeroes {
    public void setZeroes(int[][] matrix) {
        int a = matrix.length;
        int b = matrix[0].length;

        boolean f1 = false;
        boolean f2 = false;

        for (int[] matrix1 : matrix) {
            if (matrix1[0] == 0) {
                f1 = true;
            }
        }
        for (int j = 0; j < b; j++) {
            if (matrix[0][j] == 0) {
                f2 = true;
            }
        }

        for (int i = 1; i < a; i++) {
            for (int j = 1; j < b; j++) {
                if (matrix[i][j] == 0) {
                    matrix[0][j] = 0;
                    matrix[i][0] = 0;
                }
            }
        }
        for (int i = 1; i < a; i++) {
            if (matrix[i][0] == 0) {
                for (int j = 1; j < b; j++) {
                    matrix[i][j] = 0;
                }
            }
        }
        for (int j = 1; j < b; j++) {
            if (matrix[0][j] == 0) {
                for (int i = 1; i < a; i++) {
                    matrix[i][j] = 0;
                }
            }
        }
        if (f2) {
            for (int j = 0; j < b; j++) {
                matrix[0][j] = 0;
            }
        }
        if (f1) {
            for (int i = 0; i < a; i++) {
                matrix[i][0] = 0;
            }
        }
    }
}
