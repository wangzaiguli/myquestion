package com.lw.leetcode.arr.b;

/**
 * 443. 压缩字符串
 *
 * @Author liw
 * @Date 2021/8/21 15:16
 * @Version 1.0
 */
public class Compress {

    public int compress(char[] chars) {
        int length = chars.length;
        char a = chars[0];
        int count = 1;
        int index = 0;
        for (int i = 1; i < length; i++) {
            if (a == chars[i]) {
                count++;
            } else {
                chars[index++] = a;
                if (count != 1) {
                    String s = String.valueOf(count);
                    for (char c : s.toCharArray()) {
                        chars[index++] = c;
                    }
                }
                count = 1;
                a = chars[i];
            }
        }
        chars[index++] = a;
        if (count != 1) {
            String s = String.valueOf(count);
            for (char c : s.toCharArray()) {
                chars[index++] = c;
            }
        }
        return index;
    }
}
