package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * 2391. 收集垃圾的最少总时间
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/29 13:29
 */
public class GarbageCollection {

    public int garbageCollection(String[] garbage, int[] travel) {
        int length = garbage.length;
        int sum = 0;
        int g1 = 0;
        int p1 = 0;
        int m1 = 0;
        for (int i = 0; i < length; i++) {
            String str = garbage[i];
            int g2 = 0;
            int p2 = 0;
            int m2 = 0;
            for (int j = 0; j < str.length(); j++) {
                char c = str.charAt(j);
                if (c == 'G') {
                    g2++;
                } else if (c == 'P') {
                    p2++;
                } else {
                    m2++;
                }
            }
            int t = i == length - 1 ? 0 : travel[i];
            if (g2 > 0) {
                sum += g2;
                sum += g1;
                g1 = t;
            } else {
                g1 += t;
            }
            if (p2 > 0) {
                sum += p2;
                sum += p1;
                p1 = t;
            } else {
                p1 += t;
            }
            if (m2 > 0) {
                sum += m2;
                sum += m1;
                m1 = t;
            } else {
                m1 += t;
            }
        }
        return sum;
    }

}
