package com.lw.leetcode.arr.b;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 面试题 17.23. 最大黑方阵
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/27 11:33
 */
public class FindSquare {

    public int[] findSquare(int[][] matrix) {
        int[] res = new int[3];
        int m = matrix.length;
        if (m == 0) {
            return res;
        }
        int[][][] dp = new int[m + 1][m + 1][2];
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= m; j++) {
                if (matrix[i - 1][j - 1] == 0) {
                    dp[i][j][0] = 1 + dp[i][j - 1][0];
                    dp[i][j][1] = 1 + dp[i - 1][j][1];
                }
            }
        }
        int l = 0;
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= m; j++) {
                for (int side = Math.min(dp[i][j][0], dp[i][j][1]); side >= 1; side--) {
                    if (dp[i][j - side + 1][1] >= side && dp[i - side + 1][j][0] >= side) {
                        if (side > l) {
                            l = side;
                            res[0] = i - side;
                            res[1] = j - side;
                            res[2] = side;
                            break;
                        }
                    }
                }
            }
        }
        return res[2] == 0 ? new int[0] : res;
    }


}
