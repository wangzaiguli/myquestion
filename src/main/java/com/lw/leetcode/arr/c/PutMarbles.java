package com.lw.leetcode.arr.c;

import com.lw.test.util.Utils;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * c
 * arr
 * 2551. 将珠子放入背包中
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/29 14:06
 */
public class PutMarbles {

    public static void main(String[] args) {
        PutMarbles test = new PutMarbles();

        int[] arr = Utils.getArr(10000, 1, 100000);
        int k = 30;
        System.out.println(k);
        System.out.println();

        long l = test.putMarbles(arr, k);
        System.out.println(l);
    }

    public long putMarbles(int[] weights, int k) {
        int length = weights.length - 1;
        int[] arr = new int[length];
        for (int i = 0; i < length; i++) {
            arr[i] = weights[i] + weights[i + 1];
        }
        Arrays.sort(arr);
        k--;
        long s = 0;
        for (int i = 0; i < k; i++) {
            s -= arr[i];
        }
        for (int i = length - k; i < length; i++) {
            s += arr[i];
        }
        return s;
    }

}
