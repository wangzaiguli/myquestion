package com.lw.leetcode.arr.c;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1537. 最大得分
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/30 17:00
 */
public class MaxSum {

    public static void main(String[] args) {
        MaxSum test = new MaxSum();

        // 30
        int[] a = {2, 4, 5, 8, 10};
        int[] b = {4, 6, 8, 9};

        int i = test.maxSum(a, b);
        System.out.println(i);
    }

    public int maxSum(int[] nums1, int[] nums2) {
        Map<Integer, Integer> map = new HashMap<>();
        int l1 = nums1.length;
        int l2 = nums2.length;
        for (int i = 0; i < l1; i++) {
            map.put(nums1[i], i);
        }
        int st = 0;
        long sum = 0L;
        long s2 = 0L;
        for (int i = 0; i < l2; i++) {
            Integer v = map.get(nums2[i]);
            if (v != null) {
                long s1 = 0L;
                for (int j = st; j < v; j++) {
                    s1 += nums1[j];
                }
                sum += Math.max(s1, s2);
                s2 = 0;
                st = v;
            }
            s2 += nums2[i];
        }
        long s1 = 0L;
        for (int j = st; j < l1; j++) {
            s1 += nums1[j];
        }
        sum = (sum + Math.max(s1, s2)) % 1000000007;
        return (int) sum;
    }

}
