package com.lw.leetcode.arr.c;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 2262. 字符串的总引力
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/3 22:32
 */
public class AppealSum {

    public static void main(String[] args) {
        AppealSum test = new AppealSum();

        // 20
//        String str = "abcd";

        // 28
        String str = "abbca";

        long sum = test.appealSum(str);
        System.out.println(sum);
    }

    public long appealSum(String s) {
        int length = s.length();
        int[] arr = new int[26];
        Arrays.fill(arr, -1);
        long sum = 0;
        long item = 0;
        for (int i = 0; i < length; i++) {
            int c = s.charAt(i) - 'a';
            item += i - arr[c];
            sum += item;
            arr[c] = i;
        }
        return sum;
    }

}
