package com.lw.leetcode.arr.c;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1665. 完成所有任务的最少初始能量
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/24 14:45
 */
public class MinimumEffort {

    public static void main(String[] args) {
        MinimumEffort test = new MinimumEffort();

        //8
//        int[][] tasks = {{1,2},{2,4},{4,8}};

        // 32
//        int[][] tasks ={{1,3},{2,4},{10,11},{10,12},{8,9}};

        // 27
        int[][] tasks = {{1, 7}, {2, 8}, {3, 9}, {4, 10}, {5, 11}, {6, 12}};


        int i = test.minimumEffort(tasks);
        System.out.println(i);
    }

    public int minimumEffort(int[][] tasks) {
        Arrays.sort(tasks, (a, b) -> b[1] - b[0] == a[1] - a[0] ? Integer.compare(b[1], a[1]) : Integer.compare(b[1] - b[0], a[1] - a[0]));
        int sum = 0;
        int item = 0;
        for (int[] task : tasks) {
            if (item < task[1]) {
                sum += (task[1] - item);
                item = task[1] - task[0];
            } else {
                item -= task[0];
            }
        }
        return sum;
    }

}
