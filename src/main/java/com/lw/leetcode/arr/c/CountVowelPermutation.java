package com.lw.leetcode.arr.c;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1220. 统计元音字母序列的数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/17 12:34
 */
public class CountVowelPermutation {


    public static void main(String[] args) {
        CountVowelPermutation test = new CountVowelPermutation();

        // 19
//        int i = 3;

        // 89945857
//        int i = 1000;

        // 759959057
        int i = 20000;

        int i1 = test.countVowelPermutation(i);
        System.out.println(i1);
    }

    public int countVowelPermutation(int n) {
        long[] arr1 = new long[5];
        long[] arr2 = new long[5];
        Arrays.fill(arr1, 1);
        for (int i = 1; i < n; i++) {
            arr2[0] = (arr1[1] + arr1[2] + arr1[4]) % 1000000007;
            arr2[1] = (arr1[0] + arr1[2]) % 1000000007;
            arr2[2] = (arr1[1] + arr1[3]) % 1000000007;
            arr2[3] = arr1[2];
            arr2[4] = (arr1[3] + arr1[2]) % 1000000007;
            System.arraycopy(arr2, 0, arr1, 0, 5);
        }
        return (int) ((arr1[0] + arr1[1] + arr1[2] + arr1[3] + arr1[4]) % 1000000007);
    }

}
