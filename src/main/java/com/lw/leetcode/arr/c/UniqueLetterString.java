package com.lw.leetcode.arr.c;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 828. 统计子串中的唯一字符
 *
 * @author liw
 * @version 1.0
 * @date 2022/9/6 9:20
 */
public class UniqueLetterString {

    public static void main(String[] args) {
        UniqueLetterString test = new UniqueLetterString();

        // 10
//        String str = "ABC";

        // 8
//        String str = "ABA";

        // 92
        String str = "LEETCODE";

        //
//        String str = Utils.getStr(100000, 'A', 'Z');

        int i = test.uniqueLetterString(str);
        System.out.println(i);
    }

    public int uniqueLetterString(String s) {
        int length = s.length();
        int[] arr1 = new int[26];
        int[] arr2 = new int[26];
        Arrays.fill(arr1, -1);
        Arrays.fill(arr2, -1);
        int sum = 0;
        int item = 0;
        for (int i = 0; i < length; i++) {
            int c = s.charAt(i) - 'A';
            item -= (arr1[c] - arr2[c]);
            arr2[c] = arr1[c];
            arr1[c] = i;
            item += (arr1[c] - arr2[c]);
            sum += item;
        }
        return sum;
    }

}
