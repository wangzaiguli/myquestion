package com.lw.leetcode.arr.c;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * c
 * arr
 * 1001. 网格照明
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/8 10:47
 */
public class GridIllumination {

    int[][] dirs = new int[][]{{0, 0}, {0, -1}, {0, 1}, {-1, 0}, {-1, -1}, {-1, 1}, {1, 0}, {1, -1}, {1, 1}};

    public int[] gridIllumination(int n, int[][] lamps, int[][] queries) {
        long ln = n;
        Map<Integer, Integer> row = new HashMap<>();
        Map<Integer, Integer> col = new HashMap<>();
        Map<Integer, Integer> left = new HashMap<>();
        Map<Integer, Integer> right = new HashMap<>();
        Set<Long> set = new HashSet<>();
        for (int[] l : lamps) {
            int x = l[0];
            int y = l[1];
            if (set.contains(x * ln + y)) {
                continue;
            }
            increment(row, x);
            increment(col, y);
            increment(left, x + y);
            increment(right, x - y);
            set.add(x * ln + y);
        }
        int m = queries.length;
        int[] ans = new int[m];
        for (int i = 0; i < m; i++) {
            int[] q = queries[i];
            int x = q[0];
            int y = q[1];
            if (row.containsKey(x) || col.containsKey(y) || left.containsKey(x + y) || right.containsKey(x - y)) {
                ans[i] = 1;
            }

            for (int[] d : dirs) {
                int nx = x + d[0];
                int ny = y + d[1];
                if (nx < 0 || nx >= n || ny < 0 || ny >= n) {
                    continue;
                }
                if (set.contains(nx * ln + ny)) {
                    set.remove(nx * ln + ny);
                    decrement(row, nx);
                    decrement(col, ny);
                    decrement(left, nx + ny);
                    decrement(right, nx - ny);
                }
            }
        }
        return ans;
    }

    private void increment(Map<Integer, Integer> map, int key) {
        map.put(key, map.getOrDefault(key, 0) + 1);
    }

    private void decrement(Map<Integer, Integer> map, int key) {
        if (map.get(key) == 1) {
            map.remove(key);
        } else {
            map.put(key, map.get(key) - 1);
        }
    }


}
