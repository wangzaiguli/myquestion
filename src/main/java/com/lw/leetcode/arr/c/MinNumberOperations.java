package com.lw.leetcode.arr.c;

/**
 * Created with IntelliJ IDEA.
 * 1526. 形成目标数组的子数组最少增加次数
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/19 21:54
 */
public class MinNumberOperations {


    public static void main(String[] args) {
        MinNumberOperations test = new MinNumberOperations();

        // 3
//        int[] arr = {1, 2, 3, 2, 1};

        // 4
//        int[] arr = {3, 1, 1, 2};

        // 7
//        int[] arr = {3, 1, 5, 4, 2};

        // 1
        int[] arr = {1, 1, 1, 1};

        int i = test.minNumberOperations(arr);
        System.out.println(i);
    }

    public int minNumberOperations(int[] target) {
        int length = target.length;
        int sum = 0;
        int min = target[0];
        int max = target[0];
        for (int i = 1; i < length; i++) {
            int n = target[i];
//            if (n == max || n == min) {
//                continue;
//            }
//            if (n > max) {
//                sum += (max - min);
//                max = n;
//                min = n;
//            } else if (n < min) {
//                min = n;
//            } else {
//                sum += (max - min);
//                min = n;
//                max = n;
//            }

          if (n > min) {
                sum += (max - min);
                max = n;
            }
            min = n;
        }
        return sum + max;
    }
}
