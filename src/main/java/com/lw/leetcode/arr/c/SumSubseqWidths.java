package com.lw.leetcode.arr.c;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * c
 * arr
 * 891. 子序列宽度之和
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/17 21:03
 */
public class SumSubseqWidths {


    public static void main(String[] args) {
        SumSubseqWidths test = new SumSubseqWidths();

        // 6
//        int[] arr = {2,1,3};

        // 23
        int[] arr = {2, 1, 3, 4};

        // 0
//        int[] arr = {2};

        // 0
//        int[] arr = {2, 2};

        // 0
//        int[] arr = {2, 1};

        int i = test.sumSubseqWidths(arr);
        System.out.println(i);
    }


    public int sumSubseqWidths(int[] nums) {
        Arrays.sort(nums);
        long sum = 0L;
        int length = nums.length;
        long item = nums[0];
        long c = 1L;
        for (int i = 1; i < length; i++) {
            sum = (nums[i] * c - item + sum) % 1000000007;
            c = ((c << 1) + 1) % 1000000007;
            item = ((item << 1) + nums[i]) % 1000000007;
        }
        return (int) (sum % 1000000007);
    }

}
