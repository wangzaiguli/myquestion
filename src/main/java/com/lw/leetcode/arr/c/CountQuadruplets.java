package com.lw.leetcode.arr.c;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2552. 统计上升四元组
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/6 17:26
 */
public class CountQuadruplets {

    public static void main(String[] args) {
        CountQuadruplets test = new CountQuadruplets();

        // 2
        int[] nums = {1, 3, 2, 4, 5};

        // 0
//        int[] nums = {1, 2, 3, 4};

        // 0
//        int[] nums = {2, 5, 3, 1, 4};

        // 0
//        int[] nums = Utils.getArr(1000, 1, 1000);

        long l = test.countQuadruplets(nums);
        System.out.println(l);
    }

    public long countQuadruplets(int[] nums) {
        int length = nums.length;
        int[] arr = new int[length + 2];
        long sum = 0;
        for (int i = 1; i < length; i++) {
            int v1 = nums[i];
            long ac = 0;
            long bc = 0;
            Arrays.fill(arr, 0);
            for (int j = i + 1; j < length; j++) {
                arr[nums[j]]++;
            }
            int item = 0;
            for (int j = length; j >= 0; j--) {
                item += arr[j];
                arr[j] = item;
            }
            for (int j = 0; j < i; j++) {
                int v2 = nums[j];
                if (v1 < v2) {
                    bc = arr[v2 + 1];
                    sum += bc * ac;
                } else if (v1 > v2) {
                    ac++;
                }
            }
        }
        return sum;
    }

}
