package com.lw.leetcode.arr.c;

import com.lw.test.util.Utils;

/**
 * Created with IntelliJ IDEA.
 * 1793. 好子数组的最大分数
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/25 16:46
 */
public class MaximumScore {

    public static void main(String[] args) {
        MaximumScore test = new MaximumScore();


        // 15
//        int[] arr = {1, 4, 3, 7, 4, 5};
//        int k = 3;

        // 20
//        int[] arr = {5, 5, 4, 5, 4, 1, 1, 1};
//        int k = 0;

        //
        int[] arr = Utils.getArr(1000, 1, 1);
        int k = 400;
        System.out.println(k);

        int i = test.maximumScore(arr, k);
        System.out.println(i);
    }

    public int maximumScore(int[] nums, int k) {
        int max = nums[k];
        int i = k - 1;
        int j = k + 1;
        int length = nums.length;
        int min = nums[k];
        while (i >= 0 && j < length) {
            int a = nums[i];
            int b = nums[j];
            if (b == a) {
                i--;
                j++;
                min = Math.min(min, a);
            } else if (a > b) {
                i--;
                min = Math.min(min, a);
            } else {
                j++;
                min = Math.min(min, b);
            }
            max = Math.max(max, min * (j - i - 1));
        }
        while (i >= 0) {
            int a = nums[i];
            min = Math.min(min, a);
            i--;
            max = Math.max(max, min * (j - i - 1));
        }
        while (j < length) {
            int b = nums[j];
            j++;
            min = Math.min(min, b);
            max = Math.max(max, min * (j - i - 1));
        }
        return max;
    }

}
