package com.lw.leetcode.arr.a;

import java.util.PriorityQueue;

/**
 * 1046. 最后一块石头的重量
 *
 * @Author liw
 * @Date 2021/6/1 11:48
 * @Version 1.0
 */
public class LastStoneWeight {

    public static void main(String[] args) {
        LastStoneWeight test = new LastStoneWeight();

        int[] arr = {7,6,7,6,9};
//        int[] arr = {2};
        int i = test.lastStoneWeight(arr);
        System.out.println(i);
    }

    public int lastStoneWeight(int[] stones) {
        PriorityQueue<Integer> pq = new PriorityQueue<>((a, b) -> b - a);
        for (int i : stones) {
            pq.offer(i);
        }
        while (pq.size() > 1) {
            int a = pq.poll();
            int b = pq.poll();
            if ( a != b) {
                pq.offer(a - b);
            }
        }
        return pq.size() == 0 ? 0 : pq.poll();
    }
}
