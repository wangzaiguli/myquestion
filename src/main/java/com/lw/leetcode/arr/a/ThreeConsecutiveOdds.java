package com.lw.leetcode.arr.a;

/**
 * 1550. 存在连续三个奇数的数组
 *
 * @Author liw
 * @Date 2021/6/14 20:10
 * @Version 1.0
 */
public class ThreeConsecutiveOdds {

    public static void main(String[] args){
        ThreeConsecutiveOdds test = new ThreeConsecutiveOdds();
        int[] arr = {1,1,1};
        boolean b = test.threeConsecutiveOdds(arr);
        System.out.println(b);
    }

    public boolean threeConsecutiveOdds(int[] arr) {
        int length = arr.length;
        if (length < 3) {
            return false;
        }
        for (int i = length - 3; i >= 0; i--) {
            if ((arr[i] & 1) != 0 && (arr[i + 1] & 1) != 0 && (arr[i + 2] & 1) != 0) {
                return true;
            }
        }
        return false;
    }

}
