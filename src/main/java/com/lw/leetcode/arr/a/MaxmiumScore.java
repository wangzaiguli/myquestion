package com.lw.leetcode.arr.a;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * arr
 * LCP 40. 心算挑战
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/30 13:16
 */
public class MaxmiumScore {
    public static void main(String[] args) {
        MaxmiumScore test = new MaxmiumScore();


        int[] arr = {3, 7, 9, 15, 11, 20, 3, 13, 14, 12, 17, 19, 12, 12, 2, 14, 4, 4, 16, 8, 3, 17, 13, 1, 11, 12, 19, 7, 7, 4, 12, 14, 19, 19, 19, 8, 6, 1, 15, 4, 7, 19, 5, 17, 2, 7, 5, 5, 9, 6, 3, 19, 11, 15, 8, 17, 3, 10, 1, 2, 4, 19, 9, 14, 6, 1, 19, 16, 7, 7, 7, 14, 11, 4, 10, 3, 19, 3, 9, 4, 6, 16, 8, 6, 13, 20, 17, 4};
        int k = 40;
        int i = test.maxmiumScore(arr, k);
        System.out.println(i);
    }

    public int maxmiumScore(int[] cards, int cnt) {
        Arrays.sort(cards);
        int length = cards.length;
        int sum = 0;
        int l = length - 1;
        int a = 0;
        int b = 0;
        for (int i = 0; i < cnt; i++) {
            int v = cards[l--];
            sum += v;
            if ((v & 1) == 0) {
                a = v;
            } else {
                b = v;
            }
        }
        if ((sum & 1) == 0) {
            return sum;
        }
        int c = 0;
        int d = 0;
        for (int i = 0; i <= l; i++) {
            int v = cards[i];
            if ((v & 1) == 0) {
                c = v;
            } else {
                d = v;
            }
        }
        if ((a == 0 || d == 0) && (b == 0 || c == 0)) {
            return 0;
        }
        int item = 0;
        if (a != 0 && d != 0) {
            item = sum - a + d;
        }
        if (b != 0 && c != 0) {
            if (item == 0) {
                item = sum - b + c;
            } else {
                item = Math.max(item, sum - b + c);
            }
        }
        return item;
    }
}
