package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * LCP 06. 拿硬币
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/16 13:17
 */
public class MinCount {
    public int minCount(int[] coins) {
        int sum = 0;
        for (int n : coins) {
            sum += (n + 1) >> 1;
        }
        return sum;
    }
}
