package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 2341. 数组能形成多少数对
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/18 10:48
 */
public class NumberOfPairs {

    public int[] numberOfPairs(int[] nums) {
        int a = 0;
        int b = 0;
        int[] arr = new int[101];
        for (int num : nums) {
            arr[num]++;
        }
        for (int i = 0; i < 101; i++) {
            int c = arr[i];
            a += (c >> 1);
            b += (c & 1);
        }
        return new int[]{a, b};
    }

}
