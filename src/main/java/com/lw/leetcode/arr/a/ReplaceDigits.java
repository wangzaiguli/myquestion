package com.lw.leetcode.arr.a;

/**
 * 1844. 将所有数字用字符替换
 *
 * @Author liw
 * @Date 2021/6/3 12:51
 * @Version 1.0
 */
public class ReplaceDigits {
    public String replaceDigits(String s) {
        StringBuilder sb = new StringBuilder();
        int length = s.length();
        for (int i = 0; i < length; i += 2) {
            sb.append(s.charAt(i));
            if (i < length - 1) {
                char c = (char) (s.charAt(i) + Integer.parseInt(String.valueOf(s.charAt(i + 1))));
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public String replaceDigits2(String s) {
        StringBuilder sb = new StringBuilder();
        int length = s.length();
        int l = length;
        if ((length & 1) != 0) {
            length--;
        }
        for (int i = 0; i < length; i += 2) {
            sb.append(s.charAt(i));
            sb.append((char) (s.charAt(i) + s.charAt(i + 1) - 48));
        }
        if (length != l) {
            sb.append(s.charAt(length));
        }
        return sb.toString();
    }
}
