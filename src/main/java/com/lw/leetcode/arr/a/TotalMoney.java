package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/28 12:04
 */
public class TotalMoney {

    public static void main(String[] args) {
        TotalMoney test = new TotalMoney();
        int i = test.totalMoney(1000);
        System.out.println(i);
    }

    public int totalMoney(int n) {
        int week = n / 7;
        int day = n % 7;
        int sum = 0;
        if (week != 0) {
            sum = (week - 1) * week * 7 / 2 + week * 28;
        }
        sum += day * week + (1 + day) * day / 2;
        return sum;
    }

}
