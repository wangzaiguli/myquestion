package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1128. 等价多米诺骨牌对的数量
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/21 16:32
 */
public class NumEquivDominoPairs {
    public int numEquivDominoPairs(int[][] dominoes) {
        int res = 0;
        int[] log = new int[100];

        for (int[] a : dominoes) {
            int sum = a[0] > a[1] ? a[0] * 10 + a[1] : a[1] * 10 + a[0];
            res += log[sum];
            log[sum]++;
        }
        return res;
    }
}
