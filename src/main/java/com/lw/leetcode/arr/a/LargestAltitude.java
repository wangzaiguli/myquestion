package com.lw.leetcode.arr.a;

/**
 * arr
 * 1732. 找到最高海拔
 *
 * @Author liw
 * @Date 2021/6/18 17:12
 * @Version 1.0
 */
public class LargestAltitude {
    public int largestAltitude(int[] gain) {
        int max = 0;
        int sum = 0;
        for (int i : gain) {
            sum += i;
            max = Math.max(max, sum);
        }
        return max;
    }
}
