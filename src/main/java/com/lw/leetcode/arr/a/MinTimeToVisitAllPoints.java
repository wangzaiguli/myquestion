package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1266. 访问所有点的最小时间
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/14 15:37
 */
public class MinTimeToVisitAllPoints {
    public int minTimeToVisitAllPoints(int[][] points) {
        int length = points.length;
        int result = 0;
        for (int i = 1; i < length; i++) {
            result += Math.max(Math.abs(points[i - 1][0] - points[i][0]), Math.abs(points[i - 1][1] - points[i][1]));
        }
        return result;
    }
}
