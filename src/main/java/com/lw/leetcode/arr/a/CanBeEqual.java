package com.lw.leetcode.arr.a;

/**
 * arr
 * a
 * 1460. 通过翻转子数组使两个数组相等
 *
 * @Author liw
 * @Date 2021/11/6 22:02
 * @Version 1.0
 */
public class CanBeEqual {
    public boolean canBeEqual(int[] target, int[] arr) {
        if (target.length != arr.length) {
            return false;
        }
        int[] num = new int[1001];
        for (int i = 0; i < target.length; i++) {
            num[target[i]]++;
            num[arr[i]]--;
        }
        for (int i : num) {
            if (i != 0) {
                return false;
            }
        }
        return true;
    }
}
