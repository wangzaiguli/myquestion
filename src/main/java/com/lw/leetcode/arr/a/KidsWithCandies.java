package com.lw.leetcode.arr.a;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 1431. 拥有最多糖果的孩子
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/15 11:52
 */
public class KidsWithCandies {
    public List<Boolean> kidsWithCandies(int[] candies, int extraCandies) {
        int length = candies.length;
        List<Boolean> ans = new ArrayList<>(length);
        int max = candies[0];
        int i;
        for (i = 1; i < length; i++) {
            if (candies[i] > max) {
                max = candies[i];
            }
        }
        for (i = 0; i < length; i++) {
            ans.add(max - candies[i] <= extraCandies);
        }
        return ans;
    }
}
