package com.lw.leetcode.arr.a;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * 2154. 将找到的值乘以 2
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/2 15:59
 */
public class FindFinalValue {

    public int findFinalValue(int[] nums, int original) {
        Set<Integer> uset = new HashSet<>();
        for (int x : nums) {
            uset.add(x);
        }
        int x = original;
        while (uset.contains(x)) {
            x <<= 1;
        }
        return x;
    }

}
