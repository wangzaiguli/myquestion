package com.lw.leetcode.arr.a;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2475. 数组中不等三元组的数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/11/22 14:47
 */
public class UnequalTriplets {

    public static void main(String[] args) {
        UnequalTriplets test = new UnequalTriplets();

        // 3
//        int[] nums = {4, 4, 2, 4, 3};

        // 0
        int[] nums = {1, 1, 1, 1, 1};

        int i = test.unequalTriplets(nums);
        System.out.println(i);
    }

    public int unequalTriplets(int[] nums) {
        Arrays.sort(nums);
        int length = nums.length - 1;
        int count = 1;
        int sum = 0;
        for (int i = 0; i < length; i++) {
            if (nums[i] != nums[i + 1]) {
                sum += (i - count + 1) * count * (length - i);
                count = 1;
            } else {
                count++;
            }
        }
        return sum;
    }

}
