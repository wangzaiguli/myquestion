package com.lw.leetcode.arr.a;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * arr
 * 888. 公平的糖果棒交换
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/29 13:49
 */
public class FairCandySwap {

    public int[] fairCandySwap(int[] aliceSizes, int[] bobSizes) {
        Set<Integer> set = new HashSet<>(bobSizes.length);
        int a = 0;
        for (int aliceSize : aliceSizes) {
            a += aliceSize;
        }
        int sum = a;
        for (int bobSize : bobSizes) {
            sum += bobSize;
            set.add(bobSize);
        }
        sum = (sum >> 1) - a;
        for (int aliceSize : aliceSizes) {
            if (set.contains(aliceSize + sum)) {
                return new int[]{aliceSize, aliceSize + sum};
            }
        }
        return null;
    }

}
