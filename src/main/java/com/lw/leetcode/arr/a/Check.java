package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1752. 检查数组是否经排序和轮转得到
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/23 12:55
 */
public class Check {
    public boolean check(int[] nums) {
        int length = nums.length;
        int flag = nums[length - 1] <= nums[0] ? 1 : 0;
        for (int i = 1; i < length; i++) {
            if (flag == -1) {
                return false;
            }
            if (nums[i] < nums[i - 1]) {
                flag--;
            }
        }
        return flag != -1;
    }
}
