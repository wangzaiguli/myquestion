package com.lw.leetcode.arr.a;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * arr
 * 1078. Bigram 分词
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/21 16:01
 */
public class FindOcurrences {

    public String[] findOcurrences(String text, String first, String second) {
        List<String> list = new ArrayList<>();
        String[] words = text.split(" ");
        for (int i = 0; i < words.length - 2; i++) {
            if (words[i].equals(first) && words[i + 1].equals(second)) {
                list.add(words[i + 2]);
            }
        }
        return list.toArray(new String[0]);
    }

}
