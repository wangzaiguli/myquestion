package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 6124. 第一个出现两次的字母
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/24 12:52
 */
public class RepeatedCharacter {

    public char repeatedCharacter(String s) {
        int[] arr = new int[128];
        int length = s.length();
        for (int i = 0; i < length; i++) {
            char c = s.charAt(i);
            arr[c]++;
            if (arr[c] == 2) {
                return c;
            }
        }
        return ' ';
    }

}
