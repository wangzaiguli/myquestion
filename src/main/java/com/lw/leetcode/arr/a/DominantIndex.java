package com.lw.leetcode.arr.a;

/**
 * arr
 * 747. 至少是其他数字两倍的最大数
 *
 * @Author liw
 * @Date 2021/7/10 17:44
 * @Version 1.0
 */
public class DominantIndex {
    public int dominantIndex(int[] nums) {
        int max = 0, idx = 0, less = 1;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > max) {
                less = max;
                max = nums[i];
                idx = i;
            } else if (nums[i] > less) {
                less = nums[i];
            }
        }
        return max >= (less << 1) ? idx : -1;
    }
}
