package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 1629. 按键持续时间最长的键
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/9 15:15
 */
public class SlowestKey {
    public char slowestKey(int[] rt, String kp) {
        int n = rt.length;
        int idx = 0;
        int max = rt[0];
        for (int i = 1; i < n; i++) {
            int cur = rt[i] - rt[i - 1];
            if (cur > max) {
                idx = i;
                max = cur;
            } else if (cur == max && kp.charAt(i) > kp.charAt(idx)) {
                idx = i;
            }
        }
        return kp.charAt(idx);
    }

}
