package com.lw.leetcode.arr.a;

/**
 * arr
 * 657. 机器人能否返回原点
 *
 * @Author liw
 * @Date 2021/6/7 15:48
 * @Version 1.0
 */
public class JudgeCircle {
    public boolean judgeCircle(String moves) {
        int col = 0;
        int row = 0;
        for (char ch : moves.toCharArray()) {
            if (ch == 'U') {
                row++;
            } else if (ch == 'D') {
                row--;
            } else if (ch == 'L') {
                col--;
            } else {
                col++;
            }
        }
        return col == 0 && row == 0;
    }
}
