package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * a
 * arr
 * 1822. 数组元素积的符号
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/18 14:40
 */
public class ArraySign {

    public int arraySign(int[] nums) {
        int temp = 0;
        for (int num : nums) {
            if (num < 0) {
                temp++;
            }
            if (num == 0) {
                return 0;
            }
        }
        if ((temp & 1) == 0) {
            return 1;
        }
        return -1;
    }

}
