package com.lw.leetcode.arr.a;

/**
 * 896. 单调数列
 *
 * @Author liw
 * @Date 2021/10/5 20:56
 * @Version 1.0
 */
public class IsMonotonic {
    public boolean isMonotonic(int[] nums) {
        int length = nums.length;
        if (length < 3) {
            return true;
        }
        boolean flag = true;
        for (int i = 1; i < length; i++) {
            if (nums[i] < nums[i - 1]) {
                flag = false;
                break;
            }
        }
        if (flag) {
            return flag;
        }
        flag = true;
        for (int i = 1; i < length; i++) {
            if (nums[i] > nums[i - 1]) {
                flag = false;
                break;
            }
        }
        return flag;
    }
}
