package com.lw.leetcode.arr.a;

/**
 * arr
 * 1528. 重新排列字符串
 *
 * @Author liw
 * @Date 2021/6/18 16:36
 * @Version 1.0
 */
public class RestoreString {
    public String restoreString(String s, int[] indices) {
        int length = s.length();
        char[] chars = new char[length];
        for (int i = 0; i < length; i++) {
            chars[indices[i]] = s.charAt(i);
        }
        return new String(chars);
    }
}
