package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 1909. 删除一个元素使数组严格递增
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/30 14:14
 */
public class CanBeIncreasing {

    public static void main(String[] args) {
        CanBeIncreasing test = new CanBeIncreasing();
        int[] arr = {2, 3, 1, 2};
        boolean b = test.canBeIncreasing(arr);
        System.out.println(b);
    }


    public boolean canBeIncreasing(int[] nums) {
        int length = nums.length;
        if (length < 3) {
            return true;
        }
        int a = nums[0];
        int b = nums[1];
        int count = 0;
        if (a >= b) {
            count++;
        }
        for (int i = 2; i < length; i++) {
            int num = nums[i];
            if (num <= b) {
                count++;
                if (count > 1) {
                    return false;
                }
                if (num > a) {
                    b = num;
                }
            } else {
                a = b;
                b = num;
            }
        }
        return true;
    }

}
