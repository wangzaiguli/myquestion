package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * arr
 * 剑指 Offer 62. 圆圈中最后剩下的数字
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/29 16:20
 */
public class LastRemaining {
    public int lastRemaining(int n, int m) {
        int[] data = new int[n];
        for (int i = 0; i < n; i++) {
            data[i] = i;
        }
        for (int i = 0; n > 1; --n) {
            i += m - 1;
            i %= n;
            System.arraycopy(data, i + 1, data, i, n - i - 1);
        }
        return data[0];
    }
}
