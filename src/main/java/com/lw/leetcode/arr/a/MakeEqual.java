package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1897. 重新分配字符使所有字符串都相等
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/3 11:06
 */
public class MakeEqual {

    public boolean makeEqual(String[] words) {
        int[] arr = new int[26];
        for (String word : words) {
            for (int i = word.length() - 1; i >= 0; i--) {
                arr[word.charAt(i) - 'a']++;
            }
        }
        int length = words.length;
        for (int i : arr) {
            if (i % length != 0) {
                return false;
            }
        }
        return true;
    }

}
