package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1217. 玩筹码
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/23 22:24
 */
public class MinCostToMoveChips {
    public int minCostToMoveChips(int[] position) {
        int count1 = 0;
        for (int i : position) {
            count1 += i & 1;
        }
        int count0 = position.length - count1;
        return count0 > count1 ? count1 : count0;
    }

}
