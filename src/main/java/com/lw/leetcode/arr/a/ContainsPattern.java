package com.lw.leetcode.arr.a;

/**1566. 重复至少 K 次且长度为 M 的模式
 * @Author liw
 * @Date 2021/10/1 20:56
 * @Version 1.0
 */
public class ContainsPattern {

    public static void main(String[] args){
        ContainsPattern test = new ContainsPattern();
        int[] arr = {2,2};
        boolean b = test.containsPattern(arr, 1, 2);
        System.out.println(b);
    }

    public boolean containsPattern(int[] arr, int m, int k) {
        int length = arr.length;
        int v = m * k;
        a: for (int i = 0; i < length- v + 1 ; i++) {
            for (int j = 0; j < m; j++) {
                int item = arr[i + j];
                for (int n = 1; n < k; n++) {
                    if (arr[i + j + n * m] != item) {
                     continue a;
                    }
                }
            }
            return true;
        }
        return false;
    }

}
