package com.lw.leetcode.arr.a;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * arr
 * 1260. 二维网格迁移
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/14 16:03
 */
public class ShiftGrid {
    public List<List<Integer>> shiftGrid(int[][] grid, int k) {

        int m = grid.length;
        int n = grid[0].length;
        int l = n * m;
        k %= l;

        List<List<Integer>> lists = new ArrayList<>(m);
        int[] nums = new int[l];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                k %= l;
                nums[k++] = grid[i][j];
            }
        }
        k = 0;
        for (int i = 0; i < m; i++) {
            List<Integer> tempList = new ArrayList<>(n);
            for (int j = 0; j < n; j++) {
                tempList.add(nums[k++]);
            }
            lists.add(tempList);
        }
        return lists;
    }
}
