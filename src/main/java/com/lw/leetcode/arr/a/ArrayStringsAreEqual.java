package com.lw.leetcode.arr.a;

import com.lw.leetcode.tree.TreeNode;

/**
 * Created with IntelliJ IDEA.
 * a
 * arr
 * 1662. 检查两个字符串数组是否相等
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/18 14:51
 */
public class ArrayStringsAreEqual {
    public boolean arrayStringsAreEqual(String[] word1, String[] word2) {

        StringBuilder stringBuilder1 = new StringBuilder();
        StringBuilder stringBuilder2 = new StringBuilder();

        for (String s : word1) {
            stringBuilder1.append(s);
        }

        for (String s : word2) {
            stringBuilder2.append(s);
        }

        String s1 = new String(stringBuilder1);
        String s2 = new String(stringBuilder2);

        return s1.equals(s2);
    }


}
