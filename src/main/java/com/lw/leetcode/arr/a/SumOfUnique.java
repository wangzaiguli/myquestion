package com.lw.leetcode.arr.a;

/**
 * 1748. 唯一元素的和
 *
 * @Author liw
 * @Date 2021/6/7 15:23
 * @Version 1.0
 */
public class SumOfUnique {
    public int sumOfUnique(int[] nums) {
        int[] arr = new int[101];
        for (int num : nums) {
            arr[num]++;
        }
        int sum = 0;
        for (int i = 1; i < 101; i++) {
            if (arr[i] == 1) {
                sum += i;
            }
        }
        return sum;
    }
}
