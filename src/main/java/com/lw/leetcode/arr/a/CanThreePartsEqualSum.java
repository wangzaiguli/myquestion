package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/23 21:59
 */
public class CanThreePartsEqualSum {


    public static void main(String[] args) {
        CanThreePartsEqualSum test = new CanThreePartsEqualSum();
        int[] arr = {18,12,-18,18,-19,-1,10,10};
        boolean b = test.canThreePartsEqualSum(arr);
        System.out.println(b);
    }

    public boolean canThreePartsEqualSum(int[] arr) {
        int s = 0;
        for (int num : arr) {
            s += num;
        }
        if (s % 3 != 0) {
            return false;
        }
        int target = s / 3;
        int n = arr.length;
        int i = 0;
        int cur = 0;
        while (i < n) {
            cur += arr[i];
            if (cur == target) {
                break;
            }
            ++i;
        }
        if (cur != target) {
            return false;
        }
        int j = i + 1;
        cur = 0;
        while (j + 1 < n) {
            cur += arr[j];
            if (cur == target) {
                return true;
            }
            ++j;
        }
        return false;
    }


}
