package com.lw.leetcode.arr.a;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * 2062. 统计字符串中的元音子字符串
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/28 10:58
 */
public class CountVowelSubstrings {

    public int countVowelSubstrings(String word) {
        int cnt = 0;
        int l = word.length();
        if (l < 5) {
            return 0;
        }
        for (int i = 0; i < l; i++) {
            Set<Character> character = new HashSet<>();
            for (int j = i; j < l; j++) {
                if (!vowel(word.charAt(j))) {
                    break;
                }
                character.add(word.charAt(j));
                if (character.size() == 5) {
                    cnt++;
                }
            }
        }
        return cnt;
    }

    private boolean vowel(char ch) {
        return ch == 'a' || ch == 'e' || ch == 'i'
                || ch == 'o' || ch == 'u';
    }
}
