package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 2455. 可被三整除的偶数的平均值
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/31 11:05
 */
public class AverageValue {

    public int averageValue(int[] nums) {
        int res = 0;
        int n = 0;
        for (int num : nums) {
            if (num % 6 == 0) {
                res += num;
                n++;
            }
        }
        if (n == 0) {
            return 0;
        }
        return res / n;
    }

}
