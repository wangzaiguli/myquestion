package com.lw.leetcode.arr.a;

/**
 * 1893. 检查是否区域内所有整数都被覆盖
 * Created with IntelliJ IDEA.
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/23 9:38
 */
public class IsCovered {

    public static void main(String[] args) {
        IsCovered test = new IsCovered();

        //  [[1,2],[3,4],[5,6]], left = 2, right = 5
//        int[][] arr = {{1, 2}, {3, 4}, {5, 6}};
//        int left = 2;
//        int right = 6;
//        int[][] arr = {{25,42},{7,14},{2,32},{25,28},{39,49},{1,50},{29,45},{18,47}};
//        int left = 15;
//        int right = 38;

        //  [[1,10],[10,20]], left = 21, right = 21
//        int[][] arr = {{1,10},{10,20}};
//        int left = 21;
//        int right = 21;

        int[][] arr = {{1, 1}, {3, 3}};
        int left = 3;
        int right = 3;

        boolean covered = test.isCovered(arr, left, right);
        System.out.println(covered);
    }

    public boolean isCovered(int[][] ranges, int left, int right) {
        int[] arr = new int[51];
        int st = 51;
        for (int[] range : ranges) {
            arr[range[0]] = Math.max(range[1] + 1, arr[range[0]]);
            st = Math.min(st, range[0]);
        }
        if (st > left) {
            return false;
        }
        int l = arr[st];
        for (int i = st; i <= 50; i++) {
            if (i > l && i > left) {
                break;
            }
            l = Math.max(l, arr[i]);
        }
        return l > right;
    }

}
