package com.lw.leetcode.arr.a;

/**
 * arr
 * 1021. 删除最外层的括号
 *
 * @Author liw
 * @Date 2021/6/7 15:28
 * @Version 1.0
 */
public class RemoveOuterParentheses {

    public static void main(String[] args) {
        RemoveOuterParentheses test = new RemoveOuterParentheses();
//        String str = "(()())(())";
//        String str = "(()())(())(()(()))";
        String str = "()()";
        String s = test.removeOuterParentheses(str);
        System.out.println(s);
    }

    public String removeOuterParentheses(String s) {
        int length = s.length();
        if (length < 4) {
            return "";
        }
        int count = 0;
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            char c = s.charAt(i);
            if (c == '(') {
                if (count != 0) {
                    sb.append(c);
                }
                count++;
            } else {
                count--;
                if (count != 0) {
                    sb.append(c);
                }
            }
        }
        return sb.toString();
    }
}
