package com.lw.leetcode.arr.a;

/**
 * arr
 * 剑指 Offer 17. 打印从1到最大的n位数
 *
 * @Author liw
 * @Date 2021/6/18 16:17
 * @Version 1.0
 */
public class PrintNumbers {
    public int[] printNumbers(int n) {
        int m = (int) Math.pow(10, n) - 1;
        int[] arr = new int[m];
        for (int i = 0; i < m; i++) {
            arr[i] = i + 1;
        }
        return arr;
    }
}
