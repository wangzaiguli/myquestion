package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 27. 移除元素
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/12 11:55
 */
public class RemoveElement {
    public int removeElement(int[] nums, int val) {
        int j = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[j] = nums[i];
                j++;
            }
        }
        return j;
    }
}
