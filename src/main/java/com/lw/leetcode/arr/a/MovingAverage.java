package com.lw.leetcode.arr.a;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * a
 * arr
 * 剑指 Offer II 041. 滑动窗口的平均值
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/18 13:49
 */
public class MovingAverage {

    private Queue<Integer> queue;
    private int size;
    private double sum;

    public MovingAverage(int size) {
        queue = new LinkedList<>();
        this.size = size;
        sum = 0;
    }

    public double next(int val) {
        sum += val;
        if (queue.size() == size) {
            sum -= queue.poll();
        }
        queue.offer(val);
        return sum / queue.size();
    }

}
