package com.lw.leetcode.arr.a;

/**
 * 1184. 公交站间的距离
 *
 * @Author liw
 * @Date 2021/10/30 20:42
 * @Version 1.0
 */
public class DistanceBetweenBusStops {

    public int distanceBetweenBusStops(int[] distance, int start, int destination) {
        int d1 = 0;
        int d2 = 0;
        int l = Math.min(start, destination);
        int r = Math.max(start, destination);
        for (int i = 0; i < distance.length; i++) {
            if (i >= l && i < r) {
                d1 += distance[i];
            } else {
                d2 += distance[i];
            }
        }
        return d1 < d2 ? d1 : d2;
    }

}
