package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/6 12:04
 */
public class DivingBoard {
    public int[] divingBoard(int shorter, int longer, int k) {
        if (k == 0) {
            return new int[0];
        }
        int c = longer - shorter;
        if (c == 0) {
            return new int[] {shorter * k};
        }
        int[] arr = new int[k + 1];
        arr[0] = shorter * k;
        for (int i = 1; i <= k; i++) {
            arr[i] = arr[i - 1] + c;
        }
        return arr;
    }
}
