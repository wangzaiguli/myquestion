package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 997. 找到小镇的法官
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/19 16:13
 */
public class FindJudge {

    public static void main(String[] args) {
        FindJudge test = new FindJudge();

        // 3
        int[][] arr = {{1,3},{1,4},{2,3},{2,4},{4,3}};
        int n = 4;

        int judge = test.findJudge(n, arr);
        System.out.println(judge);
    }

    public int findJudge(int n, int[][] trust) {
        if (n == 1 && trust.length == 0) {
            return 1;
        }
        int[] arr = new int[n + 1];
        for (int[] ints : trust) {
            arr[ints[0]]++;
            arr[ints[1]] += 0X1000;
        }
        for (int i = 0; i <= n ;i++) {
            int v = arr[i];
            if ((v & 0Xfff) == 0 && ( v >> 12) == n - 1) {
                return i;
            }
        }
        return -1;
    }

}
