package com.lw.leetcode.arr.a;

/**
 * arr
 * 1827. 最少操作使数组递增
 *
 * @Author liw
 * @Date 2021/6/18 17:16
 * @Version 1.0
 */
public class MinOperations2 {
    public int minOperations(int[] nums) {
        int sum = 0;
        int length = nums.length;
        for (int i = 1; i < length; i++) {
            if (nums[i - 1] >= nums[i]) {
                sum += nums[i - 1] + 1 - nums[i];
                nums[i] = nums[i - 1] + 1;
            }
        }
        return sum;
    }
}
