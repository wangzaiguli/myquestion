package com.lw.leetcode.arr.a;

/**
 * arr
 * 495. 提莫攻击
 *
 * @Author liw
 * @Date 2021/6/28 11:50
 * @Version 1.0
 */
public class FindPoisonedDuration {
    public int findPoisonedDuration(int[] timeSeries, int duration) {
        if (timeSeries == null || timeSeries.length == 0) {
            return 0;
        }
        int length = timeSeries.length;
        int sum = 0;
        for (int i = 1; i < length; i++) {
            if (timeSeries[i] - timeSeries[i - 1] >= duration) {
                sum += duration;
            } else {
                sum += timeSeries[i] - timeSeries[i - 1];
            }
        }
        return sum + duration;
    }
}
