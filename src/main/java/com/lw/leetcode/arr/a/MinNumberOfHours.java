package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 6152. 赢得比赛需要的最少训练时长
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/21 17:26
 */
public class MinNumberOfHours {

    public int minNumberOfHours(int initialEnergy, int initialExperience, int[] energy, int[] experience) {
        int sum = 1;
        for (int i : energy) {
            sum += i;
        }
        int length = energy.length;
        int v = experience[length - 1] + 1;
        for (int i = length - 2; i >= 0; i--) {
            v = Math.max(experience[i] + 1, v - experience[i]);
        }
        int t = 0;
        if (initialEnergy < sum) {
            t += (sum - initialEnergy);
        }
        if (initialExperience < v) {
            t += (v - initialExperience);
        }
        return t;
    }

}
