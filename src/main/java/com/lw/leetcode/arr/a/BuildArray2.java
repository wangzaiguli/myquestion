package com.lw.leetcode.arr.a;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1441. 用栈操作构建数组
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/24 14:12
 */
public class BuildArray2 {

    public List<String> buildArray(int[] target, int n) {
        int t = 1;
        List<String> list = new ArrayList<>();
        for (int i : target) {
            if (i != t) {
                for (int j = i - t; j > 0; j--) {
                    list.add("Push");
                    list.add("Pop");
                }
            }
            list.add("Push");
            t = i + 1;
        }
        return list;
    }

}
