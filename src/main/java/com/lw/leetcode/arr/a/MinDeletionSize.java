package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 944. 删列造序
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/23 12:46♥♠
 */
public class MinDeletionSize {
    public int minDeletionSize(String[] strs) {
        int count = 0;
        int a = strs.length;
        int b = strs[0].length();
        for (int i = 0; i < b; i++) {
            for (int j = 1; j < a; j++) {
                if (strs[j].charAt(i) < strs[j - 1].charAt(i)) {
                    count += 1;
                    break;
                }
            }
        }
        return count;
    }
}
