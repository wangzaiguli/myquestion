package com.lw.leetcode.arr.a;

import java.util.Arrays;
import java.util.List;

/**
 * 1507. 转变日期格式
 *
 * @Author liw
 * @Date 2021/6/15 20:35
 * @Version 1.0
 */
public class ReformatDate {
    private final List<String> days = Arrays.asList(
            "1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th", "9th", "10th",
            "11th", "12th", "13th", "14th", "15th", "16th", "17th", "18th", "19th",
            "20th", "21st", "22nd", "23rd", "24th", "25th", "26th", "27th", "28th",
            "29th", "30th", "31st"
    );

    private final List<String> months = Arrays.asList(
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    );

    public String reformatDate(String date) {
        String[] dates = date.split(" ");
        String year = dates[2];
        int month = months.indexOf(dates[1]) + 1;
        int day = days.indexOf(dates[0]) + 1;
        return String.format("%s-%02d-%02d", year, month, day);
    }
}
