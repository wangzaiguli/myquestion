package com.lw.leetcode.arr.a;

import java.util.Arrays;

/**
 * 1720. 解码异或后的数组
 *
 * @Author liw
 * @Date 2021/5/6 9:46
 * @Version 1.0
 */
public class Decode {

    public static void main(String[] args) {
        Decode test = new Decode();
        int[] encoded = {6,2,7,3};
        int first = 4;
        int[] decode = test.decode(encoded, first);
        System.out.println(Arrays.toString(decode));
    }

    public int[] decode(int[] encoded, int first) {
        int length = encoded.length;
        int[] arr = new int[length + 1];
        arr[0] = first;
        for (int i = 0; i < length; i++) {
            arr[i + 1] = arr[i] ^ encoded[i];
        }
        return arr;
    }

}
