package com.lw.leetcode.arr.a;

/**
 * arr
 * 1450. 在既定时间做作业的学生人数
 *
 * @Author liw
 * @Date 2021/6/18 16:51
 * @Version 1.0
 */
public class BusyStudent {
    public int busyStudent(int[] startTime, int[] endTime, int queryTime) {
        int count = 0;
        for (int i = startTime.length - 1; i >= 0; i--) {
            if (startTime[i] <= queryTime && endTime[i] >= queryTime) {
                count++;
            }
        }
        return count;
    }
}
