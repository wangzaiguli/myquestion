package com.lw.leetcode.arr.a;

/**
 * 1725. 可以形成最大正方形的矩形数目
 *
 * @Author liw
 * @Date 2021/6/18 16:39
 * @Version 1.0
 */
public class CountGoodRectangles {
    public int countGoodRectangles(int[][] rectangles) {
        int res = 1;
        int cur = 0;
        int length = rectangles.length;
        for (int i = 0; i < length; i++) {
            int temp = Math.min(rectangles[i][0], rectangles[i][1]);
            if (temp > cur) {
                res = 1;
                cur = temp;
            } else if (temp == cur) {
                res++;
            }
        }
        return res;
    }
}
