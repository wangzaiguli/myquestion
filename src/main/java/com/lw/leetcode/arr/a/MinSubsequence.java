package com.lw.leetcode.arr.a;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 1403. 非递增顺序的最小子序列
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/24 13:38
 */
public class MinSubsequence {
    public List<Integer> minSubsequence(int[] nums) {
        List<Integer> list = new ArrayList<>();
        Arrays.sort(nums);
        int sum = Arrays.stream(nums).sum();
        int count = 0;
        for (int i = nums.length - 1; i >= 0; i--) {
            list.add(nums[i]);
            count += nums[i];
            sum -= nums[i];
            if (count > sum) {
                return list;
            }
        }
        return list;
    }
}
