package com.lw.leetcode.arr.a;

/**
 * 1480. 一维数组的动态和
 *
 * @Author liw
 * @Date 2021/8/28 10:36
 * @Version 1.0
 */
public class RunningSum {
    public int[] runningSum(int[] nums) {
        int length = nums.length;
        for (int i = 1; i < length; i++) {
            nums[i] += nums[i - 1];
        }
        return nums;
    }
}
