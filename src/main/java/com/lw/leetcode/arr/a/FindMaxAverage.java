package com.lw.leetcode.arr.a;


/**
 * arr
 * 643. 子数组最大平均数 I
 *
 * @Author liw
 * @Date 2021/7/5 13:39
 * @Version 1.0
 */
public class FindMaxAverage {

    public double findMaxAverage(int[] nums, int k) {
        double maxn = 0;
        for (int i = 0; i < k; i++) {
            maxn += nums[i];
        }
        int length = nums.length;
        double tmp = maxn;
        for (int i = k; i < length; i++) {
            tmp = tmp + nums[i] - nums[i - k];
            maxn = Math.max(tmp, maxn);
        }
        return maxn / k;
    }
}
