package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 1672. 最富有客户的资产总量
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/15 11:45
 */
public class MaximumWealth {

    public int maximumWealth(int[][] accounts) {
        int ans = 0;
        for (int[] account : accounts) {
            int sum = 0;
            for (int i : account) {
                sum += i;
            }
            ans = Math.max(ans, sum);
        }
        return ans;
    }

}
