package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1903. 字符串中的最大奇数
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/3 11:15
 */
public class LargestOddNumber {

    public String largestOddNumber(String num) {
        for (int i = num.length() - 1; i >= 0; i--) {
            if ((num.charAt(i) & 1) == 1) {
                return num.substring(0, i + 1);
            }
        }
        return "";
    }

}
