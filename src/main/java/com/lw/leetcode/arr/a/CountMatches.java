package com.lw.leetcode.arr.a;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 1773. 统计匹配检索规则的物品数量
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/16 13:09
 */
public class CountMatches {

    public int countMatches(List<List<String>> items, String ruleKey, String ruleValue) {
        int a = 2;
        int count = 0;
        if ("type".equals(ruleKey)) {
            a = 0;
        } else if ("color".equals(ruleKey)) {
            a = 1;
        }
        for (List<String> item : items) {
            if (ruleValue.equals(item.get(a))) {
                count++;
            }
        }
        return count;
    }


}
