package com.lw.leetcode.arr.a;

/**
 * 812. 最大三角形面积
 *
 * @Author liw
 * @Date 2021/7/9 11:26
 * @Version 1.0
 */
public class LargestTriangleArea {
    public double largestTriangleArea(int[][] points) {
        float area = 0;
        float max = 0;
        int n = points.length;
        for (int i = 0; i < n - 2; i++) {
            for (int j = i + 1; j < n - 1; j++) {
                for (int k = j + 1; k < n; k++) {
                    // S=(x1y2+x2y3+x3y1-x1y3-x2y1-x3y2) /2;
                    area = ((float) points[i][0] * (float) points[j][1] +
                            (float) points[j][0] * (float) points[k][1] +
                            (float) points[k][0] * (float) points[i][1] -
                            (float) points[i][0] * (float) points[k][1] -
                            (float) points[j][0] * (float) points[i][1] -
                            (float) points[k][0] * (float) points[j][1]);

                    area = Math.abs(area);
                    if (area > max) {
                        max = area;
                    }
                }
            }
        }
        return max / 2.0f;
    }
}
