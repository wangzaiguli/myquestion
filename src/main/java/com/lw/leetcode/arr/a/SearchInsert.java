package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 35. 搜索插入位置
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/25 14:12
 */
public class SearchInsert {
    public int searchInsert(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] >= target) {
                return i;
            }
        }
        return nums.length;
    }
}
