package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1304. 和为零的N个唯一整数
 *
 * @author liw
 * @version 1.0
 * @date 2021/11/19 15:58
 */
public class SumZero {

    public int[] sumZero(int n) {
        int[] arr = new int[n];
        int l = n >> 1;
        for (int i = 0; i < l; i++) {
            arr[i] = i + 1;
            arr[n - 1 - i] = -(i + 1);
        }
        if ((n & 1) == 1) {
            arr[l] = 0;
        }
        return arr;
    }

}
