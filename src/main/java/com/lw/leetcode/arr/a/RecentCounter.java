package com.lw.leetcode.arr.a;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * 剑指 Offer II 042. 最近请求次数
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/15 11:43
 */
public class RecentCounter {
    private Deque<Integer> q;

    public RecentCounter() {
        q = new LinkedList<>();
    }

    public int ping(int t) {
        q.offerLast(t);
        while (q.peekFirst() < t - 3000) {
            q.pollFirst();
        }
        return q.size();
    }


}
