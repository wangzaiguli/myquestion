package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1413. 逐步求和得到正数的最小值
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/24 13:46
 */
public class MinStartValue {

    public int minStartValue(int[] nums) {
        int now = 0;
        int min = 0;
        for (int n : nums) {
            now += n;
            min = Math.min(min, now);
        }
        return 1 - min;
    }

}
