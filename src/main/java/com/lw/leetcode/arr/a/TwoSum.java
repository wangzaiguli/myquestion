package com.lw.leetcode.arr.a;

/**
 * arr
 * 167. 两数之和 II - 输入有序数组
 * 剑指 Offer II 006. 排序数组中两个数字之和
 * 剑指 Offer 57. 和为s的两个数字
 *
 * @Author liw
 * @Date 2021/8/4 21:41
 * @Version 1.0
 */
public class TwoSum {
    public int[] twoSum(int[] numbers, int target) {
        int st = 0;
        int end = numbers.length - 1;
        while (st < end) {
            int sum = numbers[st] + numbers[end];
            if (sum == target) {
                return new int[]{st + 1, end + 1};
            }
            if (sum < target) {
                st++;
            } else {
                end--;
            }
        }
        return new int[]{0, 1};
    }
}
