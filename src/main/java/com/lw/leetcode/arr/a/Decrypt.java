package com.lw.leetcode.arr.a;

import java.util.Arrays;

/**1652. 拆炸弹
 * @Author liw
 * @Date 2021/10/5 20:40
 * @Version 1.0
 */
public class Decrypt {

    public static void main(String[] args){
        Decrypt test = new Decrypt();
        // [12,10,16,13]
        int[] arr = {5,7,1,4};
        int[] decrypt = test.decrypt(arr, -3);
        System.out.println(Arrays.toString(decrypt));
    }

    public int[] decrypt(int[] code, int k) {
        int length = code.length;
        int[] arr = new int[length];
        if (k == 0) {
            return arr;
        }
        int i = 1;
        int j = k;
        if (k < 0) {
            i = length + k;
            j = length - 1;
        }
        for (int m = i; m <= j; m++) {
            arr[0] += code[m];
        }
        for (int m = 1; m < length; m++) {
            j = (j + 1) % length;
            arr[m] = arr[m - 1] + code[j] - code[i];
            i = (i + 1) % length;
        }
        return arr;
    }

}
