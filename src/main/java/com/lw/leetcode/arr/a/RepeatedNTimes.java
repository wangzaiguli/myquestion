package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 961. 在长度 2N 的数组中找出重复 N 次的元素
 *
 * @author liw
 * @version 1.0
 * @date 2022/5/21 9:45
 */
public class RepeatedNTimes {

    public int repeatedNTimes(int[] A) {
        int length = A.length;
        if (A[0] == A[1] || A[0] == A[2] || A[0] == A[3]) {
            return A[0];
        }
        if (A[1] == A[2] || A[1] == A[3]) {
            return A[1];
        }
        if (A[2] == A[3]) {
            return A[2];
        }
        int i1 = length - 1;
        for (int i = 3; i < i1; i++) {
            if (A[i] == A[i + 1]) {
                return A[i];
            }
        }
        return 1;
    }

}
