package com.lw.leetcode.arr.a;

/**
 * 1572. 矩阵对角线元素的和
 *
 * @Author liw
 * @Date 2021/6/18 15:43
 * @Version 1.0
 */
public class DiagonalSum {
    public int diagonalSum(int[][] mat) {
        int length = mat.length;
        int sum = 0;
        for (int i = 0; i < length; i++) {
            sum += mat[i][length - 1 - i];
            sum += mat[i][i];
        }
        if ((length & 1) != 0) {
            sum -= mat[length >> 1][length >> 1];
        }
        return sum;
    }
}
