package com.lw.leetcode.arr.a;

/**
 * 1295. 统计位数为偶数的数字
 *
 * @Author liw
 * @Date 2021/6/16 21:35
 * @Version 1.0
 */
public class FindNumbers {
    public int findNumbers(int[] nums) {
        int count = 0;
        for (int i = nums.length - 1; i >= 0; i--) {
            int value = nums[i];
            if ((value >= 10 && value < 100) || (value >= 1000 && value < 10000) || value == 100000) {
                count++;
            }
        }
        return count;
    }
}
