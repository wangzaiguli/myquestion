package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * a
 * arr
 * 2485. 找出中枢整数
 *
 * @author liw
 * @version 1.0
 * @date 2022/11/28 20:29
 */
public class PivotInteger {

    public int pivotInteger(int n) {
        int sum = ((n + 1) * n) >> 1;
        for (int i = 1; i <= n; i++) {
            int item1 = ((1 + i) * i) >> 1;
            int item2 = sum - item1 + i;

            if (item1 == item2) {
                return i;
            }
            if (item1 > item2) {
                return -1;
            }
        }
        return -1;
    }

}
