package com.lw.leetcode.arr.a;

/**
 * 832. 翻转图像
 */
public class FlipAndInvertImage {



    public int[][] flipAndInvertImage(int[][] image) {
        int m = image.length;
        int length = image[0].length - 1;
        int n = (length) >> 1;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j <= n; j++) {
                int a = image[i][length - j];
                image[i][length - j] = (image[i][j])^1;
                image[i][j] = a^1;
            }
        }
        return image;
    }

}
