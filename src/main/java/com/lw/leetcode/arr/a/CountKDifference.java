package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 2006. 差的绝对值为 K 的数对数目
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/24 19:46
 */
public class CountKDifference {
    public int countKDifference(int[] nums, int k) {
        int[] hole = new int[101];
        for (int i = 0; i < nums.length; i++) {
            hole[nums[i]] += 1;
        }
        int res = 0;
        for (int i = 1; i + k < 101; i++) {
            res += hole[i] * hole[i + k];
        }
        return res;
    }


}
