package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 2319. 判断矩阵是否是一个 X 矩阵
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/26 18:46
 */
public class CheckXMatrix {

    public boolean checkXMatrix(int[][] grid) {
        int length = grid.length;
        for (int i = 0; i < length; i++) {
            if (grid[i][i] == 0 || grid[i][length - 1 - i] == 0) {
                return false;
            }
            grid[i][i] = 0;
            grid[i][length - 1 - i] = 0;
        }
        for (int[] ints : grid) {
            for (int j = 0; j < length; j++) {
                if (ints[j] != 0) {
                    return false;
                }
            }
        }
        return true;
    }

}
