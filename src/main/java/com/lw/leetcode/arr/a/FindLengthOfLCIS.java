package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 674. 最长连续递增序列
 *
 * @author liw
 * @version 1.0
 * @date 2021/8/26 13:35
 */
public class FindLengthOfLCIS {

    public static void main(String[] args) {
        FindLengthOfLCIS test = new FindLengthOfLCIS();
        int[] arr = {2, 2, 2, 2};

        int lengthOfLCIS = test.findLengthOfLCIS(arr);
        System.out.println(lengthOfLCIS);
    }

    public int findLengthOfLCIS(int[] nums) {
        if (nums == null) {
            return 0;
        }
        int length = nums.length;
        if (length < 2) {
            return length;
        }
        int count = 1;
        int item = 1;
        for (int i = 1; i < length; i++) {
            if (nums[i] > nums[i - 1]) {
                item++;
            } else {
                count = Math.max(count, item);
                item = 1;
            }
        }
        count = Math.max(count, item);
        return count;
    }

}
