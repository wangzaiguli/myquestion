package com.lw.leetcode.arr.a;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2164. 对奇偶下标分别排序
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/11 14:18
 */
public class SortEvenOdd {
    public int[] sortEvenOdd(int[] nums) {
        int length = nums.length;
        int b = length >> 1;
        int a = length - b;
        int[] arr0 = new int[a];
        int[] arr1 = new int[b];
        if ((length & 1) == 1) {
            arr0[a - 1] = nums[length - 1];
        }
        for (int i = 0; i < b; i++) {
            arr0[i] = nums[i << 1];
            arr1[i] = nums[(i << 1) + 1];
        }
        Arrays.sort(arr0);
        Arrays.sort(arr1);
        for (int i = 0; i < b; i++) {
            nums[i << 1] = arr0[i];
            nums[(i << 1) + 1] = arr1[b - i - 1];
        }
        if ((length & 1) == 1) {
            nums[length - 1] = arr0[a - 1];
        }
        return nums;
    }
}
