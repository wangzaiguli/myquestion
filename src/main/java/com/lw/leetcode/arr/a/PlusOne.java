package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 66. 加一
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/21 17:22
 */
public class PlusOne {
    public int[] plusOne(int[] digits) {
        if (digits == null) {
            return digits;
        }
        int length = digits.length;

        if (length == 0) {
            return digits;
        }

        int jw = 1;

        for (int i = length - 1; i >= 0; i--) {
            int digit = digits[i] + jw;
            if (digit > 9) {
                digits[i] = 0;
                jw = 1;
            } else {
                digits[i] = digit;
                jw = 0;
            }
        }

        if (jw == 1) {
            int[] a = new int[length + 1];

            a[0] = 1;
            for (int i = 0; i < length; i++) {
                a[i + 1] = digits[i];
            }
            return a;
        } else {
            return digits;
        }
    }
}
