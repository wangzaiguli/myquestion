package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 169. 多数元素
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/16 21:48
 */
public class MajorityElement169 {

    public int majorityElement(int[] nums) {
        int item = nums[0];
        int n = 1;
        for (int i = 1, length = nums.length; i < length; i++) {
            if (item == nums[i]) {
                n++;
            } else {
                n--;
                if (n == 0) {
                    item = nums[i + 1];
                }
            }
        }
        return item;
    }

}
