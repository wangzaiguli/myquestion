package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 1399. 统计最大组的数目
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/23 14:14
 */
public class CountLargestGroup {

    public static void main(String[] args) {
        CountLargestGroup test = new CountLargestGroup();
        int i = test.countLargestGroup(9999);
        System.out.println(i);
    }

    public int countLargestGroup(int n) {
        int[] arr = new int[37];
        for (int i = 1; i <= n; ++i) {
            int key = 0;
            int i0 = i;
            while (i0 != 0) {
                key += i0 % 10;
                i0 /= 10;
            }
            arr[key]++;
        }
        int max = 0;
        for (int i : arr) {
            max = Math.max(max, i);
        }
        int count = 0;
        for (int i : arr) {
            if (i == max) {
                count++;
            }
        }
        return count;
    }

}
