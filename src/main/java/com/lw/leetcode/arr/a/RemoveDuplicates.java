package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 26. 删除有序数组中的重复项
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/11 11:50
 */
public class RemoveDuplicates {
    public int removeDuplicates(int[] nums) {
        if (nums == null) {
            return 0;
        }
        int length = nums.length;
        if (length < 2) {
            return length;
        }
        int n = 0;
        for (int i = 1; i < length; i++) {
            if (nums[i] == nums[i - 1]) {
                n++;
            } else {
                nums[i - n] = nums[i];
            }
        }
        return length - n;
    }
}
