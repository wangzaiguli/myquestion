package com.lw.leetcode.arr.a;

/**
 * 485. 最大连续 1 的个数
 *
 * @Author liw
 * @Date 2021/5/23 10:20
 * @Version 1.0
 */
public class FindMaxConsecutiveOnes {
    public int findMaxConsecutiveOnes(int[] nums) {

        if (nums == null || nums.length == 0) {
            return 0;
        }
        int item = nums[0];
        int count = item ;
        int max = count;
        int length = nums.length;
        for (int i = 1; i < length; i++) {
            if (nums[i] == item) {
                count++;
            } else {
                if (item == 1 && count > max) {
                    max = count;
                }
                item = nums[i];
                count = item;
            }
        }
        if (item == 1 && count > max) {
            max = count;
        }
        return max;
    }
}
