package com.lw.leetcode.arr.a;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * 2032. 至少在两个数组中出现的值
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/29 9:10
 */
public class TwoOutOfThree {
    public List<Integer> twoOutOfThree(int[] nums1, int[] nums2, int[] nums3) {
        Map<Integer, Integer> map1 = new HashMap<>();
        Map<Integer, Integer> map2 = new HashMap<>();
        Map<Integer, Integer> map3 = new HashMap<>();
        for (int i : nums1) {
            map1.put(i, 1);
        }
        for (int i : nums2) {
            map2.put(i, 1);
        }
        for (int i : nums3) {
            map3.put(i, 1);
        }

        Set<Integer> set = new HashSet<>();

        for (int i : nums1) {
            int c = 0;
            c += map1.getOrDefault(i, 0);
            c += map2.getOrDefault(i, 0);
            c += map3.getOrDefault(i, 0);
            if (c > 1) {
                set.add(i);
            }
        }
        for (int i : nums2) {
            int c = 0;
            c += map1.getOrDefault(i, 0);
            c += map2.getOrDefault(i, 0);
            c += map3.getOrDefault(i, 0);
            if (c > 1) {
                set.add(i);
            }
        }
        for (int i : nums3) {
            int c = 0;
            c += map1.getOrDefault(i, 0);
            c += map2.getOrDefault(i, 0);
            c += map3.getOrDefault(i, 0);
            if (c > 1) {
                set.add(i);
            }
        }
        return new ArrayList<>(set);
    }

}
