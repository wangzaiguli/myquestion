package com.lw.leetcode.arr.a;

/**
 * 1812. 判断国际象棋棋盘中一个格子的颜色
 *
 * @Author liw
 * @Date 2021/6/18 16:53
 * @Version 1.0
 */
public class SquareIsWhite {
    public boolean squareIsWhite(String coordinates) {
        int value = coordinates.charAt(0) + coordinates.charAt(1);
        return (value & 1) == 1;
    }
}
