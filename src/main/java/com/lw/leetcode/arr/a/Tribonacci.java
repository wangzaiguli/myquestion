package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * arr
 * 1137. 第 N 个泰波那契数
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/21 16:34
 */
public class Tribonacci {
    public int tribonacci(int n) {

        int[] dp = new int[n + 3];
        dp[0] = 0;
        dp[1] = 1;
        dp[2] = 1;
        for (int i = 3; i <= n; i++) {
            dp[i] = dp[i - 1] + dp[i - 2] + dp[i - 3];
        }
        return dp[n];
    }
}
