package com.lw.leetcode.arr.a;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * arr
 * 728. 自除数
 *
 * @Author liw
 * @Date 2021/7/10 16:23
 * @Version 1.0
 */
public class SelfDividingNumbers {
    public List<Integer> selfDividingNumbers(int left, int right) {
        List<Integer> list = new ArrayList<>(right - left);
        for (int i = left; i <= right; i++) {
            int value = i;
            while (value > 0) {
                int v = value % 10;
                if (v != 0 && i % v == 0) {
                    value /= 10;
                } else {
                    break;
                }
            }
            if (value == 0) {
                list.add(i);
            }
        }
        return list;
    }


}
