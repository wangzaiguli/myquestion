package com.lw.leetcode.arr.a;

/**
 * arr
 * a
 * 1974. 使用特殊打字机键入单词的最少时间
 *
 * @Author liw
 * @Date 2021/11/6 22:08
 * @Version 1.0
 */
public class MinTimeToType {
    public int minTimeToType(String word) {
        int res = 0, dis = 0;
        char start = 'a';
        char[] cc = word.toCharArray();
        for (char c : cc) {
            dis = Math.abs(c - start);
            res += Math.min(dis, 26 - dis);
            start = c;
        }
        return res + cc.length;
    }
}
