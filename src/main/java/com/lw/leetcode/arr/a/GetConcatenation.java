package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 1929. 数组串联
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/15 11:25
 */
public class GetConcatenation {
    public int[] getConcatenation(int[] nums) {
        int n = nums.length;
        int[] ans = new int[n << 1];
        for (int i = 0; i < n; i++) {
            ans[i] = nums[i];
            ans[i + n] = nums[i];
        }
        return ans;
    }
}
