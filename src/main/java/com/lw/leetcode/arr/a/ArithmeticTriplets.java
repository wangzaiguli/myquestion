package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 6136. 算术三元组的数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/8/7 20:59
 */
public class ArithmeticTriplets {

    public static void main(String[] args) {
        ArithmeticTriplets test = new ArithmeticTriplets();

        //  2
//        int[] arr = {0, 1, 4, 6, 7, 10};
//        int t = 3;

        //  2
        int[] arr = {4, 5, 6, 7, 8, 9};
        int t = 2;

        int i = test.arithmeticTriplets(arr, t);
        System.out.println(i);
    }

    public int arithmeticTriplets(int[] nums, int diff) {
        int a = 0;
        int c = 2;

        int length = nums.length - 1;
        int count = 0;
        for (int b = 1; b < length; b++) {
            boolean flag = false;
            while (a < b) {
                int t = nums[b] - nums[a];
                if (diff < t) {
                    a++;
                } else if (diff == t) {
                    flag = true;
                    break;
                } else {
                    break;
                }
            }
            if (flag) {
                if (c <= b) {
                    c = b + 1;
                }
                while (c <= length) {
                    int t = nums[c] - nums[b];
                    if (diff > t) {
                        c++;
                    } else if (diff == t) {
                        count++;
                        break;
                    } else {
                        break;
                    }
                }
            }
        }
        return count;
    }

}
