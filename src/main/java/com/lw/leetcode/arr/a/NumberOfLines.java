package com.lw.leetcode.arr.a;

/**
 * create by idea
 * 806. 写字符串需要的行数
 *
 * @author lmx
 * @version 1.0
 * @date 2022/1/22 13:40
 */
public class NumberOfLines {
    public int[] numberOfLines(int[] widths, String S) {
        int lines = 1;
        int width = 0;
        for (char c : S.toCharArray()) {
            int w = widths[c - 'a'];
            width += w;
            if (width > 100) {
                lines++;
                width = w;
            }
        }
        return new int[]{lines, width};
    }

}
