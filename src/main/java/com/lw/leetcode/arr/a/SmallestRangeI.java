package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 908. 最小差值 I
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/30 10:07
 */
public class SmallestRangeI {

    public int smallestRangeI(int[] A, int K) {
        int min = A[0];
        int max = A[0];
        for (int c : A) {
            min = Math.min(min, c);
            max = Math.max(max, c);
        }
        return Math.max(0, max - min - (K << 1));
    }

}
