package com.lw.leetcode.arr.a;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 2404. 出现最频繁的偶数元素
 *
 * @author liw
 * @version 1.0
 * @date 2022/9/11 20:56
 */
public class MostFrequentEven {

    public static void main(String[] args) {
        MostFrequentEven test = new MostFrequentEven();

        // 2
        int[] arr = {0,1,2,2,4,4,1};

        int i = test.mostFrequentEven(arr);
        System.out.println(i);
    }

    public int mostFrequentEven(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            if ((num & 1) == 0) {
                map.merge(num, 1, (a, b) -> a + b);
            }
        }
        int c = 0;
        int v = -1;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            int key = entry.getKey();
            int value = entry.getValue();
            if (value > c || (value == c && key < v)) {
                v = key;
                c = value;
            }
        }
        return v;
    }

}
