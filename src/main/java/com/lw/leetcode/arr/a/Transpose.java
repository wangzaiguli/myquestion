package com.lw.leetcode.arr.a;

/**
 * create by idea
 * 867. 转置矩阵
 *
 * @author lmx
 * @version 1.0
 * @date 2022/1/22 13:46
 */
public class Transpose {
    public int[][] transpose(int[][] matrix) {
        int m = matrix.length;
        int n = matrix[0].length;
        int[][] transposed = new int[n][m];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                transposed[j][i] = matrix[i][j];
            }
        }
        return transposed;
    }

}
