package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1394. 找出数组中的幸运数
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/24 13:35
 */
public class FindLucky {
    public int findLucky(int[] arr) {
        int[] freq = new int[501];
        for (int i1 : arr) {
            freq[i1]++;
        }
        for (int i = 500; i > 0; i--) {
            if (freq[i] == i) {
                return i;
            }
        }
        return -1;
    }
}
