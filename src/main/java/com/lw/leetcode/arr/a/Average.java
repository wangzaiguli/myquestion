package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1491. 去掉最低工资和最高工资后的工资平均值
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/23 14:31
 */
public class Average {
    public double average(int[] salary) {
        double sum = 0;
        double maxValue = Integer.MIN_VALUE;
        int minValue = Integer.MAX_VALUE;
        for (int num : salary) {
            sum += num;
            maxValue = Math.max(maxValue, num);
            minValue = Math.min(minValue, num);
        }
        return (sum - maxValue - minValue) / (salary.length - 2);
    }
}
