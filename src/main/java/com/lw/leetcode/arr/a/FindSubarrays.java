package com.lw.leetcode.arr.a;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * 2395. 和相等的子数组
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/28 10:55
 */
public class FindSubarrays {

    public boolean findSubarrays(int[] nums) {
        int n = nums.length;
        Set<Integer> vis = new HashSet<>();
        for (int i = 1; i < n; i++) {
            int c = nums[i] + nums[i - 1];
            if (vis.contains(c)) {
                return true;
            }
            vis.add(c);
        }
        return false;
    }

}
