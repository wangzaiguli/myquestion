package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 1470. 重新排列数组
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/16 13:07
 */
public class Shuffle {

    public int[] shuffle(int[] nums, int n) {
        int[] ret = new int[2 * n];
        for (int i = 0; i < n; i++) {
            ret[i * 2] = nums[i];
            ret[i * 2 + 1] = nums[n + i];
        }
        return ret;
    }
}
