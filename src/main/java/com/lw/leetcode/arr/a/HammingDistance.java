package com.lw.leetcode.arr.a;

/**
 * 461. 汉明距离
 *
 * @Author liw
 * @Date 2021/5/27 9:20
 * @Version 1.0
 */
public class HammingDistance {
    public int hammingDistance(int x, int y) {
        int c = x ^ y;
        int n = 0;
        while (c != 0) {
            c = c & (c - 1);
            n++;
        }
        return n;
    }
}
