package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 面试题 17.04. 消失的数字
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/6 11:59
 */
public class MissingNumber {
    public int missingNumber(int[] nums) {
        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        return nums.length * (nums.length + 1) / 2 - sum;
    }
}
