package com.lw.leetcode.arr.a;

/**
 * 1848. 到目标元素的最小距离
 *
 * @Author liw
 * @Date 2021/6/5 23:16
 * @Version 1.0
 */
public class GetMinDistance {
    public int getMinDistance(int[] nums, int target, int start) {

        if (nums[start] == target) {
            return 0;
        }
        int length = nums.length - 1;
        int min = Math.min(length - start, start);
        int item = 1;
        while (item <= min) {
            if (nums[start + item] == target || nums[start - item] == target) {
                return item;
            }
            item++;
        }
        if (length - start > start) {
            while (nums[start + item] != target) {
                item++;
            }
            return item;
        }
        while (nums[start - item] != target) {
            item++;
        }
        return item;
    }
}
