package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * a
 * arr
 * https://leetcode.cn/contest/sf-tech/problems/8oimK4/
 * 顺丰03. 收件节节高
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/10 14:47
 */
public class FindMaxCI {

    public int findMaxCI(int[] nums) {
        int count = 0;
        int item = -1;
        int max = 0;
        for (int num : nums) {
            if (num > item) {
                count++;
                max = Math.max(max, count);
            } else {
                count = 1;
            }
            item = num;
        }
        return Math.max(max, count);
    }

}
