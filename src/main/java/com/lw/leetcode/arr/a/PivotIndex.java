package com.lw.leetcode.arr.a;

/**
 * arr
 * 724. 寻找数组的中心下标
 * 剑指 Offer II 012. 左右两边子数组的和相等
 *
 * @Author liw
 * @Date 2021/7/10 16:16
 * @Version 1.0
 */
public class PivotIndex {

    public static void main(String[] args) {
        PivotIndex test = new PivotIndex();
        int[] arr = {};
    }

    public int pivotIndex(int[] nums) {
        int length = nums.length;
        for (int i = 1; i < length; i++) {
            nums[i] += nums[i - 1];
        }
        int sum = nums[length - 1];
        if (sum - nums[0] == 0) {
            return 0;
        }
        for (int i = 1; i < length; ++i) {
            if (nums[i - 1] + nums[i] == sum) {
                return i;
            }
        }
        return -1;
    }
}
