package com.lw.leetcode.arr.a;

/**
 * 1232. 缀点成线
 *
 * @Author liw
 * @Date 2021/10/1 20:48
 * @Version 1.0
 */
public class CheckStraightLine {
    public boolean checkStraightLine(int[][] coordinates) {
        int length = coordinates.length;
        if (length < 3) {
            return true;
        }
        int c1 = coordinates[1][0];
        int c2 = coordinates[1][1];
        int a = coordinates[0][0] - c1;
        int b = coordinates[0][1] - c2;
        for (int i = 2; i < length; i++) {
            if ((coordinates[i][0] - c1) * b != (coordinates[i][1] - c2) * a) {
                return false;
            }
        }
        return true;
    }
}
