package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 2399. 检查相同字母间的距离
 *
 * @author liw
 * @version 1.0
 * @date 2022/9/4 14:12
 */
public class CheckDistances {

    public boolean checkDistances(String s, int[] distance) {
        int[] arr = new int[26];
        int length = s.length();
        for (int i = 0; i < length; i++) {
            int c = s.charAt(i) - 'a';
            arr[c] = i - arr[c];
        }
        for (int i = 0; i < 26; i++) {
            if (arr[i] != 0 && distance[i] != arr[i] - 1) {
                return false;
            }
        }
        return true;
    }

}
