package com.lw.leetcode.arr.a;

/**
 * 766. 托普利茨矩阵
 */
public class IsToeplitzMatrix {
    public boolean isToeplitzMatrix(int[][] matrix) {
        int a = matrix.length - 1;
        int b = matrix[0].length - 1;
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                if (matrix[i][j] != matrix[i + 1][j + 1]) {
                    return false;
                }
            }
        }
        return true;
    }
}
