package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1800. 最大升序子数组和
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/23 12:52
 */
public class MaxAscendingSum {

    public int maxAscendingSum(int[] nums) {
        int length = nums.length;
        int item = nums[0];
        int max = nums[0];
        for (int i = 1; i < length; i++) {
            int v = nums[i];
            if (v > nums[i - 1]) {
                item = Math.max(v, item + v);
            } else {
                item = v;
            }
            max = Math.max(max, item);
        }
        return max;
    }

}
