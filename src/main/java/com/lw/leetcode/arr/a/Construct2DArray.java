package com.lw.leetcode.arr.a;

/**
 * 2022. 将一维数组转变成二维数组
 *
 * @Author liw
 * @Date 2021/10/5 20:34
 * @Version 1.0
 */
public class Construct2DArray {
    public int[][] construct2DArray(int[] original, int m, int n) {
        int length = original.length;
        if (m * n != length) {
            return new int[][]{};
        }
        int[][] arr = new int[m][n];
        for (int i = 0; i < length; i++) {
            arr[i / n][i % n] = original[i];
        }
        return arr;
    }
}
