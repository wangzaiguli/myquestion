package com.lw.leetcode.arr.a;

/**
 * arr
 * 1512. 好数对的数目
 *
 * @Author liw
 * @Date 2021/6/15 21:19
 * @Version 1.0
 */
public class NumIdenticalPairs {
    public int numIdenticalPairs(int[] nums) {
        int[] arr = new int[101];
        int ans = 0;
        for (int num : nums) {
            ans += arr[num];
            arr[num]++;
        }
        return ans;
    }
}
