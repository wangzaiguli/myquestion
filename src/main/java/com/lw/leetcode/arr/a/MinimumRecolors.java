package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 *6156. 得到 K 个黑块的最少涂色次数
 * @author liw
 * @version 1.0
 * @date 2022/8/21 9:09
 */
public class MinimumRecolors {

    public int minimumRecolors(String blocks, int k) {
        int length = blocks.length();
        int w = 0;
        for (int i = 0; i < k; i++) {
            if (blocks.charAt(i) == 'W') {
                w++;
            }
        }
        int v = w;
        for (int i = k; i < length; i++) {
            if (blocks.charAt(i) == 'W') {
                w++;
            }
            if (blocks.charAt(i - k) == 'W') {
                w--;
            }
            w = Math.min(w, v);
        }
        return v;
    }

}
