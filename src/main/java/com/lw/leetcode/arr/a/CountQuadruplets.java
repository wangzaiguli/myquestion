package com.lw.leetcode.arr.a;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 1995. 统计特殊四元组
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/29 10:02
 */
public class CountQuadruplets {
    public int countQuadruplets(int[] nums) {
        int n = nums.length;
        int count = 0;
        for (int a = 0; a < n; ++a) {
            for (int b = a + 1; b < n; ++b) {
                for (int c = b + 1; c < n; ++c) {
                    for (int d = c + 1; d < n; ++d) {
                        if (nums[a] + nums[b] + nums[c] == nums[d]) {
                            ++count;
                        }
                    }
                }
            }
        }
        return count;
    }
}
