package com.lw.leetcode.arr.a;

/**
 * arr
 * 1534. 统计好三元组
 *
 * @Author liw
 * @Date 2021/6/18 16:44
 * @Version 1.0
 */
public class CountGoodTriplets {

    public int countGoodTriplets(int[] arr, int a, int b, int c) {
        int n = arr.length, cnt = 0;
        for (int i = 0; i < n; ++i) {
            for (int j = i + 1; j < n; ++j) {
                if (Math.abs(arr[i] - arr[j]) <= a) {
                    for (int k = j + 1; k < n; ++k) {
                        if (Math.abs(arr[j] - arr[k]) <= b && Math.abs(arr[i] - arr[k]) <= c) {
                            ++cnt;
                        }
                    }
                }
            }
        }
        return cnt;
    }


}
