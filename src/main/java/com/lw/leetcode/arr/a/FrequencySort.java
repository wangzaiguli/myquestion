package com.lw.leetcode.arr.a;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 1636. 按照频率将数组升序排序
 *
 * @author liw
 * @version 1.0
 * @date 2022/9/19 10:13
 */
public class FrequencySort {

    public int[] frequencySort(int[] nums) {
        int[][] arr = new int[201][2];
        for (int num : nums) {
            arr[num + 100][0]++;
        }
        for (int i = 0; i < 201; i++) {
            arr[i][1] = i;
        }
        Arrays.sort(arr, (a, b) -> a[0] == b[0] ? Integer.compare(b[1], a[1]) : Integer.compare(a[0], b[0]));
        int i = 0;
        for (int[] ints : arr) {
            int c = ints[0];
            int v = ints[1] - 100;
            for (int j = 0; j < c; j++) {
                nums[i++] = v;
            }
        }
        return nums;
    }

}
