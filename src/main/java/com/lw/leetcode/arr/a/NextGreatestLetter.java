package com.lw.leetcode.arr.a;

/**
 * arr
 * 744. 寻找比目标字母大的最小字母
 *
 * @Author liw
 * @Date 2021/7/10 17:36
 * @Version 1.0
 */
public class NextGreatestLetter {
    public char nextGreatestLetter(char[] letters, char target) {
        int l = 0;
        int h = letters.length - 1;
        char ans = letters[0];
        while (l <= h) {
            int mid = l + (h - l) / 2;
            if (letters[mid] > target) {
                h = mid - 1;
                ans = letters[mid];
            } else {
                l = mid + 1;
            }
        }
        return ans;
    }
}
