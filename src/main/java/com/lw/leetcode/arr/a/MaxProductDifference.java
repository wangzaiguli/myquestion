package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 1913. 两个数对之间的最大乘积差
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/16 13:12
 */
public class MaxProductDifference {

    public int maxProductDifference(int[] nums) {
        int max1;
        int max2;
        int min1;
        int min2;
        max1 = min2 = Math.max(nums[0], nums[1]);
        min1 = max2 = Math.min(nums[0], nums[1]);
        for (int i = 2; i < nums.length; ++i) {
            if (nums[i] > max1) {
                max2 = max1;
                max1 = nums[i];
            } else if (nums[i] > max2) {
                max2 = nums[i];
            }

            if (nums[i] < min1) {
                min2 = min1;
                min1 = nums[i];
            } else if (nums[i] < min2) {
                min2 = nums[i];
            }
        }
        return max1 * max2 - min1 * min2;
    }


}
