package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 2016. 增量元素之间的最大差值
 *
 * @author liw
 * @version 1.0
 * @date 2022/2/26 15:38
 */
public class MaximumDifference {
    public int maximumDifference(int[] nums) {
        int max = -1;
        int length = nums.length;

        int min = nums[0];
        for (int i = 1; i < length; i++) {
            max = Math.max(max, nums[i] - min);
            min = Math.min(min, nums[i]);
        }
        return max == 0 ? -1 : max;
    }
}
