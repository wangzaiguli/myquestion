package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 剑指 Offer 03. 数组中重复的数字
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/23 13:07
 */
public class FindRepeatNumber {

    public static void main(String[] args) {
        FindRepeatNumber test = new FindRepeatNumber();
//        int[] arr = {2, 3, 1, 0, 2, 5, 3};
//        int[] arr = {0,0};
        int[] arr = {1, 1};
        int repeatNumber = test.findRepeatNumber(arr);
        System.out.println(repeatNumber);
    }

    public int findRepeatNumber(int[] nums) {
        int length = nums.length;

        for (int i = 0; i < length; i++) {
            int value = nums[i];
            nums[i] = -1;
            while (value > -1) {
                if (nums[value] == value) {
                    return value;
                }
                int v = nums[value];
                nums[value] = value;
                value = v;
            }
        }
        return -1;
    }
}
