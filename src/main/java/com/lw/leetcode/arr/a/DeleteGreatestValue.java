package com.lw.leetcode.arr.a;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 2500. 删除每行中的最大值
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/11 13:40
 */
public class DeleteGreatestValue {

    public int deleteGreatestValue(int[][] grid) {
        for (int[] ints : grid) {
            Arrays.sort(ints);
        }
        int sum = 0;
        int n = grid[0].length;
        for (int i = 0; i < n; i++) {
            int item = 0;
            for (int[] ints : grid) {
                item = Math.max(item, ints[i]);
            }
            sum += item;
        }
        return sum;
    }

}
