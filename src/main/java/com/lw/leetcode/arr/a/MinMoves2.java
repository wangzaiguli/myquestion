package com.lw.leetcode.arr.a;

/**
 * arr
 * a
 * 453. 最小操作次数使数组元素相等
 *
 * @Author liw
 * @Date 2021/6/24 13:56
 * @Version 1.0
 */
public class MinMoves2 {

    /*
    查找最小值， 每个值 - 最小值 的和。
    1：n - 1个数字 + 1, 就相当于  1个数字 - 1.
    2：所以可以转换为 每次操作1个数字减去1，使得数组中所有的数字都相同的操作次数。
    3：要想相同，则都变成最小值即可。
    4：找到最小值，遍历数组，每个值 - 最小值的和，即为所求。
     */
    public int minMoves(int[] nums) {
        int min = Integer.MAX_VALUE;
        for (int num : nums) {
            min = Math.min(min, num);
        }

        int sum = 0;
        for (int num : nums) {
            sum += (num - min);
        }
        return sum;
    }

}
