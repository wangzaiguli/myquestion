package com.lw.leetcode.arr.a;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 1436. 旅行终点站
 *
 * @Author liw
 * @Date 2021/5/25 16:32
 * @Version 1.0
 */
public class DestCity {
    public String destCity2(List<List<String>> paths) {
        Map<String, Integer> map = new HashMap<>(paths.size() << 2);
        for (List<String> list : paths) {
            map.put(list.get(0), 1);
            map.putIfAbsent(list.get(1), 2);
        }

        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getValue() == 2) {
                return entry.getKey();
            }
        }
        return null;
    }
    public String destCity(List<List<String>> paths) {
        Map<String,String> map = new HashMap<>(paths.size() << 2);
        for(int i=0;i<paths.size();i++){
            map.put(paths.get(i).get(0),"");
        }
        for(int i=0;i<paths.size();i++){
            if(map.get(paths.get(i).get(1)) == null){
                return paths.get(i).get(1);
            }
        }
        return "";
    }


}
