package com.lw.leetcode.arr.a;

/**
 * arr
 * 541. 反转字符串 II
 *
 * @Author liw
 * @Date 2021/7/5 14:02
 * @Version 1.0
 */
public class ReverseStr {

    public String reverseStr(String s, int k) {
        char[] ary = s.toCharArray();
        char temp;
        for (int i = 0, len = ary.length; i < len; i += 2 * k) {
            if (len - i < k) {
                k = len - i;
            }
            for (int j = 0; j < k / 2; j++) {
                temp = ary[i + j];
                ary[i + j] = ary[i + k - j - 1];
                ary[i + k - j - 1] = temp;
            }
        }
        return String.valueOf(ary);
    }
}
