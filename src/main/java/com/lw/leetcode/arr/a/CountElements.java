package com.lw.leetcode.arr.a;

/**
 * create by idea
 * 2148. 元素计数
 *
 * @author lmx
 * @version 1.0
 * @date 2022/7/17 10:51
 */
public class CountElements {
    public int countElements(int[] nums) {
        if (nums.length < 3) {
            return 0;
        }
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (int num : nums) {
            min = Math.min(num, min);
            max = Math.max(num, max);
        }
        int ans = 0;
        for (int num : nums) {
            if (num != min && num != max) {
                ans++;
            }
        }
        return ans;
    }

}
