package com.lw.leetcode.arr.a;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2037. 使每位学生都有座位的最少移动次数
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/24 20:21
 */
public class MinMovesToSeat {
    public int minMovesToSeat(int[] seats, int[] students) {
        Arrays.sort(seats);
        Arrays.sort(students);
        int count = 0;
        for (int i = students.length - 1; i >= 0; i--) {
            count += (Math.abs(students[i] - seats[i]));
        }
        return count;
    }
}
