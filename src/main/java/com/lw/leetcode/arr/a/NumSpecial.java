package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * a
 * arr
 * 1582. 二进制矩阵中的特殊位置
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/23 14:34
 */
public class NumSpecial {
    public int numSpecial(int[][] mat) {
        int count = 0;
        int a = mat.length;
        int b = mat[0].length;
        int[] row = new int[a];
        int[] col = new int[b];
        for (int i = 0; i < a; ++i) {
            int[] ints = mat[i];
            for (int j = 0; j < b; ++j) {
                if (ints[j] == 1) {
                    row[i]++;
                    col[j]++;
                }
            }
        }
        for (int i = 0; i < a; ++i) {
            int[] ints = mat[i];
            for (int j = 0; j < b; ++j) {
                if (ints[j] == 1 && row[i] == 1 && col[j] == 1) {
                    count++;
                }
            }
        }
        return count;
    }

}
