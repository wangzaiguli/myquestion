package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 566. 重塑矩阵
 *
 * @author liw
 * @version 1.0
 * @date 2021/8/26 13:29
 */
class MatrixReshape {

    public int[][] matrixReshape(int[][] mat, int r, int c) {
        int m = mat.length;
        int n = mat[0].length;
        int s = m * n;
        if (s != r * c) {
            return mat;
        }
        int[][] arr = new int[r][c];
        for (int i = 0; i < s; i++) {
            arr[i / c][i % c] = mat[i / n][i % n];
        }
        return arr;
    }

}
