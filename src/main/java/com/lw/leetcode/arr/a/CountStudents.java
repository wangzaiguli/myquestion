package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 1700. 无法吃午餐的学生数量
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/19 9:37
 */
public class CountStudents {

    public int countStudents(int[] students, int[] sandwiches) {
        int a = 0;
        int b = 0;
        for (int student : students) {
            if (student == 0) {
                a++;
            } else {
                b++;
            }
        }
        for (int sandwich : sandwiches) {
            if (sandwich == 0) {
                if (a == 0) {
                    return b;
                }
                a--;
            } else {
                if (b == 0) {
                    return a;
                }
                b--;
            }
        }
        return 0;
    }

}
