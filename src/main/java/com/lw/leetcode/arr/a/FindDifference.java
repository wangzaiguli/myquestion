package com.lw.leetcode.arr.a;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * 2215. 找出两数组的不同
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/19 10:09
 */
public class FindDifference {

    public List<List<Integer>> findDifference(int[] nums1, int[] nums2) {
        List<List<Integer>> ans = new ArrayList<>();
        List<Integer> list1 = new ArrayList<>();
        List<Integer> list2 = new ArrayList<>();
        Set<Integer> set1 = new HashSet<>();
        Set<Integer> set2 = new HashSet<>();
        for (int i1 : nums1) {
            set1.add(i1);
        }
        for (int i1 : nums2) {
            set2.add(i1);
        }
        for (int i1 : nums1) {
            if (!set2.contains(i1) && !list1.contains(i1)) {
                list1.add(i1);
            }
        }
        for (int i1 : nums2) {
            if (!set1.contains(i1) && !list2.contains(i1)) {
                list2.add(i1);
            }
        }
        ans.add(list1);
        ans.add(list2);
        return ans;
    }

}
