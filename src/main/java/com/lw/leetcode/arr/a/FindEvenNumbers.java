package com.lw.leetcode.arr.a;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 2094. 找出 3 位偶数
 *
 * @author liw
 * @version 1.0
 * @date 2021/12/6 17:24
 */
public class FindEvenNumbers {

    public static void main(String[] args) {
        FindEvenNumbers test = new FindEvenNumbers();

        // [102,120,130,132,210,230,302,310,312,320]
        //[102, 120, 130, 132, 210, 230, 302, 310, 312, 320]
//        int[] arr = {2,1,3,0};

        int[] arr = {2, 2, 8, 8, 2};

        int[] evenNumbers = test.findEvenNumbers(arr);
        System.out.println(Arrays.toString(evenNumbers));
    }

    public int[] findEvenNumbers(int[] digits) {
        List<Integer> list = new ArrayList<>();
        Set<Integer> set = new HashSet<>();
        int length = digits.length;
        Arrays.sort(digits);
        int a;
        int b;
        int c;
        for (int i = 0; i < length; i++) {
            a = digits[i];
            if (i > 0 && a == digits[i - 1]) {
                continue;
            }
            for (int j = i + 1; j < length; j++) {
                b = digits[j];
                if (j > i + 1 && b == digits[j - 1]) {
                    continue;
                }
                for (int k = j + 1; k < length; k++) {
                    c = digits[k];
                    if (k > j + 1 && c == digits[k - 1]) {
                        continue;
                    }
                    find(a, b, c, list, set);
                }
            }
        }
        int size = list.size();
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = list.get(i);
        }
        Arrays.sort(arr);
        return arr;
    }

    private void find(int a, int b, int c, List<Integer> list, Set<Integer> set) {
        int v;
        v = a * 100 + b * 10 + c;
        if (v >= 100 && (v & 1) == 0) {
            set.add(v);
        }
        v = a * 100 + c * 10 + b;
        if (v >= 100 && (v & 1) == 0) {
            set.add(v);
        }
        v = b * 100 + c * 10 + a;
        if (v >= 100 && (v & 1) == 0) {
            set.add(v);
        }
        v = b * 100 + a * 10 + c;
        if (v >= 100 && (v & 1) == 0) {
            set.add(v);
        }
        v = c * 100 + a * 10 + b;
        if (v >= 100 && (v & 1) == 0) {
            set.add(v);
        }
        v = c * 100 + b * 10 + a;
        if (v >= 100 && (v & 1) == 0) {
            set.add(v);
        }
        list.addAll(set);
        set.clear();
    }
}
