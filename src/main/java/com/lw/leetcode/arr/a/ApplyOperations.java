package com.lw.leetcode.arr.a;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2460. 对数组执行操作
 *
 * @author liw
 * @version 1.0
 * @date 2022/11/7 9:50
 */
public class ApplyOperations {

    public static void main(String[] args) {
        ApplyOperations test = new ApplyOperations();

        // [1,4,2,0,0,0]
//        int[] nums = {1, 2, 2, 1, 1, 0};

        // [1,0]
        int[] nums = {0, 1};

        int[] ints = test.applyOperations(nums);
        System.out.println(Arrays.toString(ints));
    }

    public int[] applyOperations(int[] nums) {
        int length = nums.length;
        for (int i = 1; i < length; i++) {
            if (nums[i - 1] == nums[i]) {
                nums[i - 1] <<= 1;
                nums[i] = 0;
                i++;
            }
        }
        int i = 0;
        int j = 0;
        while (j < length) {
            if (nums[j] != 0) {
                nums[i++] = nums[j];
            }
            j++;
        }
        while (i < length) {
            nums[i++] = 0;
        }
        return nums;
    }

}
