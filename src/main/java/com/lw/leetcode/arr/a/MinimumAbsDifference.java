package com.lw.leetcode.arr.a;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 1200. 最小绝对差
 *
 * @Author liw
 * @Date 2021/10/30 20:27
 * @Version 1.0
 */
public class MinimumAbsDifference {



    public List<List<Integer>> minimumAbsDifference(int[] arr) {

        Arrays.sort(arr);
        int min = Integer.MAX_VALUE;

        int length = arr.length;
        for (int i = 1; i < length; i++) {
            min = Math.min(min, arr[i] - arr[i - 1]);
        }
        List<List<Integer>> list = new ArrayList<>();
        for (int i = 1; i < length; i++) {
            if (min == arr[i] - arr[i - 1]) {
                list.add(Arrays.asList(arr[i - 1], arr[i]));
            }
        }
        return list;
    }

}
