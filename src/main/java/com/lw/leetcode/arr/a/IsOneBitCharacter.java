package com.lw.leetcode.arr.a;

/**
 * arr
 * 717. 1比特与2比特字符
 *
 * @Author liw
 * @Date 2021/7/10 15:50
 * @Version 1.0
 */
public class IsOneBitCharacter {
    public boolean isOneBitCharacter(int[] bits) {
        int index = 0;
        int length = bits.length - 1;
        while (index < length) {
            if (bits[index] == 0) {
                index++;
            } else {
                index += 2;
            }
        }
        return index == bits.length - 1;
    }
}
