package com.lw.leetcode.arr.a;

/**
 * 2057. 值相等的最小索引
 *
 * @Author liw
 * @Date 2021/11/6 21:38
 * @Version 1.0
 */
public class SmallestEqual {
    public int smallestEqual(int[] nums) {
        for (int i = 0; i < nums.length; i++)
            if (i % 10 == nums[i]) return i;
        return -1;
    }
}
