package com.lw.leetcode.arr.a;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * 2441. 与对应负数同时存在的最大正整数
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/17 16:36
 */
public class FindMaxK {

    public static void main(String[] args) {
        FindMaxK test = new FindMaxK();

        // 3
//        int[] arr = {-1, 2, -3, 3};

        // 7
//        int[] arr = {-1, 10, 6, 7, -7, 1};

        // -1
        int[] arr = {-10, 8, 6, 7, -2, -3};

        int maxK = test.findMaxK(arr);
        System.out.println(maxK);
    }

    public int findMaxK(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int num : nums) {
            if (num < 0) {
                set.add(num);
            }
        }
        int max = -1;
        for (int num : nums) {
            if (num > 0 && num > max && set.contains(-num)) {
                max = num;
            }
        }
        return max;
    }

}
