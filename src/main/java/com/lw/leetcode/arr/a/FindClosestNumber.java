package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 2239. 找到最接近 0 的数字
 *
 * @author liw
 * @version 1.0
 * @date 2022/4/18 13:59
 */
public class FindClosestNumber {

    public int findClosestNumber(int[] nums) {
        int value = Integer.MAX_VALUE;
        int item = 0;
        for (int num : nums) {
            if (num == 0) {
                return 0;
            }
            if (num < 0 && 0 - num < value) {
                value = 0 - num;
                item = num;
            }
            if (num > 0 && num <= value) {
                value = num;
                item = num;
            }
        }
        return item;
    }

}
