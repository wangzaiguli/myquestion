package com.lw.leetcode.arr.a;

/**
 * arr
 * a
 * 1252. 奇数值单元格的数目
 *
 * @Author liw
 * @Date 2021/11/6 21:55
 * @Version 1.0
 */
public class OddCells {

    public int oddCells(int n, int m, int[][] indices) {
        boolean[] r = new boolean[n];
        boolean[] c = new boolean[m];
        int i;
        for (i = 0; i < indices.length; i++) {
            r[indices[i][0]] = !r[indices[i][0]];
            c[indices[i][1]] = !c[indices[i][1]];
        }
        int rr = 0, cc = 0;
        for (i = 0; i < r.length; i++) {
            if (r[i]) {
                rr++;
            }
        }
        for (i = 0; i < c.length; i++) {
            if (c[i]) {
                cc++;
            }
        }
        return rr * m + cc * n - rr * cc * 2;
    }

}
