package com.lw.leetcode.arr.a;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * 2133. 检查是否每一行每一列都包含全部整数
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/28 10:40
 */
public class CheckValid {

    public boolean checkValid(int[][] matrix) {
        int n = matrix.length;
        boolean[] row = new boolean[101];
        boolean[] col = new boolean[101];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (row[matrix[i][j]]) {
                    return false;
                }
                row[matrix[i][j]] = true;
                if (col[matrix[j][i]]) {
                    return false;
                }
                col[matrix[j][i]] = true;
            }
            Arrays.fill(row, false);
            Arrays.fill(col, false);
        }
        return true;
    }

}
