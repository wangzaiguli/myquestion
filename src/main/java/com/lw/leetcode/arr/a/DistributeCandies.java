package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * arr
 * 1103. 分糖果 II
 *
 * @author liw
 * @version 1.0
 * @date 2021/7/21 16:05
 */
public class DistributeCandies {
    public int[] distributeCandies(int candies, int n) {
        int i = 0;
        int[] res = new int[n];
        while (candies > 0) {
            res[i % n] += Math.min(++i, candies);
            candies -= i;
        }
        return res;
    }
}
