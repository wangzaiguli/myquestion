package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 2423. 删除字符使频率相同
 *
 * @author liw
 * @version 1.0
 * @date 2022/10/8 9:20
 */
public class EqualFrequency {


    public static void main(String[] args) {
        EqualFrequency test = new EqualFrequency();
        String str = "abbcc";

        boolean b = test.equalFrequency(str);
        System.out.println(b);
    }

    public boolean equalFrequency(String word) {
        int[] arr = new int[26];
        for (int i = word.length() - 1; i >= 0; i--) {
            arr[word.charAt(i) - 'a']++;
        }
        int a = 0;
        int c1 = 0;
        int b = 0;
        int c2 = 0;
        for (int i = 0; i < 26; i++) {
            int c = arr[i];
            if (c == 0) {
                continue;
            }
            if (c1 == 0) {
                a++;
                c1 = c;
            } else if (c == c1) {
                a++;
            } else if (c2 == 0) {
                b++;
                c2 = c;
            } else if (c == c2) {
                b++;
            } else {
                return false;
            }
        }
        if ((c1 == 1 && c2 == 0) || (a == 1 && b == 0)) {
            return true;
        }
        if ((c1 == 1 && a == 1) || (c2 == 1 && b == 1)) {
            return true;
        }
        return ((a == 1 && c1 - 1 == c2) || (b == 1 && c2 - 1 == c1));
    }

}
