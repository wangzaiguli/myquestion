package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 1886. 判断矩阵经轮转后是否一致
 *
 * @author liw
 * @version 1.0
 * @date 2023/1/28 11:01
 */
public class FindRotation {

    public boolean findRotation(int[][] mat, int[][] target) {
        int n = mat.length;
        for (int count = 0; count < 4; ++count) {
            for (int i = 0; i < n / 2; ++i) {
                for (int j = 0; j < (n + 1) / 2; ++j) {
                    int temp = mat[i][j];
                    mat[i][j] = mat[n - 1 - j][i];
                    mat[n - 1 - j][i] = mat[n - 1 - i][n - 1 - j];
                    mat[n - 1 - i][n - 1 - j] = mat[j][n - 1 - i];
                    mat[j][n - 1 - i] = temp;
                }
            }
            if (isequals(mat, target)) {
                return true;
            }
        }
        return false;
    }

    private boolean isequals(int[][] mat, int[][] target) {
        for (int i = 0; i < mat.length; ++i) {
            for (int j = 0; j < mat[0].length; ++j) {
                if (mat[i][j] != target[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

}
