package com.lw.leetcode.arr.a;

/**
 * create by idea
 * arr
 * 706. 设计哈希映射
 *
 * @author lmx
 * @version 1.0
 * @date 2022/1/22 14:22
 */
public class MyHashMap {
    int[] arr = new int[10000000];

    public MyHashMap() {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = -1;
        }
    }

    public void put(int key, int value) {
        arr[key] = value;

    }

    public int get(int key) {
        return arr[key];
    }

    public void remove(int key) {
        arr[key] = -1;
    }
}
