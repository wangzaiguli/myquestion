package com.lw.leetcode.arr.a;

/**
 * arr
 * 598. 范围求和 II
 *
 * @Author liw
 * @Date 2021/7/5 13:24
 * @Version 1.0
 */
public class MaxCount {
    public int maxCount(int m, int n, int[][] ops) {
        for (int[] op : ops) {
            m = Math.min(m, op[0]);
            n = Math.min(n, op[1]);
        }
        return m * n;
    }
}
