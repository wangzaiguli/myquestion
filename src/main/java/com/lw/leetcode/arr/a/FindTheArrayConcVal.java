package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * a
 * arr
 * 2562. 找出数组的串联值
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/12 16:43
 */
public class FindTheArrayConcVal {

    public long findTheArrayConcVal(int[] nums) {
        int st = 0;
        int end = nums.length - 1;
        long sum = 0;
        while (st < end) {
            sum += find(nums[st++], nums[end--]);
        }
        if (st == end) {
            sum += nums[st];
        }
        return sum;
    }

    private int find(int a, int b) {
        int c = b;
        int t = 1;
        while (c != 0) {
            t *= 10;
            c /= 10;
        }
        return a * t + b;
    }

}
