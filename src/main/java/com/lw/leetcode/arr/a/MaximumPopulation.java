package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 1854. 人口最多的年份
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/18 13:57
 */
public class MaximumPopulation {

    public int maximumPopulation(int[][] logs) {
        int length = 101;
        int[] arr = new int[length];
        for (int[] log : logs) {
            arr[log[0] - 1950] += 1;
            arr[log[1] - 1950] -= 1;
        }
        int max = arr[0];
        int year = 0;
        for (int i = 1; i < length; i++) {
            arr[i] = arr[i - 1] + arr[i];
            if (arr[i] > max) {
                max = arr[i];
                year = i;
            }
        }
        return year + 1950;
    }

}
