package com.lw.leetcode.arr.a;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * a
 * arr
 * 6362. 合并两个二维数组 - 求和法
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/20 9:24
 */
public class MergeArrays {

    public int[][] mergeArrays(int[][] nums1, int[][] nums2) {
        int m = nums1.length;
        int n = nums2.length;
        List<Integer[]> list = new ArrayList<>(m + n);
        int i = 0;
        int j = 0;
        while (i < m && j < n) {
            if (nums1[i][0] == nums2[j][0]) {
                list.add(new Integer[]{nums1[i][0], nums1[i][1] + nums2[j][1]});
                i++;
                j++;
            } else if (nums1[i][0] < nums2[j][0]) {
                list.add(new Integer[]{nums1[i][0], nums1[i][1]});
                i++;
            } else {
                list.add(new Integer[]{nums2[j][0], nums2[j][1]});
                j++;
            }
        }
        while (i < m) {
            list.add(new Integer[]{nums1[i][0], nums1[i][1]});
            i++;
        }
        while (j < n) {
            list.add(new Integer[]{nums2[j][0], nums2[j][1]});
            j++;
        }
        int size = list.size();
        int[][] arr = new int[size][2];
        for (i = 0; i < size; i++) {
            Integer[] integers = list.get(i);
            int[] ints = arr[i];
            ints[0] = integers[0];
            ints[1] = integers[1];
        }
        return arr;
    }

}
