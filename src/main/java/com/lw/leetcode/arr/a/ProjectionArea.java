package com.lw.leetcode.arr.a;

/**
 * 883. 三维形体投影面积
 *
 * @Author liw
 * @Date 2021/7/9 11:09
 * @Version 1.0
 */
public class ProjectionArea {

    public static void main(String[] args) {
        ProjectionArea test = new ProjectionArea();
//        int[][] arr = {{1,2}, {3,4}};
        int[][] arr = {{2, 2, 2}, {2, 1, 2}, {2, 2, 2}};
        int i = test.projectionArea(arr);
        System.out.println(i);
        int i1 = test.projectionArea2(arr);
        System.out.println(i1);
    }

    public int projectionArea(int[][] grid) {
        int length = grid.length;
        int sum = 0;
        for (int i = 0; i < length; i++) {
            int b = 0;
            int c = 0;
            for (int j = 0; j < length; j++) {
                int value = grid[i][j];
                if (value > 0) {
                    sum++;
                }
                b = Math.max(b, value);
                c = Math.max(c, grid[j][i]);
            }
            sum += b;
            sum += c;
        }
        return sum;
    }


    public int projectionArea2(int[][] grid) {
        int xy = 0, xz = 0, yz = 0;
        int row = 0;   //记录每一行最大值(xz平面)
        int[] col = new int[grid[0].length]; //记录每一列最大值(yz平面)
        for (int i = 0; i < grid.length; i++) {
            row = 0;
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] > 0) {
                    xy++;
                }
                row = Math.max(row, grid[i][j]);
                col[j] = Math.max(col[j], grid[i][j]);
            }
            xz += row;
        }
        for (int i : col) {
            yz += i;
        }
        return xy + xz + yz;
    }
}
