package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 面试题 03.01. 三合一
 *
 * @author liw
 * @version 1.0
 * @date 2022/1/20 22:16
 */
public class TripleInOne {
    private int[] values;
    private int a;
    private int b;
    private int c;
    private int size;

    public TripleInOne(int stackSize) {
        values = new int[stackSize * 3];
        this.size = stackSize;
    }

    public void push(int stackNum, int value) {
        int index;
        if (stackNum == 0) {
            if (size > a) {
                index = a * 3;
                a++;
                values[index] = value;
            }

        } else if (stackNum == 1) {
            if (size > b) {
                index = b * 3 + 1;
                b++;
                values[index] = value;
            }
        } else {
            if (size > c) {
                index = c * 3 + 2;
                c++;
                values[index] = value;
            }
        }

    }

    public int pop(int stackNum) {
        int index;
        if (stackNum == 0) {
            if (a > 0) {
                index = a * 3 - 3;
                a--;
                return values[index];
            }

        } else if (stackNum == 1) {
            if (b > 0) {
                index = b * 3 - 2;
                b--;
                return values[index];
            }
        } else {
            if (c > 0) {
                index = c * 3 - 1;
                c--;
                return values[index];
            }
        }
        return -1;
    }

    public int peek(int stackNum) {
        int index;
        if (stackNum == 0) {
            if (a > 0) {
                index = a * 3 - 3;
                return values[index];
            }

        } else if (stackNum == 1) {
            if (b > 0) {
                index = b * 3 - 2;
                return values[index];
            }
        } else {
            if (c > 0) {
                index = c * 3 - 1;
                return values[index];
            }
        }
        return -1;
    }

    public boolean isEmpty(int stackNum) {
        if (stackNum == 0) {
            if (a > 0) {
                return false;
            }
        } else if (stackNum == 1) {
            if (b > 0) {
                return false;
            }
        } else {
            if (c > 0) {
                return false;
            }
        }
        return true;
    }

}
