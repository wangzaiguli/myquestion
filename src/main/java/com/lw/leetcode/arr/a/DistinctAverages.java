package com.lw.leetcode.arr.a;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * 2465. 不同的平均值数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/11/14 14:27
 */
public class DistinctAverages {

    public static void main(String[] args) {
        DistinctAverages test = new DistinctAverages();

        // 2
        int[] arr = {10, 2, 2, 0, 4, 0};

        int i = test.distinctAverages(arr);
        System.out.println(i);
    }

    public int distinctAverages(int[] nums) {
        int length = nums.length;
        int l = length >> 1;
        Set<Integer> set = new HashSet<>();
        Arrays.sort(nums);
        for (int i = 0; i < l; i++) {
            set.add(nums[i] + nums[length - 1 - i]);
        }
        return set.size();
    }

}
