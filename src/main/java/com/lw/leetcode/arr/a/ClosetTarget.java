package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * a
 * arr
 * 2515. 到目标字符串的最短距离
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/26 14:10
 */
public class ClosetTarget {
    public int closetTarget(String[] words, String target, int startIndex) {
        int ans = 101;
        int n = words.length;
        for (int i = 0; i < n; i++) {
            if (words[i].equals(target)) {
                int abs = Math.abs(startIndex - i);
                ans = Math.min(ans, Math.min(abs, n - abs));
            }
        }
        return ans == 101 ? -1 : ans;
    }
}
