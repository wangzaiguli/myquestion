package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * a
 * arr
 * 2511. 最多可以摧毁的敌人城堡数目
 *
 * @author liw
 * @version 1.0
 * @date 2022/12/26 9:34
 */
public class CaptureForts {

    public static void main(String[] args) {
        CaptureForts test = new CaptureForts();

        // 4
        int[] arr = {1, 0, 0, -1, 0, 0, 0, 0, 1};

        // 0
//        int[] arr = {0, 0, 1, -1};

        int i = test.captureForts(arr);
        System.out.println(i);
    }

    public int captureForts(int[] forts) {
        int length = forts.length;
        int max = 0;
        for (int i = 0; i < length; i++) {
            if (forts[i] == 1) {
                max = Math.max(max, find(forts, i, 1));
                max = Math.max(max, find(forts, i, -1));
            }
        }
        return max;
    }

    private int find(int[] forts, int st, int step) {
        int length = forts.length;
        int count = 0;
        st += step;
        while (st >= 0 && st < length) {
            if (forts[st] == 0) {
                count++;
                st += step;
            } else if (forts[st] == -1) {
                return count;
            } else {
                return 0;
            }
        }
        return 0;
    }

}
