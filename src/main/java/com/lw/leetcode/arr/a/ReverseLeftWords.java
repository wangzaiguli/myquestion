package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 剑指 Offer 58 - II. 左旋转字符串
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/15 11:30
 */
public class ReverseLeftWords {

    public static void main(String[] args) {
        ReverseLeftWords test = new ReverseLeftWords();

        //输出: "cdefgab"
        String str = "abcdefg";
        int n = 2;

        String s = test.reverseLeftWords(str, n);
        System.out.println(s);
    }

    public String reverseLeftWords(String s, int n) {
        char[] arr = s.toCharArray();
        find(arr, 0, n - 1);
        find(arr, n, s.length() - 1);
        find(arr, 0, s.length() - 1);
        return new String(arr);
    }

    private void find(char[] arr, int st, int end) {
        while (st < end) {
            char b = arr[st];
            arr[st++] = arr[end];
            arr[end--] = b;
        }
    }
}
