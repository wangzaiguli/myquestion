package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * <p>
 * a
 * arr
 * 1869. 哪种连续子字符串更长
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/18 14:07
 */
public class CheckZeroOnes {

    public boolean checkZeroOnes(String s) {

        int a = 0;
        int b = 0;
        int item = 1;

        char[] arr = s.toCharArray();
        char c = arr[0];
        int length = arr.length;
        for (int i = 1; i < length; i++) {
            char v = arr[i];
            if (c == v) {
                item++;
            } else {
                if (v == '0') {
                    a = Math.max(a, item);
                } else {
                    b = Math.max(b, item);
                }
                item = 1;
                c = v;
            }
        }
        if (arr[length - 1] == '1') {
            a = Math.max(a, item);
        } else {
            b = Math.max(b, item);
        }
        return a > b;
    }

}
