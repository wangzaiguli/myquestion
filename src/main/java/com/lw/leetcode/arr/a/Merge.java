package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 面试题 10.01. 合并排序的数组
 *
 * @author liw
 * @version 1.0
 * @date 2022/7/7 11:12
 */
public class Merge {
    public void merge(int[] A, int m, int[] B, int n) {
        int i = m - 1;
        int j = n - 1;
        int idx = m + n - 1;
        while (j >= 0) {
            if (i < 0 || B[j] >= A[i]) {
                A[idx--] = B[j--];
            } else {
                A[idx--] = A[i--];
            }
        }
    }
}
