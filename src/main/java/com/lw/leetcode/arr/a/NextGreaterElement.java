package com.lw.leetcode.arr.a;

import java.util.*;

/**
 * 496. 下一个更大元素 I
 *
 * @Author liw
 * @Date 2021/6/28 11:53
 * @Version 1.0
 */
public class NextGreaterElement {
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        int len1 = nums1.length;
        int len2 = nums2.length;

        Stack<Integer> stack = new Stack<>();
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < len2; i++) {
            while (!stack.isEmpty() && stack.peek() < nums2[i]) {
                map.put(stack.pop(), nums2[i]);
            }
            stack.add(nums2[i]);
        }

        while (!stack.isEmpty()) {
            map.put(stack.pop(),-1);
        }

        int[] res = new int[len1];
        for (int i = 0; i < len1; i++) {
            res[i] = map.get(nums1[i]);
        }
        return res;

    }
}
