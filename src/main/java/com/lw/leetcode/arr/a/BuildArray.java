package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * 1920. 基于排列构建数组
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/15 11:22
 */
public class BuildArray {
    public int[] buildArray(int[] nums) {
        int length = nums.length;
        int ans[] = new int[length];
        for (int i = 0; i < length; i++) {
            ans[i] = nums[nums[i]];
        }
        return ans;
    }
}
