package com.lw.leetcode.arr.a;

import java.util.HashMap;
import java.util.Map;

/**
 * create by idea
 * 389. 找不同
 *
 * @author lmx
 * @version 1.0
 * @date 2022/3/12 23:53
 */
public class FindTheDifference {


    public static void main(String[] args){
        FindTheDifference test = new FindTheDifference();
        String s = "abcd";
        String t = "abcde";

        char theDifference = test.findTheDifference(s, t);
        System.out.println(theDifference);
    }

    public char findTheDifference(String s, String t) {

        int a = s.length();
        if (a == 0) {
            return t.charAt(0);
        }

        int b = t.length();

        Map<Character, Integer> mapa = new HashMap<>();
        Map<Character, Integer> mapb = new HashMap<>();

        for (int i = 0; i < a; i++) {
            char c = s.charAt(i);
            Integer count = mapa.get(c);
            if (count == null) {
                mapa.put(c, 1);
            } else {
                mapa.put(c, count + 1);
            }
        }


        for (int i = 0; i < b; i++) {
            char c = t.charAt(i);
            Integer count = mapb.get(c);
            if (count == null) {
                mapb.put(c, 1);
            } else {
                mapb.put(c, count + 1);
            }
        }

        for (Map.Entry<Character, Integer> entry : mapb.entrySet()) {
            Character key = entry.getKey();
            Integer value = entry.getValue();
            Integer integer = mapa.get(key);
            if (integer == null || integer + 1 == value) {
                return key;
            }
        }
        return ' ';
    }

}
