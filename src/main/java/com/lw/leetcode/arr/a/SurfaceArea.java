package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 892. 三维形体的表面积
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/29 13:30
 */
public class SurfaceArea {

    public static void main(String[] args) {
        SurfaceArea test = new SurfaceArea();
        int[][] arr = {{1,2}, {3,4}};
        int i = test.surfaceArea2(arr);
        System.out.println(i);
    }

    public int surfaceArea(int[][] grid) {
        int a = grid.length;
        int b = grid[0].length;
        int sum = a * b * 2;
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                int v = grid[i][j];

                if (v == 0) {
                    sum -= 2;
                }
                if (i == 0) {
                    sum += v;
                }
                if (i == a - 1) {
                    sum += v;
                } else {
                    sum += Math.abs(v - grid[i + 1][j]);
                }
                if (j == 0) {
                    sum += v;
                }
                if (j == b - 1) {
                    sum += v;
                } else {
                    sum += Math.abs(v - grid[i][j + 1]);
                }
            }
        }
        return sum;
    }

    public int surfaceArea2(int[][] grid) {
        int a = grid.length;
        int b = grid[0].length;
        int sum = a * b * 2;
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                int v = grid[i][j];
                sum -= (v == 0 ? 2 : 0);
                sum += (i == 0 ? v : 0);
                sum += (i == a - 1 ? v : Math.abs(v - grid[i + 1][j]));
                sum += (j == 0 ? v : 0);
                sum += (j == b - 1 ? v : Math.abs(v - grid[i][j + 1]));
            }
        }
        return sum;
    }

}
