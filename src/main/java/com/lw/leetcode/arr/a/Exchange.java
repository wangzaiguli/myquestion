package com.lw.leetcode.arr.a;

/**
 * create by idea
 * 剑指 Offer 21. 调整数组顺序使奇数位于偶数前面
 *
 * @author lmx
 * @version 1.0
 * @date 2022/7/17 10:42
 */
public class Exchange {

    public int[] exchange(int[] nums) {
        int i = 0;
        int j = nums.length - 1;
        int tmp;
        while (i < j) {
            while (i < j && (nums[i] & 1) == 1) {
                i++;
            }
            while (i < j && (nums[j] & 1) == 0) {
                j--;
            }
            tmp = nums[i];
            nums[i] = nums[j];
            nums[j] = tmp;
        }
        return nums;
    }

}
