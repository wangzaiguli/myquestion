package com.lw.leetcode.arr.a;

import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * arr
 * 917. 仅仅反转字母
 *
 * @author liw
 * @version 1.0
 * @date 2021/10/29 13:25
 */
public class ReverseOnlyLetters {

    public String reverseOnlyLetters(String s) {
        Stack<Character> letters = new Stack<>();
        for (char c : s.toCharArray()) {
            if (Character.isLetter(c)) {
                letters.push(c);
            }
        }
        StringBuilder ans = new StringBuilder();
        for (char c : s.toCharArray()) {
            if (Character.isLetter(c)) {
                ans.append(letters.pop());
            } else {
                ans.append(c);
            }
        }
        return ans.toString();
    }

}
