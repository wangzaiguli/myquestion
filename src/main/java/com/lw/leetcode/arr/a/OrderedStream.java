package com.lw.leetcode.arr.a;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * a
 * arr
 * 1656. 设计有序流
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/18 13:36
 */
public class OrderedStream {

    private String[] stream;
    private int ptr;
    private int n;

    public OrderedStream(int n) {
        stream = new String[n + 1];
        ptr = 1;
        this.n = n;
    }

    public List<String> insert(int id, String value) {
        stream[id] = value;
        List<String> list = new ArrayList<>();
        for (int i = ptr; i <= n; i++) {
            if (stream[i] == null) {
                break;
            }
            list.add(stream[i]);
            ptr++;
        }
        return list;
    }


}
