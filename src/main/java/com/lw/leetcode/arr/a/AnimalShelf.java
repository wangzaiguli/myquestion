package com.lw.leetcode.arr.a;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 面试题 03.06. 动物收容所
 *
 * @Author liw
 * @Date 2021/10/2 14:07
 * @Version 1.0
 */
public class AnimalShelf {
    Queue<int[]> dogs = new LinkedList<>();
    Queue<int[]> cats = new LinkedList<>();
    int t = 0;

    public AnimalShelf() {
    }

    public void enqueue(int[] animal) {
        if (animal[1] == 0) {
            cats.offer(new int[]{animal[0], t++});
        } else {
            dogs.offer(new int[]{animal[0], t++});
        }
    }

    public int[] dequeueAny() {
        if (cats.isEmpty() && dogs.isEmpty()) {
            return new int[]{-1, -1};
        }
        if (dogs.isEmpty()) {
            return dequeueCat();
        }
        if (cats.isEmpty()) {
            return dequeueDog();
        }
        return cats.peek()[1] < dogs.peek()[1] ? dequeueCat() : dequeueDog();
    }

    public int[] dequeueDog() {
        if (dogs.isEmpty()) {
            return new int[]{-1, -1};
        }
        return new int[]{dogs.poll()[0], 1};
    }

    public int[] dequeueCat() {
        if (cats.isEmpty()) {
            return new int[]{-1, -1};
        }
        return new int[]{cats.poll()[0], 0};
    }
}
