package com.lw.leetcode.arr.a;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * a
 * arr
 * https://leetcode.cn/contest/ubiquant2022/problems/xdxykd/
 * 九坤-01. 可以读通讯稿的组数
 *
 * @author liw
 * @version 1.0
 * @date 2023/2/10 16:50
 */
public class NumberOfPairs01 {

    public static void main(String[] args) {
        NumberOfPairs01 test = new NumberOfPairs01();

        // [17, 28, 39, 71]
        int[] arr = {17, 28, 39, 71};
        int i = test.numberOfPairs(arr);
        System.out.println(i);
    }

    public int numberOfPairs(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            map.merge(num - find(num), 1, (a, b) -> a + b);
        }
        System.out.println(map);
        long sum = 0;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            long count = entry.getValue();
            sum = (count * (count - 1) / 2 + sum) % 1000000007;
        }
        return (int) sum;
    }

    private int find(int num) {
        int item = 0;
        while (num != 0) {
            item = item * 10 + num % 10;
            num /= 10;
        }
        return item;
    }

}
