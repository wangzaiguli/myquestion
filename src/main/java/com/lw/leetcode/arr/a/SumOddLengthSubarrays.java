package com.lw.leetcode.arr.a;

/**
 * 1588. 所有奇数长度子数组的和
 *
 * @Author liw
 * @Date 2021/6/7 13:32
 * @Version 1.0
 */
public class SumOddLengthSubarrays {

    public static void main(String[] args) {
        SumOddLengthSubarrays test = new SumOddLengthSubarrays();
        int[] arr = {1,4,65,34,56,22,44,9,3,2,6,88};
        int i = test.sumOddLengthSubarrays(arr);
        System.out.println(i);

    }

    public int sumOddLengthSubarrays(int[] arr) {

        int sum = 0;
        int length = arr.length;
        int l = length - 1;
        int end = (length >> 1);
        for (int i = 0; i < end; i++) {
            int count = 0;
            for (int j = 0; j < length; j += 2) {
                count += (Math.min(j, Math.min(i, l - j)) + 1);
            }
            sum = sum + (arr[i] + arr[l - i]) * count;
        }
        if ((length & 1) != 0) {
            int count = 0;
            for (int j = 0; j < length; j += 2) {
                count += (Math.min(j, Math.min(end, l - j)) + 1);
            }
            sum = sum + arr[end] * count;
        }
        return sum;
    }

}
