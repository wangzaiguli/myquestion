package com.lw.leetcode.arr.a;

/**
 * Created with IntelliJ IDEA.
 * a
 * arr
 * 1323. 6 和 9 组成的最大数字
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/18 13:28
 */
public class Maximum69Number {

    public int maximum69Number(int num) {
        char[] arr = String.valueOf(num).toCharArray();
        int length = arr.length;
        for (int i = 0; i < length; i++) {
            if (arr[i] == '6') {
                arr[i] = '9';
                return Integer.valueOf(String.valueOf(arr));
            }
        }

        return num;
    }

}
