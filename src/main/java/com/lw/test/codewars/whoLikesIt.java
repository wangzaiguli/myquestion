package com.lw.test.codewars;

import javax.naming.Name;

/**
 * create by idea
 *
 * @author lmx
 * @version 1.0
 * @date 2022/3/12 20:46
 */
public class whoLikesIt {
    public static void main(String[] args) {
       String name= whoLikesIt("Alex", "Jacob", "Mark", "Max");
        System.out.println(name);
    }
    public static String whoLikesIt(String... names) {
        //Do your magic here
        int  num=names.length;
        int nums=num-2;

        String name="";
        switch (num){

            case 0:
                name ="no one likes this";
                break;
            case 1:
                name= names[0] +" likes this";
                break;
            case 2:
                name =names[0]+ " and "+ names[1] +" like this";
                break;
            case 3:
                name= names[0] +","+ names[1] +" and "+ names[2]+" like this";
                break;

            default:
                name= names[0] +","+ names[1] + " and " +nums+" others like this";

        }

        return name;
    }
}
