package com.lw.test.codewars;

import java.util.ArrayList;
import java.util.List;

/**
 * create by idea
 *
 * @author lmx
 * @version 1.0
 * @date 2022/3/12 22:47
 */
public class SumDigPow {
    public static void main(String[] args) {
        ArrayList list = (ArrayList) sumDigPow(1, 110);
        System.out.println(list);
    }

    public static List<Long> sumDigPow(long a, long b) {
        // your code
        String s = "";
        ArrayList<Long> arraryList = new ArrayList<>();
        for (long i = a; i <= b; i++) {
            if (i < 10) {
                arraryList.add(i);
            } else {
                s = i + "";
                String[] ss = s.split("");
                long num = 0;
                for (int i1 = 0; i1 < s.length(); i1++) {
                    num += Math.pow(s.charAt(i1) - '0', i1 + 1);
                }
                if (num == i) {
                    arraryList.add(i);
                }
            }
        }
        return arraryList;
    }

}
