package com.lw.test.codewars;

import java.util.*;

/**
 * create by idea
 *
 * @author lmx
 * @version 1.0
 * @date 2022/3/13 17:52
 */
public class Encode {

    public static void main(String[] args) {
        Encode encode = new Encode();
        String recede = encode.encode("recede");
        System.out.println(recede);

    }

    static String encode(String word) {
        String s = word.toLowerCase();
        char[] chars = s.toCharArray();
        HashMap<Character, Integer> map = new HashMap<>();
        for (char aChar : chars) {
            Integer integer = map.get(aChar);
            if (integer == null) {
                map.put(aChar, 1);
            } else {
                map.put(aChar, integer + 1);
            }
        }
        for (int i = 0; i < chars.length; i++) {
            Integer integer = map.get(chars[i]);
            if (integer > 1) {
                chars[i] = ')';
            } else {
                chars[i] = '(';
            }
        }
        return String.valueOf(chars);

    }
}
