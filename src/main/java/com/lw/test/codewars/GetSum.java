package com.lw.test.codewars;

/**
 * create by idea
 *
 * @author lmx
 * @version 1.0
 * @date 2022/3/12 21:37
 */
public class GetSum {
    public static void main(String[] args) {
        GetSum getSum = new GetSum();
        int sum = getSum.getSum(0, -1);
        System.out.println(sum);
    }

    public int getSum(int a, int b) {
        if (a == b) {
            return a;
        }
        int min = a < b ? a : b;
        int max = a < b ? b : a;
        int num = 0;
        for (int i = min; i <= max; i++) {
            num += i;
        }
        return num;
    }
}
