package com.lw.test.codewars;

/**
 * create by idea
 *
 * @author lmx
 * @version 1.0
 * @date 2022/3/13 19:59
 */
public class Evaluate {

    public static void main(String[] args) {
        Evaluate aa = new Evaluate();
        double evaluate = aa.evaluate("1 3 -");
        System.out.println(evaluate);
    }

    public double evaluate(String expr) {
        // TODO: Your awesome code here
        if (expr == null || expr.equals("")) {
            return 0D;
        }
        String[] s = expr.split(" ");
        double num = 0;
        if (s.length == 1) {
            return Double.parseDouble(expr);
        }
        switch (s[2]) {
            case "+":
                num = Double.parseDouble(s[0]) + Double.parseDouble(s[1]);
                break;
            case "-":
                num = Double.parseDouble(s[0]) - Double.parseDouble(s[1]);
                break;
            case "*":
                num = Double.parseDouble(s[0]) * Double.parseDouble(s[1]);
                break;
            default:
                num = Double.parseDouble(s[0]) / Double.parseDouble(s[1]);

        }
        return num;
    }

}
