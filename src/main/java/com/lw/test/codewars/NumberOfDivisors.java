package com.lw.test.codewars;

/**
 * create by idea
 *
 * @author lmx
 * @version 1.0
 * @date 2022/3/12 21:19
 */
public class NumberOfDivisors {
    public static void main(String[] args) {
        NumberOfDivisors numberOfDivisors = new NumberOfDivisors();

        long   count = numberOfDivisors.numberOfDivisors(10);
        System.out.println(count);

    }
    public long numberOfDivisors(int n) {
        // TODO please write your code below this comment
        long count=0;
        for (int i = 1; i <= n; i++) {
            if(n%i==0){
                count+=1;
            }
        }
        return count;
    }
}
