package com.lw.test.util;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author liw
 * @version 1.0
 * @date 2022/6/2 13:48
 */
public class Utils {

    private Utils() {
    }

    public static String[] getStrs(int length, int strLength, char st, char end) {
        String[] values = new String[length];
        for (int j = 0; j < length; j++) {
            char[] arr = new char[strLength];
            for (int i = 0; i < strLength; i++) {
                arr[i] = (char) (st + (end - st + 1) * Math.random());
            }
            values[j] = String.valueOf(arr);
        }
        String join = String.join("\",\"", values);
        System.out.println("[\"" + join + "\"]");
        return values;
    }

    public static String[] getStrs(int length, int strLength, char[] items) {
        String[] values = new String[length];
        int t = items.length;
        for (int j = 0; j < length; j++) {
            char[] arr = new char[strLength];
            for (int i = 0; i < strLength; i++) {
                arr[i] = items[(int) (t * Math.random())];
            }
            values[j] = String.valueOf(arr);
        }
        String join = String.join("\",\"", values);
        System.out.println("[\"" + join + "\"]");
        return values;
    }

    public static String getStr(int length, char st, char end) {
        char[] arr = new char[length];
        for (int i = 0; i < length; i++) {
            arr[i] = (char) (st + (end - st + 1) * Math.random());
        }
        String str = String.valueOf(arr);
        System.out.println("\"" + str + "\"");
        return str;
    }

    public static int[] getArr(String path) {
        return str2Arr(readFileNote(path));
    }

    public static int[] str2Arr(String s) {
        String[] split = s.split(",");
        List<Integer> list = new ArrayList<>(split.length);
        for (String s1 : split) {
            String trim = s1.trim();
            if (!"".equals(trim)) {
                list.add(Integer.parseInt(trim));
            }
        }
        int length = list.size();
        int[] arr = new int[length];
        for (int i = 0; i < length; i++) {
            arr[i] = list.get(i);
        }
        return arr;
    }

    public static boolean compare(int[][] arr1, int[][] arr2) {
        int length = arr1.length;
        if (length != arr2.length) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            if (!compare(arr1[i], arr2[i])) {
                return false;
            }
        }
        return true;
    }

    public static boolean compare(int[] arr1, int[] arr2) {
        int length = arr1.length;
        if (length != arr2.length) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            if (arr1[i] != arr2[i]) {
                return false;
            }
        }
        return true;
    }

    public static int[][] getArr2(String path) {
        String s = readFileNote(path);
        String[] split = s.split("],\\[");
        int[][] arr2 = new int[split.length][];
        int length1 = split.length;
        for (int i = 0; i < length1; i++) {
            String s1 = split[i];
            String replace = s1.replace("[", "").replace("]", "").trim();
            String[] split1 = replace.split(",");
            arr2[i] = new int[split1.length];
            for (int j = 0; j < split1.length; j++) {
                arr2[i][j] = Integer.parseInt(split1[j]);
            }
        }
        return arr2;
    }

    public static int[] getArr(int length, long st, long end) {
        return getArr(length, st, end, false, null);
    }

    // "C:\\lw\\myword\\123.txt"
    public static int[] getArr(int length, long st, long end, String outPath) {
        return getArr(length, st, end, false, outPath);
    }

    public static int[] getArr(int length, long st, long end, boolean sort, String outPath) {
        int[] arr = new int[length];
        for (int j = 0; j < length; j++) {
            arr[j] = (int) (st + Math.random() * (end - st));
        }
        if (sort) {
            Arrays.sort(arr);
        }

        if (outPath != null) {
            createFile(Arrays.toString(arr), outPath);
        }
        System.out.println(Arrays.toString(arr));
        return arr;
    }

    public static int[] getArrExcit(int length, long st, long end) {
        int[] arr = new int[length];
        HashSet<Integer> set = new HashSet<>();
        for (int j = 0; j < length; j++) {
            int v = (int) (st + Math.random() * (end - st));
            if (set.contains(v)) {
                j--;
            } else {
                set.add(v);
                arr[j] = v;
            }
        }
        System.out.println(Arrays.toString(arr));
        return arr;
    }

    public static int[][] getArr(int m, int n, long st, long end) {
        int[][] arr = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = (int) (st + Math.random() * (end - st));
            }
        }
        toPrint(arr);
//        createFile(s, "C:\\lw\\myword\\123.txt");
        return arr;
    }


    public static int[] getArrN(int length) {
        return getArrN(length, 0, null);
    }

    public static int[] getArrN(int length, int st, String outPath) {
        int[] arr = new int[length];
        for (int j = 0; j < length; j++) {
            arr[j] = st + j;
        }

        for (int i = 0; i < length; i++) {
            int a = (int) (Math.random() * length);
            int b = (int) (Math.random() * length);
            int t = arr[a];
            arr[a] = arr[b];
            arr[b] = t;
        }

        if (outPath != null) {
            createFile(Arrays.toString(arr), outPath);
        }
        System.out.println(Arrays.toString(arr));
        return arr;
    }

    public static void toPrint(String[] arr) {
        String s = Arrays.toString(arr);
        s = s.replace(", ", "\", \"")
                .replace("[", "[\"")
                .replace("]", "\"]");
        System.out.println(s);
    }

    public static void toPrint(int[] arr) {
        System.out.println(Arrays.toString(arr));
    }

    public static void toPrint(int[][] arr) {
        StringBuilder sb = new StringBuilder();
        for (int[] ints : arr) {
            sb.append(Arrays.toString(ints) + ", ");
        }
        String str = "[" + sb.substring(0, sb.length() - 2) + "]";
        createFile(str, "C:\\lw\\123.txt");
        System.out.println(str);
    }

    public static void toPrintln(int[][] arr) {
        for (int[] ints : arr) {
            System.out.println(Arrays.toString(ints));
        }
    }

    public static void toPrint(long[][] arr) {
        for (long[] ints : arr) {
            System.out.print(Arrays.toString(ints) + ", ");
            System.out.println();
        }
    }

    public static int[][] getArrSort(int m, int n, long st, long end) {
        int[][] arr = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = (int) (st + Math.random() * (end - st));
            }
        }
        for (int[] ints : arr) {
            if (ints[0] > ints[1]) {
                int t = ints[0];
                ints[0] = ints[1];
                ints[1] = t;
            } else if (ints[0] == ints[1]) {
                ints[1] += 2;
            }
        }
        Arrays.sort(arr, (a, b) -> Integer.compare(a[0], b[0]));
        StringBuilder sb = new StringBuilder();
        for (int[] ints : arr) {
            sb.append(Arrays.toString(ints));
            sb.append(",");
        }
        System.out.println("[" + sb.substring(0, sb.length() - 1) + "]");
        return arr;
    }

    public static int[] getPrimeList(int length) {
        int[] arr = new int[length];
        int index = 1;
        arr[0] = 1;
        for (int i = 2; i < Integer.MAX_VALUE; i++) {
            if (isPrime(i) && Math.random() < 0.3) {
                arr[index++] = i;
                if (index == length) {
                    break;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
        return arr;
    }

    public static boolean isPrime(int num) {
        int k = (int) Math.sqrt(num);
        for (int i = 2; i <= k; i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }


    public static String readFileNote(String filePath) {
        StringBuilder sb = new StringBuilder();
        try {
            File f = new File(filePath);
            if (f.isFile() && f.exists()) {
                InputStreamReader read = new InputStreamReader(new FileInputStream(f), "UTF-8");
                BufferedReader reader = new BufferedReader(read);
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                    sb.append("\n");
                }
                read.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }


    /**
     * 创建  String -> 文件
     *
     * @param pageStr    代码
     * @param createPath 路径 + 名称
     */
    public static void createFile(String pageStr, String createPath) {
        try {
            //生成文件
            File file = new File(createPath);
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            file.createNewFile();
            OutputStream os = new FileOutputStream(file);
            os.write(pageStr.getBytes());
            os.flush();
            os.close();
            System.out.println(createPath + "  生成。");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("生成文件页面出错！" + createPath);
        }
    }


    public static String[] getStrs(String str) {
        String replace = str.replace("[", "")
                .replace("]", "")
                .replace("\"", "")
                .replace("\n", "");
        return replace.split(",");
    }

}
