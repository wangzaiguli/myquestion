package com.lw.test.lmx;


import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * create by idea
 *
 * @author lmx
 * @version 1.0
 * @date 2022/1/17 20:13
 */
public class MyArraryList<E> implements List<E> {
    public static void main(String[] args) {
        MyArraryList<Integer> test = new MyArraryList<>();

        test.add(1);
        test.add(2);
        test.add(3);
        test.add(4);
        test.add(5);
        test.add(6);
        test.add(6);
        test.add(6);
        test.add(6);
        test.add(6);
        test.add(6);
        test.add(6);
        test.add(6);
        test.add(6);


        for (int i = 0; i < test.size(); i++) {
            System.out.println(test.get(i));
        }
    }


    private int size;

    private Object[] arr;

    private int length;

    public MyArraryList(){
        length = 10;
        arr = new Object[length];
    }
    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }



    //扩容
    private void aa(){
        Object[] items = new Object[length * 2];
        for (int i = 0; i < length; i++) {
            items[i] = arr[i];

        }
        length = length * 2;
        arr = items;

    }

    @Override
    public boolean add(E e) {
        size++;
        if(size>length){
            aa();
        }
        arr[size - 1] = e;
        return true;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public E get(int index) {
        return (E) arr[index];
    }

    @Override
    public E set(int index, E element) {
        return null;
    }

    @Override
    public void add(int index, E element) {

    }

    @Override
    public E remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<E> listIterator() {
        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return null;
    }
}
