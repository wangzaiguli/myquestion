package com.lw.test.lmx;

import java.util.*;

/**
 * @Author liw
 * @Date 2021/8/29 21:23
 * @Version 1.0
 */
public class Test {
    public static void main(String[] args) {

        stringRe();
        stringTest();

    }


    private static void stringRe() {
//        String str = "hello";
        String str = "adbc";
        int length = str.length();
        char[] chars = str.toCharArray();
//        int mid = (length >> 1);
        for (int i = 0; i < (length >> 1); i++) {
            char c = chars[i];
            chars[i] = chars[length - 1 - i];
            chars[length - 1 - i] = c;
        }
        System.out.println(String.valueOf(chars));

        String s = new StringBuilder(str).reverse().toString();
        System.out.println(s);

    }


    private static void stringTest() {
        String str = "etetetetret";

        int length = str.length();
        String substring = str.substring(0, 2);
        String[] split = str.split(",");
        System.out.println(Arrays.toString(split));

        String s = str.toLowerCase();

        char[] chars = str.toCharArray();

        byte[] bytes = str.getBytes();

        char c = str.charAt(2);

        boolean a = str.startsWith("aa");
        System.out.println(a);

        boolean a1 = str.endsWith("af");
        System.out.println(a1);

        String replace = str.replace("a", "b");

        String s1 = String.valueOf(1);


    }


    private static void stackTest() {
        Stack<Integer> stack = new Stack<>();
        stack.add(5);
        stack.add(7);

        Integer peek = stack.peek();
        System.out.println(peek);

        System.out.println(stack.size() + "  size");
        Integer pop = stack.pop();
        System.out.println(stack.size() + "  size");
        System.out.println(pop);

    }

    private static void listTest() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);

        for (Integer integer : list) {
            System.out.println(integer);
        }

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        // 替换
        list.set(0, 33);

    }


    private static void mapTest() {
        Map<String, String> map = new HashMap<>();
        map.put("a", "1");
        map.put("b", "2");

        //
        for (Map.Entry<String, String> entry : map.entrySet()) {
            System.out.println(entry.getKey() + "   " + entry.getValue());
        }

        //
        Set<String> setKey = map.keySet();
        for (String s : setKey) {
            System.out.println(s + "   " + map.get(s));
        }
    }
}
