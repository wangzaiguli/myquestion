package com.lw.test.lmx;

import java.util.Date;

/**
 * @Author liw
 * @Date 2021/8/29 21:19
 * @Version 1.0
 */
public class User {

    private String name;

    private int age;

    private Date both;

    public String eat() {
        System.out.println(name + " 吃了 ");
        return "OK";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getBoth() {
        return both;
    }

    public void setBoth(Date both) {
        this.both = both;
    }

}
