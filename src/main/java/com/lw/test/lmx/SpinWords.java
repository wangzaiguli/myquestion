package com.lw.test.lmx;

/**
 * create by idea
 *
 * @author lmx
 * @version 1.0
 * @date 2022/3/13 16:50
 */
public class SpinWords {
    public static void main(String[] args) {
        SpinWords spinWords = new SpinWords();
        String emocleW = spinWords.spinWords("Hey wollef sroirraw");
        System.out.println(emocleW);

    }

    public String spinWords(String sentence) {
        String[] s = sentence.split(" ");
        for (int i = 0; i < s.length; i++) {
            if (s[i].length() >= 5) {
                s[i] = new StringBuilder(s[i]).reverse().toString();
            }
        }
        return String.join(" ", s);
    }


    // 创建一个StringBuilder
    public String spinWords2(String sentence) {
        String[] s = sentence.split(" ");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length; i++) {
            if (s[i].length() >= 5) {
                s[i] = sb.append(s[i]).reverse().toString();
                sb.setLength(0);
            }
        }
        return String.join(" ", s);
    }
}
