package com.lw.test.lmx;

/**
 * @Author liw
 * @Date 2021/8/29 21:21
 * @Version 1.0
 */
public class Woman extends User {

    public void huaZhuang () {
        System.out.println(super.getName() + " 化妆 ");
    }

    @Override
    public String eat() {
        System.out.println(super.getName() + " woman 吃了 ");
        return "OK";
    }
}
