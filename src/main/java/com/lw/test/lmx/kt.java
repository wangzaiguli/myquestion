package com.lw.test.lmx;

/**
 * Created with IntelliJ IDEA.
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/2 16:03
 */
import java.util.ArrayList;
import java.util.List;

public class kt {
    public static void main(String[] args) {
        System.out.println("使用ClassName");
        SyncThread syncThread = new SyncThread();
        Thread thread1 = new Thread(new SyncThread(), "SyncThread1");
        Thread thread2 = new Thread(new SyncThread(), "SyncThread2");
        thread1.start();
        thread2.start();
    }


    class ClassName {
        public void method() {
            synchronized(ClassName.class) {

            }
        }
    }
    static class SyncThread implements Runnable {
        private static int count;

        public SyncThread() {
            count = 0;
        }

        public static void method() {
            synchronized(SyncThread.class) {
                for (int i = 0; i < 5; i ++) {
                    try {
                        System.out.println(Thread.currentThread().getName() + ":" + (count++));
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        public  void run() {
            method();

//        for (int i = 10; i > 0; i--) {
//
//            System.out.println(Thread.currentThread().getName() + ":" + (count++));
//        }
        }
    }
}

