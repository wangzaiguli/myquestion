package com.lw.test.lmx;

/**
 * @Author liw
 * @Date 2021/8/29 21:27
 * @Version 1.0
 */
public class Man extends User{

    public void paoBu () {
        System.out.println(super.getName() + " 跑步 ");
    }

    @Override
    public String eat() {
        System.out.println(super.getName() + " man 吃了 ");
        return "OK";
    }
}
