package com.lw.test.lmx;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * create by idea
 *
 * @author lmx
 * @version 1.0
 * @date 2022/1/17 19:46
 */
public class MyArrayList01<E> implements List<E> {


    public static void main(String[] args) {


        MyArrayList01<Integer> list = new MyArrayList01<>();

        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(4);
        Integer set = list.set(3, 5);
        System.out.println(set);

        for (int i = 0; i < list.size; i++) {
            System.out.println(list.get(i));
        }

    }


    private int size;

    private Object[] arr;

    private int length;

    public MyArrayList01() {
        length = 10;
        arr = new Object[length];
    }

    public MyArrayList01(int length) {
        this.length = length;
        arr = new Object[length];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    // 扩容
    private void aa() {
        Object[] items = new Object[length * 2];
        for (int i = 0; i < length; i++) {
            items[i] = arr[i];
        }
        length = length * 2;
        arr = items;
    }

    @Override
    public boolean add(E e) {
        size++;
        if (size > length) {
            aa();
        }
        arr[size - 1] = e;
        return true;
    }

    @Override
    public void clear() {
        size = 0;
    }

    @Override
    public E get(int index) {
        return (E) arr[index];
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < size; i++) {
            if (o.equals(arr[i])) {
                return true;
            }

        }
        return false;
    }

    @Override
    public Object[] toArray() {
        // todo self
        return new Object[0];
    }


    @Override
    public E set(int index, E element) {
//        Object[] arrs = new Object[length];
        E value = null;
        for (int i = 0; i < length; i++) {
            if (i == index) {
                value = (E) arr[i];
                arr[i] = element;
            }
        }
        return value;
    }

    @Override
    public void add(int index, E element) {
        size++;
        if (size > length) {
            aa();
        }
        for (int i = size - 2; i >= index; i--) {
            arr[i + 1] = arr[i] ;
        }
        arr[index] = element;

        for(int i=size-2;i>=index;i--){
            arr[i+1]=arr[i];
        }
        arr[index] = element;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public E remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }


    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<E> listIterator() {
        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }
}
