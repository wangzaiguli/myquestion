package com.lw.test;

/**
 * Created with IntelliJ IDEA.
 *
 * @author liw
 * @version 1.0
 * @date 2021/9/10 17:14
 */
public class FindMedianSortedArrays {

    public static void main(String[] args) {
        FindMedianSortedArrays test = new FindMedianSortedArrays();
        int[] arr1 = {2};
        int[] arr2 = {1,3,4,5,6,7,8,9,10, 11};

        test.getKthElement(arr1, arr2, 6);
    }

//    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
//        int length = nums1.length + nums2.length;
//        if ((length & 1) == 1) {
//            return getKthElement(nums1, nums2, (length >> 1) + 1);
//        } else {
//            int midIndex1 = totalLength / 2 - 1;
//            int midIndex2 = totalLength / 2;
//            return (getKthElement(nums1, nums2, midIndex1 + 1) + getKthElement(nums1, nums2, midIndex2 + 1)) / 2.0;
//        }
//    }

    private int find (int[] arr1, int[] arr2, int k) {
        int m = arr1.length;
        int n = arr2.length;
        int i1 = 0;
        int i2 = 0;
        while (true) {
            if (i1 == m) {
                return arr2[i2 + k - 1];
            }
            if (i2 == n) {
                return arr1[i1 + k - 1];
            }
            if (k == 1) {
                return Math.max(arr1[i1], arr2[i2]);
            }
            int f = k >> 1;
            int m1 = Math.min(i1 + f, m) - 1;
            int m2 = Math.min(i2 + f, n) - 1;
            if (arr1[m1] <= arr2[m2]) {
                k = k - (m1 - i1 + 1);
                i1 = m1 + 1;
            } else {
                k = k - (m2 - i2 + 1);
                i2 = m2 + 1;
            }
        }
    }

    public double findMedianSortedArrays2(int[] nums1, int[] nums2) {
        int length1 = nums1.length;
        int length2 = nums2.length;
        int totalLength = length1 + length2;
        if (totalLength % 2 == 1) {
            int midIndex = totalLength / 2;
            return getKthElement(nums1, nums2, midIndex + 1);
        } else {
            int midIndex1 = totalLength / 2 - 1;
            int midIndex2 = totalLength / 2;
            return (getKthElement(nums1, nums2, midIndex1 + 1) + getKthElement(nums1, nums2, midIndex2 + 1)) / 2.0;
        }
    }

    public int getKthElement(int[] nums1, int[] nums2, int k) {
        /* 主要思路：要找到第 k (k>1) 小的元素，那么就取 pivot1 = nums1[k/2-1] 和 pivot2 = nums2[k/2-1] 进行比较
         * 这里的 "/" 表示整除
         * nums1 中小于等于 pivot1 的元素有 nums1[0 .. k/2-2] 共计 k/2-1 个
         * nums2 中小于等于 pivot2 的元素有 nums2[0 .. k/2-2] 共计 k/2-1 个
         * 取 pivot = min(pivot1, pivot2)，两个数组中小于等于 pivot 的元素共计不会超过 (k/2-1) + (k/2-1) <= k-2 个
         * 这样 pivot 本身最大也只能是第 k-1 小的元素
         * 如果 pivot = pivot1，那么 nums1[0 .. k/2-1] 都不可能是第 k 小的元素。把这些元素全部 "删除"，剩下的作为新的 nums1 数组
         * 如果 pivot = pivot2，那么 nums2[0 .. k/2-1] 都不可能是第 k 小的元素。把这些元素全部 "删除"，剩下的作为新的 nums2 数组
         * 由于我们 "删除" 了一些元素（这些元素都比第 k 小的元素要小），因此需要修改 k 的值，减去删除的数的个数
         */

        int length1 = nums1.length;
        int length2 = nums2.length;
        int index1 = 0;
        int index2 = 0;

        while (true) {
            // 边界情况
            if (index1 == length1) {
                return nums2[index2 + k - 1];
            }
            if (index2 == length2) {
                return nums1[index1 + k - 1];
            }
            if (k == 1) {
                return Math.min(nums1[index1], nums2[index2]);
            }

            // 正常情况
            int half = k / 2;
            int newIndex1 = Math.min(index1 + half, length1) - 1;
            int newIndex2 = Math.min(index2 + half, length2) - 1;
            int pivot1 = nums1[newIndex1];
            int pivot2 = nums2[newIndex2];
            if (pivot1 <= pivot2) {
                k -= (newIndex1 - index1 + 1);
                index1 = newIndex1 + 1;
            } else {
                k -= (newIndex2 - index2 + 1);
                index2 = newIndex2 + 1;
            }
        }
    }



}
