package com.lw.test;

import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 *
 * @author liw
 * @version 1.0
 * @date 2022/3/29 9:48
 */
public class Test01 {


    public static void main(String[] args) {
        Test01 test = new Test01();

        test.aa();

    }

    private void aa () {

//        String pattern = "^([\u56fd][\u5185])|([\u56fd][\u9645])$";
//        String pattern = "^[a-zA-Z]{1,5}([,][a-zA-Z]{1,5})*$";
//        String pattern = "^([\u4e00-\u9fa5]|[\\w]){1,5}([,]([\u4e00-\u9fa5]|[\\w]){1,5})*$";
        String pattern = "^([\u4e00-\u9fa5]|[^,]){1,5}([,]([\u4e00-\u9fa5]|[^,]){1,5})*$";

        String content = "s阿萨df,wer,撒旦法给,多A34个,*%$#@,_-+=,{}[]";

        boolean isMatch = Pattern.matches(pattern, content);
        System.out.println(isMatch);
    }
}
